import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"><b><a href="https://profaj.com/" target="_blank">PROFAJ </a> All Right Reserved ©</b></span>
    <div class="socials">
      <a href="https://www.instagram.com/profajcom/" target="_blank" class="ion ion-social-instagram"></a>
      <a href="https://www.facebook.com/profajcom/" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://twitter.com/profajcom" target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://tr.linkedin.com/company/profaj" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
