import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService, NbMenuItem } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { filter,map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ApicallService } from '../../../pages/apicall.service';
import { Syslanguages } from '../../../pages/syslanguages/syslanguages';
import{ Turkish } from '../../../pages/syslanguages/turkish';
import{ English } from '../../../pages/syslanguages/english';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
  providers:[ApicallService,HttpModule,Syslanguages],
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;
  Username:any;
  public sysLanguages : any;
  customerList: any;
  taskCount : any;

  expanded = false;
  wasExpanded = false;

  userMenu = [{ title: '' }, { title: '', children: [ { title: ''}, { title: ''}]},{ title: '' },];
  //languageMenu = [{ title: ''}, { title: ''}];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserData,
              private analyticsService: AnalyticsService,
              private nbMenuService : NbMenuService,
              private _apicallservice: ApicallService,
              private _router: Router,
              private actr: ActivatedRoute) {

              this.sysLanguages = this.actr.snapshot.data['lang'];

              this.userMenu[0].title=this.sysLanguages.ProfileText ;
              this.userMenu[1].title=this.sysLanguages.HcLanguage;
              this.userMenu[1].children[0].title=this.sysLanguages.LanguageOptionTurkish;
              this.userMenu[1].children[1].title=this.sysLanguages.LanguageOptionEnglish;
              this.userMenu[2].title=this.sysLanguages.LogoutText;

              /*this.languageMenu[0].title=this.sysLanguages.LanguageOptionTurkish;
              this.languageMenu[1].title=this.sysLanguages.LanguageOptionEnglish;*/
              //this.getTaskByUser();
              this.getCustomers();
              
              
  }

  ngOnInit() {
    //this.userService.getUsers().subscribe((users: any) => this.user = users.nick);
    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'logout-menu'),
        map(({ item: { title } }) => title),
      ).subscribe(
        title => this.logout(title)

      );

      /*this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'language-menu'),
        map(({ item: { title } }) => title),
      ).subscribe(
        title => this.chooseLanguage(title)

      );*/
      
      this._apicallservice.GetAll("http://localhost:61232/api/User/GetUserByLogin").subscribe(
        (result) => {
          this.Username = result;
        }
      )
  }

  chooseLanguage(title)
  {
    if(title == this.sysLanguages.LanguageOptionTurkish)
    {      
      let _user = { LanguageId: 1 };
      this._apicallservice.Update("http://localhost:61232/api/User/UpdateLanguageOption", 1, _user)
      .subscribe((result) => {
        sessionStorage.setItem("langFile", JSON.stringify(Turkish));
        location.reload();
      },)
    }
    else if(title == this.sysLanguages.LanguageOptionEnglish)
    {
      let _user = { LanguageId: 2 };
      this._apicallservice.Update("http://localhost:61232/api/User/UpdateLanguageOption", 1, _user)
      .subscribe((result) => {
        sessionStorage.setItem("langFile", JSON.stringify(English));
        location.reload();
      },)
    }
  }
  

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }
  
  logout(title){
    if(title == this.sysLanguages.LogoutText)
    {
      this._apicallservice.GetAll("http://localhost:61232/api/User/Logout")
      .subscribe((result) => {
        if(result[0].Result == "True")
        {
          localStorage.removeItem("EuToken");
          this._router.navigate(['/auth']); 
        }
        else if(result[0].Result == "Session_Expired")
        {
          alert("Bir hata oluştu..");
          return;
        }
      },)
    }
    else if(title == this.sysLanguages.LanguageOptionTurkish)
    {
      this.chooseLanguage(this.sysLanguages.LanguageOptionTurkish);
    }
    else if(title == this.sysLanguages.LanguageOptionEnglish)
    {
      this.chooseLanguage(this.sysLanguages.LanguageOptionEnglish);
    }
    else if (title == this.sysLanguages.ProfileText)
    {
      sessionStorage.setItem('tasksTab', '0');
      this._router.navigate(['/pages/profile']); 
    }
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
              this.customerList.push(_customers);
            }
          }
      }
    )
  }

  getTaskByUser()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Task/GetTaskByUser").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.taskCount=0;
          }
        }
        else
        {
          const a = resulttArray.filter(x=>x.StatusDesc != "Tamamlandı");
          this.taskCount = a.length;
        }
      }
    )
  }

  openTask()
  {
    this.sidebarService.toggle(false, 'settings-sidebar');
    this.expanded = !this.expanded;
    this.wasExpanded = true;
  }

  openCalendar()
  {
    this._router.navigate(['/pages/activities']);
  }

}
