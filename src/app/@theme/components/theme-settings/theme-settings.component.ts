import { Component } from '@angular/core';

import { StateService } from '../../../@core/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../../pages/apicall.service';
import { DashboardComponent } from '../../../pages/dashboard/dashboard.component';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'ngx-theme-settings',
  styleUrls: ['./theme-settings.component.scss'],
  providers: [ApicallService, NbToastrService],
  template: `
    <!--<h6>LAYOUTS</h6>
    <div class="settings-row">
      <a *ngFor="let layout of layouts"
         href="#"
         [class.selected]="layout.selected"
         [attr.title]="layout.name"
         (click)="layoutSelect(layout)">
        <i [attr.class]="layout.icon"></i>
      </a>
    </div>-->
    <h6>{{ sysLanguages.TasksMyTasks }}</h6>
    <div class="settings-row">
      <p class="ion-android-add-circle addTaskIcon" (click)="showAddTaskArea()"></p>
      <p class="addTaskText" (click)="showAddTaskArea()">{{ sysLanguages.AddTaskHeaderText }}</p>
      <p class="ion-android-close closeTaskIcon" *ngIf="isAddTaskActive == true" (click)="closeAddTaskArea()"></p>
      <div *ngIf="isAddTaskActive == true">
        <div class="chipsclass">
          <tag-input [(ngModel)]="_taskDesc" [secondaryPlaceholder]="sysLanguages.TasksTableTascDescText" (onTextChange)="getTaskDesc($event)" (onAdd)="saveTask($event, 0)" [modelAsStrings]="true"> </tag-input>   
          <p class="ion-android-create detailIcon" (click)="openRelationArea()"></p>
        </div>
        <br/>
        <br/>
        <input [nbDatepicker]="datepicker" placeholder="{{sysLanguages.PcProposalDate}}" *ngIf="isRelationAreaOpen == true" class="form-control taskFormGroups" id="taskDate">
        <nb-datepicker #datepicker ></nb-datepicker>
        <br/>
        <p-dropdown [options]="relationList" *ngIf="isRelationAreaOpen == true" placeholder=" " [panelStyle]="{'background-color': '#ffffff'}" [style]="{'background-color':'#ffffff', 'border': '2px solid #dadfe6', 'color': '#2a2a2a', 'width': '100%', 'height': '32px', 'padding': '0.30rem 0.80rem', 'border-radius': '0.375rem'}" [(ngModel)]="selectedRelation" filter="true" showClear="true">
          <ng-template let-rel pTemplate="item">
              <div class="ui-helper-clearfix" style="background-color: #ffffff;">
                  <div style="font-size:14px;">{{rel.label}}</div>
              </div>
          </ng-template>
        </p-dropdown>
        <br/>
        <br/>
        <p-dropdown [virtualScroll]="true" itemSize="30" [options]="projectList" *ngIf="isRelationAreaOpen == true && selectedRelation == 1" placeholder=" " [panelStyle]="{'background-color': '#ffffff'}" [style]="{'background-color':'#ffffff', 'border': '2px solid #dadfe6', 'color': '#2a2a2a', 'width': '100%', 'height': '32px', 'padding': '0.30rem 0.80rem', 'border-radius': '0.375rem'}" [(ngModel)]="selectedProject" filter="true" showClear="true">
          <ng-template let-pro pTemplate="item">
              <div class="ui-helper-clearfix" style="background-color: #ffffff;">
                  <div style="font-size:14px;width:15px;">{{pro.label}}</div>
              </div>
          </ng-template>
        </p-dropdown>
        <p-dropdown [virtualScroll]="true" itemSize="30" [options]="customerList" *ngIf="isRelationAreaOpen == true && selectedRelation == 2" placeholder=" " [panelStyle]="{'background-color': '#ffffff'}" [style]="{'background-color':'#ffffff', 'border': '2px solid #dadfe6', 'color': '#2a2a2a', 'width': '100%', 'height': '32px', 'padding': '0.30rem 0.80rem', 'border-radius': '0.375rem'}" [(ngModel)]="selectedCustomer" filter="true" showClear="true">
          <ng-template let-cust pTemplate="item">
              <div class="ui-helper-clearfix" style="background-color: #ffffff;">
                <div style="font-size:14px;width:15px;">{{cust.label}}</div>
              </div>
          </ng-template>
        </p-dropdown>
        <p-dropdown [virtualScroll]="true" itemSize="30" [options]="leadList" *ngIf="isRelationAreaOpen == true && selectedRelation == 3" placeholder=" " [panelStyle]="{'background-color': '#ffffff'}" [style]="{'background-color':'#ffffff', 'border': '2px solid #dadfe6', 'color': '#2a2a2a', 'width': '100%', 'height': '32px', 'padding': '0.30rem 0.80rem', 'border-radius': '0.375rem'}" [(ngModel)]="selectedLead" filter="true" showClear="true">
          <ng-template let-lead pTemplate="item">
            <div class="ui-helper-clearfix" style="background-color: #ffffff;">
              <div style="font-size:14px;width:15px;">{{lead.label}}</div>
            </div>
          </ng-template>
        </p-dropdown>
        <p-dropdown [virtualScroll]="true" itemSize="30" [options]="invoiceList" *ngIf="isRelationAreaOpen == true && selectedRelation == 4" placeholder=" " [panelStyle]="{'background-color': '#ffffff'}" [style]="{'background-color':'#ffffff', 'border': '2px solid #dadfe6', 'color': '#2a2a2a', 'width': '100%', 'height': '32px', 'padding': '0.30rem 0.80rem', 'border-radius': '0.375rem'}" [(ngModel)]="selectedInvoice" filter="true" showClear="true">
          <ng-template let-inv pTemplate="item">
              <div class="ui-helper-clearfix" style="background-color: #ffffff;">
                <div style="font-size:14px;width:15px;">{{inv.label}}</div>
              </div>
          </ng-template>
        </p-dropdown>
        <br/>
        <br/>
        <button nbButton *ngIf="isRelationAreaOpen == true && selectedRelation != 0 && loading == false " (click)="saveTask('', 1)" class="addTaskBtn" status="info">{{ sysLanguages.AcSave }}</button>
        <button nbButton *ngIf="isRelationAreaOpen == true && selectedRelation != 0 && loading == true " class="addTaskBtn" status="info"> <div class="loader"></div> </button>
      </div>
    </div>
    <nb-list *ngIf="isAddTaskActive == false" class="tasklistclass">
    <nb-list-item *ngFor="let task of taskList">
      <input type="text" nbInput value="{{ task.Title }}" (change)="updateTask($event, task.TaskId)" id="_updateTaskDesc" class="taskListGroups"/>
      <nb-checkbox id="{{ task.TaskId }}" class="completeCheckbox" (change)="completeTask(task.TaskId)"></nb-checkbox>
    </nb-list-item>
    </nb-list>   
    <!--<div class="settings-row">
      <div class="switcher">
        <ngx-layout-direction-switcher></ngx-layout-direction-switcher>
      </div>
    </div>-->
  `,
})
export class ThemeSettingsComponent {

  layouts = [];
  sidebars = [];
  taskList = [];
  customerList = [];
  projectList = [];
  leadList = [];
  invoiceList = [];
  relationList = [];

  sysLanguages: any;
  isAddTaskActive = false;
  _taskDesc: string;
  _copyTaskDesc: string;
  isRelationAreaOpen = false;

  selectedRelation = 0;
  selectedCustomer = 0;
  selectedProject = 0;
  selectedInvoice = 0;
  selectedLead = 0;

  loading = false;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  
  constructor(protected stateService: StateService, private actr: ActivatedRoute, private _apicallservice: ApicallService, private _router: Router, private toastrService: NbToastrService) {
    this.stateService.getLayoutStates()
      .subscribe((layouts: any[]) => this.layouts = layouts);

    this.stateService.getSidebarStates()
      .subscribe((sidebars: any[]) => this.sidebars = sidebars);

      this.sysLanguages = this.actr.snapshot.data['lang'];

      this.getProjects();
      this.getCustomers();
      this.getTasks();
      this.getLeads();
      this.getInvoices();

      this.relationList.push({ label: this.sysLanguages.ProjectsSubscriptionText, value: 1 }, { label: this.sysLanguages.CustomerText, value: 2 } ,  { label: this.sysLanguages.LeadText, value : 3 } , { label : this.sysLanguages.InvoiceText , value : 4}  );
  }

  /*layoutSelect(layout: any): boolean {
    this.layouts = this.layouts.map((l: any) => {
      l.selected = false;
      return l;
    });

    layout.selected = true;
    this.stateService.setLayoutState(layout);
    return false;
  }*/

  sidebarSelect(sidebars: any): boolean {
    this.sidebars = this.sidebars.map((s: any) => {
      s.selected = false;
      return s;
    });

    sidebars.selected = true;
    this.stateService.setSidebarState(sidebars);
    return false;
  }

  showAddTaskArea()
  {
    this.isAddTaskActive = true;
  }

  closeAddTaskArea()
  {
    this.isAddTaskActive = false;
    this.isRelationAreaOpen = false;
  }

  openRelationArea()
  {
    this.isRelationAreaOpen = true;
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }

  getProjects()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Project/GetTaskBoards").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.projectList = [];
            }
          }
          else
          { 
            this.projectList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].BoardId != 0)
              {
                let _projects = { label: resultArray[i].BoardName, value: resultArray[i].BoardId };
                this.projectList.push(_projects);
              }  
            }
          }
      }
    )
  }

  getLeads()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Lead/GetAllLead").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.leadList = [];
            }
          }
          else
          { 
            this.leadList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].LeadId != 0)
              {
                let _leads = { label: resultArray[i].LeadName + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].LeadId };
                this.leadList.push(_leads);
              }
            }
          }
      }
    )
  }

  getInvoices()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.invoiceList = [];
            }
          }
          else
          { 
            this.invoiceList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].InvoiceId != 0)
              {
                let _invoices = { label: resultArray[i].InvoiceNumber, value: resultArray[i].InvoiceId };
                this.invoiceList.push(_invoices);
              }
            }
          }
      }
    )
  }

  getTasks()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Task/GetTaskByUser") 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.taskList = [];
        }
      }
      else
      {
        this.taskList = [];
        for(var i in resultArray)
        {
          if(resultArray[i].StatusId != 5)
          {
            this.taskList.push(resultArray[parseInt(i)]);
          }
        }
      }     
    }, 
    )
  }

  completeTask(_taskId)
  {
    let task={ TaskId : _taskId, TaskComplete : "0.5", StatusId : 5 };
    this._apicallservice.Update("http://localhost:61232/api/Task/TaskComplete", _taskId, task).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "False") {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.TasksToastrCantCompleted);
      }
      else if (resultArray[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      } 
      else
      {
        this.getTasks();
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.TasksToastrCompleted);
      }  
    })
  }

  getTaskDesc(event)
  {
    this._copyTaskDesc = event;
  }

  saveTask(event, val)
  {
    let _today = new Date().toISOString(); 

    if(val == 0)
    {
      let task = {
        ParentId:0,
        Title: event.value,
        TaskDesc: event.value,
        TypeId:1,
        CreatedDate: _today.split("T")[0],
        ResponsibleId: 1 ,
        BeginCheckListId:1,
        EndCheckListId:1,
        BoardId:0,
        ContractId:1,
        BeginDate: this.formatIsoDateToCrypted(_today.split("T")[0]),
        EndDate: this.formatIsoDateToCrypted(_today.split("T")[0]),
        StatusId: 2,
        CreatorId: 1,
        TeamId: 1,
        CustomerId:0,
        LeadId:0,
        InvoiceId:0,
      }

      this._apicallservice.Add("http://localhost:61232/api/Task/CreateTaskForMe", task) 
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this.getTasks();
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.TasksToastrSaved);
          this.isAddTaskActive = false;
          this.isRelationAreaOpen = false;
        }
        else if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }        
      } 
      )
    }
    else if(val == 1)
    {
      this.loading = true;
      
      let task = {
        ParentId: 0,
        Title: this._copyTaskDesc,
        TaskDesc: this._copyTaskDesc,
        TypeId:1,
        CreatedDate: _today.split("T")[0],
        ResponsibleId: 1 ,
        BeginCheckListId:1,
        EndCheckListId:1,
        BoardId: this.selectedProject,
        ContractId:1,
        BeginDate: $("#taskDate").val(),
        EndDate: $("#taskDate").val(),
        StatusId: 2,
        CreatorId: 1,
        TeamId: 1,
        CustomerId: this.selectedCustomer,
        LeadId: this.selectedLead,
        InvoiceId: this.selectedInvoice
      }
  
      this._apicallservice.Add("http://localhost:61232/api/Task/CreateTaskForMe", task) 
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this.getTasks();
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.TasksToastrSaved);
          this.isAddTaskActive = false;
          this.isRelationAreaOpen = false;
          this.loading = false;
        }
        else if (resultArray[0].Result == "Session_Expired") {
          this.loading = false;
          this._router.navigate(['/auth']);
          return;
        }        
      } 
      )
    }

    this._taskDesc = "";
  }

  updateTask(event, taskId)
  {
    let _today = new Date(); 
    let _expiredDate = new Date();
    _expiredDate.setDate(_today.getDate()+30);

    let task = {
      TaskId: taskId,
      ParentId:0,
      Title: event.target.value,
      TaskDesc: event.target.value,
      TypeId:1,
      CreatedDate: _today.toISOString().split("T")[0],
      ResponsibleId: 1 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId: this.selectedProject,
      ContractId:1,
      BeginDate: _today.toISOString().split("T")[0],
      EndDate: _today.toISOString().split("T")[0],
      StatusId: 2,
      CreatorId: 1,
      TeamId: 1,
      CustomerId: this.selectedCustomer,
      LeadId: this.selectedLead,
      InvoiceId: this.selectedInvoice,
    }

    this._apicallservice.Update("http://localhost:61232/api/Task/UpdateToDo", taskId, task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getTasks();
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.TasksToastrSaved);
        this.isAddTaskActive = false;
        this.isRelationAreaOpen = false;
      }
      else if (resultArray[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }        
    } 
    )

    this._taskDesc = "";
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

}
