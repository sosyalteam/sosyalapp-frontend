import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import { LoginComponent } from './login/login/login.component';
 
const routes: Routes = [
  {
     path: 'pages', 
     loadChildren: 'app/pages/pages.module#PagesModule', 
  },
  { path: 'login', loadChildren: 'app/login/login.module#LoginModule' },
  { path: 'proposal', loadChildren: 'app/proposal/proposal.module#ProposalModule' },
  { path: 'proform', loadChildren: 'app/sentproform/sentproform.module#SentproformModule' },
  { path: 'invoice', loadChildren: 'app/invoice/invoice.module#InvoiceModule' },
  {
    path: 'auth',
    component: LoginComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
    ],
  },
  { 
    path: '',
    redirectTo: 'pages',
    pathMatch: 'full',  
  },
  {
     path: '**',
     redirectTo: 'pages',
  },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
