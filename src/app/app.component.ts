/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { Syslanguages } from './pages/syslanguages/syslanguages';
import { DashboardchartService } from './pages/dashboard/dashboardchart.service';
import { DashboardProjectService } from './pages/dashboard/dashboard-project.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
  providers: [Syslanguages,DashboardchartService,DashboardProjectService],
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService, private _sysLanguage: Syslanguages) {
    //this._sysLanguage.getLanguages();
  }

  ngOnInit() {
    this.analytics.trackPageViews();
  }
}
