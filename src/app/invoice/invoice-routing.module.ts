import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceComponent } from './invoice/invoice.component';
import { BrowserlanguageService } from '../pages/syslanguages/browserlanguage.service';
import { InvoicepdfComponent } from './invoicepdf/invoicepdf.component';

const routes: Routes = [
  {
    path: ':id',
    component: InvoiceComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
  {
    path: 'pdf/:id',
    component: InvoicepdfComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
