import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './invoice-routing.module';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicepdfComponent } from './invoicepdf/invoicepdf.component';
import { NbLayoutModule, NbCardModule, NbButtonModule } from '@nebular/theme';

@NgModule({
  declarations: [InvoiceComponent, InvoicepdfComponent],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    NbLayoutModule,
    NbCardModule,
    NbButtonModule,
  ]
})
export class InvoiceModule { }
