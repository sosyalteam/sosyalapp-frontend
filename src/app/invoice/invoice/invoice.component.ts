import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../pages/apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  sysLanguages: any;
  invoiceData = { Address: "", Amount: "", City: "", CreatedDate: "", LastPaymentDate: "", Phone: "", PostCode: "", Status: "", CustomerName: "" , ContactName : "" , CustomerId : 0 , Discount : "" };
  lineData = [];
  invoiceId: any;
  currency : any;


  constructor(private _apicallService: ApicallService, private _router: Router, private _avRoute: ActivatedRoute, private titleService: Title) 
  {
    this.sysLanguages = this._avRoute.snapshot.data['lang'];
    this.titleService.setTitle(this.sysLanguages.InvoiceText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.invoiceId = this._avRoute.snapshot.params["id"];
    }

    this.getInvoiceDetail();
  }

  ngOnInit() {
  }

  getInvoiceDetail()
  {
    this.lineData = [];

    if(this.invoiceId>0)
    {
      this._apicallService.Get("http://localhost:61232/api/Invoice/GetInvoiceWoSession", this.invoiceId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {          
            if(resulttArray[0].StatusId == 0)    
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusCanceled;
            }
            else if(resulttArray[0].StatusId == 1)
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusPaid;
            }
            else if(resulttArray[0].StatusId == 2)
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusNotPaid;
            }
            else if(resulttArray[0].StatusId == 3)
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusPartlyPaid;
            }
            else if(resulttArray[0].StatusId == 4)
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusExpired;
            }
            else if(resulttArray[0].StatusId == 5)
            {
              this.invoiceData.Status = this.sysLanguages.InvoiceStatusDraft;
            }   

            this.invoiceData.Phone = resulttArray[0].Phone;
            this.invoiceData.PostCode = resulttArray[0].PostCode;
            this.invoiceData.Address = resulttArray[0].Address;
            this.invoiceData.Amount = resulttArray[0].Amount;
            this.invoiceData.City = resulttArray[0].City;     
            this.invoiceData.CustomerName = resulttArray[0].CustomerName;
            this.invoiceData.CustomerId = resulttArray[0].CustomerId;
            this.invoiceData.Discount = resulttArray[0].Discount;
            this.invoiceData.LastPaymentDate = resulttArray[0].LastPaymentDate.toString().split(" ")[0];
            this.invoiceData.CreatedDate = resulttArray[0].CreatedDate.toString().split(" ")[0];
            if(resulttArray[0].CurrencyTypeId == 1)
            {
              this.currency = "TL"
            }
            else if(resulttArray[0].CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(resulttArray[0].CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(resulttArray[0].CurrencyTypeId == 4)
            {
              this.currency = "£";
            }

            this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.invoiceData.CustomerId).subscribe(
            (result) => {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray.length != 0 && resultArray[0].Result) {
                if (resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
                else if(resultArray[0].Result == "Empty")
                {

                }
              }
              else
              {
                this.invoiceData.ContactName = resultArray[0].Fullname;
              }
            }
          )
            
          }              
      }
      )

      this._apicallService.Get("http://localhost:61232/api/Invoice/GetInvoiceLinesWoSession", this.invoiceId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }
          }
        }
      )
    }
  }

  goToPdfPage()
  {
    this._router.navigate([]).then(result => {  window.open('/invoice/pdf/' + this.invoiceId, '_blank'); });
  }

}
