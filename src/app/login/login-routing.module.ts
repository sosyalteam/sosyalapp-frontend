import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { Syslanguages } from '../pages/syslanguages/syslanguages';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    resolve: {
      lang: Syslanguages
     }, 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [Syslanguages],
})
export class LoginRoutingModule { }
