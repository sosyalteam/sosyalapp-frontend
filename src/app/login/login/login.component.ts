import { Component, OnInit } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { ApicallService } from '../../pages/apicall.service';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ApicallService, HttpModule],
})
export class LoginComponent implements OnInit {
  usernameck:any;
  public sysLanguages: any;
  
  constructor(private _apicallService: ApicallService, private _router: Router, private _http: HttpClient, private titleService: Title) 
    {
      this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
      this.titleService.setTitle(this.sysLanguages.LgHeaderText + " - Crmplus");
    }

  ngOnInit() { 
  }

  getUserData() {
    let login = { Username : $("#Username").val() , EncryptedKey: $("#EncryptedKey").val() };
    this._http.post("http://localhost:61232/api/User/Login", login)
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "NotFound") {
          window.alert(this.sysLanguages.LgWrongUserAlertText);
          //this._router.navigate(['/login']);
        }
        else if(resultArray[0].Result == "DeletedLogin")
        {
          alert("Hesabınız silinmiştir.");
        }
        else if (resultArray[0].Result == "True"){
          localStorage.setItem("EuToken", resultArray[1].Result);
          localStorage.setItem("UserId", resultArray[2].Result);
          this._router.navigate(['/pages/dashboard']);
        }
      },)

  }

  checked(){
    this.usernameck=$("#Username").val();
  }

}
