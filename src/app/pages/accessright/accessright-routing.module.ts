import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetRightComponent } from './get-right/get-right.component';

const routes: Routes = [
  {
    path: 'get-right',
    component: GetRightComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessrightRoutingModule { }
