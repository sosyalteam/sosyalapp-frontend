import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessrightRoutingModule } from './accessright-routing.module';
import { GetRightComponent } from './get-right/get-right.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  declarations: [GetRightComponent],
  imports: [
    CommonModule,
    AccessrightRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbCardModule,
  ]
})
export class AccessrightModule { }
