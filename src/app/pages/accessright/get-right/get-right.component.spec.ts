import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetRightComponent } from './get-right.component';

describe('GetRightComponent', () => {
  let component: GetRightComponent;
  let fixture: ComponentFixture<GetRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
