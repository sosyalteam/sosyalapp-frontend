import { Component, OnInit } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'get-right',
  templateUrl: './get-right.component.html',
  styleUrls: ['./get-right.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class GetRightComponent implements OnInit {
  public sysLanguages: any;
  public accessList: any; 

    constructor(public http: Http, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) { 
      
        this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
        this.titleService.setTitle(this.sysLanguages.ArHeaderText + " - Crmplus");

        this.settings.columns.RightDesc.title = this.sysLanguages.ArRightNameText;
        this.settings.actions.columnTitle = this.sysLanguages.ArActionText;
        this.getRights();  
    }

    settings = {
      columns: {
        AccessRightId: {
          title: "ID",
          width:'5%',
        },
        RightDesc: {
          title: ""
        },
      },
      actions:{
        position:'right',
        columnTitle: ""
      },
      add: {
        confirmCreate: true,
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      edit: {
        confirmSave: true,
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
    };
    
    ngOnInit() {
    }

    getRights() {
      this._apicallservice.GetAll("http://localhost:61232/api/Right/Get").subscribe(
        (result) =>
        {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length !=0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              this.accessList = [];
            }
          }
          else
          {
            this.accessList = result;
          }
        }
        )  
    }
  
    onSaveConfirm(event){
      var id = `${event.data.AccessRightId}`;
      var name=`${event.newData.RightDesc}`;
      let rightArray = {  AccessRightId: parseInt(id), RightDesc:name };
             this._apicallservice.Update("http://localhost:61232/api/Right/Update", parseInt(id), rightArray)
               .subscribe((result) => {
                 if(result[0].Result == "True")
                 {
                   this.getRights();
                   this._router.navigate(['/pages/accessright/get-right']); 
                 }
                 else if(result[0].Result == "Session_Expired")
                 {
                   this._router.navigate(['/auth']);
                   return;
                 }
               },)
  
    }
  
    onDeleteConfirm(event) {
      var id = `${event.data.AccessRightId}`;
      var ans = confirm(this.sysLanguages.ArDeleteRightText + parseInt(id));
     if (ans) {
       this._apicallservice.Deactive("http://localhost:61232/api/Right/Delete/", parseInt(id)).subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getRights();
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
       
       }, error => console.error(error))
     }
      
    }

    onCreateConfirm(event){
      var name=`${event.newData.RightDesc}`;
      let rightArray = {  AccessRightId: 0, RightDesc:name };
      this._apicallservice.Add("http://localhost:61232/api/Right/Create",rightArray) 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.getRights();
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )

    }

}