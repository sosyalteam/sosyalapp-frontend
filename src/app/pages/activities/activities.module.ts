import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesRoutingModule } from './activities-routing.module';
import { ActivitiesComponent } from './activities/activities.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NbCardModule, NbDialogModule, NbInputModule, NbButtonModule, NbDatepickerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  declarations: [ActivitiesComponent],
  imports: [
    CommonModule,
    ActivitiesRoutingModule,
    FullCalendarModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbDialogModule,
    NbInputModule,
    NbButtonModule,
    NbDatepickerModule.forRoot(),
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
  ]
})
export class ActivitiesModule { }
