import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Calendar, EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { NbDialogService, NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { FullCalendarComponent } from '@fullcalendar/angular';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
  providers:[ApicallService, NbToastrService]
})
export class ActivitiesComponent implements OnInit {

  sysLanguages : any;
  calendarPlugins = [dayGridPlugin,interactionPlugin];
  calendarHeaders: any;
  calendarEvents = [];
  isupdateorSave: any;
  draggedTaskID = 0;
  selectedTask = { selectedRelation: 0, selectedCustomer: 0, selectedProject: 0, selectedInvoice: 0, selectedLead: 0, selectedTaskType: 0, TaskId: 0, taskDesc: "", taskTitle: "", endDate: "", scheduledStartDate: "", scheduledEndDate: "" };
  selectedEventforDialog = [];
  selectedEndDate = "";
  selectedStartDate = "";

  taskList = [];
  taskTypeList = [];
  leadList = [];
  invoiceList = [];
  projectList = [];
  customerList = [];
  relationList = [];
  userList = [];
  selectedAssignUsers = [];
  copyselectedAssignUsers = [];
  unplannedTasks = [];
  strDay : any;
  endDay : any;
  loading = false;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  @ViewChild('external') external: ElementRef;

  constructor(private _router: Router, private _apiCallservice: ApicallService,private dialogService: NbDialogService,private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.AcActivity + " - Crmplus");

    this.getTasks();
    //this.getGoogleCalendarEvents();
    this.getTaskTypes();
    this.getInvoices();
    this.getProjects();
    this.getCustomers();
    this.getLeads();
    this.getUsers();

    this.relationList.push({ label: this.sysLanguages.ProjectText, value: 1}, { label: this.sysLanguages.CustomerText , value: 2 }, { label: this.sysLanguages.LeadText, value: 3 }, { label: this.sysLanguages.InvoiceText, value: 4 });
  }

  ngOnInit() 
  {
    new Draggable(this.external.nativeElement, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText
        };
      }
    });

    this.calendarHeaders = {
        left: 'prev,next today ',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
    };
  }

  calendarEventClick(ref)
  {
    var eventid = (<any>this.selectedEventforDialog).event._def.extendedProps.eventid;
    /*var ans = confirm(this.sysLanguages.AcCompleteActivity );
    if(ans)
    {
      this._apiCallservice.Deactive("http://localhost:61232/api/Task/DeleteGoogleCalendarEvent",eventid).subscribe(
        (result) =>{
          this.getTasks();
          //this.getGoogleCalendarEvents();
        })
    }*/

    let _taskId = (<any>this.selectedEventforDialog).event.title.split(" * ")[1];
    var _task = this.taskList.filter(x=>x.TaskId == _taskId);
    if(_task.length > 0)
    {
      var ans = confirm(this.sysLanguages.AcCompleteActivity );
      if(ans)
      {
        let _task_ = { TaskId: _taskId, StatusId: 5 };

        this._apiCallservice.Update("http://localhost:61232/api/Task/ChangeTaskStatus", _taskId, _task_).subscribe((result) => {
          ref.close();
          this.getTasks();
          // this.getGoogleCalendarEvents();
        })
      }
    }
  }

  editTaskScheduledDates(ref)
  {
    debugger;
    if($("#_taskScheduledStartDate").val() == "" || $("#_taskScheduledEndDate").val() == "")
    {
      this.makeToast(NbToastStatus.DANGER,this.sysLanguages.PcToastrErrorTitle,this.sysLanguages.AcEmptyDatesToastr);
      return;
    }
    else
    {
      let _taskId = (<any>this.selectedEventforDialog).event.title.split(" * ")[1];
      var _task = this.taskList.filter(x=>x.TaskId == _taskId);
      if(_task.length > 0)
      {
        let _task_ = { TaskId: _taskId, ScheduledStartDate: $("#_taskScheduledStartDate").val(), ScheduledEndDate: $("#_taskScheduledEndDate").val(), CreatedDate: "fromCalendar" };

        this._apiCallservice.Add("http://localhost:61232/api/Task/Update", _task_).subscribe((result) => {
          ref.close();
          this.getTasks();
          this.makeToast(NbToastStatus.SUCCESS,this.sysLanguages.SuccessMessage,this.sysLanguages.AcDatesUpdatedToastr);
          // this.getGoogleCalendarEvents();
        })
      }
    }
  }

  getTasks()
  {
    this.unplannedTasks = [];
    this.taskList = [];
    this.calendarEvents = [];

    this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskByUser").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.lenght != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taskList = [];
            this.calendarEvents = [];
          }
        }
        else 
        {       
          this.taskList = resultArray.filter(x=>x.StatusId != 5);

          let _today = new Date(); 

          for(let i in resultArray)
          {
            if(!resultArray[i].ScheduledStartDate && resultArray[i].StatusId != 5)
            {
              this.unplannedTasks.push({title : resultArray[i].Title + " * " + resultArray[i].TaskId , id :resultArray[i].TaskId });
            }
            else if(resultArray[i].ScheduledStartDate && resultArray[i].TaskId != this.draggedTaskID  && resultArray[i].StatusId != 5)
            {
              this.calendarEvents.push({ title: resultArray[i].Title + " * " + resultArray[i].TaskId, start: resultArray[i].ScheduledStartDate.split(" ")[0], end: resultArray[i].ScheduledEndDate.split(" ")[0], eventid: 1 });
            }
          }
        }
      }
    )
  }

 /* getGoogleCalendarEvents()
 {
   this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetGoogleCalendarEvents").subscribe(
     (result) => {
       this.calendarEvents = [];
       let resultArray = JSON.parse(JSON.stringify(result));
       if(resultArray.hasOwnProperty("accessRole"))
       {
         let newDate;
         for(let i in resultArray.items)
         {
           // bizim isistemden eklenen datetimede geliyor
           if(resultArray.items[i].end.dateTime)
           {
            newDate = new Date(resultArray.items[i].end.dateTime.split("+")[0]);
            newDate.setDate(newDate.getDate() + 1);
            newDate = newDate.toLocaleString();
            newDate = newDate.split(" ")[0].split(".")[2] + "-" + newDate.split(" ")[0].split(".")[1] + "-" + newDate.split(" ")[0].split(".")[0];

             this.calendarEvents.push({ title: resultArray.items[i].summary, start: resultArray.items[i].start.dateTime.split("T")[0], end: newDate, eventid: resultArray.items[i].id });
           }
           // gmailden eklenen datede geliyor ve enddate 1 fazla geliyor
           else if(resultArray.items[i].end.date)
           {
             this.calendarEvents.push({ title: resultArray.items[i].summary, start: resultArray.items[i].start.date, end: resultArray.items[i].end.date, eventid: resultArray.items[i].id });
           }
         }
       }
       else
       {
         if(resultArray[0].Result == "Empty")
         {
           this.calendarEvents = [];
         }
         else if(resultArray[0].Result == "Session_Expired")
         {
           this._router.navigate(['/auth']);
           return;
         }
       }
     }
   )
 }
*/

  getTaskTypes()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskType").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.lenght != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taskTypeList = [];
          }
        }
        else
        { 
          this.taskTypeList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TypeId != 1)
            {
              let _types = { label: resultArray[i].TypeDesc, value: resultArray[i].TypeId };
              this.taskTypeList.push(_types);
            }
          }
        }
      }
    )
  }

  getUsers()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].UserId != 0)
            {
              let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
              this.userList.push(_user);
            }
          }
        }        
      },
      )
  }

  getLeads()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Lead/GetAllLead").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.lenght != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.leadList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].LeadId != 0)
              {
                let _leads = { label: resultArray[i].LeadName + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].LeadId };
                this.leadList.push(_leads);
              }
            }
          }
      }
    )
  }

  getInvoices()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.lenght != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.invoiceList = [];
            }
          }
          else
          { 
            this.invoiceList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].InvoiceId != 0)
              {
                let _invoices = { label: resultArray[i].InvoiceNumber, value: resultArray[i].InvoiceId };
                this.invoiceList.push(_invoices);
              }
            }
          }
      }
    )
  }

  getProjects()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Project/GetTaskBoards").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.lenght != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.projectList = [];
            }
          }
          else
          { 
            this.projectList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].BoardId != 0)
              {
                let _projects = { label: resultArray[i].BoardName, value: resultArray[i].BoardId };
                this.projectList.push(_projects);
              }  
            }
          }
      }
    )
  }

  getCustomers()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.lenght != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }

  openDialog(dialog: TemplateRef<any>, val, id,event) {
    if(val == "save")
    {    
      this.isupdateorSave = "save";
      this.selectedTask.endDate = "";
      this.selectedTask.taskTitle = "";
      this.selectedTask.scheduledEndDate = this.formatIsoDateToCrypted(event.endStr);
      this.selectedTask.scheduledStartDate = this.formatIsoDateToCrypted(event.startStr);
      this.strDay = event.startStr;
      this.endDay = event.endStr;
      this.selectedTask.taskDesc = "";
      this.selectedTask.TaskId = 0;
      this.selectedTask.selectedCustomer = 0;
      this.selectedTask.selectedInvoice = 0;
      this.selectedTask.selectedLead = 0;
      this.selectedTask.selectedProject = 0;
      this.selectedTask.selectedRelation = 0;
      this.selectedTask.selectedTaskType = 0;
      this.copyselectedAssignUsers = [];
      this.selectedAssignUsers = [];

      this.dialogService.open(dialog);
    }
    else if(val == "update")
    {
      this.isupdateorSave = "update";

      for(let i in this.taskList)
      {
        if(this.taskList[i].TaskId == id)
        {
          if(this.taskList[i].ScheduledStartDate)
          {
            this.selectedTask.scheduledStartDate = this.formatIsoDateToCrypted(this.taskList[i].ScheduledStartDate.split(" ")[0]);
          }
          if(this.taskList[i].ScheduledEndDate)
          {
            this.selectedTask.scheduledEndDate = this.formatIsoDateToCrypted(this.taskList[i].ScheduledEndDate.split(" ")[0]);
          }

          this.selectedAssignUsers = [];
          this.copyselectedAssignUsers = [];
          this.selectedTask.endDate = this.formatIsoDateToCrypted(this.taskList[i].EndDate.split(" ")[0]);
          this.selectedTask.taskTitle = this.taskList[i].Title;
          this.selectedTask.taskDesc = this.taskList[i].TaskDesc;
          this.selectedTask.selectedCustomer = this.taskList[i].CustomerId;
          this.selectedTask.selectedInvoice = this.taskList[i].InvoiceId;
          this.selectedTask.selectedLead = this.taskList[i].LeadId;
          this.selectedTask.selectedProject = this.taskList[i].BoardId;
          this.selectedTask.TaskId = id;
          this.selectedTask.selectedTaskType = this.taskList[i].TypeId;

          if(this.taskList[i].CustomerId != 0)
            this.selectedTask.selectedRelation = 2;
          else if(this.taskList[i].InvoiceId != 0)
            this.selectedTask.selectedRelation = 4;
          else if(this.taskList[i].LeadId != 0)
            this.selectedTask.selectedRelation = 3;
          else if(this.taskList[i].BoardId != 0)
            this.selectedTask.selectedRelation = 1;
          else 
            this.selectedTask.selectedRelation = 0;

          this._apiCallservice.Get("http://localhost:61232/api/Task/GetTaskMembers", id).subscribe(
            (result) => {
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray.length != 0 && resultArray[0].Result) {
                  if (resultArray[0].Result == "Session_Expired") {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else if(resultArray[0].Result == "Empty")
                  {
                    this.selectedAssignUsers = [];
                  }
                }
                else
                { 
                  this.selectedAssignUsers = resultArray.map(x=> x.UserId);
                  this.copyselectedAssignUsers = this.selectedAssignUsers.slice();
                }
            }
          )  

          break;
        }
      }

      this.dialogService.open(dialog);
    }
    if(val == "EditOrComplete")
    {    
      let endDate = (<any>id).event._instance.range.end.toISOString();
      let startDate = (<any>id).event._instance.range.start.toISOString();

      this.selectedEndDate = this.formatIsoDateToCrypted(endDate.split("T")[0]);
      this.selectedStartDate = this.formatIsoDateToCrypted(startDate.split("T")[0]);

      this.selectedEventforDialog = id;

      this.dialogService.open(dialog);
    }
  }

  getRelationData(val)
  {
    this.selectedTask.selectedRelation = val;
  }

  saveActivity(ref)
  {
    this.loading = true;
    let _today = new Date(); 

    let task = {
      TaskId: 0,
      ParentId: 1,
      Title: $("#_taskTitle").val(),
      TaskDesc: $("#_taskDesc").val(),
      TypeId: this.selectedTask.selectedTaskType,
      CreatedDate: _today.toISOString().split("T")[0],
      ResponsibleId: 1,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: 0,
      ContractId: 1,
      ScheduledStartDate: this.strDay + " " + "12:00:00",
      ScheduledEndDate: this.endDay + " " + "12:00:00",
      BeginDate: this.selectedTask.scheduledStartDate,
      EndDate: this.selectedTask.scheduledEndDate ,
      StatusId: 2,
      CreatorId: 1,
      TeamId: 1,
      CustomerId: 0,
      LeadId: 0,
      InvoiceId: 0,
      TaskTime : ""
    }
    if(task.Title == "" || task.TypeId == 0)
    {
      this.loading = false;
      this.makeToast(this.status,this.sysLanguages.AddTaskAlertText,this.sysLanguages.PcToastrErrorTitle);
    }
    else
    {
      this._apiCallservice.Add("http://localhost:61232/api/Task/CreateTaskForMe", task).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "Session_Expired") {
        this.loading = false;
        this._router.navigate(['/auth']);
        return;
      }
      else 
      {
        this.loading = false;
        this.getTasks();
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, "kaydedildi");
        ref.close();      
      }        
    } 
    )
    }
  }

  /*updateActivity(ref)
  {
    let task = {
      TaskId: this.selectedTask.TaskId,
      ParentId:1,
      Title: $("#_activityname").val(),
      TaskDesc: $("#_activitydesc").val(),
      TypeId: this.selectedTask.selectedTaskType,
      CreatedDate: "",
      ResponsibleId: 1 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId: this.selectedTask.selectedProject,
      ColumnId:0,
      ContractId:1,
      ScheduledStartDate: $("#_activityscheduledstartdate").val(),
      ScheduledEndDate: $("#_activityscheduledenddate").val(),
      BeginDate: "",
      EndDate: $("#_activitydeadline").val(),
      DependsOnTaskId:1,
      StatusId: 2,
      CreatorId: 1,
      TeamId: 1,
      CustomerId: this.selectedTask.selectedCustomer,
      LeadId: this.selectedTask.selectedLead,
      InvoiceId: this.selectedTask.selectedInvoice,
      TaskComplete: "fromEditActivity"
    }

    this._apiCallservice.Update("http://localhost:61232/api/Activity/EditActivity", this.selectedTask.TaskId, task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      debugger;
      if (resultArray[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }
      else if (resultArray[0].Result == "True") {
        if(this.copyselectedAssignUsers != [])
        {
          for(let i in this.copyselectedAssignUsers)
          {
            if(this.selectedAssignUsers.findIndex(x => x.value == this.copyselectedAssignUsers[i]) == -1)
            {
              let taskmember = { TaskId: this.selectedTask.TaskId, UserId: this.copyselectedAssignUsers[i] }; 
              this._apiCallservice.Add("http://localhost:61232/api/Activity/RemoveActivityMember", taskmember).subscribe(
                (result) =>{
                }
              )
            }
          }

          for(let j in this.selectedAssignUsers)
          {
            if(this.copyselectedAssignUsers.findIndex(x => x.value == this.selectedAssignUsers[j]) == -1)
            {
              let taskmember = { TaskId: this.selectedTask.TaskId, UserId: this.selectedAssignUsers[j] }; 
              this._apiCallservice.Add("http://localhost:61232/api/Activity/CreateActivityMember", taskmember).subscribe(
                (result) =>{
                }
              )
            }
          }
        }

        this.getTasks();
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, "düzenlendi");
        ref.close();
      }  
    } 
    )
  }*/

  eventDrag(event)
  {
    var title = event.event.title.split(" * ")[0];
    let _taskId = event.event.title.split(" * ")[1];
    let newStartDate;
    let newEndDate;

    newEndDate = new Date(event.event._instance.range.end.toString().split("+")[0]);
    newEndDate.setDate(newEndDate.getDate());
    newEndDate = newEndDate.toISOString().split("T")[0];

    newStartDate = new Date(event.event._instance.range.start.toString().split("+")[0]);
    newStartDate = newStartDate.toISOString().split("T")[0];

    let eventid = event.event._def.extendedProps.eventid;

    let task = 
    {
      TaskId: _taskId,
      ParentId: 1,
      Title: title,
      TaskDesc: "",
      TypeId: 1,
      CreatedDate: "",
      ResponsibleId: 1,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: 0,
      ContractId: 1,
      ScheduledStartDate: newStartDate,
      ScheduledEndDate: newEndDate,
      BeginDate: "",
      EndDate: "",
      StatusId: 2,
      CreatorId: 1,
      TeamId: 1,
      CustomerId: 0,
      LeadId: 0,
      InvoiceId: 0
    };

    this._apiCallservice.Update("http://localhost:61232/api/Task/EditGoogleCalendarEvent", eventid, task).subscribe(
      (result) =>{
        this.getTasks();
        //this.getGoogleCalendarEvents();
      })
  }

  firstDrop(event)
  {
    var title = event.draggedEl.textContent;
    let newEndDate;
    let newStartDate;
    newEndDate = new Date(event.date.toString().split("+")[0]);
    newStartDate = newEndDate.toISOString().split("T")[0];
    newEndDate.setDate(newEndDate.getDate() + 1);
    newEndDate = newEndDate.toISOString().split("T")[0];

    var _task = this.unplannedTasks.filter(x => x.id == title.split(" * ")[1]);
    var taskId = _task[0].id;

    let task = 
    {
      TaskId: taskId,
      ParentId: 1,
      Title: title + " * " + taskId,
      TaskDesc: "Drag-Drop",
      TypeId: 0,
      CreatedDate: "",
      ResponsibleId: 1,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: 0,
      ContractId: 1,
      ScheduledStartDate: newStartDate,
      ScheduledEndDate: newEndDate,
      BeginDate: "",
      EndDate: "",
      StatusId: 2,
      CreatorId: 1,
      TeamId: 1,
      CustomerId: 0,
      LeadId: 0,
      InvoiceId: 0
    };

    this.draggedTaskID = taskId;

    this._apiCallservice.Add("http://localhost:61232/api/Task/AddGoogleCalendarEvent", task).subscribe(
      (result) =>{
        this.getTasks();
        //this.getGoogleCalendarEvents();
      })
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
}
