import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdcostsComponent } from './adcosts/adcosts.component';

const routes: Routes = [
  {
    path: '',
    component: AdcostsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdcostsRoutingModule { }
