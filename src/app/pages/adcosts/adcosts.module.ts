import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdcostsRoutingModule } from './adcosts-routing.module';
import { AdcostsComponent } from './adcosts/adcosts.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbContextMenuModule, NbSelectModule, NbTabsetModule, NbRouteTabsetModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  declarations: [AdcostsComponent],
  imports: [
    CommonModule,
    AdcostsRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbProgressBarModule,
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule,
    NbTabsetModule,
    NbSelectModule,
    NbRouteTabsetModule,
  ]
})
export class AdcostsModule { }
