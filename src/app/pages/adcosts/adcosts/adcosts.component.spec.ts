import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdcostsComponent } from './adcosts.component';

describe('AdcostsComponent', () => {
  let component: AdcostsComponent;
  let fixture: ComponentFixture<AdcostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdcostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdcostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
