import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NbDialogService, NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
@Component({
  selector: 'adcosts',
  templateUrl: './adcosts.component.html',
  styleUrls: ['./adcosts.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class AdcostsComponent implements OnInit {

  public sysLanguages : any;

  costId = 0;
  CostList = [];
  customerList = [];
  currencyList = [];
  monthList = [];
  monthListForFilter = [];
  CostData = {CustomerId : 0 , CurrencyTypeId : 0 , MonthId : 0 , AdCostDesc : "", Price : ""};
  edit = false;

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title, private dialogService: NbDialogService, private toastrService: NbToastrService) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.AdCosts + " - Crmplus");

    this.settings.columns.CustomerName.title = this.sysLanguages.BoCardCustomerText;
    this.settings.columns.AdCostDesc.title = this.sysLanguages.AddTaskDescText;
    this.settings.columns.Amount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settings.columns.MonthId.title = this.sysLanguages.MonthText;
    this.settings.columns.CurrencyDesc.title = this.sysLanguages.AddInvoiceCurrencyType;
    this.settings.columns.MonthId.filter.config.selectText = this.sysLanguages.BpAll;
    
    this.getCost();
    this.getCurrencies();
    this.getCustomers();
    this.monthList = [{label: this.sysLanguages.JanText, value: 1},{label: this.sysLanguages.FebText, value: 2},{label: this.sysLanguages.MarchText, value: 3},{label: this.sysLanguages.AprilText, value: 4},{label: this.sysLanguages.MayText, value: 5},{label: this.sysLanguages.JuneText, value: 6},{label: this.sysLanguages.JulyText, value: 7},{label: this.sysLanguages.AugustText, value: 8},{label: this.sysLanguages.SeptemberText, value: 9},{label: this.sysLanguages.OctoberText, value: 10},{label: this.sysLanguages.NovemberText, value: 11},{label: this.sysLanguages.DecemberText, value: 12}];
    
    for(var i = 0; i < this.monthList.length; i++ )
    {
      this.monthListForFilter.push({value:this.monthList[i].value , title:this.monthList[i].label});
    }
    this.settings.columns.MonthId.filter.config.list = this.monthListForFilter;
    this.settings = Object.assign({}, this.settings);
  }

  settings = {
    columns: {
      AdCostId: {
        title: 'ID',
        width: '2%'
      },
      CustomerName : {
        title : ''
      },
      AdCostDesc: {
        title: '',
      },
      MonthId: {
        title: '',
        valuePrepareFunction: (value) => {
          if(value == 1)
          {
            return this.sysLanguages.JanText;
          }
          else if (value == 2)
          {
            return this.sysLanguages.FebText;
          }
          else if (value == 3)
          {
            return this.sysLanguages.MarchText;
          }
          else if (value == 4)
          {
            return this.sysLanguages.AprilText;
          }
          else if (value == 5)
          {
            return this.sysLanguages.MayText;
          }
          else if (value == 6)
          {
            return this.sysLanguages.JuneText;
          }
          else if (value == 7)
          {
            return this.sysLanguages.JulyText;
          }
          else if (value == 8)
          {
            return this.sysLanguages.AugustText;
          }
          else if (value == 9)
          {
            return this.sysLanguages.SeptemberText;
          }
          else if (value == 10)
          {
            return this.sysLanguages.OctoberText;
          }
          else if (value == 11)
          {
            return this.sysLanguages.NovemberText;
          }
          else if (value == 12)
          {
            return this.sysLanguages.DecemberText;
          }
        },
        filter: {
          type: 'list',
            config: {
              selectText: '',
                list: this.monthListForFilter
              }
            }
      },
      Amount: {
        title: ''
      },
      CurrencyDesc : {
        title : ''
      }
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  getCost()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Cost/GetAllAdCosts").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.CostList = [];
          }
        }
        else 
        {
          this.CostList = resulttArray;  
        }
      }
    )
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
              this.customerList.push(_customers);
            }
          }
      }
    )
  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.currencyList = [];
          }
        }
        else
        {
          this.currencyList = [];
          for(let i in resultArray)
          {
            let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
            this.currencyList.push(_currency);
          }
        }
    },
    )
  }

  openDialog(dialog: TemplateRef<any>,value)
  {
    if(value == "save")
    {
      this.edit = false;
      this.CostData.AdCostDesc = "";
      this.CostData.CurrencyTypeId = 1;
      this.CostData.CustomerId = 0;
      this.CostData.MonthId = 1;
      this.CostData.Price = "";
    }
    else
    {
      this.edit = true;
      this.costId = value.data.AdCostId;
      this.CostData.AdCostDesc = value.data.AdCostDesc;
      this.CostData.MonthId = value.data.MonthId;
      this.CostData.Price = value.data.Amount;
      if(value.data.CurrencyDesc == "TL")
      {
        this.CostData.CurrencyTypeId = 1;
      }
      else if(value.data.CurrencyDesc == "American Dollar")
      {
        this.CostData.CurrencyTypeId = 2;
      }
      else if(value.data.CurrencyDesc == "Euro")
      {
        this.CostData.CurrencyTypeId = 3;
      }
      else if(value.data.CurrencyDesc == "Sterlin")
      {
        this.CostData.CurrencyTypeId = 4;
      }
      for(let i in this.customerList)
      {
        if(this.customerList[i].label == value.data.CustomerName)
        {
          this.CostData.CustomerId = this.customerList[i].value;
        }
      }
    }
    this.dialogService.open(dialog);
  }

  save(ref)
  {
    let cost = {
      AdCostDesc:  $("#AdCostDesc").val(),
      Amount : Number($("#price").val()).toLocaleString(),
      CustomerId : this.CostData.CustomerId,
      MonthId : this.CostData.MonthId,
      CurrencyTypeId : this.CostData.CurrencyTypeId
    };
    if(cost.AdCostDesc == "" || cost.Amount == '0' || cost.CustomerId == 0 )
    {
      this.makeToast(this.status , this.sysLanguages.ErrorMessage , this.sysLanguages.ErrorMessage1 );
    }
    else
    {
        this._apicallservice.Add("http://localhost:61232/api/Cost/CreateAdCost",cost).subscribe((result) => {    
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            ref.close();
            this.getCost();
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }                    
      },)
    }
  }
  
  editCost(ref)
  {
    let cost = {
      AdCostId : this.costId,
      AdCostDesc:  $("#AdCostDesc").val(),
      Amount : $("#price").val(),
      CustomerId : this.CostData.CustomerId,
      MonthId : this.CostData.MonthId,
      CurrencyTypeId : this.CostData.CurrencyTypeId
    };
    this._apicallservice.Add("http://localhost:61232/api/Cost/EditAdCost",cost).subscribe((result) => {    
         let resultArray = JSON.parse(JSON.stringify(result));
         if (resultArray[0].Result == "True") {
           ref.close();
           this.getCost();
         }
         else if (resultArray[0].Result == "Session_Expired") {
           this._router.navigate(['/auth']);
           return;
         }                    
      }, )
  }

    //toastr

    destroyByClick = true;
    duration = 3500;
    hasIcon = true;
    position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
    preventDuplicates = false;
    status: NbToastStatus = NbToastStatus.DANGER;
    statusDiscount : NbToastStatus = NbToastStatus.WARNING;
  
    types: NbToastStatus[] = [
      NbToastStatus.DEFAULT,
      NbToastStatus.DANGER,
      NbToastStatus.INFO,
      NbToastStatus.PRIMARY,
      NbToastStatus.SUCCESS,
      NbToastStatus.WARNING,
    ];
    positions: string[] = [
      NbGlobalPhysicalPosition.TOP_RIGHT,
      NbGlobalPhysicalPosition.TOP_LEFT,
      NbGlobalPhysicalPosition.BOTTOM_LEFT,
      NbGlobalPhysicalPosition.BOTTOM_RIGHT,
      NbGlobalLogicalPosition.TOP_END,
      NbGlobalLogicalPosition.TOP_START,
      NbGlobalLogicalPosition.BOTTOM_END,
      NbGlobalLogicalPosition.BOTTOM_START,
    ];
  
    makeToast(status, title, content) {
      this.showToast(status, title, content);
    }
  
    private showToast(type: NbToastStatus, title1: string, body: string) {
      const config = {
        status: type,
        destroyByClick: this.destroyByClick,
        duration: this.duration,
        hasIcon: this.hasIcon,
        position: this.position,
        preventDuplicates: this.preventDuplicates,
      };
      const titleContent = title1 ? `${title1}` : '';
  
      this.toastrService.show(
        body,
        `${titleContent}`,
        config);
    }

}
