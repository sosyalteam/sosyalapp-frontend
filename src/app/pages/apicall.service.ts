import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw'; 
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ApicallService {

    constructor(private _http: HttpClient) { }

    public GetAll = (apiname: string): Observable<Object> => {
        if(localStorage.length == 0)
            return this._http.get(apiname)
        else
        {
            const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
            return this._http.get(apiname, { headers: headers })
        }
    }
    public Get = (apiname: string, id: any): Observable<Object> => {
        if(localStorage.length == 0)
            return this._http.get(apiname + "/" + id)
        else
        {
            const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
            return this._http.get(apiname + "/" + id, { headers: headers })
        }
    }
    public Add = (apiname, item: any): Observable<Object> => {
        if(localStorage.length == 0)
            return this._http.post(apiname, item)
        else
        {
            const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
            return this._http.post(apiname, item, { headers: headers })
        }
    }
    public Update = (apiname, id: any, item: any): Observable<Object> => {
        if(localStorage.length == 0)
            return this._http.put(apiname + "/" + id, item)
        else
        {
            const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
            return this._http.put(apiname + "/" + id, item, { headers: headers })
        }
    }
    public Deactive = (apiname: string, id: any): Observable<Object> => {
        if(localStorage.length == 0)
            return this._http.delete(apiname + "/" + id)
        else
        {
            const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
            return this._http.delete(apiname + "/" + id, { headers: headers })
        }
    }
}
