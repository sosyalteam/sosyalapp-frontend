import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCostComponent } from './add-edit-cost.component';

describe('AddEditCostComponent', () => {
  let component: AddEditCostComponent;
  let fixture: ComponentFixture<AddEditCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
