import { Component, OnInit } from '@angular/core';
import { NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { elementStart } from '@angular/core/src/render3';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-cost',
  templateUrl: './add-edit-cost.component.html',
  styleUrls: ['./add-edit-cost.component.scss']
})
export class AddEditCostComponent implements OnInit {

  sysLanguages : any;
  costId : any;
  title = "";
  currency = "";
  isBilliable = 0;

  statusList = [];
  customerList = [];
  currencyList = [];
  projectList = [];
  _copyProjectList = [];
  proformList = [];
  _copyProformList = [];
  invoiceList = [];
  _copyInvoiceList = [];
  productList = [];
  taxList = [];

  LineData = [{ Id: '', ProductName: '', LineDesc: '', ProductCount: 1, ProductPrice: 0, LineAmount: "0", updatedefaultTax: ""}];
  addedDynamicLineData = [];

  _total = 0;
  topTax = 0;
  topTaxArray = [];
  tax: any;
  TaxDesc:string;
  selectedProduct : any;
  loading = false;

  relationList = [];
  isSelectedRelation: any;
  isSelectedInvoice = 0;
  isSelectedCustomer = 0;
  isSelectedProform = 0;
  isSelectedProject = 0;

  CostData = {StatusId : 0 , CurrencyTypeId : 0 , CostName : "" ,  CostDate : "" , PaymentDate : "" , CostDesc : "" , SupplierId : 0  };

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.Costs + " - Crmplus");

    this.relationList.push({ label: this.sysLanguages.CustomerText, value: 1 }, { label: this.sysLanguages.ProjectText, value: 2 } ,  { label: this.sysLanguages.InvoiceText, value : 3 } , { label : this.sysLanguages.ProformText , value : 4}   );

    if (this._avRoute.snapshot.params["id"]) {
      this.costId = this._avRoute.snapshot.params["id"];
    }

    this.getStatus();
    this.getCustomers();
    this.getCurrencies();
    this.getProject();
    this.getInvoice();
    this.getProform();
    this.getProducts();
    this.getTax();

  }

  ngOnInit() {
    if(this.costId > 0)
    {
      this.title = this.sysLanguages.EditCostText;
      this._apicallservice.Get("http://localhost:61232/api/Cost/GetCost",this.costId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            this.CostData.CostDate = this.formatIsoDateToCrypted(resulttArray[0].CostDate.split(" ")[0]);
            this.CostData.PaymentDate = this.formatIsoDateToCrypted(resulttArray[0].PaymentDate.split(" ")[0]);
            this.CostData.CostName = resulttArray[0].CostName;
            this.CostData.CostDesc = resulttArray[0].CostDesc;
            this.CostData.CurrencyTypeId = resulttArray[0].CurrencyTypeId;
            this.isSelectedCustomer = resulttArray[0].CustomerId;
            this.isSelectedInvoice = resulttArray[0].InvoiceId;
            this.isSelectedProform = resulttArray[0].ProformId;
            this.isSelectedProject = resulttArray[0].ProjectId;
            this.CostData.StatusId = resulttArray[0].StatusId;
            this.CostData.SupplierId = resulttArray[0].SupplierId;
            this.isBilliable = resulttArray[0].IsBilliable;
            if(this.CostData.CurrencyTypeId == 1)
            {
              this.currency = "TL";
            }
            else if (this.CostData.CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(this.CostData.CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(this.CostData.CurrencyTypeId == 4)
            {
              this.currency = "£";
            }

            if(this.isSelectedCustomer != 0)
            {
              this.isSelectedRelation=1;
            }
            else if (this.isSelectedProject != 0)
            {
              this.isSelectedRelation=2;
            }
            else if(this.isSelectedInvoice != 0)
            {
              this.isSelectedRelation=3;
            }
            else if (this.isSelectedProform != 0)
            {
              this.isSelectedRelation=4;
            }
            else
            {
              this.isSelectedRelation=null;
            }
            
          }              
      },
      )
      this._apicallservice.Get("http://localhost:61232/api/Cost/GetCostsLines", this.costId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.LineData = [];
            }
          }
          else
          {
            let _lineData;          
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                if(resultArray[i].ProductId == 0)
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName  };
                  this.LineData.push(_lineData);
                }
                else
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName };
                  this.LineData.push(_lineData);
                }

              }
              else
              {
                if(resultArray[i].ProductId == 0)
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName  };
                  this.LineData.push(_lineData);
                }
                else
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName };
                  this.LineData.push(_lineData);
                }
              }
            }

            this.updatedTotal();
          }
        }
      )
    }
    else
    {
      let _today = new Date();
      this.title = this.sysLanguages.NewCostText;
      this.CostData.StatusId = 1;
      this.CostData.CurrencyTypeId = 1;
      this.currency = "TL";
      this.CostData.CostDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
    }
  }

  changeCurrencyType(event)
  {
    if(event == 1)
    {
      this.currency = "TL";
    }
    else if(event == 2)
    {
      this.currency = "$";
    }
    else if(event == 3)
    {
      this.currency = "€";
    }
    else if(event == 4)
    {
      this.currency = "£";
    }
  }

  getStatus()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Cost/GetAllCostStatus").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.statusList = [];
          }
        }
        else
        {
          this.statusList = [];

          for(let i in resultArray)
          {
            let _status;

            if(resultArray[i].CostStatusId == 1)    
            {
              _status = { label: this.sysLanguages.PayableCost, value: resultArray[i].CostStatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].CostStatusId == 2)
            {
              _status = { label: this.sysLanguages.PaidCost, value: resultArray[i].CostStatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].CostStatusId == 3)
            {
              _status = { label: this.sysLanguages.PaidByEmployee, value: resultArray[i].CostStatusId };
              this.statusList.push(_status);
            }     
          }
        }
    },
    )
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId, taxNumber: resultArray[i].TaxNumber };
              this.customerList.push(_customers);
            }
          }
      }
    )
  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.currencyList = [];
          }
        }
        else
        {
          this.currencyList = [];
          for(let i in resultArray)
          {
            let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
            this.currencyList.push(_currency);
          }
        }
    },
    )
  }

  getProject()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Project/GetTaskBoards").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.projectList = [];
          }
        }
        else
        {
          this.projectList = [];
          for(let i in resultArray)
          {
            let _projectlist = { label: resultArray[i].BoardName, value: resultArray[i].BoardId , id: resultArray[i].CustomerId };
            this.projectList.push(_projectlist);
          }
        }
      }
    )
  }

  getProform()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proform/GetAllProforms").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.proformList = [];
          }
        }
        else
        {
          this.proformList = [];
          for(let i in resultArray)
          {
            let _proformlist = { label: resultArray[i].ProformNumber, value: resultArray[i].ProformId , id: resultArray[i].CustomerId };
            this.proformList.push(_proformlist);
          }
        }
      }
    )
  }

  getInvoice()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.invoiceList = [];
          }
        }
        else
        {
          this.invoiceList = [];
          for(let i in resultArray)
          {
            let _invoicelist = { label: resultArray[i].InvoiceNumber, value: resultArray[i].InvoiceId , id: resultArray[i].CustomerId };
            this.invoiceList.push(_invoicelist);
          }
        }
      }
    )
  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }
                  }
                }

                _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, price: resultArray[i].Price, desc: resultArray[i].ProductDesc };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }
          }
        }
    },
    )
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          this.taxList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )
  }

  getProductDetail(event)
  {
    if(!this.costId)
    {
        this.LineData = [];
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" };
            this.LineData.push(_lineData);
          }
        }
    }
    else
    {
        this.LineData.shift();
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: ""  };
            this.LineData.unshift(_lineData);
          }
        }
    }
  }

  billiableCost(value)
  {
    this.isBilliable = value;
  }

  updatedTotal()
  {
    var total = 0;
    var z = document.getElementById("total");
    for(var i = 1; i < this.LineData.length; i++)
    {
      if(this.LineData[i].LineAmount != "0")
      {
        var sub=parseFloat(this.LineData[i].LineAmount);
        total=total+sub;
        z.innerHTML=Number(total).toLocaleString()+" " + this.currency;
        this._total = total;

        if(this.LineData[i].updatedefaultTax == "%18 KDV")
        {
          this.topTaxArray.push({ value: (total * 0.18), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.18);
        }
        else if(this.LineData[i].updatedefaultTax == "%17.65 Stopaj")
        {
          this.topTaxArray.push({ value: total * 17.65/100, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 17.65/100);
        }
        else if(this.LineData[i].updatedefaultTax == "%15 Stopaj")
        {
          this.topTaxArray.push({ value: total * 0.15, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.15);
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj")
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.18) + (total * 17.65/100);
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%15 Stopaj")
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.15) + (total * 0.18);
        }
        else if(this.LineData[i].updatedefaultTax == "%17.65 Stopaj,%15 Stopaj")
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.15) + (total * 17.65/100);
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj,%15 Stopaj")
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
          this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
        }
      }
    }
    z.innerHTML= Number(this._total).toLocaleString() + " " + this.currency;
  }

  total(id)
  {
    var y = document.getElementById("f"+id).innerHTML.split(" ")[0];
    var x =document.getElementById("total");
    this._total = parseFloat(y) + this._total;
    x.innerText=Number(this._total).toLocaleString()+" " + this.currency;  
  }

  createLine()
  {
    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid = line[0].childElementCount+1;
    var a1 = $("#a1").val();
    var b1 = $("#b1").val();
    var c1 = $("#c1").val().toString();
    var d1 = $("#d1").val().toString();
    var total = (parseFloat(c1)*parseFloat(d1));

    if(!this.tax || this.tax.length == 0)
    {
      this.TaxDesc = "Vergi seçimi yapılmadı.";
    }
    else if(this.tax.length == 1)
    {
      if(this.tax[0] == 1)
      {
        this.TaxDesc = "%18 KDV";
        this.topTaxArray.push({ value: (total * 0.18), amount: total + (total * 0.18), count: c1, price: d1});
        this.topTax += (total * 0.18);

        total = total + (total*0.18);
      }
      else if(this.tax[0] == 2)
      {
        this.TaxDesc = "%17.65 Stopaj";
        this.topTaxArray.push({ value: total * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
        this.topTax += (total * 17.65/100);

        total = total + (total*17.65/100);
      }
      else if(this.tax[0] == 3)
      {
        this.TaxDesc = "%15 Stopaj";
        this.topTaxArray.push({ value: total * 0.15, amount: total+(total*0.15), count: c1, price: d1});
        this.topTax += (total * 0.15);

        total = total + (total*0.15);
      }
    }
    else if(this.tax.length == 2)
    {
      if((this.tax[0] == 1 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj";
        this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
        this.topTax += (total * 0.18) + (total * 17.65/100);

        total = total + (total*0.18)+(total*17.65/100);
      }
      else if((this.tax[0] == 1 && this.tax[1] == 3) || (this.tax[0] == 3 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%15 Stopaj";
        this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
        this.topTax += (total * 0.15) + (total * 0.18);

        total = total + (total*0.15) + (total*0.18);
      }
      else if((this.tax[0] == 3 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 3))
      {
        this.TaxDesc = "%17.65 Stopaj,%15 Stopaj";
        this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
        this.topTax += (total * 0.15) + (total * 17.65/100);

        total = total + (total*0.15) + (total*17.65/100);
      }
    }
    else if(this.tax.length == 3)
    {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj,%15 Stopaj";
        this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
        this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);

        total = total + (total*0.18) + (total*0.15) + (total*17.65/100);
    }

    if (total.toString() == "NaN")
    {
      total = 0;
    }

    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><p id='c"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;''>" + c1 + "</p></td><td><p id='d"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + d1 + "</p></td><td><p id='updatedefaultTax' style='float: left;margin-top: 26px;margin-left:5px;' >"+this.TaxDesc+"</p></td><td><p id='f"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>"+total+" "+this.currency+"</p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red ;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_"+newid);
    let mythis = this;

    this.total(newid);

    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let counttext = element.parentElement.parentElement.children[2].children[0].innerHTML;

      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue), 1);

      var id = element.id.split("_")[1];
      var minus = $("#f"+id).text().split(" ")[0];
      document.getElementById(id).remove();
      var a = document.getElementById("total");
      this._total = this._total - parseFloat(minus);
      a.innerText = Number(this._total).toLocaleString() + " " + this.currency; 

      for(let i in this.topTaxArray)
      {
        if( this.topTaxArray[i].count == counttext)
        {
          this.topTax -= this.topTaxArray[i].value;
          if(this.topTax < 0)
          {
            this.topTax = 0;
          }
          break;
        }
      }

    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString(), ProductCount: Number(c1), ProductPrice: Number(d1), LineAmount: total + " " + this.currency, updatedefaultTax: "", LineTax: this.TaxDesc };
    this.addedDynamicLineData.push(_lineData); 
  }

  saveCost()
  {
    this.loading = true;
    if(this.title == this.sysLanguages.NewCostText)
    {
      var x = document.getElementById("total");
      var amount = x.innerText;
      let cost = {
        CostName : $("#CostName").val(),
        ProjectId : this.isSelectedProject,
        CustomerId : this.isSelectedCustomer,
        ProformId : this.isSelectedProform,
        InvoiceId : this.isSelectedInvoice,
        CostDesc : $("#CostDesc").val(),
        CostDate : $("#CostDate").val(),
        PaymentDate : $("#PaymentDate").val(),
        StatusId : this.CostData.StatusId,
        isBilliable : this.isBilliable,
        CurrencyTypeId : this.CostData.CurrencyTypeId,
        Amount : amount,
        SupplierId : this.CostData.SupplierId
      };
      if(cost.CostName == "")
      {
        this.makeToast(this.status , this.sysLanguages.ErrorMessage , this.sysLanguages.ErrorMessage1 );
        this.loading = false;
      }
      else if (cost.Amount == "0.00 " +this.currency || cost.Amount == "0 " +this.currency  )
      {
        this.makeToast(this.status , this.sysLanguages.ErrorMessage , this.sysLanguages.ErrorNoProduct );
        this.loading = false;
      }
      else
      {
        this._apicallservice.Add("http://localhost:61232/api/Cost/CreateCost", cost).subscribe(
        (result) => {
          var costId = result[0].Result;
          let costDetail;           

          for(let i = 0; i < this.addedDynamicLineData.length; i++)
          {
            let _product = 0;
            
            for(let j in this.productList)
            {
              if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
              {
                _product = this.productList[j].value;
              }
            }

            if(_product == 0)
                costDetail = {ID: 0, CostId: costId, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName };
            else
                costDetail = {ID: 0, CostId: costId, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" };
              
            this._apicallservice.Add("http://localhost:61232/api/Cost/CreateCostDetail", costDetail).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
              else
              {
                this.loading = false;
                this._router.navigate(['/pages/costs']);    
              }
            })
          }  
        })
      }
    }
    else if (this.title == this.sysLanguages.EditCostText)
    {
      var x = document.getElementById("total");
      var amount = x.innerText;

      if(this.CostData.CostName == "" || amount == "0 " + this.currency )
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {

        let editCost = {
          CostId : this.costId,
          CostName : $("#CostName").val(),
          ProjectId : this.isSelectedProject,
          CustomerId : this.isSelectedCustomer,
          ProformId : this.isSelectedProform,
          InvoiceId : this.isSelectedInvoice,
          CostDesc : $("#CostDesc").val(),
          CostDate : $("#CostDate").val(),
          PaymentDate : $("#PaymentDate").val(),
          StatusId : this.CostData.StatusId,
          isBilliable : this.isBilliable,
          CurrencyTypeId : this.CostData.CurrencyTypeId,
          Amount : amount,
          SupplierId : this.CostData.SupplierId
        };

        this._apicallservice.Add("http://localhost:61232/api/Cost/EditCost", editCost).subscribe(
          (result) => {
            let total = 0;
            let lineCount = 0;

            let resultArray = JSON.parse(JSON.stringify(result));

            if(this.LineData != [])
            {
              for(let i in this.LineData)
              {
                if(this.LineData[i].LineAmount != "0")
                {
                  total += Number(this.LineData[i].LineAmount.split(" ")[0]);
                }
              }
            }

            if(this.addedDynamicLineData != [])
            {
              for(let j in this.addedDynamicLineData)
              {
                if(this.addedDynamicLineData[j].LineAmount != "0")
                {
                  total += Number(this.addedDynamicLineData[j].LineAmount.split(" ")[0]);
                }
              }
            }

            if(total != Number($("#total").val().toString().split(" ")[0]))
            {
                if(this.LineData.length != 0)
                {
                  for(let i = 0; i < this.LineData.length; i++)
                  {
                    if(this.LineData[i].LineAmount != "0")
                    {
                      lineCount++;   
                      let _line = { Id: this.LineData[i].Id, LineId: lineCount };                               
                      this._apicallservice.Update("http://localhost:61232/api/Cost/EditCostDetail", Number(this.LineData[i].Id), _line).subscribe(
                        (resultt) =>{
                        }
                      )              
                    }                
                  }
                }
                if(this.addedDynamicLineData != [])
                {
                  let _product = 0;
                  for(let i in this.addedDynamicLineData)
                  {
                    for(let j in this.productList)
                    {
                      if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                      {
                        _product = this.productList[j].value;
                      }
                    }
                    
                    lineCount++;
                    let _costdetail;

                    if(_product == 0)
                      _costdetail = { CostId: this.costId, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: lineCount, LineName: this.addedDynamicLineData[i].ProductName };
                    else
                      _costdetail = { CostId: this.costId, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: lineCount, LineName: "" };
                      
                      this._apicallservice.Add("http://localhost:61232/api/Cost/CreateCostDetail", _costdetail).subscribe(
                      (resultt) =>{
                      }
                    )
                  }
                }
            }

            this.loading = false;
            this._router.navigate(['/pages/costs']);

          }
        )
      }
    }
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let priceforTaxValue = event.currentTarget.parentElement.parentElement.children[3].children[0].innerText;
    let priceValue = event.currentTarget.parentElement.parentElement.children[6].children[0].innerText;
    let counttext = event.currentTarget.parentElement.parentElement.children[2].children[0].innerText;

    
    for(let i in this.topTaxArray)
    {
      if(this.topTaxArray[i].amount == priceValue && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
      {
        this.topTax -= this.topTaxArray[i].value;
        if(this.topTax < 0)
        {
          this.topTax = 0;
        }
        break;
      }
    }
    
    this._apicallservice.Deactive("http://localhost:61232/api/Cost/RemoveCostDetail", Number(this.LineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          var x = this.LineData[id].LineAmount.split(" ")[0];
          var total =document.getElementById("total");
          var newTotal = this._total-parseFloat(x);
          this._total = newTotal;
          total.innerHTML=Number(newTotal).toLocaleString()+" " + this.currency;
          document.getElementById("lineData_" + id).remove();
          this.LineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  //toastr

  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  statusDiscount : NbToastStatus = NbToastStatus.WARNING;

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
