import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostOverviewComponent } from './cost-overview.component';

describe('CostOverviewComponent', () => {
  let component: CostOverviewComponent;
  let fixture: ComponentFixture<CostOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
