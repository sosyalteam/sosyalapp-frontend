import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'cost-overview',
  templateUrl: './cost-overview.component.html',
  styleUrls: ['./cost-overview.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class CostOverviewComponent implements OnInit {

  sysLanguages: any;
  public CostArray = [{CostName : "", BoardName : "" , CustomerName : "", ProformNumber : "", InvoiceNumber : "", CostDesc : "", CostDate : "", PaymentDate : "", Amount : "" , isBilliableText : "" , CurrencyDesc : "" , CostStatusDesc :""   }];
  productList = [];
  isBillable = 0;

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CostOverviewText + " - Crmplus");

    this.tabs[0].title = this.sysLanguages.CostOverviewText;

  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/costs/cost-overview', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/Cost/DetailsForCostOverview",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              this.CostArray = [];
            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.CostArray[0].CostName = resulttArray[0].CostName;
            this.CostArray[0].CostDesc = resulttArray[0].CostDesc;
            this.CostArray[0].ProformNumber = resulttArray[0].ProformNumber;
            this.CostArray[0].InvoiceNumber = resulttArray[0].InvoiceNumber;
            this.CostArray[0].CustomerName = resulttArray[0].CustomerName;
            this.CostArray[0].BoardName = resulttArray[0].BoardName;
            this.CostArray[0].CostDate = resulttArray[0].CostDate.split(" ")[0];
            this.CostArray[0].PaymentDate = resulttArray[0].PaymentDate.split(" ")[0];
            this.CostArray[0].Amount =  resulttArray[0].Amount;
            this.CostArray[0].CurrencyDesc = resulttArray[0].CurrencyDesc;
            this.isBillable = resulttArray[0].IsBilliable;
            this.CostArray[0].CostStatusDesc = resulttArray[0].CostStatusDesc;

            if(this.isBillable == 1)
            {
              this.CostArray[0].isBilliableText = this.sysLanguages.BillableCost;
            }
            else if(this.isBillable == 0)
            {
              this.CostArray[0].isBilliableText = this.sysLanguages.NotBillableCost;
            }

            if(this.CostArray[0].CostDesc == "")
            {
              this.CostArray[0].CostDesc = this.sysLanguages.PcNoNote;
            }

            if(resulttArray[0].ProjectId == 0)
            {
              this.CostArray[0].BoardName = this.sysLanguages.NotValueText;
            }
            if(resulttArray[0].CustomerId == 0)
            {
              this.CostArray[0].CustomerName = this.sysLanguages.NotValueText;
            }
            if(resulttArray[0].InvoiceId == 0)
            {
              this.CostArray[0].InvoiceNumber = this.sysLanguages.NotValueText;
            }
            if(resulttArray[0].ProformId == 0)
            {
              this.CostArray[0].ProformNumber = this.sysLanguages.NotValueText;
            }
            

            if(resulttArray[0].StatusId == 1)
            {
              this.CostArray[0].CostStatusDesc = this.sysLanguages.PayableCost;
            }  
            else if(resulttArray[0].StatusId == 2)
            {
              this.CostArray[0].CostStatusDesc = this.sysLanguages.PaidCost;
            }  
            else if(resulttArray[0].StatusId == 3)
            {
              this.CostArray[0].CostStatusDesc = this.sysLanguages.PaidByEmployee;
            }
          }       
      },
      );
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/costs/add-edit-cost', this._avRoute.snapshot.params["id"]]);
  }

}
