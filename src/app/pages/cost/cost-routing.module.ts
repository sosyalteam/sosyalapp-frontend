import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostsComponent } from './costs/costs.component';
import { AddEditCostComponent } from './add-edit-cost/add-edit-cost.component';
import { CostOverviewComponent } from './cost-overview/cost-overview.component';

const routes: Routes = [
  {
    path: '',
    component: CostsComponent,
  },
  {
    path: 'add-edit-cost',
    component: AddEditCostComponent,
  },
  {
    path: 'add-edit-cost/:id',
    component: AddEditCostComponent,
  },
  {
    path: 'cost-overview/:id',
    component: CostOverviewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostRoutingModule { }
