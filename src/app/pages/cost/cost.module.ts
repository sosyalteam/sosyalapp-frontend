import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CostRoutingModule } from './cost-routing.module';
import { CostsComponent } from './costs/costs.component';
import { AddEditCostComponent } from './add-edit-cost/add-edit-cost.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbContextMenuModule, NbSelectModule, NbTabsetModule, NbRouteTabsetModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { CostOverviewComponent } from './cost-overview/cost-overview.component';

@NgModule({
  declarations: [CostsComponent, AddEditCostComponent, CostOverviewComponent],
  imports: [
    CommonModule,
    CostRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbProgressBarModule,
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule,
    NbTabsetModule,
    NbSelectModule,
    NbRouteTabsetModule,
  ]
})
export class CostModule { }
