import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'costs',
  templateUrl: './costs.component.html',
  styleUrls: ['./costs.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class CostsComponent implements OnInit {

  public sysLanguages : any;
  public CostList = [];
  allCostCount = 0;
  payableCost = 0;
  paidCost = 0;
  paidByEmployeeCost = 0;

  filter =false;
  filteredCostList : any;

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.Costs + " - Crmplus");

    this.settings.columns.CostName.title=this.sysLanguages.CostNameText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.CustomerName.title = this.sysLanguages.CustomerText;
    this.settings.columns.CostDate.title=this.sysLanguages.CreatedDateText;
    this.settings.columns.CostDesc.title=this.sysLanguages.AddTaskDescText;
    this.settings.columns.CostStatusDesc.title=this.sysLanguages.AddTaskStatusText;

    this.getCost();
  }

  settings = {
    columns: {
      CostId: {
        title: 'ID',
        width: '2%'
      },
      CostName: {
        title: '',
      },
      CustomerName : {
        title : ''
      },
      CostDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CostDesc: {
        title: ''
      },
      CostStatusDesc: {
        title: ''
      },
      Amount: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  getCost()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Cost/GetAllCosts").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.CostList = [];
          }
        }
        else 
        {
          this.CostList = resulttArray;
          this.allCostCount = resulttArray.length;
          this.payableCost = 0;
          this.paidCost = 0;
          this.paidByEmployeeCost = 0;
          for(let i in this.CostList)
          {

            if(this.CostList[i].StatusId == 1)
            {
              this.CostList[i].StatusDesc = this.sysLanguages.PayableCost;
              this.payableCost++;
            }  
            else if(this.CostList[i].StatusId == 2)
            {
              this.CostList[i].StatusDesc = this.sysLanguages.PaidCost;
              this.paidCost++;
            }  
            else if(this.CostList[i].StatusId == 3)
            {
              this.CostList[i].StatusDesc = this.sysLanguages.PaidByEmployee;
              this.paidByEmployeeCost++;
            }
          }   
        }
      }
    )
    

  }

  addCost()
  {
    this._router.navigate(['/pages/costs/add-edit-cost']);
  }

  editCost(event)
  {
    var id = `${event.data.CostId}`;
    this._router.navigate(['/pages/costs/cost-overview',id]);
  }

  filterCost(value)
  {
    this.filter = true;
    if(value == "1")
    {
      const a = this.CostList.filter(x=>x.StatusId == 1);
      this.filteredCostList=a;
    }
    else if(value == "2")
    {
      const a = this.CostList.filter(x=>x.StatusId == 2);
      this.filteredCostList=a;
    }
    else if(value == "3")
    {
      const a = this.CostList.filter(x=>x.StatusId == 3);
      this.filteredCostList=a;
    }
    else if(value == "All")
    {
      this.filter = false;
    }
  }

}
