import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-customer',
  templateUrl: './add-edit-customer.component.html',
  styleUrls: ['./add-edit-customer.component.scss'],
  providers: [ApicallService, NbToastrService]
})
export class AddEditCustomerComponent implements OnInit {

  sysLanguages: any;
  customerStatusList = [];
  customerData = { CustomerId: 0, CustomerName: "", TaxNumber: "", Phone: "", Website: "", StatusID: 10, Address: "", City: "", PostCode: "", CustomerDiscount : "" , BrandName : ""};
  customerId: any;
  loading = false;

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _apicallService: ApicallService, private _router: Router, private toastrService: NbToastrService, private _avRoute: ActivatedRoute, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompCstomer + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.getCustomerStatus();

  }

  ngOnInit() 
  {
    if(this.customerId > 0)
    {
      setTimeout(()=>{
        this._apicallService.Get("http://localhost:61232/api/Customer/GetCustomerById", this.customerId)
        .subscribe(
          (result) => {
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray.length != 0 && resultArray[0].Result) {
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
              else if(resultArray[0].Result == "Empty")
              {
                // do smth.
              }
            }
            else
            {
              this.customerData.Address = resultArray[0].Address;
              this.customerData.City = resultArray[0].City;
              this.customerData.CustomerDiscount = resultArray[0].CustomerDiscount;
              this.customerData.CustomerId = this.customerId;
              this.customerData.CustomerName = resultArray[0].CustomerName;
              this.customerData.Phone = resultArray[0].Phone;
              this.customerData.PostCode = resultArray[0].PostCode;
              this.customerData.StatusID = resultArray[0].StatusId;
              this.customerData.TaxNumber = resultArray[0].TaxNumber;
              this.customerData.Website = resultArray[0].Website;
              this.customerData.BrandName = resultArray[0].BrandName;        
            }              
        },
        )
      }, 500);
    }
    else
    {
      this.customerData.Address = "";
      this.customerData.City = "";
      this.customerData.CustomerDiscount = "%";
      this.customerData.CustomerId = 0;
      this.customerData.CustomerName = "";
      this.customerData.Phone = "";
      this.customerData.PostCode = "";
      this.customerData.StatusID = 10;
      this.customerData.TaxNumber = "";
      this.customerData.Website = "";
      this.customerData.BrandName = "";
    }
  }
  
  
  getCustomerStatus()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/GetCustomerStatus").subscribe(
      (result) =>
      {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerStatusList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _status = { label: resultArray[i].StatusDesc, value: resultArray[i].StatusId };
            this.customerStatusList.push(_status);
          }
          this.customerData.StatusID = 1;
        }
      }
      ) 
  }

  saveCustomer()
  {
    this.loading = true;
    if(this.customerId > 0)
    {
      let _customer = { CustomerId: this.customerId, CustomerName: $("#customerName").val(), TaxNumber: $("#customerTax").val(), Phone: $("#customerPhone").val(), Website: $("#customerWebsite").val(), StatusId: this.customerData.StatusID, Address: $("#customerAddress").val(), City: $("#customerCity").val(), PostCode: $("#customerPostcode").val(), CustomerDiscount: $("#customerDiscount").val() , BrandName: $("#brandName").val() };
      if( _customer.CustomerName == "" || _customer.Phone == "" || _customer.City == "" )
      {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.CustomerToastrEmptyAreas);
        this.loading = false;
      }
      else
      {
        this._apicallService.Update("http://localhost:61232/api/Customer/Update", this.customerId, _customer)
        .subscribe((result) => {
          if (result[0].Result == "True") {
            /*$(".dialogAddCustomerCard").parent().remove();
            $(".cdk-overlay-backdrop").remove();*/
            this.loading = false;
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrSaved);
            this._router.navigate(['/pages/customer']);
          }
          else if (result[0].Result == "Session_Expired") {
            this.loading = false;
            this._router.navigate(['/auth']);
            return;
          }
        },
        )
      }
    }
    else
    {
      let _customer = { CustomerName: $("#customerName").val(), TaxNumber: $("#customerTax").val(), Phone: $("#customerPhone").val(), Website: $("#customerWebsite").val(), StatusId: this.customerData.StatusID, Address: $("#customerAddress").val(), City: $("#customerCity").val(), PostCode: $("#customerPostcode").val(),CustomerDiscount: $("#customerDiscount").val() , BrandName: $("#brandName").val() };
      if( _customer.CustomerName == "" || _customer.Phone == "" || _customer.City == "" )
      {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.CustomerToastrEmptyAreas);
        this.loading = false;
      }
      else
      {
        this._apicallService.Add("http://localhost:61232/api/Customer/Create", _customer)
        .subscribe((result) => {
        if (result[0].Result == "True") {
          this.loading = false;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrSaved);
          var customerid=result[1].Value;
          this._router.navigate(['/pages/customer/customer-overview', customerid]);
        }
        else if (result[0].Result == "Session_Expired") {
          this.loading = false;
          this._router.navigate(['/auth']);
          return;
        }
      },
      )
      }  
    }  
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
