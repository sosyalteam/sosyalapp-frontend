import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import $ from 'jquery';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [ApicallService, HttpModule],
})
export class ContactComponent implements OnInit {
  sysLanguages: any;
  contactList:any;
  customerId:any;
  customerList = [];
  title: "";
  dialogTitle:"";
  ContactID : any;
  contactData = {Fullname: "", Email: "", Phone: "", Position: "",CustomerId:"",Password : ""};
  selectedCustomer: any;


  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private dialogService: NbDialogService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompContacts + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.settings.columns.Fullname.title=this.sysLanguages.ContactTableFullnameText;
    this.settings.columns.Position.title=this.sysLanguages.ContactTablePositionText;
    this.settings.columns.Phone.title=this.sysLanguages.ContactTablePhoneText;
    this.settings.columns.CustomerName.title=this.sysLanguages.ContactTableCustomerText;

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title=this.sysLanguages.ContactProjectOverviewContactsText;
    this.tabs[2].title = this.sysLanguages.LeadText;
    this.tabs[3].title = this.sysLanguages.InvoiceText;
    this.tabs[4].title = this.sysLanguages.ProposalText;
    this.tabs[5].title = this.sysLanguages.ProformText;
    this.tabs[6].title = this.sysLanguages.ProjectText;
    this.tabs[7].title = this.sysLanguages.Costs;
    this.tabs[8].title = this.sysLanguages.FilesText;
    this.tabs[9].title = this.sysLanguages.BoardsNoteText;
    this.tabs[10].title = this.sysLanguages.SafeText;

    this.getCustomers();

  }

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/contact', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'ion-ios-copy-outline',
      responsive: true,
      route: ['/pages/customer/customer-notes', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  settings = {
    columns: {
      ContactId: {
        title: 'ID',
        width:'2%'
      },
      Fullname: {
        title: ''
      },
      Email: {
        title: 'Mail'
      },
      Position: {
        title: ''
      },
      Phone: {
        title: ''
      },
      CustomerName: {
        title: ''
      },
    },
    actions:{
      columnTitle:'',
      position:'right', 
      add:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode: 'external', // bu önemli bu, add butonunda özel fonksiyon yazmamıza yarıyor bkz.(create)
  };

  ngOnInit() {
    if(this.customerId>0){
      this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          {
            this.contactList = resultArray;
            this.title=resultArray[0].CustomerName;
          }
        }
      )
    }
    else
    {
      var x = document.getElementById("tabs");
      x.style.display="none";
      this.getContacts();
    }
  }

  getCustomers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
            this.customerList.push(_customers);
          }
        }
      }
    )
  }

  getContacts()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.contactList = [];
          }
        }
        else
        {
          this.contactList = resultArray;
        }
      }
    )
  }

  getContactById()
  {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          {
            this.contactList = resultArray;
            this.title=resultArray[0].CustomerName;
          }
        }
      )
  }

  openDialog(dialog: TemplateRef<any>, val, event) {
    
    if(val == "save")
    {    
      this.dialogTitle=this.sysLanguages.AddContactHeaderText;
      this.contactData.Fullname = "";
      this.contactData.Email = "";
      this.contactData.Phone = "";
      this.contactData.Position = "";
      this.contactData.CustomerId="";
      this.selectedCustomer = "";

      this.dialogService.open(dialog);
    }
    else if(val == "update")
    {
      this.ContactID=parseInt(`${event.data.ContactId}`);
      this.dialogTitle=this.sysLanguages.EditContactHeaderText;
      this.contactData.Fullname = event.data.Fullname;
      this.contactData.Email = event.data.Email;
      this.contactData.Phone = event.data.Phone;
      this.contactData.Position = event.data.Position;

      if(this.customerId)
        this.contactData.CustomerId=this.customerId;
      else
      {
        for(let i in this.customerList)
        {
          if(event.data.CustomerName == this.customerList[i].label)
          {
            this.selectedCustomer = this.customerList[i].value;
            break;
          }
        }
      }

      this.dialogService.open(dialog);
    }
  }

  save(ref)
  {
    let contact;

    if(this.dialogTitle==this.sysLanguages.AddContactHeaderText)
    {
      if(this.customerId)
      {
        contact = { ContactId: 0, Fullname:  $("#Fullname").val(), Email : $("#Email").val(), Phone : $("#Phone").val(), Position : $("#Position").val(), CustomerId : this.customerId, Password : $("#Password").val()};
      }
      else{
        contact = { ContactId: 0, Fullname:  $("#Fullname").val(), Email : $("#Email").val(), Phone : $("#Phone").val(), Position : $("#Position").val(), CustomerId : this.selectedCustomer, Password : $("#Password").val()};
      }

      this._apicallService.Add("http://localhost:61232/api/Customer/CreateContact",contact) 
         .subscribe((result) => {    
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result == "True") {
              ref.close();
              if(this.customerId)
              {
                this.getContactById();
              }
              else{
                this.getContacts();
              }
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }                    
         },) 
    }
    else if(this.sysLanguages.EditContactHeaderText)
    {
      let editcontact;

      if(this.customerId)
      {
        editcontact = 
        {
          ContactId: this.ContactID,
          Fullname:  $("#Fullname").val(),
          Email : $("#Email").val(),
          Phone : $("#Phone").val(),
          Position : $("#Position").val(),
          CustomerId : this.customerId,
          Password : $("#Password").val(),
        };
      }
      else
      {
        editcontact = 
        {
          ContactId: this.ContactID,
          Fullname:  $("#Fullname").val(),
          Email : $("#Email").val(),
          Phone : $("#Phone").val(),
          Position : $("#Position").val(),
          CustomerId : this.selectedCustomer,
          Password : $("#Password").val(),
        };
      }

      this._apicallService.Update("http://localhost:61232/api/Customer/UpdateContact",this.ContactID,editcontact) 
         .subscribe((result) => {    
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result == "True") {
              ref.close();
              if(this.customerId)
              {
                this.getContactById();
              }
              else{
                this.getContacts();
              }
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }                    
         }, )

    }
  }

  deleteContact(event)
  {
    var ans = confirm(this.sysLanguages.ContactDeleteText + parseInt(event.data.ContactId));
    if(ans)
    {
      this._apicallService.Deactive("http://localhost:61232/api/Customer/DeleteContact", parseInt(event.data.ContactId)).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        if(this.customerId)
        {
          this.getContactById();
        }
        else{
          this.getContacts();
        }
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }    
     },)
    }    
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }
}
