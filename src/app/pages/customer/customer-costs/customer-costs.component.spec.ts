import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerCostsComponent } from './customer-costs.component';

describe('CustomerCostsComponent', () => {
  let component: CustomerCostsComponent;
  let fixture: ComponentFixture<CustomerCostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerCostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
