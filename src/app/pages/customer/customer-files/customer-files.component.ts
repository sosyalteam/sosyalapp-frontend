import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { Title } from '@angular/platform-browser';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService, NbDialogService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'customer-files',
  templateUrl: './customer-files.component.html',
  styleUrls: ['./customer-files.component.scss'],
  providers: [ApicallService, HttpClient, NbToastrService]
})
export class CustomerFilesComponent implements OnInit {

  public sysLanguages: any;
  customerId : any;
  fileList = [];
  selectedFile = { FileName: "", FileUrl: "" };
  loading = false;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private dialogService: NbDialogService, private _router: Router, private titleService: Title, private http: HttpClient, private toastrService: NbToastrService) 
  { 
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.FilesText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.FileName.title = this.sysLanguages.CustomerFileName;
    this.settings.columns.FileUrl.title = this.sysLanguages.CustomerFileUrl;
    this.settings.columns.CreatedDate.title = this.sysLanguages.CreatedDateText;

    this.getFiles();
  }

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  settings = {
    columns: {
      FileName: {
        title: '',
      },
      FileUrl: {
        title: '',
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    edit: {
      editButtonContent: '<i class="nb-email"></i>',
    },
    mode:"external",
  };


  ngOnInit() {
  }

  getFiles()
  {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetCustomerFiles", this.customerId).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.fileList = [];
          }
        }
        else 
        {
          this.fileList = [];
          this.fileList = resulttArray;
        }
      }
    )
  }

  uploadFiles(files)
  {
    const formData = new FormData();

    for(let i=0; i<files.length; i++)
    {
      formData.append(files[i].name, files[i]);
    }

    if(files.length > 0)
    {
      this.http.post("http://localhost:61232/api/Customer/UploadFile/" + this.customerId , formData, {reportProgress: true, observe: 'events'})
      .subscribe((result) => {  
        }
        )
    }

    var element = <HTMLInputElement> document.getElementById("_file");
    element.value = "";

    setTimeout(()=>{
      this.getFiles();
      this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrUploadedFile);
    }, 3000);

  }

  sendMail(ref)
  {
    this.loading = true;

    // ---------- mail data customer class'ında göndertiyorum ----------
    let mailData ={
      CustomerName: this.selectedFile.FileName,
      Phone: this.selectedFile.FileUrl,
      CreatedDate: $("#mailWho").val(),
      TaxNumber: $("#mailSubject").val(),
      WebSite: $("#mailMessage").val()
    };

    this._apicallService.Update("http://localhost:61232/api/Customer/SendFileMail", 1, mailData).subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "True")
          {
            this.selectedFile.FileName = "";
            this.selectedFile.FileUrl = "";
            this.loading = false;
            ref.close();
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrMailSent);
          }
        }
      }
    )
  }

  addFile(ref)
  {
    let fileData = { FileName: $("#_fileName").val(), FileUrl: $("#_fileLink").val(), CreatedDate: "", IsVisibleCustomer: 1, CustomerId: this.customerId };
    this.loading = true;

    this._apicallService.Add("http://localhost:61232/api/Customer/AddFileLink", fileData) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getFiles();
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrUploadedFileLink);
        this.loading = false;
        ref.close();
      }
      else if (resultArray[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }          
  }
  )
  }

  openDialog(dialog: TemplateRef<any>, event) {

    if(event != 0)
    {
      this.selectedFile.FileName = event.data.FileName;
      this.selectedFile.FileUrl = event.data.FileUrl;
    }

    this.dialogService.open(dialog);
    
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
