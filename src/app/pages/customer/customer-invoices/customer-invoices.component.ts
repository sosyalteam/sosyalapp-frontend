import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'customer-invoices',
  templateUrl: './customer-invoices.component.html',
  styleUrls: ['./customer-invoices.component.scss'],
  providers: [ApicallService, HttpModule]
})
export class CustomerInvoicesComponent implements OnInit {

  public sysLanguages: any;
  customerId : any;
  invoiceList : any;

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompInvoices + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }


    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.InvoiceNumber.title=this.sysLanguages.InvoiceNumberText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.CreatedDate.title=this.sysLanguages.CreatedDateText;
    this.settings.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;
    this.settings.columns.BoardName.title=this.sysLanguages.BoardNameText;
    this.settings.columns.LastPaymentDate.title=this.sysLanguages.LastDatePaymentText;
    this.settings.columns.StatusDesc.title=this.sysLanguages.AddTaskStatusText;
  }

  settings = {
    columns: {
      InvoiceId: {
        title: 'ID',
        width: '2%'
      },
      InvoiceNumber: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CustomerName: {
        title: ''
      },
      BoardName: {
        title: ''
      },
      LastPaymentDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  ngOnInit() {

    this._apicallService.Get("http://localhost:61232/api/Customer/GetInvoiceByCustomerId",this.customerId).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.invoiceList = [];
          }
        }
        else 
        {
          this.invoiceList = resulttArray;
  
        }
      }
    )
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  editInvoice(event)
  {
    this._router.navigate(['/pages/invoices/add-edit-invoices', event.data.InvoiceId]);
  }

}
