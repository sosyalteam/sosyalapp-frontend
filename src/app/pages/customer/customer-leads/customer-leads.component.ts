import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'customer-leads',
  templateUrl: './customer-leads.component.html',
  styleUrls: ['./customer-leads.component.scss'],
  providers: [ApicallService, HttpModule]
})
export class CustomerLeadsComponent implements OnInit {

  public sysLanguages: any;
  customerId : any;
  LeadList : any;

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompLeads + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.LeadName.title = this.sysLanguages.LeadName;
    this.settings.columns.Name.title = this.sysLanguages.LeadTableNameText;
    this.settings.columns.Phone.title = this.sysLanguages.LeadTablePhoneText;
    this.settings.columns.Fullname.title = this.sysLanguages.LeadTableUserNameText;
    this.settings.columns.StatusDesc.title = this.sysLanguages.LeadTableStatusDescText;
    this.settings.columns.SourceDesc.title = this.sysLanguages.LeadTableSourceDescText;
    this.settings.columns.CreatedDate.title=this.sysLanguages.LeadCreatedDateText;
    this.settings.columns.PossibilityDesc.title=this.sysLanguages.LeadCalledStatus;
    this.settings.actions.columnTitle = this.sysLanguages.GrpActionText;
  }

  settings = {
    columns: {
      LeadId: {
        title: 'ID',
        width: '2%',
      },
      LeadName: {
        title: ''
      },
      Name: {
        title: ''
      },
      Phone: {
        title: '',
      },
      Fullname: {
        title: '',
      },
      StatusDesc: {
        title: '',
        width : '8%'
      },
      SourceDesc: {
        title: '',
        width : '4%'
      },
      CalledStatus: {
        title: 'Bound',
        width : '4%'
      },
      PossibilityDesc: {
        title: 'İhtimal',
        width: '3%'
      },
      Tags : {
        title : 'Tags', 
      },
      CreatedDate: {
        title: '',
        width: '5%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetLeadByCustomerId",this.customerId).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.LeadList = [];
          }
        }
        else 
        {
          this.LeadList = resulttArray;
        }
      }
    )
  }

  editCustomer(customerid)
  {
    this._router.navigate(['/pages/customer/add-edit-customer', customerid]);
  }

  editLead(event)
  {
    this._router.navigate(['/pages/lead/add-edit-lead', event.data.LeadId]);
  }

  addLead()
  {
    sessionStorage.setItem("customerId", this.customerId );
    this._router.navigate(['/pages/lead/add-edit-lead']);
  }
}
