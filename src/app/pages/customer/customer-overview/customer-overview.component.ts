import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbToastrService, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbGlobalPosition, NbDialogService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'customer-overview',
  templateUrl: './customer-overview.component.html',
  styleUrls: ['./customer-overview.component.scss'],
  providers: [ApicallService, HttpModule]
})
export class CustomerOverviewComponent implements OnInit {

  public sysLanguages: any;
  public customerId:any;
  public title:"";
  public customerData = { CustomerName: "", TaxNumber: "", Phone: "", Website: "", StatusDesc: "", Address: "", City: "", PostCode: "" , BrandName : ""};
  public statusId:any;

  contactData = {Fullname: "", Email: "", Phone: "", Position: "",CustomerId:"",Password : ""};
  contactList = [];
  dialogTitle:"";
  ContactID : any;

  emptyComment = false;
  CommentList = [];
  
  //pagination 
  p: number = 1;
  collection = [];

  //task
  taskList = [];

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  status1: NbToastStatus = NbToastStatus.SUCCESS;

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  //

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router,private toastrService: NbToastrService, private titleService: Title,private dialogService: NbDialogService) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompOverview + " - Crmplus");

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.Fullname.title=this.sysLanguages.ContactTableFullnameText;
    this.settings.columns.Position.title=this.sysLanguages.ContactTablePositionText;
    this.settings.columns.Phone.title=this.sysLanguages.ContactTablePhoneText;

    this.settingsForTaskTable.columns.Title.title=this.sysLanguages.TasksTableTitleText;
    this.settingsForTaskTable.columns.TaskDesc.title=this.sysLanguages.TasksTableTascDescText;
    this.settingsForTaskTable.columns.StatusDesc.title=this.sysLanguages.TasksTableStatusText;
    this.settingsForTaskTable.columns.BeginDate.title=this.sysLanguages.TasksTableStartDateText;
    this.settingsForTaskTable.columns.EndDate.title=this.sysLanguages.TasksTableEndDateText;
    this.settingsForTaskTable.columns.Fullname.title=this.sysLanguages.TasksTableAssignText;



    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.getComments();
    this.getTasks();

   }

   settings = {
    columns: {
      ContactId: {
        title: 'ID',
        width:'2%'
      },
      Fullname: {
        title: ''
      },
      Email: {
        title: 'Mail'
      },
      Position: {
        title: ''
      },
      Phone: {
        title: ''
      },
    },
    actions:{
      columnTitle:'',
      position:'right', 
      add:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode: 'external', // bu önemli bu, add butonunda özel fonksiyon yazmamıza yarıyor bkz.(create)
  };

  settingsForTaskTable = {
    columns: {
      TaskId: {
        title: 'ID',
        width: '2%'
      },
      Title: {
        title: ''
      },
      TaskDesc: {
        title: ''
      },
      StatusDesc: {
        title: '',
      },
      BeginDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },       
      },
      EndDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      Fullname: {
        title: '',
      },
    },
    actions:{
      position:'right',
      edit:false,
      delete:false,
      add:false,
    },
    mode: 'external',
  };

   tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  ngOnInit() 
  {
    if(this.customerId>0){
      this._apicallService.Get("http://localhost:61232/api/Customer/GetCustomerById",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              //do smth.
            }
          }
          else
          {
            this.customerData.CustomerName = resultArray[0].CustomerName;
            this.customerData.Phone = resultArray[0].Phone;
            this.customerData.City = resultArray[0].City;
            this.customerData.PostCode = resultArray[0].PostCode;
            this.customerData.Address = resultArray[0].Address;
            this.customerData.Website = resultArray[0].Website;
            this.customerData.StatusDesc = resultArray[0].StatusDesc;
            this.customerData.TaxNumber = resultArray[0].TaxNumber;
            this.title = resultArray[0].CustomerName;
            this.statusId = resultArray[0].StatusId;
            this.customerData.BrandName = resultArray[0].BrandName;
          }
        }
      ),
      this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          {
            this.contactList = resultArray;
          }
        }
      )
    }
  }

  save()
  {
    let _customer = { CustomerId : parseInt(this.customerId),CustomerName: $("#dialogCustomerName").val(), TaxNumber: $("#dialogTaxNumber").val(), Phone: $("#dialogPhone").val(), Website: $("#dialogWebsite").val(), StatusID:this.statusId,  Address: $("#dialogAddress").val(), City: $("#dialogCity").val(), PostCode: $("#dialogPostCode").val(), CustomerDiscount: $("#dialogDiscount").val() , BrandName: $("#brandName").val() };
    if( _customer.CustomerName == "" || _customer.Phone == "" || _customer.City == "" )
    {
       this.makeToast(this.status,this.sysLanguages.ErrorMessage,this.sysLanguages.ErrorMessage1);
    }
    else
    {
      this._apicallService.Update("http://localhost:61232/api/Customer/Update", parseInt(this.customerId), _customer).subscribe((result) => {
      if (result[0].Result == "True") {
        this.makeToast(this.status1,this.sysLanguages.SuccessMessage,this.sysLanguages.SuccessMessage1);
      }
      else if (result[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }
    },
    )
    }
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  getTasks()
  {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetTaskByCustomerId",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.taskList = [];
            }
          }
          else
          {
            this.taskList = resultArray;
          }
        }
      )
  }

  getContactById()
  {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.customerId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          {
            this.contactList = resultArray;
          }
        }
      )
  }

  openDialog(dialog: TemplateRef<any>, val, event) {
    
    if(val == "save")
    {    
      this.dialogTitle=this.sysLanguages.AddContactHeaderText;
      this.contactData.Fullname = "";
      this.contactData.Email = "";
      this.contactData.Phone = "";
      this.contactData.Position = "";
      this.contactData.CustomerId="";

      this.dialogService.open(dialog);
    }
    else if(val == "update")
    {
      this.ContactID=parseInt(`${event.data.ContactId}`);
      this.dialogTitle=this.sysLanguages.EditContactHeaderText;
      this.contactData.Fullname = event.data.Fullname;
      this.contactData.Email = event.data.Email;
      this.contactData.Phone = event.data.Phone;
      this.contactData.Position = event.data.Position;
      this.contactData.CustomerId = this.customerId;

      this.dialogService.open(dialog);
    }
  }

  saveContact(ref)
  {
    let contact;

    if(this.dialogTitle==this.sysLanguages.AddContactHeaderText)
    {
      contact = { ContactId: 0, Fullname:  $("#Fullname").val(), Email : $("#Email").val(), Phone : $("#Phone").val(), Position : $("#Position").val(), CustomerId : this.customerId, Password : $("#Password").val()};

      this._apicallService.Add("http://localhost:61232/api/Customer/CreateContact",contact) 
         .subscribe((result) => {    
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result == "True") {
              ref.close();
              this.getContactById();
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }                    
         },) 
    }
    else if(this.sysLanguages.EditContactHeaderText)
    {
      let editcontact;

      if(this.customerId)
      {
        editcontact = 
        {
          ContactId: this.ContactID,
          Fullname:  $("#Fullname").val(),
          Email : $("#Email").val(),
          Phone : $("#Phone").val(),
          Position : $("#Position").val(),
          CustomerId : this.customerId,
          Password : $("#Password").val(),
        };
        this._apicallService.Update("http://localhost:61232/api/Customer/UpdateContact",this.ContactID,editcontact) 
         .subscribe((result) => {    
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result == "True") {
              ref.close();
              this.getContactById();
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }                    
         }, )
      }
    }
  }

  deleteContact(event)
  {
    var ans = confirm(this.sysLanguages.ContactDeleteText + parseInt(event.data.ContactId));
    if(ans)
    {
      this._apicallService.Deactive("http://localhost:61232/api/Customer/DeleteContact", parseInt(event.data.ContactId)).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getContactById();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }    
     },)
    }    
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Customer/CustomerComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : this.customerId,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  addTask()
  {
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01" || splittedDate[1] == "1")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02" || splittedDate[1] == "2")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03" || splittedDate[1] == "3" )
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04" || splittedDate[1] == "4")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05" || splittedDate[1] == "5" )
    {
        val = "May";
    }
    else if (splittedDate[1] == "06" || splittedDate[1] == "6" )
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07" || splittedDate[1] == "7" )
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08" || splittedDate[1] == "8" )
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09" || splittedDate[1] == "9" )
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
