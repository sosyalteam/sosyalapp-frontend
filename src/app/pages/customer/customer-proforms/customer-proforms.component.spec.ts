import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerProformsComponent } from './customer-proforms.component';

describe('CustomerProformsComponent', () => {
  let component: CustomerProformsComponent;
  let fixture: ComponentFixture<CustomerProformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerProformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerProformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
