import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'customer-proforms',
  templateUrl: './customer-proforms.component.html',
  styleUrls: ['./customer-proforms.component.scss'],
  providers: [ApicallService, HttpModule]
})
export class CustomerProformsComponent implements OnInit {

  public sysLanguages: any;
  customerId : any;
  proformList : any;

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private titleService: Title)
   {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompProforms + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.ProformNumber.title=this.sysLanguages.ProformNumberText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.CreatedDate.title=this.sysLanguages.CreatedDateText;
    this.settings.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;
    this.settings.columns.BoardName.title=this.sysLanguages.BoardNameText;
    this.settings.columns.ExpirementDate.title=this.sysLanguages.ProfcExpiredDate;
    this.settings.columns.StatusDesc.title=this.sysLanguages.AddTaskStatusText;

   }

   settings = {
    columns: {
      ProformId: {
        title: 'ID',
        width: '2%'
      },
      ProformNumber: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CustomerName: {
        title: ''
      },
      BoardName: {
        title: ''
      },
      ExpirementDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  ngOnInit() {

    this._apicallService.Get("http://localhost:61232/api/Customer/GetProformByCustomerId",this.customerId).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.proformList = [];
          }
        }
        else 
        {
          this.proformList = resulttArray;
  
        }
      }
    )
  }


  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  editProform(event)
  {
    this._router.navigate(['/pages/proforms/add-edit-proform', event.data.ProformId]);
  }

}
