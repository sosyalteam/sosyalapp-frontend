import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'customer-project',
  templateUrl: './customer-project.component.html',
  styleUrls: ['./customer-project.component.scss'],
  providers: [ApicallService, HttpModule]
})
export class CustomerProjectComponent implements OnInit {

  public sysLanguages: any;
  customerId : any;
  ProjectList = [];

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompProjects + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;

    this.settings.columns.BoardName.title=this.sysLanguages.BoardTableNameText;
    this.settings.columns.BeginDate.title=this.sysLanguages.BoardTableStartDateText;
    this.settings.columns.EndDate.title=this.sysLanguages.BoardTableEndDateText;
    this.settings.columns.StatusDesc.title=this.sysLanguages.BoardTableStatusText;
    this.settings.columns.CustomerName.title = this.sysLanguages.BoCardCustomerText;
    this.settings.actions.columnTitle = this.sysLanguages.ArActionText;

  }

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  settings = {
    columns: {
      BoardId : {
        title : 'ID',
        width: '2%'
      }, 
      BoardName: {
        title: ''
      },
      CustomerName: {
        title: ''
      },
      BeginDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      EndDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      columnTitle: '',
      position:'right',
      add:false,
      delete : false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
    edit:{
      editButtonContent: '<i class="nb-edit"></i>'
    },

    mode: 'external',
  };

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/Customer/GetProjectByCustomerId",this.customerId).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.ProjectList = [];
          }
        }
        else 
        {
          this.ProjectList = resulttArray;
        }
      }
    )
  }


  editCustomer(customerid)
  {
    this._router.navigate(['/pages/customer/add-edit-customer', customerid]);
  }

  editProject(event)
  {
    this._router.navigate(['/pages/task/board-overview', event.data.BoardId]);
  }

}
