import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerProposalsComponent } from './customer-proposals.component';

describe('CustomerProposalsComponent', () => {
  let component: CustomerProposalsComponent;
  let fixture: ComponentFixture<CustomerProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
