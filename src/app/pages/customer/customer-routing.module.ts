import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { ContactComponent } from './contact/contact.component';
import { CustomerOverviewComponent } from './customer-overview/customer-overview.component';
import { SafeComponent } from './safe/safe.component';
import { AddEditCustomerComponent } from './add-edit-customer/add-edit-customer.component';
import { CustomerInvoicesComponent } from './customer-invoices/customer-invoices.component';
import { CustomerProposalsComponent } from './customer-proposals/customer-proposals.component';
import { CustomerProformsComponent } from './customer-proforms/customer-proforms.component';
import { CustomerLeadsComponent } from './customer-leads/customer-leads.component';
import { CustomerFilesComponent } from './customer-files/customer-files.component';
import { CustomerProjectComponent } from './customer-project/customer-project.component';
import { CustomerNotesComponent } from './customer-notes/customer-notes.component';
import { CustomerCostsComponent } from './customer-costs/customer-costs.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerComponent,
  },
  {
    path: 'add-edit-customer',
    component: AddEditCustomerComponent,
  },
  {
    path: 'add-edit-customer/:id',
    component: AddEditCustomerComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'contact/:id',
    component: ContactComponent,
  },
  {
    path: 'customer-overview/:id',
    component: CustomerOverviewComponent,
  },
  {
    path: 'safe/:id',
    component: SafeComponent,
  },
  {
    path: 'customer-invoices/:id',
    component: CustomerInvoicesComponent,
  },
  {
    path: 'customer-proposals/:id',
    component: CustomerProposalsComponent,
  },
  {
    path: 'customer-proforms/:id',
    component: CustomerProformsComponent,
  },
  {
    path: 'customer-leads/:id',
    component: CustomerLeadsComponent,
  },
  {
    path: 'customer-files/:id',
    component: CustomerFilesComponent,
  },
  {
    path: 'customer-project/:id',
    component: CustomerProjectComponent,
  },
  {
    path: 'customer-notes/:id',
    component: CustomerNotesComponent,
  },
  {
    path: 'customer-costs/:id',
    component: CustomerCostsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
