import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from '././customer/customer.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbDialogModule, NbRouteTabsetModule, NbPopoverModule, NbListModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactComponent } from './contact/contact.component';
import { CustomerOverviewComponent } from './customer-overview/customer-overview.component';
import { DropdownModule } from 'primeng/dropdown';
import { SafeComponent } from './safe/safe.component';
import { AddEditCustomerComponent } from './add-edit-customer/add-edit-customer.component';
import { CustomerInvoicesComponent } from './customer-invoices/customer-invoices.component';
import { CustomerProposalsComponent } from './customer-proposals/customer-proposals.component';
import { CustomerProformsComponent } from './customer-proforms/customer-proforms.component';
import { CustomerLeadsComponent } from './customer-leads/customer-leads.component';
import { CustomerFilesComponent } from './customer-files/customer-files.component';
import { CustomerProjectComponent } from './customer-project/customer-project.component';
import { CustomerNotesComponent } from './customer-notes/customer-notes.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CustomerCostsComponent } from './customer-costs/customer-costs.component';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  declarations: [CustomerComponent, ContactComponent, CustomerOverviewComponent, SafeComponent, AddEditCustomerComponent, CustomerInvoicesComponent, CustomerProposalsComponent, CustomerProformsComponent, CustomerLeadsComponent, CustomerFilesComponent, CustomerProjectComponent, CustomerNotesComponent, CustomerCostsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomerRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbDialogModule, 
    NbRouteTabsetModule,
    DropdownModule,
    NgxPaginationModule,
    MultiSelectModule,
    NbPopoverModule,
    NbListModule,
  ]
})
export class CustomerModule { }
