import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import $ from 'jquery';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';
import {ExcelService} from "../../excel-service/excel.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';


@Component({
  selector: 'customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [ApicallService, HttpModule],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class CustomerComponent implements OnInit {
 
  public customerList: any;
  public _customerList: any;

  filter = "false";
  _filteredCustomerList: any;
  sysLanguages: any;
  isChecked;

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _apicallService: ApicallService, private _router: Router, private dialogService: NbDialogService,private toastrService: NbToastrService, private titleService: Title, private excelService:ExcelService) 
  {
    
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompCstomer + " - Crmplus");
   
    this.getCustomers();

    this.settings.columns.CustomerName.title = this.sysLanguages.CustomerTableColumnCustomerName;
    this.settings.columns.Fullname.title = this.sysLanguages.CustomerTableColumnContactPerson;
    this.settings.columns.Email.title = this.sysLanguages.CustomerTableColumnContactEmail;
    this.settings.columns.Phone.title = this.sysLanguages.CustomerTableColumnPhone;
    this.settings.columns.StatusDesc.title = this.sysLanguages.CustomerTableColumnStatusDesc;
    this.settings.columns.CreatedDate.title = this.sysLanguages.CustomerTableColumnCreatedDate;
    this.settings.actions.columnTitle = this.sysLanguages.UserTableActionText;

    this.title1=this.sysLanguages.ErrorMessage;
    this.content=this.sysLanguages.ErrorMessage1;
  }

  settings = {
    columns: {
      CustomerId: {
        title: 'ID'
      },
      CustomerName: {
        title: ''
      },
      Fullname: {
        title: ''
      },
      Email: {
        title: ''
      },
      Phone: {
        title: ''
      },
      StatusDesc: {
        title: ''
      },
      CreatedDate: {
        title: ''
      },
    },
    actions:{
      columnTitle:'',
      position:'right',
      add:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode: 'external', // bu önemli bu, add butonunda özel fonksiyon yazmamıza yarıyor bkz.(create)
  };

  ngOnInit() {
  }

  getCustomers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerList = [];
          }
        }
        else
        {
          for(var i in resultArray)
          {
           resultArray[i].CreatedDate =  resultArray[i].CreatedDate.split(" ")[0];
          }
          this.customerList = resultArray.filter(x=>x.StatusId == 1);
          this._customerList = resultArray;
        }
      }
    )
  }

  deleteCustomer(event)
  {
     this._apicallService.Deactive("http://localhost:61232/api/Customer/Delete/", parseInt(event.data.CustomerId)).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.CustomerToastrDeleted);
        this.getCustomers();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
     
     }, error => console.error(error))
  }

  activeCustomer(event)
  {
    this.filter="true";
    if(event=="true")
    {
      this._filteredCustomerList=this._customerList;
    }
    else if(event=="false")
    {
      this.filter="false";
    }
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  openExportArea()
  {
    document.getElementById("myExportDropdown").classList.toggle("show");

    setTimeout(()=>{
      var myDropdown = document.getElementById("myExportDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
    },2500);
  }

  onClick(event) {
    var classname = event.target.className;
    if(classname == "btn" || classname == "filterStatus" )
    {
      //menu kapanmasın dıye
    }
    else
    {
      //menu kapatma
      var myDropdown = document.getElementById("myExportDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
    }
      
  }

  exportExcel()
  {
    this.excelService.exportAsExcelFile(this._customerList, this.sysLanguages.CustomerCompCstomer);
  }

  exportPdf()
  {
    var doc = new jsPDF("l","mm","a4");
    var col = ["CustomerName", "Fullname","Phone","TaxNumber","City","Status","Address"];
    var rows = [];
    var itemNew = this._customerList;

   itemNew.forEach(element => {      
        var temp = [element.CustomerName.toUpperCase(), element.Fullname, element.Phone, element.TaxNumber, element.City.toUpperCase(), element.StatusDesc.toUpperCase(), element.Address ];
        rows.push(temp);
    });        

    doc.autoTable(col, rows,{
        columnStyles: {
        0: {columnWidth: 45},
        1: {columnWidth: 45},
        2: {columnWidth: 30},
        3: {columnWidth: 30},
        4: {columnWidth: 22},
        5: {columnWidth: 22},
        6: {columnWidth: 81}
        },
        theme : "grid"
      }
    );
    doc.save(this.sysLanguages.CustomerCompCstomer + ".pdf");
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
}
