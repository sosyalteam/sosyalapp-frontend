import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import $ from 'jquery';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'safe',
  templateUrl: './safe.component.html',
  styleUrls: ['./safe.component.scss']
})
export class SafeComponent implements OnInit {

  sysLanguages: any;
  customerId:any;
  SafeDesc : any;
  accessrightSee = 0;
  accessrightWrite = 0;
  OpenArea : any;
  header = "";
  SafeId : number;

  constructor(private _avRoute: ActivatedRoute,private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.CustomerCompSafe + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.customerId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.ContactTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.LeadText;
    this.tabs[2].title = this.sysLanguages.InvoiceText;
    this.tabs[3].title = this.sysLanguages.ProposalText;
    this.tabs[4].title = this.sysLanguages.ProformText;
    this.tabs[5].title = this.sysLanguages.ProjectText;
    this.tabs[6].title = this.sysLanguages.Costs;
    this.tabs[7].title = this.sysLanguages.FilesText;
    this.tabs[8].title = this.sysLanguages.SafeText;
    
    this.header = this.sysLanguages.NewSafe;

  }

  tabs: any[] = [
    {
      title: ' ',
      icon: 'nb-grid-b-outline',
      route: ['/pages/customer/customer-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-leads', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-invoices', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proposals', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-proforms', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-project', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-costs', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/customer-files', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/customer/safe', this._avRoute.snapshot.params["id"]],
    },
    
  ];

  ngOnInit() {
    //buraya yetki kontrolu yapılacak..
    this._apicallService.Get("http://localhost:61232/api/Customer/GetSafeById",this.customerId).subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.SafeDesc = "";
          }
        }
        else
        { 
          this.SafeId = resultArray[0].Id;
          this.SafeDesc = resultArray[0].SafeDesc;
          if(this.SafeDesc!="")
          {
            this.header = this.sysLanguages.EditSafe;
          }
        }
      }
    )
    if(this.accessrightSee==1 && this.accessrightWrite==0)
    {

    }
    else if (this.accessrightWrite == 1)
    {
      this.OpenArea ="true";
    }
    else 
    {
      this.OpenArea = "false";
    }
    
  }

  save()
  {
    if(this.header == this.sysLanguages.NewSafe)
    {
      let safe = {Id : 0, SafeDesc: $("#desc").val() , CustomerId : parseInt(this.customerId)};
      this._apicallService.Add("http://localhost:61232/api/Customer/CreateSafe",safe) 
           .subscribe((result) => {    
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray[0].Result == "True") {
                this._router.navigate(['/pages/customer/customer-overview',this.customerId]);
              }
              else if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }                    
           },)

    }
    else if (this.header== this.sysLanguages.EditSafe)
    {
      let safe = {Id : this.SafeId, SafeDesc: $("#desc").val() , CustomerId : parseInt(this.customerId)};
      this._apicallService.Update("http://localhost:61232/api/Customer/UpdateSafe", this.SafeId, safe).subscribe((result) => {
        if (result[0].Result == "True") {
          this._router.navigate(['/pages/customer/customer-overview']);
        }
        else if (result[0].Result == "Session_Expired"){
          this._router.navigate(['/auth']);
          return;
        }
      },
      )
    }

  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

}
