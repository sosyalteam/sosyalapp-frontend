import { Component, TemplateRef, AfterViewInit, OnDestroy, OnInit, HostListener } from '@angular/core';
import { NbDialogService, NbThemeService, NbColorHelper, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/observable/interval';
import { ApicallService } from '../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { Title } from '@angular/platform-browser';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ApicallService,HttpModule]
})
export class DashboardComponent implements OnInit  {

  public sysLanguages: any;
  public leadList : any;
  public projectList : any;
  
  notCompletedTaskListCount = 0;
  completedTaskCount = 0;
  _notCompletedTaskListCount = 0;
  _completedTaskCount = 0;
  notcompletedTaskList = [];
  completedTaskList = [] ;
  copynotCompletedTaskList = [];
  copyCompletedTaskList = [];

  LeadList = [];
  ProjectList = [];

  teamTask = false;
  isShowingNotDefaultTeamTask = false;
  teamsTaskDefaultList = [];
  teamsTaskNotDefaultList = [];
  teamTasksList = [];

  pageNumfornotCompleted = 1;
  pageCountfornotCompleted = 8;

  pageNumforCompleted = 1;
  pageCountforCompleted = 8;

  //Haftalık Ödeme grafiği
  data: any;
  options: any;
  themeSubscription: any;

  //Fırsatlar grafiği
  options1: any = {};
  themeSubscription1: any;

  //Proje durumları grafiği
  options2:any;
  themeSubscription2: any;


  
  constructor(private _fb: FormBuilder,private dialogService: NbDialogService,private theme: NbThemeService, private _apiCallservice: ApicallService,private _router: Router, private titleService: Title, private toastrService: NbToastrService) {

    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.HomeText + " - Crmplus");
    this.leadList = JSON.parse(sessionStorage.getItem("leadList"));
    this.projectList = JSON.parse(sessionStorage.getItem("projectList"));
    this.getMyTasks();
    this.getTeamTasks();

    this.settingsTeamTask.columns.Title.title=this.sysLanguages.TasksTableTascDescText;
    this.settingsTeamTask.columns.BeginDate.title=this.sysLanguages.BoardTableStartDateText;
    this.settingsTeamTask.columns.EndDate.title=this.sysLanguages.BoardTableEndDateText;
    this.settingsTeamTask.columns.StatusDesc.title=this.sysLanguages.TasksTableStatusText;

    this.settingsTeamTaskNotDefault.columns.Title.title=this.sysLanguages.TasksTableTascDescText;
    this.settingsTeamTaskNotDefault.columns.BeginDate.title=this.sysLanguages.BoardTableStartDateText;
    this.settingsTeamTaskNotDefault.columns.EndDate.title=this.sysLanguages.BoardTableEndDateText;
    this.settingsTeamTaskNotDefault.columns.StatusDesc.title=this.sysLanguages.TasksTableStatusText;
    this.settingsTeamTaskNotDefault.columns.Fullname.title=this.sysLanguages.LeadTableUserNameText;

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: ['', '', '', '', ''],
        datasets: [{
          data: [10, 10, 10, 10, 10],
          label: '',
          backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
        }, 
        {
          data: [10, 10, 10, 10, 10],
          label: '',
          backgroundColor: NbColorHelper.hexToRgbA(colors.infoLight, 0.8),
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });

    this.themeSubscription1 = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;
  
      this.options1 = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
        tooltip: {
          trigger: 'item',
          formatter: '{b} : {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['', '', '' , '', '','',''],
          textStyle: {
            color: echarts.textColor,
          },
        },
        series: 
          {
            name: '',
            type: 'pie',
            radius: '60%',
            center: ['50%', '50%'],
            data:  [
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' },            
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' }
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
      };
    });  
    

    this.themeSubscription2 = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;

      this.options2 = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['', ' ', '', ' ', ''],
          textStyle: {
            color: echarts.textColor,
          },
        },
        series:
          {
            name: '',
            type: 'pie',
            radius: '60%',
            center: ['50%', '50%'],
            data: [
              { value: 0, name: '' },
              { value: 0, name: '' },
              { value: 0, name: '' },
              { value: 0, name: '' },            
              { value: 0, name: '' },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
      };
    });

  }

  settingsTeamTask = {
    pager: {
      display: true,
      perPage: 5
    },
    columns: {
      TaskId: {
        title: 'ID',
        width: '2%'
      },
      Title:
      {
        title:''
      },
      StatusDesc: {
        title: '',
        width: '12%',
      },
      BeginDate: {
        title: '',
        width: '11%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      EndDate: {
        title: '',
        width: '11%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },

    },
    actions:{
      position:'right',
      columnTitle:"",
      add:false,
      edit:false,
      delete:false
    },
    mode:"external",
  };

  settingsTeamTaskNotDefault = {
    pager: {
      display: true,
      perPage: 5
    },
    columns: {
      TaskId: {
        title: 'ID',
        width: '2%'
      },
      Title:
      {
        title:''
      },
      Fullname: 
      {
        title:'',
        width: '18%'
      },
      StatusDesc: {
        title: '',
        width: '12%'
      },
      BeginDate: {
        title: '',
        width: '11%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      EndDate: {
        title: '',
        width: '11%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },

    },
    actions:{
      position:'right',
      columnTitle:"",
      add:false,
      edit:false,
      delete:false
    },
    mode:"external",
  };


  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  ngOnInit() {
    
    for(var i in this.leadList)
    {
      if(this.leadList[i].StatusId == 1)
      {
        this.options1.series.data[0].value++;
      }
      else if(this.leadList[i].StatusId == 2)
      {
        this.options1.series.data[1].value++;
      }
      else if(this.leadList[i].StatusId == 3)
      {
        this.options1.series.data[2].value++;
      }
      else if(this.leadList[i].StatusId == 4)
      {
        this.options1.series.data[3].value++;
      }
      else if(this.leadList[i].StatusId == 5)
      {
        this.options1.series.data[4].value++;
      }
      else if(this.leadList[i].StatusId == 6)
      {
        this.options1.series.data[5].value++;
      }
      else if(this.leadList[i].StatusId == 7)
      {
        this.options1.series.data[6].value++;
      }
    }

    for(var i in this.projectList )
    {
      if(this.projectList[i].StatusId == 0)
      {
        this.options2.series.data[0].value++;
      }
      else if(this.projectList[i].StatusId == 1)
      {
        this.options2.series.data[1].value++;
      }
      else if(this.projectList[i].StatusId == 2)
      {
        this.options2.series.data[2].value++;
      }
      else if(this.projectList[i].StatusId == 3)
      {
        this.options2.series.data[3].value++;
      }
      else if(this.projectList[i].StatusId == 4)
      {
        this.options2.series.data[4].value++;
      }
    }

    this.data.labels[0]=this.sysLanguages.Monday;
    this.data.labels[1]=this.sysLanguages.Tuesday;
    this.data.labels[2]=this.sysLanguages.Wednesday;
    this.data.labels[3]=this.sysLanguages.Thursday;
    this.data.labels[4]=this.sysLanguages.Friday;
    this.data.datasets[0].label=this.sysLanguages.DashBoardThisWeekPayments;
    this.data.datasets[1].label=this.sysLanguages.DashBoardLastWeekPayments;

    this.options2.legend.data[0]=this.sysLanguages.DashBoardProjectChartNotStart;
    this.options2.series.data[0].name=this.sysLanguages.DashBoardProjectChartNotStart;
    this.options2.legend.data[1]=this.sysLanguages.DashBoardProjectChartContinues;
    this.options2.series.data[1].name=this.sysLanguages.DashBoardProjectChartContinues;
    this.options2.legend.data[2]=this.sysLanguages.DashBoardProjectChartStay;
    this.options2.series.data[2].name=this.sysLanguages.DashBoardProjectChartStay;
    this.options2.legend.data[3]=this.sysLanguages.DashBoardProjectChartCanceled;
    this.options2.series.data[3].name=this.sysLanguages.DashBoardProjectChartCanceled;
    this.options2.legend.data[4]=this.sysLanguages.DashBoardProjectChartCompleted;
    this.options2.series.data[4].name=this.sysLanguages.DashBoardProjectChartCompleted;
    this.options2.series.name=this.sysLanguages.DashBoardProjectChart;

    
    this.options1.legend.data[0]=this.sysLanguages.DashboardLeadChartPossibility;
    this.options1.series.data[0].name=this.sysLanguages.DashboardLeadChartPossibility;
    this.options1.legend.data[1]=this.sysLanguages.DashboardLeadChartCandidate;
    this.options1.series.data[1].name=this.sysLanguages.DashboardLeadChartCandidate;
    this.options1.legend.data[2]=this.sysLanguages.DashboardLeadChartStatusStay;
    this.options1.series.data[2].name=this.sysLanguages.DashboardLeadChartStatusStay;
    this.options1.legend.data[3]=this.sysLanguages.DashBoardLeadChartHotStatus;
    this.options1.series.data[3].name=this.sysLanguages.DashBoardLeadChartHotStatus;
    this.options1.legend.data[4]=this.sysLanguages.DashboardLeadChartBargain;
    this.options1.series.data[4].name=this.sysLanguages.DashboardLeadChartBargain;
    this.options1.legend.data[5]=this.sysLanguages.DashBoardLeadChartCustomerStatus;
    this.options1.series.data[5].name=this.sysLanguages.DashBoardLeadChartCustomerStatus;
    this.options1.legend.data[6]=this.sysLanguages.DashBoardLeadChartLost;
    this.options1.series.data[6].name=this.sysLanguages.DashBoardLeadChartLost;
    this.options1.series.name=this.sysLanguages.DashBoardLeadStatus;
    
  }

  addTask()
  {
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

  @HostListener('window:scroll', ['$event']) 
  lazyLoadingforNotCompleted(event)
  {
    if ($("#notCompletedTaskList").scrollTop() >= (document.getElementById("notCompletedTaskList").scrollHeight - document.getElementById("notCompletedTaskList").offsetHeight)) 
    {
      this.pageNumfornotCompleted++;
      this.getNotCompletedTasksforLazyLoading();
    }
  }

  @HostListener('window:scroll', ['$event']) 
  lazyLoadingforCompleted(event)
  {
    if ($("#completedTaskList").scrollTop() >= (document.getElementById("completedTaskList").scrollHeight - document.getElementById("completedTaskList").offsetHeight)) 
    {
      this.pageNumforCompleted++;
      this.getCompletedTasksforLazyLoading();
    }
  }

  getMyTasks()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskByUser") 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.notcompletedTaskList = [];
          this.notCompletedTaskListCount = 0;
          this.completedTaskList = [];
          this.completedTaskCount = 0;
          this.copyCompletedTaskList = [];
          this.copynotCompletedTaskList = [];
        }
      }
      else
      {
        this.notcompletedTaskList= [];
        this.completedTaskList=[];
        this.copyCompletedTaskList = [];
        this.copynotCompletedTaskList = [];
        this.completedTaskCount = 0;
        this.notCompletedTaskListCount = 0;
        this._completedTaskCount = 0;
        this._notCompletedTaskListCount = 0;

        for(var i in resultArray)
        {
          if(resultArray[parseInt(i)].StatusId != 5 && resultArray[parseInt(i)].StatusId != 6)
          {
            if(this.notCompletedTaskListCount < this.pageCountfornotCompleted * this.pageNumfornotCompleted)
            {
              this.notcompletedTaskList.push(resultArray[parseInt(i)]);
              this.notCompletedTaskListCount++;
            }

            this.copynotCompletedTaskList.push(resultArray[parseInt(i)]);
          }
          else if (resultArray[parseInt(i)].StatusId == 5)
          {
            if(this.completedTaskCount < this.pageCountforCompleted * this.pageNumforCompleted)
            {
              this.completedTaskList.push(resultArray[parseInt(i)]);
              this.completedTaskCount++;
            }

            this.copyCompletedTaskList.push(resultArray[parseInt(i)]);
          }
        }
        this._notCompletedTaskListCount = resultArray.filter(x=>x.StatusId != 5 && x.StatusId != 6).length;
        this._completedTaskCount = resultArray.filter(x=>x.StatusId == 5).length;
      }     
    }, 
    )
  }

  getTeamTasks()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/User/GetTeamsTask").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.teamsTaskDefaultList = [];
              this.teamsTaskNotDefaultList = [];
            }
          }
          else
          { 
            this.teamTasksList = resultArray.slice();
            this.teamsTaskDefaultList = resultArray.filter(x => x.ResponsibleId == 0).slice();
            this.teamsTaskNotDefaultList = resultArray.filter(x => x.ResponsibleId != 0).slice();
          }
      }
    )
  }

  getNotCompletedTasksforLazyLoading()
  {
    this.notcompletedTaskList = [];
    this.notCompletedTaskListCount = 0;

    for(let i in this.copynotCompletedTaskList)
    {
      if(this.notCompletedTaskListCount == this.pageCountfornotCompleted * this.pageNumfornotCompleted)
      {
        break;
      }
      else
      {
        this.notcompletedTaskList.push(this.copynotCompletedTaskList[parseInt(i)]);
        this.notCompletedTaskListCount++;
      }
    }
  }

  getCompletedTasksforLazyLoading()
  {
    this.completedTaskList = [];
    this.completedTaskCount = 0;

    for(let i in this.copyCompletedTaskList)
    {
      if(this.completedTaskCount == this.pageCountforCompleted * this.pageNumforCompleted)
      {
        break;
      }
      else
      {
        this.completedTaskList.push(this.copyCompletedTaskList[parseInt(i)]);
        this.completedTaskCount++;
      }
    }
  }

  completeTask(id)
  {
    let task={ TaskId : id, TaskComplete : "0.5", StatusId : 5 };
    this._apiCallservice.Update("http://localhost:61232/api/Task/TaskComplete", id, task) 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.getMyTasks();
          }
          else if (resultArray[0].Result == "False") {
            this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.TasksToastrCantCompleted);
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )
  }

  filterLead(event)
  {
    var leadStatus = event.currentTarget.innerText.split(":")[0].slice(0,-1);
    if(leadStatus == this.sysLanguages.DashboardLeadChartPossibility )
    {
      sessionStorage.setItem("leadStatus","1");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashboardLeadChartCandidate)
    {
      sessionStorage.setItem("leadStatus","2");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashboardLeadChartStatusStay)
    {
      sessionStorage.setItem("leadStatus","3");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashBoardLeadChartHotStatus)
    {
      sessionStorage.setItem("leadStatus","4");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashboardLeadChartBargain)
    {
      sessionStorage.setItem("leadStatus","5");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashBoardLeadChartCustomerStatus)
    {
      sessionStorage.setItem("leadStatus","6");
      this._router.navigate(['/pages/lead/get-lead']);
    }
    else if(leadStatus == this.sysLanguages.DashBoardLeadChartLost)
    {
      sessionStorage.setItem("leadStatus","7");
      this._router.navigate(['/pages/lead/get-lead']);
    }

  }

  myTaskandTeamTask()
  {
    if(this.teamTask == false)
    {
      this.teamTask = true;
    }
    else if(this.teamTask == true)
    {
      this.teamTask = false;
    }
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
}
