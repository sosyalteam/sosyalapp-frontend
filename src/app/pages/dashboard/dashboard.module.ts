import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ChartModule } from 'angular2-chartjs';


import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { NbButtonModule, NbCardModule, NbDialogModule, NbListModule, NbDatepickerModule,NbBadgeModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
  imports: [
    ThemeModule,
    NbButtonModule,
    NbCardModule,
    NbDialogModule.forRoot(),
    NgxEchartsModule, 
    ChartModule,
    NbListModule,
    NbDatepickerModule,
    NbDatepickerModule.forRoot(),
    DropdownModule,
    MultiSelectModule,
    NbListModule,
    Ng2SmartTableModule,
    NbBadgeModule
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule {
 }
