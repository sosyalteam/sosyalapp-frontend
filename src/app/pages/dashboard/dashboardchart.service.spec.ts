import { TestBed } from '@angular/core/testing';

import { DashboardchartService } from './dashboardchart.service';

describe('DashboardchartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardchartService = TestBed.get(DashboardchartService);
    expect(service).toBeTruthy();
  });
});
