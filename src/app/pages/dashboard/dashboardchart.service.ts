
import { Injectable, NgModule } from '@angular/core';
import { Router, Resolve } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

@NgModule({
  providers: [HttpClient],
})

export class DashboardchartService implements Resolve<any> {

  public leadList: any;


  resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): Observable<any> {
    if(localStorage.length == 0)
    {
      //
    }
    else
    {
      const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
      return this._http.get("http://localhost:61232/api/Dashboard/GetLead", { headers: headers }).pipe(
        map( (dataFromApi) => {
          let resultArray = JSON.parse(JSON.stringify(dataFromApi));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              //this._router.navigate(['/auth']);
              return;
            }
            else if (resultArray[0].Result == "Empty") {
            }

          }
          else
          {
              this.leadList = dataFromApi;
              sessionStorage.setItem("leadList", JSON.stringify(this.leadList));
  
              return this.leadList;
          }
        } ),
      );
    } 
    }

    constructor(private _http: HttpClient) { 
    
    }
  }
