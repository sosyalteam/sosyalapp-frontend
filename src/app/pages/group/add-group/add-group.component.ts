import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class AddGroupComponent implements OnInit {

    groupID: number; 
    errorMessage: any;
    public grouprightList: any;
    public rightList: any;
    public sysLanguages: any;

  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute, private _apicallservice: ApicallService, private _router: Router, private titleService: Title) { 

    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.GrpGroups + " - Crmplus");
       
    if (this._avRoute.snapshot.params["id"]) {
        this.groupID = this._avRoute.snapshot.params["id"];
     }
  
    this.getRights();
    this.getGroupRights();
  }

  ngOnInit() {
  }

  getRights()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Right/Get").subscribe(
      (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.rightList = [];
          }
        }
        else
        {
          this.rightList = result;
        }
      },
    )
  }

  getGroupRights()
  {
    this._apicallservice.Get("http://localhost:61232/api/Group/GroupRight", this.groupID).subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.grouprightList = [];
          }
        }
        else
        {
          this.grouprightList = result;
        }
      },
    )
  }

  //--- mantık şu şekilde işliyor classa göre ara o classların içideki checkboxları tektek döndür eğer checked özellikleri true ise işlem yap
  addRightToGroup()
  {
      let checkboxes = <HTMLCollection> document.getElementsByClassName('checkboxRight');
      for(var i = 0; i < checkboxes.length; i++)
      {
        var element = <HTMLInputElement> document.getElementById(checkboxes[i].id);
        if(element.checked == true)
        {
            var flag = false;
            for(let i=0; i<this.grouprightList.length; i++)
            {
                if(this.grouprightList[i].GroupId == this.groupID && this.grouprightList[i].AccessRightId == element.value)
                {
                    flag= true;
                    break;
                }
            }
            if(flag == true)
            {
                window.alert(this.sysLanguages.AddAccessRightToGroupListAlertText);
            }
            else
            {
                let groupRight = { GroupID: Number(this.groupID), AccessRightID: parseInt(element.value), GrantDate: "noMatter" };
                this._apicallservice.Add("http://localhost:61232/api/Group/AddGroupRight", groupRight) 
                  .subscribe((result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray[0].Result == "True") {
                      this.getRights();
                      this.getGroupRights();
                      this._router.navigate(['/pages/group/add-group/' + this.groupID]);
                    }
                    else if (resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }          
                },//error için birşey gelebilir
                )
            }
        }

      }
      
  }

  deleteRightGroup()
  {
    let checkboxes = <HTMLCollection> document.getElementsByClassName('checkboxGroupRight');
    for(var i = 0; i < checkboxes.length; i++)
    {
      var element = <HTMLInputElement> document.getElementById(checkboxes[i].id);
      if(element.checked == true)
      {
        let groupRight = { GroupID: Number(this.groupID), AccessRightID: element.value };
        this._apicallservice.Add("http://localhost:61232/api/Group/DeleteGroupRight",groupRight) 
        .subscribe((data) => {
        this.getGroupRights();
        this._router.navigate(['/pages/group/add-group/' + this.groupID]);
        },//error için birşey gelebilir
        )
      }
    }
  }


}