import { Component, OnInit } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'get-group',
  templateUrl: './get-group.component.html',
  providers:[ApicallService,HttpModule],
  styleUrls: ['./get-group.component.scss']
})
export class GetGroupComponent implements OnInit {

  public grpList: any; 
  public sysLanguages: any;
  
    constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) {  
      
      this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
      this.titleService.setTitle(this.sysLanguages.GrpGroups + " - Crmplus");

      this.settings.columns.GroupDesc.title = this.sysLanguages.GrpNameText;
      this.settings.actions.columnTitle = this.sysLanguages.GrpActionText;

      this.getGroups();  
    }  


    settings = {
      columns: {
        GroupId: {
          title: 'ID',
          width:'5%',
        },
        GroupDesc: {
          title: ''
        },
      },
      actions:{
        position:'right',
        columnTitle: ""
      },
      add: {
        confirmCreate: true,
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
    };
  
  ngOnInit() {
  }
  

  getGroups() {
    this._apicallservice.GetAll("http://localhost:61232/api/Group/Get").subscribe(
      (result) =>
      {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.grpList = [];
          }
        }
        else {
          this.grpList = result;
        }
      }
        )  
    }  

    onCustomAction(event){
      var id = `${event.data.GroupId}`;
      this._router.navigate(['/pages/group/add-group/',id]);
  
    }
  
    onSaveConfirm(event){
      var id = `${event.data.GroupId}`;
      var name=`${event.newData.GroupDesc}`;
      let groupArray = {  GroupId: parseInt(id), GroupDesc:name };
             this._apicallservice.Update("http://localhost:61232/api/Group/Update", parseInt(id), groupArray)
               .subscribe((result) => {
                 if(result[0].Result == "True")
                 {
                   this.getGroups();
                   this._router.navigate(['/pages/group/get-group']);
                 }
                 else if(result[0].Result == "Session_Expired")
                 {
                   this._router.navigate(['/auth']);
                   return;
                 }
               },)
  
    }
  
    onDeleteConfirm(event) {
      var id = `${event.data.GroupId}`;
      var ans = confirm(this.sysLanguages.GrpDeleteText + parseInt(id));
     if (ans) {
       this._apicallservice.Deactive("http://localhost:61232/api/Group/Delete/", parseInt(id)).subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getGroups();
          this._router.navigate(['/pages/group/get-group']);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
       
       }, error => console.error(error))
     }
      
    }
    
    onCreateConfirm(event){
      var name=`${event.newData.GroupDesc}`;
      let groupArray = {  GroupId: 0, GroupDesc:name };
      this._apicallservice.Add("http://localhost:61232/api/Group/Create",groupArray) 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.getGroups();
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )

    }




}