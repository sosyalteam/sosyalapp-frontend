import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetGroupComponent } from './get-group/get-group.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { GetLogComponent } from '../log/get-log/get-log.component';

const routes: Routes = [
  {
    path: 'get-group',
    component: GetGroupComponent,
  },
  {
    path: 'add-group',
    component: AddGroupComponent,
  },
  {
    path: 'add-group/:id',
    component: AddGroupComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
