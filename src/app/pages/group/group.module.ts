import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { GroupRoutingModule } from './group-routing.module';
import { GetGroupComponent } from './get-group/get-group.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbListModule, NbCardModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
  declarations: [GetGroupComponent, AddGroupComponent],
  imports: [
    CommonModule,
    GroupRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NbListModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbCardModule
  ]
})
export class GroupModule {

 }
