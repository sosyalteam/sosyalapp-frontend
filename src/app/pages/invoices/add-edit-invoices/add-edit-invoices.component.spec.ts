import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditInvoicesComponent } from './add-edit-invoices.component';

describe('AddEditInvoicesComponent', () => {
  let component: AddEditInvoicesComponent;
  let fixture: ComponentFixture<AddEditInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
