import { Component, OnInit, ElementRef } from '@angular/core';
import { NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { elementStart } from '@angular/core/src/render3';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-invoices',
  templateUrl: './add-edit-invoices.component.html',
  styleUrls: ['./add-edit-invoices.component.scss']
})
export class AddEditInvoicesComponent implements OnInit {

  sysLanguages : any;
  Invoiceid : any;
  InvoiceData = { CustomerId: 0, ProjectId : 0, InvoiceNumber : '', CreatedDate : '',LastPaymentDate : '', PayingMethodId : 0, CurrencyTypeId : 0, SalesmanId : 0, DiscountId : 0,  AdminNote : '', CustomerNote : '', Conditions : '', StatusId: 0, Language: 0 , TotalDiscount : "0" };
  LineData = [{ Id: '', ProductName: '', LineDesc: '', ProductCount: 1, ProductPrice: 0, LineAmount: "0", updatedefaultTax: "" , LineDiscount : '0'}];
  addedDynamicLineData = [];
  customerList: any;
  taxList : any;
  userList : any;
  methodList : any;
  currencyList : any;
  discountList : any;
  productList : any;
  statusList: any;
  projectList:any;
  title:"";
  tax: any;
  TaxDesc:string;
  customerData = { TaxNumber: "", CustomerDiscount: "" };
  selectedProduct : any;
  CustomerDiscount = "";
  contactList = [];
  customerContact = [];
  selectedCustomerContacts = [];
  copyselectedCustomerContacts = [];
  languageList = [];
  sendMail = false;

  _total = 0;
  topTax = 0;
  topTaxArray = [];
  _totalDiscount = 0;
  loading = false;

  currency = "TL";

   //toastr
  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  statusDiscount : NbToastStatus = NbToastStatus.WARNING;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToastDiscount()
  {
    this.showToast(this.statusDiscount,"Müşteriye ait %"+ this.CustomerDiscount+" indirim bulunmaktadır!","Fatura tutarına indirim uygulanacaktır.");
  }

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  {

    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.InvoicesText + " - Crmplus");

    this.title1=this.sysLanguages.ErrorMessage;
    this.content=this.sysLanguages.ErrorMessage1;

    if (this._avRoute.snapshot.params["id"]) {
      this.Invoiceid = this._avRoute.snapshot.params["id"];
    }

    this.languageList.push({ label: this.sysLanguages.LanguageOptionTurkish, value: 0 }, { label: this.sysLanguages.LanguageOptionEnglish, value: 1 });

    this.getCustomers();
    this.getUsers();
    this.getPaymentMethods();
    this.getDiscountTypes();
    this.getCurrencies();
    this.getProducts();
    this.getStatus();
    this.getTax();
    this.getAllContact();

  }

  ngOnInit() 
  {
    if(this.Invoiceid>0)
    {
      setTimeout(()=>{
      this._apicallservice.Get("http://localhost:61232/api/Invoice/GetInvoice",this.Invoiceid)
      .subscribe(
        (result) => {
          this.title=this.sysLanguages.EditInvoice;
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            this.InvoiceData.StatusId = resulttArray[0].StatusId;
            this.InvoiceData.AdminNote = resulttArray[0].AdminNote;
            this.InvoiceData.Conditions = resulttArray[0].Conditions;
            this.InvoiceData.CreatedDate = this.formatIsoDateToCrypted(resulttArray[0].CreatedDate.split(" ")[0]);
            this.InvoiceData.LastPaymentDate = this.formatIsoDateToCrypted(resulttArray[0].LastPaymentDate.split(" ")[0]);
            this.InvoiceData.CurrencyTypeId = resulttArray[0].CurrencyTypeId;
            this.InvoiceData.CustomerId = resulttArray[0].CustomerId;
            this.InvoiceData.CustomerNote = resulttArray[0].CustomerNote;
            this.InvoiceData.DiscountId = resulttArray[0].DiscountTypeId;
            this.InvoiceData.InvoiceNumber = resulttArray[0].InvoiceNumber;
            this.InvoiceData.PayingMethodId = resulttArray[0].PayingMethodId;
            this.InvoiceData.SalesmanId = resulttArray[0].SalesmanId;    
            this.customerData.TaxNumber = resulttArray[0].TaxNumber;
            this.InvoiceData.ProjectId = resulttArray[0].ProjectId;
            this.InvoiceData.TotalDiscount = resulttArray[0].Discount;
            
            this.getProject(this.InvoiceData.CustomerId);
            
            this.customerContact = [];

            var customerDesc = document.getElementById("customerDesc");
            customerDesc.style.visibility="visible";

            for(let i in this.contactList)
            {
              if(this.contactList[i].customerId == this.InvoiceData.CustomerId)
              {
                this.customerContact.push(this.contactList[i]);
              }
            }

            for(let i in this.customerList)
            {
              if(this.customerList[i].value == this.InvoiceData.CustomerId)
              {
                this.customerData.TaxNumber = this.customerList[i].taxNumber;
                if(this.customerList.CustomerDiscount)
                {
                this.CustomerDiscount = this.customerList[i].customerDiscount.slice(1,4);
                this.updatedTotal();
                }
                break;
              }
            }

            this._apicallservice.Get("http://localhost:61232/api/Invoice/GetInvoiceContacts", this.Invoiceid).subscribe(
              (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(resultArray[0].Result == "Empty")
                    {
                      this.selectedCustomerContacts = [];
                    }
                  }
                  else
                  { 
                    this.selectedCustomerContacts = resultArray.map(x=> x.ContactId);
                    this.copyselectedCustomerContacts = this.selectedCustomerContacts.slice();
                  }
              }
            )
          }              
      },
      )
      this._apicallservice.Get("http://localhost:61232/api/Invoice/GetInvoiceLines", this.Invoiceid).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.LineData = [];
            }
          }
          else
          {
           /* for(var i in resultArray)
            {
              let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax };
              this.LineData.push(_lineData);
            }*/

            let _lineData;          
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                if(resultArray[i].ProductId == 0)
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                  this.LineData.push(_lineData);
                }
                else
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                  this.LineData.push(_lineData);
                }

              }
              else
              {
                if(resultArray[i].ProductId == 0)
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                  this.LineData.push(_lineData);
                }
                else
                {
                  _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                  this.LineData.push(_lineData);
                }
              }
            }

            this.updatedTotal();
          }
        }
      )
      }, 1000);
    }
    else
    {
      let _today = new Date(); 
      let _expiredDate = new Date();
      _expiredDate.setDate(_today.getDate()+30);

      this.title = this.sysLanguages.NewInvoice;
      this.InvoiceData.AdminNote = '';
      this.InvoiceData.Conditions = '';
      this.InvoiceData.CreatedDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
      this.InvoiceData.CurrencyTypeId = 1;
      this.InvoiceData.CustomerId = 0;
      this.InvoiceData.CustomerNote = '';
      this.InvoiceData.InvoiceNumber = '';
      this.InvoiceData.DiscountId = 1;
      this.InvoiceData.LastPaymentDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
      this.InvoiceData.PayingMethodId = 1;
      this.InvoiceData.ProjectId = 0;
      this.InvoiceData.SalesmanId = 0;
      this.InvoiceData.StatusId = 5;
      this.InvoiceData.Language = 0;
      this.selectedCustomerContacts = []; 

    }

  }

  save()
  {
    this.loading = true;
    if(this.title == this.sysLanguages.NewInvoice)
    {
      var x = document.getElementById("total");
      if(!this.CustomerDiscount)
      {
        var amount = x.innerText;
      }
      else
      {
        var amount = parseInt(x.innerText.split(" ")[0]) - (parseInt(x.innerText.split(" ")[0])*parseInt(this.CustomerDiscount)/100 ) + " " + this.currency;
      }

      let invoice = {
        InvoiceId:0,
        CustomerId: this.InvoiceData.CustomerId,
        ProjectId: this.InvoiceData.ProjectId,
        InvoiceNumber: $("#InvoiceNumber").val(),
        CreatedDate: $("#CratedDate").val(),
        LastPaymentDate: $("#LastDate").val(),
        PayingMethodId: this.InvoiceData.PayingMethodId,
        SalesmanId: this.InvoiceData.SalesmanId,
        ProposalId: 0,
        ProformId: 0,
        DiscountTypeId: this.InvoiceData.DiscountId,
        CurrencyTypeId: this.InvoiceData.CurrencyTypeId,
        AdminNote: $("#adminNote").val(),
        CustomerNote: $("#CustomerNote").val(),
        Conditions: $("#Conditions").val(),
        StatusId: this.InvoiceData.StatusId,
        Amount : amount,
        Discount : $("#totaldiscount").val()
      };
      if(invoice.CustomerId == 0 || invoice.LastPaymentDate == "" || invoice.PayingMethodId == 0 || invoice.SalesmanId == 0 || invoice.DiscountTypeId == 0 || invoice.CurrencyTypeId == 0 || invoice.Amount == "0.00 " + this.currency || this.InvoiceData.StatusId == 0 || invoice.InvoiceNumber == "" || invoice.CreatedDate == "" || invoice.Amount == "0 " + this.currency )
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        this._apicallservice.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
        (result) => {
          var invoiceID = result[0].Result;
          let invoiceDetail;           

          for(let i = 0; i < this.addedDynamicLineData.length; i++)
          {
            let _product = 0;
            
            for(let j in this.productList)
            {
              if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
              {
                _product = this.productList[j].value;
              }
            }

            if(_product == 0)
              invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
            else
              invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
              
            this._apicallservice.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
              else
              {
                this.loading = false;
                this._router.navigate(['/pages/invoices']);    
              }
            }
          )
          }     
            this.addedDynamicLineData = [];   
            
            let invoicecontact;

            for(let j in this.selectedCustomerContacts)
            {
              invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
            
              this._apicallservice.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
              (result) =>{
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray[0].Result == "Session_Expired") {
                  this.loading = false;
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
    
              if(this.sendMail == true)
              {
                invoice.InvoiceNumber = this.contactList[this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])].email;
                invoice.InvoiceId = invoiceID;
                this._apicallservice.Update("http://localhost:61232/api/Invoice/SendInvoice", this.InvoiceData.Language, invoice).subscribe(
                  (result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray.length != 0 && resultArray[0].Result) {
                      if (resultArray[0].Result == "Session_Expired") {
                        this.loading = false;
                        this._router.navigate(['/auth']);
                        return;
                      }
                    }
                  }
                )
              }
            }
          }
       )
      }    
    }
    else if (this.title == this.sysLanguages.EditInvoice)
    {
      var x = document.getElementById("total");

      if(this.InvoiceData.CustomerId == 0 || $("#LastDate").val() == "" || this.InvoiceData.PayingMethodId == 0 || this.InvoiceData.SalesmanId == 0 || this.InvoiceData.DiscountId == 0 || this.InvoiceData.CurrencyTypeId == 0 || x.innerHTML == "0 " + this.currency || this.InvoiceData.StatusId == 0 || $("#InvoiceNumber").val() == "" || $("#CratedDate").val() == "")
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        if(!this.CustomerDiscount)
        {
          var amount = x.innerText;
        }
        else
        {
          var amount = parseInt(x.innerText.split(" ")[0]) - (parseInt(x.innerText.split(" ")[0])*parseInt(this.CustomerDiscount)/100 ) + " " + this.currency;
        }
        let editInvoice = {
          InvoiceId:this.Invoiceid,
          CustomerId: this.InvoiceData.CustomerId,
          ProjectId: this.InvoiceData.ProjectId,
          InvoiceNumber: $("#InvoiceNumber").val(),
          CreatedDate: $("#CratedDate").val(),
          LastPaymentDate: $("#LastDate").val(),
          PayingMethodId: this.InvoiceData.PayingMethodId,
          SalesmanId: this.InvoiceData.SalesmanId,
          ProposalId: 0,
          ProformId: 0,
          DiscountTypeId: this.InvoiceData.DiscountId,
          CurrencyTypeId: this.InvoiceData.CurrencyTypeId,
          AdminNote: $("#adminNote").val(),
          CustomerNote: $("#CustomerNote").val(),
          Conditions: $("#Conditions").val(),
          StatusId: this.InvoiceData.StatusId,
          Amount : amount,
          Discount : $("#totaldiscount").val()
        };

        this._apicallservice.Add("http://localhost:61232/api/Invoice/EditInvoice", editInvoice).subscribe(
          (result) => {
            let total = 0;
            let lineCount = 0;

            let resultArray = JSON.parse(JSON.stringify(result));

            if(this.LineData != [])
            {
              for(let i in this.LineData)
              {
                if(this.LineData[i].LineAmount != "0")
                {
                  total += Number(this.LineData[i].LineAmount.split(" ")[0]);
                }
              }
            }

            if(this.addedDynamicLineData != [])
            {
              for(let j in this.addedDynamicLineData)
              {
                if(this.addedDynamicLineData[j].LineAmount != "0")
                {
                  total += Number(this.addedDynamicLineData[j].LineAmount.split(" ")[0]);
                }
              }
            }

            if(total != Number($("#total").val().toString().split(" ")[0]))
            {
                if(this.LineData.length != 0)
                {
                  for(let i = 0; i < this.LineData.length; i++)
                  {
                    if(this.LineData[i].LineAmount != "0")
                    {
                      lineCount++;   
                      let _line = { Id: this.LineData[i].Id, LineId: lineCount };                               
                      this._apicallservice.Update("http://localhost:61232/api/Invoice/EditInvoiceDetail", Number(this.LineData[i].Id), _line).subscribe(
                        (resultt) =>{
                        }
                      )              
                    }                
                  }
                }
                if(this.addedDynamicLineData != [])
                {
                  let _product = 0;
                  for(let i in this.addedDynamicLineData)
                  {
                    for(let j in this.productList)
                    {
                      if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                      {
                        _product = this.productList[j].value;
                      }
                    }
                    
                    lineCount++;
                    let _invdetail;

                    if(_product == 0)
                      _invdetail = { InvoiceId: this.Invoiceid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: lineCount, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                    else
                      _invdetail = { InvoiceId: this.Invoiceid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: lineCount, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                      
                      this._apicallservice.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", _invdetail).subscribe(
                      (resultt) =>{
                      }
                    )
                  }
                }
            }

            //---------- eğer eski seçilmiş kontak varsa onları sil
            for(let i in this.copyselectedCustomerContacts)
            {
              if(this.selectedCustomerContacts.findIndex(x => x.value == this.copyselectedCustomerContacts[i]) == -1)
              {
                let invoicecontact = { InvoiceId: this.Invoiceid, ContactId: this.copyselectedCustomerContacts[i] }; 
                this._apicallservice.Add("http://localhost:61232/api/Invoice/RemoveInvoiceContact", invoicecontact).subscribe(
                  (result) =>{
                  }
                )
              }
            }

            //----------- yeni eklenen varsada onları ekle
            for(let j in this.selectedCustomerContacts)
            {
              if(this.copyselectedCustomerContacts.findIndex(x => x.value == this.selectedCustomerContacts[j]) == -1)
              {
                let invoicecontact = { InvoiceId: this.Invoiceid, ContactId: this.selectedCustomerContacts[j] };   
                this._apicallservice.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                  (result) =>{
                  }
                )
              }

              if(this.sendMail == true)
              {
                let index = this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])
                editInvoice.InvoiceNumber = this.contactList[index].email;
                this._apicallservice.Update("http://localhost:61232/api/Invoice/SendInvoice", this.InvoiceData.Language, editInvoice).subscribe(
                  (result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray.length != 0 && resultArray[0].Result) {
                      if (resultArray[0].Result == "Session_Expired") {
                        this.loading = false;
                        this._router.navigate(['/auth']);
                        return;
                      }
                    }
                  }
                )
              }
            }

            this.loading = false;
            this._router.navigate(['/pages/invoices']);

            }
        )
      }
    }
  }

  saveandSent()
  {
    this.sendMail = true;
    this.save();
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            this.customerList = [];
            for(let i in resultArray)
            {
              let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId, customerDiscount: resultArray[i].CustomerDiscount, taxNumber: resultArray[i].TaxNumber };
              this.customerList.push(_customers);
            }
          }
      }
    )
  }

  getUsers()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length != 0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.userList = [];
        }
      }
      else
      {
        this.userList = [];
        for(let i in resultArray)
        {
          let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
          this.userList.push(_user);
        }
      }        
    },
    )
  }

  getPaymentMethods()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetPaymentMethods").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.methodList = [];
          }
        }
        else
        {
          this.methodList = [];
          for(let i in resultArray)
          {
            let _method = { label: resultArray[i].MethodDesc, value: resultArray[i].MethodId };
            this.methodList.push(_method);
          }
        } 
    },
    )
  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.currencyList = [];
          }
        }
        else
        {
          this.currencyList = [];
          for(let i in resultArray)
          {
            let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
            this.currencyList.push(_currency);
          }
        }
    },
    )
  }

  getDiscountTypes()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetDiscountTypes").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.discountList = [];
          }
        }
        else
        {
          this.discountList = []

          for(let i in resultArray)
          {
            let _discount;

            if(resultArray[i].DiscountTypeId == 1)
            {
              _discount = { label: this.sysLanguages.DiscountTypeNoDiscount, value: resultArray[i].DiscountTypeId };
              this.discountList.push(_discount);
            }
            else if(resultArray[i].DiscountTypeId == 2)
            {
              _discount = { label: this.sysLanguages.DiscountTypeBeforeTax, value: resultArray[i].DiscountTypeId };
              this.discountList.push(_discount);
            }
            else if(resultArray[i].DiscountTypeId == 3)
            {
              _discount = { label: this.sysLanguages.DiscountTypeAfterTax, value: resultArray[i].DiscountTypeId };
              this.discountList.push(_discount);
            }
          }
        }
    },
    )

  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }

                  }
                }

                _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, price: resultArray[i].Price, desc: resultArray[i].ProductDesc };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }
          }
        }
    },
    )
  }

  getStatus()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetAllStatus").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.statusList = [];
          }
        }
        else
        {
          this.statusList = [];

          for(let i in resultArray)
          {
            let _status;

            if(resultArray[i].StatusId == 0)    
            {
              _status = { label: this.sysLanguages.InvoiceStatusCanceled, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].StatusId == 1)
            {
              _status = { label: this.sysLanguages.InvoiceStatusPaid, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].StatusId == 2)
            {
              _status = { label: this.sysLanguages.InvoiceStatusNotPaid, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].StatusId == 3)
            {
              _status = { label: this.sysLanguages.InvoiceStatusPartlyPaid, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].StatusId == 4)
            {
              _status = { label: this.sysLanguages.InvoiceStatusExpired, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }
            else if(resultArray[i].StatusId == 5)
            {
              _status = { label: this.sysLanguages.InvoiceStatusDraft, value: resultArray[i].StatusId };
              this.statusList.push(_status);
            }      
          }
        }
    },
    )
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          this.taxList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  getAllContact()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].LeadId != 0)
              {
                if(resultArray[i].ContactId != 0)
                {
                  let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId, customerId: resultArray[i].CustomerId, email: resultArray[i].Email };
                  this.contactList.push(_contact);
                }
                
              }
            }
          }
      }
    )
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  getCustomerDetail(event)
  {
    var customerDesc = document.getElementById("customerDesc");
    customerDesc.style.visibility="visible";

    this.customerContact = [];

    var customerid = event;

    for(let i in this.customerList)
    {
      if(this.customerList[i].value == customerid)
      {
        this.customerData.TaxNumber = this.customerList[i].taxNumber;

        if(this.customerList[i].customerDiscount == null)
        {
          this.customerData.CustomerDiscount = null;
        }
        else
        {
          this.customerData.CustomerDiscount = this.customerList[i].customerDiscount.slice(1,4);
          this.makeToastDiscount();
        }
        
        break;
      }
    }

    for(let i in this.contactList)
    {
      if(this.contactList[i].customerId == this.InvoiceData.CustomerId)
      {
        this.customerContact.push(this.contactList[i]);
      }
    }
     
    this.selectedCustomerContacts = [];
    this.projectList = [];
    this.getProject(customerid);
  }

  getProject(customerid)
  {
    this._apicallservice.Get("http://localhost:61232/api/Invoice/GetBoardDetail",this.InvoiceData.CustomerId).subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.projectList = [];
          }
        }
        else
        {
          this.projectList = [];
          for(let i in resultArray)
          {
            let _projectlist = { label: resultArray[i].BoardName, value: resultArray[i].BoardId };
            this.projectList.push(_projectlist);
          }
        }
      }
    )
  }

  getProductDetail(event)
  {
    if(!this.Invoiceid)
    {
        this.LineData = [];
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" , LineDiscount : "" };
            this.LineData.push(_lineData);
          }
        }
    }
    else
    {
        this.LineData.shift();
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" , LineDiscount : "" };
            this.LineData.unshift(_lineData);
          }
        }
    }
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let priceforTaxValue = event.currentTarget.parentElement.parentElement.children[3].children[0].innerText;
    let priceValue = event.currentTarget.parentElement.parentElement.children[6].children[0].innerText;
    let discounttext = event.currentTarget.parentElement.parentElement.children[4].children[0].innerText;
    let counttext = event.currentTarget.parentElement.parentElement.children[2].children[0].innerText;

    
    for(let i in this.topTaxArray)
    {
      if(this.topTaxArray[i].amount == priceValue && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
      {
        this.topTax -= this.topTaxArray[i].value;
        if(this.topTax < 0)
        {
          this.topTax = 0;
        }
        break;
      }
    }

    this._totalDiscount -= parseFloat(discounttext);
    
    this._apicallservice.Deactive("http://localhost:61232/api/Invoice/RemoveInvoiceDetail", Number(this.LineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          var x = this.LineData[id].LineAmount.split(" ")[0];
          var total =document.getElementById("total");
          var newTotal = this._total-parseFloat(x);
          this._total = newTotal;
          total.innerHTML=Number(newTotal).toLocaleString()+" " + this.currency;
          document.getElementById("lineData_" + id).remove();
          this.LineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  updatedTotal()
  {
    var total = 0;
    var z = document.getElementById("total");
    for(var i = 1; i < this.LineData.length; i++)
    {
      if(this.LineData[i].LineAmount != "0")
      {
        var sub=parseFloat(this.LineData[i].LineAmount);
        total=total+sub;
        z.innerHTML=Number(total).toLocaleString()+" " + this.currency;
        this._total = total;

        if(this.LineData[i].LineDiscount)
        {
            this._totalDiscount += Number(this.LineData[i].LineDiscount);
        }

        if(this.LineData[i].updatedefaultTax == "%18 KDV")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.LineData[i].LineDiscount)) * 0.18, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%17.65 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 17.65/100, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.LineData[i].LineDiscount)) * 17.65/100, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%15 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 0.15, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.15);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.LineData[i].LineDiscount)) * 0.15, amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.15);
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.LineData[i].LineDiscount)) * 0.18) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice});
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.18) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100);   
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%15 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 0.18), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%17.65 Stopaj,%15 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.LineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj,%15 Stopaj")
        {
          if(this.LineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.LineData[i].LineDiscount)) * 0.18) + ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100), amount: this.LineData[i].LineAmount, count: this.LineData[i].ProductCount, price: this.LineData[i].ProductPrice });
            this.topTax += ((total + Number(this.LineData[i].LineDiscount)) * 0.18) + ((total + Number(this.LineData[i].LineDiscount)) * 0.15) + ((total + Number(this.LineData[i].LineDiscount)) * 17.65/100);
          }
        }
      }
    }
    var discount = this.InvoiceData.TotalDiscount;
    if(discount)
    {
      this._total = this._total - parseFloat(discount);
    }
    z.innerHTML=Number(this._total).toLocaleString()+" " + this.currency;

    if(!this.CustomerDiscount)
    {
      z.innerHTML = Number(this._total).toLocaleString()+" " + this.currency;
    }
    else
    {
      this._total = this._total - this._total*parseInt(this.CustomerDiscount)/100;
      z.innerHTML= Number(this._total).toLocaleString() + " " + this.currency;
    }
  }

  total(id)
  {
    var y = document.getElementById("f"+id).innerHTML.split(" ")[0];
    var x =document.getElementById("total");
    this._total = parseFloat(y) + this._total;
    x.innerText=Number(this._total).toLocaleString()+" " + this.currency;  
  }

  createLine()
  {
    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid = line[0].childElementCount+1;
    var a1 = $("#a1").val();
    var b1 = $("#b1").val();
    var c1 = $("#c1").val().toString();
    var d1 = $("#d1").val().toString();
    var dis1=$("#dis1").val().toString();
    if(dis1 == "")
    {
      var total = (parseFloat(c1)*parseFloat(d1));
      dis1 = '0'; 
    }
    else
    {
      this._totalDiscount += parseFloat(dis1);
      var total = (parseFloat(c1)*parseFloat(d1))-parseFloat(dis1);
    }

    if(!this.tax || this.tax.length == 0)
    {
      this.TaxDesc = "Vergi seçimi yapılmadı.";
    }
    else if(this.tax.length == 1)
    {
      if(this.tax[0] == 1)
      {
        this.TaxDesc = "%18 KDV";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18), amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.18, amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18);
        }

        total = total + (total*0.18);
      }
      else if(this.tax[0] == 2)
      {
        this.TaxDesc = "%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*17.65/100);
      }
      else if(this.tax[0] == 3)
      {
        this.TaxDesc = "%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += (total * 0.15);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15);
        }

        total = total + (total*0.15);
      }
    }
    else if(this.tax.length == 2)
    {
      if((this.tax[0] == 1 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100);   
        }

        total = total + (total*0.18)+(total*17.65/100);
      }
      else if((this.tax[0] == 1 && this.tax[1] == 3) || (this.tax[0] == 3 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18);
        }

        total = total + (total*0.15) + (total*0.18);
      }
      else if((this.tax[0] == 3 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 3))
      {
        this.TaxDesc = "%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*0.15) + (total*17.65/100);
      }
    }
    else if(this.tax.length == 3)
    {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*0.18) + (total*0.15) + (total*17.65/100);
    }

    if (total.toString() == "NaN")
    {
      total = 0;
    }

    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><p id='c"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;''>" + c1 + "</p></td><td><p id='d"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + d1 + "</p></td><td><p id='dis"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + dis1 + "</p></td><td><p id='updatedefaultTax' style='float: left;margin-top: 26px;margin-left:5px;' >"+this.TaxDesc+"</p></td><td><p id='f"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>"+total+" "+this.currency+"</p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red ;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_"+newid);
    let mythis = this;

    this.total(newid);

    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let priceValue = element.parentElement.parentElement.children[6].children[0].innerHTML;
      let priceforTaxValue = element.parentElement.parentElement.children[3].children[0].innerHTML;
      let discounttext = element.parentElement.parentElement.children[4].children[0].innerHTML;
      let counttext = element.parentElement.parentElement.children[2].children[0].innerHTML;

      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue && x.LineAmount == priceValue), 1);

      var id = element.id.split("_")[1];
      var minus = $("#f"+id).text().split(" ")[0];
      document.getElementById(id).remove();
      var a = document.getElementById("total");
      this._total = this._total - parseFloat(minus);
      a.innerText = Number(this._total).toLocaleString() + " " + this.currency; 

      for(let i in this.topTaxArray)
      {
        if(this.topTaxArray[i].amount == priceValue.split(" ")[0] && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
        {
          this.topTax -= this.topTaxArray[i].value;
          if(this.topTax < 0)
          {
            this.topTax = 0;
          }
          break;
        }
      }

      this._totalDiscount -= parseFloat(discounttext);

    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString(), ProductCount: Number(c1), ProductPrice: Number(d1), LineAmount: total + " " + this.currency, updatedefaultTax: "", LineTax: this.TaxDesc , LineDiscount : dis1 };
    this.addedDynamicLineData.push(_lineData);
    
  }

  totalDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total - parseFloat(y);
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
  }

  cleanDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total + parseFloat(y);
    this._total = discountedTotal;
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
    $("#totaldiscount").val("0");
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  changeCurrencyType(event)
  {
    if(event == 1)
    {
      this.currency = "TL";
    }
    else if(event == 2)
    {
      this.currency = "$";
    }
    else if(event == 3)
    {
      this.currency = "€";
    }
    else if(event == 4)
    {
      this.currency = "£";
    }
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
