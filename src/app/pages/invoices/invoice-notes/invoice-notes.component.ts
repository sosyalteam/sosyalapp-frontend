import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'invoice-notes',
  templateUrl: './invoice-notes.component.html',
  styleUrls: ['./invoice-notes.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class InvoiceNotesComponent implements OnInit {

  sysLanguages: any;
  invoiceId : any;

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.InvoiceNotesText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.invoiceId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title = this.sysLanguages.InvoiceOverview;
    this.tabs[1].title = this.sysLanguages.BoardsNoteText;

    this.getComments();
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/invoices/invoice-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'ion-ios-copy-outline',
      route: ['/pages/invoices/invoice-notes', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/invoices/add-edit-invoices', this._avRoute.snapshot.params["id"]]);
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Invoice/InvoiceComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : this.invoiceId 
    }
    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }
}
