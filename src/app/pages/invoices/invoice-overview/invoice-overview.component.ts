import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'invoice-overview',
  templateUrl: './invoice-overview.component.html',
  styleUrls: ['./invoice-overview.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class InvoiceOverviewComponent implements OnInit {

  sysLanguages: any;
  public invoiceArray = [{InvoiceNumber : "", CustomerName : "" , BoardName : "", CreatedDate : "", LastPaymentDate : "", AdminNote : "", CustomerNote : "", Conditions : "", Amount : "" , StatusDesc : "" , Fullname : "" , MethodDesc :"" , CurrencyDesc : ""  }];
  productList = [];

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.InvoiceOverviewText + " - Crmplus");

    this.tabs[0].title = this.sysLanguages.InvoiceOverview;

    this.settings.columns.ProductName.title = this.sysLanguages.AddInvoiceTableProduct;
    this.settings.columns.Description.title = this.sysLanguages.AddInvoiceTableDesc;
    this.settings.columns.LineAmount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settings.columns.LineTax.title = this.sysLanguages.AddInvoiceTableTax;
    this.settings.columns.LineDiscount.title = this.sysLanguages.AddInvoiceDiscount;
    
    this.getInvoiceProduct();
    this.getComments();

  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/invoices/invoice-overview', this._avRoute.snapshot.params["id"]],
    },
  ];

  settings = {
    columns: {
      ProductName: {
        title: ''
      },
      Description: {
        title: ''
      },
      LineAmount: {
        title: ''
      },
      LineTax: {
        title: ''
      },
      LineDiscount: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/Invoice/DetailsForInvoiceOverview",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {

            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.invoiceArray[0].InvoiceNumber = resulttArray[0].InvoiceNumber;
            this.invoiceArray[0].CustomerName = resulttArray[0].CustomerName;
            this.invoiceArray[0].BoardName = resulttArray[0].BoardName;
            this.invoiceArray[0].CreatedDate = resulttArray[0].CreatedDate.split(" ")[0];
            this.invoiceArray[0].LastPaymentDate = resulttArray[0].LastPaymentDate.split(" ")[0];
            this.invoiceArray[0].AdminNote = resulttArray[0].AdminNote;
            this.invoiceArray[0].CustomerNote = resulttArray[0].CustomerNote;
            this.invoiceArray[0].Conditions = resulttArray[0].Conditions;
            this.invoiceArray[0].Amount =  resulttArray[0].Amount;
            this.invoiceArray[0].Fullname = resulttArray[0].Fullname;
            this.invoiceArray[0].MethodDesc = resulttArray[0].MethodDesc;
            this.invoiceArray[0].CurrencyDesc = resulttArray[0].CurrencyDesc;

            if(this.invoiceArray[0].AdminNote == "")
            {
              this.invoiceArray[0].AdminNote = this.sysLanguages.PcNoNote;
            }
            if(this.invoiceArray[0].CustomerNote == "")
            {
              this.invoiceArray[0].CustomerNote = this.sysLanguages.PcNoNote;
            }
            if(this.invoiceArray[0].Conditions == "")
            {
              this.invoiceArray[0].Conditions = this.sysLanguages.PcNoNote;
            }

            if(resulttArray[0].ProjectId == 0)
            {
              this.invoiceArray[0].BoardName = this.sysLanguages.NotValueText;
            }

            if(resulttArray[0].StatusId == 0)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusCanceled;
            }  
            else if(resulttArray[0].StatusId == 1)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusPaid;
            }  
            else if(resulttArray[0].StatusId == 2)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusNotPaid;
            }
            else if(resulttArray[0].StatusId == 3)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusPartlyPaid;
            }
            else if(resulttArray[0].StatusId == 4)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusExpired;
            }
            else if(resulttArray[0].StatusId == 5)
            {
              this.invoiceArray[0].StatusDesc = this.sysLanguages.InvoiceStatusDraft;
            }
          }       
      },
      );
  }

  getInvoiceProduct()
  {
    this._apicallService.Get("http://localhost:61232/api/Invoice/GetInvoiceLines",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              if(resultArray[i].ProductId == 0)
              {
                resultArray[i].ProductName = resultArray[i].LineName;
              }
            }

            this.productList = resultArray.slice();
          }
      }
    )
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Invoice/InvoiceComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : this._avRoute.snapshot.params["id"]
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  openUrl(link)
  {
    if(link == "/invoice")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else if(link == "/invoice/pdf")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else
    {
      this._router.navigate([link, this._avRoute.snapshot.params["id"]]);
    }
  }

}
