import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicesComponent } from './invoices/invoices.component';
import { AddEditInvoicesComponent } from './add-edit-invoices/add-edit-invoices.component';
import { InvoiceOverviewComponent } from './invoice-overview/invoice-overview.component';
import { InvoiceNotesComponent } from './invoice-notes/invoice-notes.component';

const routes: Routes = [
  {
    path: '',
    component: InvoicesComponent,
  },
  {
    path: 'add-edit-invoices',
    component: AddEditInvoicesComponent,
  },
  {
    path: 'add-edit-invoices/:id',
    component: AddEditInvoicesComponent,
  },
  {
    path: 'invoice-overview/:id',
    component: InvoiceOverviewComponent,
  },
  {
    path: 'invoice-notes/:id',
    component: InvoiceNotesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
