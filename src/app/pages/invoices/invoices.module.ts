import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { AddEditInvoicesComponent } from './add-edit-invoices/add-edit-invoices.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbDialogModule, NbContextMenuModule, NbSelectModule, NbTabsetModule, NbRouteTabsetModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InvoiceOverviewComponent } from './invoice-overview/invoice-overview.component';
import { InvoiceNotesComponent } from './invoice-notes/invoice-notes.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [InvoicesComponent, AddEditInvoicesComponent, InvoiceOverviewComponent, InvoiceNotesComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule,
    NbTabsetModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NgxPaginationModule
  ]
})
export class InvoicesModule { }
