import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class InvoicesComponent implements OnInit {

  public sysLanguages : any;
  public InvoiceList : any;
  public allInvoiceCount = 0;
  public canceledCount = 0;
  public paidCount = 0;
  public notPaidCount = 0;
  public partlyPaidCount = 0;
  public expiredCount = 0;
  public draftCount = 0;
  ListToKanban = false;
  openKanbanText = "";
  public Statuss :any;
  public InvoiceKanban = [{InvoiceId: "", InvoiceNumber: "", AdminNote : "" , StatusId: "", Amount:"", Company: ""}];

  filter =false;
  filteredInvoiceList : any;

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.InvoicesText + " - Crmplus");

    this.settings.columns.InvoiceNumber.title=this.sysLanguages.InvoiceNumberText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.CreatedDate.title=this.sysLanguages.CreatedDateText;
    this.settings.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;
    this.settings.columns.BoardName.title=this.sysLanguages.BoardNameText;
    this.settings.columns.LastPaymentDate.title=this.sysLanguages.LastDatePaymentText;
    this.settings.columns.StatusDesc.title=this.sysLanguages.AddTaskStatusText;
    this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;
    this.getInvoice();
    this.getStatus();



   }

   settings = {
    columns: {
      InvoiceId: {
        title: 'ID',
        width: '2%'
      },
      InvoiceNumber: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CustomerName: {
        title: ''
      },
      BoardName: {
        title: ''
      },
      LastPaymentDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
      /*Fullname: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: 'All',
                list: this.userList
              }
            }
      },*/
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  getInvoice()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.InvoiceList = [];
          }
        }
        else 
        {
          this.InvoiceList = resulttArray;
          this.allInvoiceCount = resulttArray.length;
          this.canceledCount = 0;
          this.paidCount = 0;
          this.notPaidCount = 0;
          this.partlyPaidCount = 0;
          this.expiredCount = 0;
          this.draftCount = 0;
          for(let i in this.InvoiceList)
          {

            if(this.InvoiceList[i].StatusId == 0)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusCanceled;
              this.canceledCount++;
            }  
            else if(this.InvoiceList[i].StatusId == 1)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusPaid;
              this.paidCount++;
            }  
            else if(this.InvoiceList[i].StatusId == 2)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusNotPaid;
              this.notPaidCount++;
            }
            else if(this.InvoiceList[i].StatusId == 3)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusPartlyPaid;
              this.partlyPaidCount++;
            }
            else if(this.InvoiceList[i].StatusId == 4)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusExpired;
              this.expiredCount++;
            }
            else if(this.InvoiceList[i].StatusId == 5)
            {
              this.InvoiceList[i].StatusDesc = this.sysLanguages.InvoiceStatusDraft;
              this.draftCount++;
            }
          }   
        }
      }
    )
    

  }

  addInvoice()
  {
    this._router.navigate(['/pages/invoices/add-edit-invoices']);
  }

  editInvoice(event)
  {
    var id = `${event.data.InvoiceId}`;
    this._router.navigate(['/pages/invoices/invoice-overview',id]);
  }
  onCustomAction(event)
  {
    var id = `${event.data.InvoiceId}`;
    this._router.navigate(['/pages/invoices/invoice-overview',id]);
  }

  filterInvoice(value)
  {
    this.filter = true;
    if(value == "0")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 0);
      this.filteredInvoiceList=a;
    }
    else if(value == "1")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 1);
      this.filteredInvoiceList=a;
    }
    else if(value == "2")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 2);
      this.filteredInvoiceList=a;
    }
    else if(value == "3")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 3);
      this.filteredInvoiceList=a;
    }
    else if(value == "4")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 4);
      this.filteredInvoiceList=a;
    }
    else if(value == "5")
    {
      const a = this.InvoiceList.filter(x=>x.StatusId == 5);
      this.filteredInvoiceList=a;
    }
    else if(value == "All")
    {
      this.filter = false;
    }
  }

  openKanban()
  {
    if(this.openKanbanText == this.sysLanguages.TasksBackToKanbanButtonText)
    {
      this.ListToKanban = true;
      this.openKanbanText = this.sysLanguages.TasksListButtonText;
      this.InvoiceKanban = [];

      for(let i in this.Statuss) 
      {
        for(let j in this.InvoiceList)
        {
          if(this.InvoiceList[j].StatusId == this.Statuss[i].StatusId)
          {
            let invoicelistkanban = {InvoiceId: this.InvoiceList[j].InvoiceId, AdminNote: this.InvoiceList[j].AdminNote.substring(0, 10), InvoiceNumber: this.InvoiceList[j].InvoiceNumber , StatusId: this.InvoiceList[j].StatusId, Amount: this.InvoiceList[j].Amount, Company: this.InvoiceList[j].CustomerName};
            this.InvoiceKanban.push(invoicelistkanban);
          }
        }
      }
    }
    else if (this.openKanbanText == this.sysLanguages.TasksListButtonText)
    {
      this.ListToKanban = false;
      this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;
      this.getInvoice();
    }   
  }

  getStatus()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetAllStatus").subscribe(
    (result) => {       
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          //
        }
      }
      else
      {
        for(let i in resulttArray)
        {
          if(resulttArray[i].StatusId == 0)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusCanceled;
          }  
          else if(resulttArray[i].StatusId == 1)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusPaid;
          }  
          else if(resulttArray[i].StatusId == 2)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusNotPaid;
          }
          else if(resulttArray[i].StatusId == 3)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusPartlyPaid;
          }
          else if(resulttArray[i].StatusId == 4)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusExpired;
          }
          else if(resulttArray[i].StatusId == 5)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.InvoiceStatusDraft;
          }
        }

        this.Statuss = resulttArray;
      }
    },
    )
  }

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropInvoice(event)
  {
    var data = event.dataTransfer.getData("text2");

    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addInvoiceBtn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("invoice_desc"))
    {
      //window.alert("You can't drop here");
    }   
    else if(event.target.className == "cardInvoiceDesc")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "assignedInvoiceCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "endDateInvoiceCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "cardDetailButton")
    {
      //window.alert("You can't drop here");
    }
    else
    {
      var invoiceID = data.split('_')[1];
      var statusID = event.target.id.split('_')[1];
      
      let _invoice = {InvoiceId: invoiceID, StatusId: statusID};

      this._apicallservice.Update("http://localhost:61232/api/Invoice/ChangeInvoiceStatus", invoiceID, _invoice).subscribe((result) => {
        this.getInvoice();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  dragInvoice(event)
  {
     event.dataTransfer.setData("text", event.target.parentNode.id);
     event.dataTransfer.setData("text2", event.target.id);
  }

}
