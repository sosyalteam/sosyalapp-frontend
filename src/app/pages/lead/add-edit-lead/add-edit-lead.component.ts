import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import * as $ from "jquery";
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-lead',
  templateUrl: './add-edit-lead.component.html',
  styleUrls: ['./add-edit-lead.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class AddEditLeadComponent implements OnInit {

  public sysLanguages: any;
  public UserList : any;
  public StatusList: any;
  public SourceList: any;
  public CallList : any;
  public PossibilityList: any;
  customName = "";
  keyword = 'name';
  public _customers = [{ id: '', name: '', source: '' }];
  public allCustomers = [];
  public leadArray = [{ LeadDesc:' ', LeadName : '' ,Name:' ', Email:' ', Customer:' ', Address:' ', Phone:' ', City:' ', Website:' ', CreatedDate:' ', selectedItem:'', selectedItem1:2, selectedItem2:' ', selectedItem3:1, MobilePhone : '',Town: '',selectedItem4 : 2, Position : '',selectedItem5 : 9,selectedItem6 :'', CreatorId : 0 }];
  title : string = "";
  isChecked = 0;
  productList : any;
  LineData = [{ Id: '', ProductName: '', LineDesc: ''}];
  LeadID : any;
  addedDynamicLineData = [];
  TeamList = [];
  contactList = [];
  _CopycontactList : any;
  add = "true";
  items = [];
  selectedProduct : any;
  customerid : any;
  customerName : any;

  public _contacts = [{ id: '', name: '', source: '' }];
  contactName = "";
  _contactName = "";
  contactid : any;
  keyword1 = 'name';

  customerIdForAddLead : any;

  loading = false;


  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  status1: NbToastStatus = NbToastStatus.SUCCESS;
  status2:NbToastStatus=NbToastStatus.INFO;

  title1 = '';
  content = '';

  

  //success add customer
  title2 = '';
  content1 = '';

  //customerınfo
  title3='';
  content2 = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast() {
    this.showToast(this.status, this.title1, this.content);
  }
  makeToastSuccess() {
    this.showToast(this.status1, this.title2, this.content1);
  }
  makeToastCustomerInfo()
  {
    this.showToast(this.status2, this.title3, this.content2);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  //

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService,private toastrService: NbToastrService,private _avRoute: ActivatedRoute, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.LeadsText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.LeadID = this._avRoute.snapshot.params["id"];
    }
    
      this.getUsers();
      this.getSources();
      this.getStatus();
      this.getCallList();
      this.getPossibilityList();
      this.getCustomers();
      this.getProducts();
      this.getTeams();
      this.getAllContact();

      this.title1=this.sysLanguages.ErrorMessage;
      this.content=this.sysLanguages.ErrorMessage1;
      this.title2=this.sysLanguages.SuccessMessage;
      this.content1=this.sysLanguages.SuccessMessage1;

      this.customerIdForAddLead = parseInt(sessionStorage.getItem('customerId'));
      sessionStorage.removeItem('customerId');

  }

  ngOnInit() {
    if(this.LeadID>0)
    {
      this.title=this.sysLanguages.LeadEditText;
      this._apicallservice.Get("http://localhost:61232/api/lead/GetLeadDetail",parseInt(this.LeadID)).subscribe(
      (result) => {      
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if(resulttArray[0].Result == "Empty")
          {
            //do smth
          }
        }
        else
        {
          this.leadArray[0].LeadDesc = resulttArray[0].LeadDesc;
          this.contactName = resulttArray[0].Name;
          this._contactName =  resulttArray[0].Name;
          this.leadArray[0].Address = resulttArray[0].Address;
          this.leadArray[0].Email = resulttArray[0].Email;
          this.leadArray[0].Phone = resulttArray[0].Phone;
          this.leadArray[0].City = resulttArray[0].City;
          this.leadArray[0].MobilePhone = resulttArray[0].MobilePhone;
          this.leadArray[0].Town = resulttArray[0].Town;
          this.leadArray[0].LeadName = resulttArray[0].LeadName;
          //this.leadArray[0].Customer = resulttArray[0].CustomerId;
          this.customerid = resulttArray[0].CustomerId;
          this.customerName = resulttArray[0].CustomerName;
          this.leadArray[0].Website = resulttArray[0].Website;
          this.leadArray[0].CreatedDate = this.formatIsoDateToCrypted(resulttArray[0].CreatedDate.split(" ")[0]);
          this.leadArray[0].selectedItem = resulttArray[0].UserId;
          this.leadArray[0].selectedItem1 = resulttArray[0].StatusId;
          this.leadArray[0].selectedItem2 = resulttArray[0].SourceId;
          this.leadArray[0].selectedItem3 = resulttArray[0].CalledId;
          this.leadArray[0].selectedItem4 = resulttArray[0].PossibilityId;
          this.leadArray[0].Position = resulttArray[0].Position;
          this.leadArray[0].selectedItem5 = resulttArray[0].TeamId;
          this.leadArray[0].CreatorId = resulttArray[0].CreatorId;
          this.contactid = resulttArray[0].ContactId;
          var stringTags = resulttArray[0].Tags;
          this.isChecked = parseInt(resulttArray[0].PrivateStatus);
          if(stringTags=="")
          {
            this.items = [];
          }
          else
          {
            this.items = stringTags.split(",");
          }

          var createButton = document.getElementById("createCustomer");
          createButton.style.display="inline-block";
          var call = document.getElementById("CallAgain");
          call.style.display="inline-block";
        }
    },
    ),
    this._apicallservice.Get("http://localhost:61232/api/Lead/GetLeadDetailLines", parseInt(this.LeadID)).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.LineData = [];
            }
          }
          else
          {
            let _lineData;          
            for(var i in resultArray)
            {
                _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].LineDesc };
                this.LineData.push(_lineData);
            }
          }
        }
      )
    }
    else
    {
      let _today = new Date().toISOString();
      if(this.customerIdForAddLead)
      {
        setTimeout(()=>{
          const customer = this.allCustomers.filter(x=> x.CustomerId == this.customerIdForAddLead);
          this.leadArray[0].CreatedDate = _today.split("T")[0];
          this.leadArray[0].Phone = customer[0].Phone;
          this.leadArray[0].Address = customer[0].Address;
          this.leadArray[0].City = customer[0].City;
          this.customerid = customer[0].CustomerId;
          this.customerName = customer[0].CustomerName;
          this.leadArray[0].Website = customer[0].Website;
          this.leadArray[0].LeadDesc = '';
          this.leadArray[0].Town= '';
          this.leadArray[0].Name = '';
          this.leadArray[0].MobilePhone= '';
          this.leadArray[0].Email= '';
          this.leadArray[0].selectedItem = '';
          this.leadArray[0].Position = '';
          this.leadArray[0].selectedItem2= '';
          this.leadArray[0].selectedItem5= 9;
          this.leadArray[0].selectedItem6= '';
          this.items=[];
          this.isChecked = 0;
          this.title=this.sysLanguages.LeadAddText;
        },2000);
        
      }
      else
      {
        this.leadArray[0].LeadDesc = '';
        this.leadArray[0].Name = '';
        this.leadArray[0].Address= '';
        this.leadArray[0].Email= '';
        this.leadArray[0].Phone= '';
        this.leadArray[0].MobilePhone= '';
        this.leadArray[0].Town= '';
        this.leadArray[0].City= '';
        this.leadArray[0].Customer= '';
        this.leadArray[0].Website= '';
        this.leadArray[0].CreatedDate= _today.split("T")[0];
        this.leadArray[0].selectedItem = '';
        this.leadArray[0].Position = '';
        this.leadArray[0].selectedItem2= '';
        this.leadArray[0].selectedItem5= 9;
        this.leadArray[0].selectedItem6= '';
        this.items=[];
        this.isChecked = 0;
        this.title=this.sysLanguages.LeadAddText;
      } 
    }
  }

  getUsers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {       
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resulttArray[0].Result == "Empty") {
            this.UserList = []; 
          }
        }
        else
        {
          this.UserList = []; 
          for(let i in resulttArray)
          {
            if(resulttArray[i].UserId != 0)
            {
              let _userlist = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
              this.UserList.push(_userlist);
            }    
          }
        }
      },
      )
  }

  getSources()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/lead/GetSourceList").subscribe(
    (result) => {      
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          this.SourceList = [];
        }
      }
      else
      {
        this.SourceList = [];
        for(let i in resulttArray)
        {
          if(resulttArray[i].SourceId == 1)
          {
            let _source = { label: this.sysLanguages.LeadSourceActivity, value: resulttArray[i].SourceId };
            this.SourceList.push(_source);
          }
          else if(resulttArray[i].SourceId == 5)
          {
            let _source = { label: this.sysLanguages.LeadSourceReference, value: resulttArray[i].SourceId };
            this.SourceList.push(_source);
          }
          else if(resulttArray[i].SourceId == 6)
          {
            let _source = { label: this.sysLanguages.LeadSourceWebForm, value: resulttArray[i].SourceId };
            this.SourceList.push(_source);
          }
          else if(resulttArray[i].SourceId == 7)
          {
            let _source = { label: this.sysLanguages.LeadTablePhoneText, value: resulttArray[i].SourceId };
            this.SourceList.push(_source);
          }
          else
          {
            let _source = { label: resulttArray[i].SourceDesc, value: resulttArray[i].SourceId };
            this.SourceList.push(_source);
          }
        }
      }
    },
    )
  }

  getStatus()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/lead/GetStatusList").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.StatusList = [];
          }
        }
        else
        {
          this.StatusList = [];
          for(var i = 0; i < resultArray.length; i++ )
          {
            if(resultArray[i].StatusId == 1)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartPossibility;
            }
            else if(resultArray[i].StatusId == 2)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartCandidate;
            }
            else if(resultArray[i].StatusId == 3)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartStatusStay;
            }
            else if(resultArray[i].StatusId == 4)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartHotStatus;
            }
            else if(resultArray[i].StatusId == 5)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartBargain;
            }
            else if(resultArray[i].StatusId == 6)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCustomerStatus;
            }
            else if(resultArray[i].StatusId == 7)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartLost;
            }
            else if(resultArray[i].StatusId == 8)
            {
              resultArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCallAgain;
            }

            let _statusList = { label: resultArray[i].StatusDesc, value: resultArray[i].StatusId };
            this.StatusList.push(_statusList);
          }
        }
    },
    )
  }

  getCallList()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/lead/GetCallStatus").subscribe(
    (result) => {      
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          this.CallList = [];
        }
      }
      else
      {
        this.CallList = [];
        for(let i in resulttArray){
          let _callList = { label: resulttArray[i].CalledStatus, value: resulttArray[i].Id };
          this.CallList.push(_callList);
        }
      }
    },
    )
  }

  getPossibilityList()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/lead/GetPossibility").subscribe(
    (result) => {      
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          this.PossibilityList = [];
        }
      }
      else
      {
        this.PossibilityList = [];
        for(let i in resulttArray){

          let _possibilityList = { label: resulttArray[i].PossibilityDesc, value: resulttArray[i].Id };
          this.PossibilityList.push(_possibilityList);
        }
      }
    },
    )
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
         /* else if(resultArray[0].Result == "Empty")
          {
            this._customers = [];
          }*/
        }
        else
        {
          let flag = false;
          this.allCustomers = resultArray;

          for(let i in resultArray){
            if(resultArray[i].CustomerId != 0)
            {
              for(let j in this._customers)
              {
                if(resultArray[i].CustomerName.toString().toLowerCase() == this._customers[j].name.toString().toLowerCase())
                {
                  flag = true;
                  break;
                }
              }

              if(flag == false)
              {
                let cust = { id: resultArray[i].CustomerId, name: resultArray[i].CustomerName, source: "customer" };
                this._customers.push(cust);
              }

              flag = false;
            }
          }
        }
      }
    )
  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetProductsWithoutPrice").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              let _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, desc: resultArray[i].ProductDesc };
              this.productList.push(_product);
            }
          }
        }
    },
    )
  }

  getTeams()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.TeamList = [];
          }
        }
        else
        {
          this.TeamList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TeamName != "NoTeam")
            {
              let _team = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
              this.TeamList.push(_team);
            }
            
          }
        }
    },
    )
  }
  getAllContact()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          { 
            this.contactList = [];
            for(let i in resultArray)
            {
              if(resultArray[i].ContactId != 0)
              {
                let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId, customerId: resultArray[i].CustomerId, email: resultArray[i].Email };
                this.contactList.push(_contact);

                let cont = { id: resultArray[i].ContactId, name: resultArray[i].Fullname, source: "contact" };
                this._contacts.push(cont);
              }
            }
          }
      }
    )
  }

  getProductDetail(event)
  {
    if(!this.LeadID)
    {
        this.LineData = [];
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" };
            this.LineData.push(_lineData);
          }
        }
    }
    else
    {
        this.LineData.shift();
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" };
            this.LineData.unshift(_lineData);
          }
        }
    }
  }

  createLine()
  {
    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid= line[0].childElementCount+1;
    var a1 =$("#a1").val();
    var b1 =$("#b1").val();

    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red;margin-left:50%;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_"+newid);
    let mythis = this;

    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let descValue = element.parentElement.parentElement.children[1].children[0].innerHTML;
      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue && x.LineDesc == descValue), 1);

      var id = element.id.split("_")[1];
      document.getElementById(id).remove();

    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString() };
    this.addedDynamicLineData.push(_lineData);
    
  }

  selectEvent(event)
  {
    this._contacts = [];
    const a  = this.contactList.filter(x=>x.customerId == event.id);
    for (let i in a)
    {
      let cont = { id: a[i].value, name: a[i].label, source: "customer" };
      this._contacts.push(cont);
    }
    this.customerid= event.id;
  }

  onChangeSearch(event)
  {
    this.customName = event;
    this.customerid = "";
    this.leadArray[0].selectedItem6  = '0';
    this._contacts = [];
  }

  onChangeSearchContact(event)
  {
    this.contactName = event;
    this.contactid = "";
  }

  selectEventContact(event)
  {
    this.contactid = event.id;
    this._contactName = event.name;
  }

  privateLead(event)
  {
    this.isChecked=event;
  }

  save()
  {
    this.loading = true;
    if(this.title==this.sysLanguages.LeadAddText)
    {
      var mphone = $("#mobilephone").val()
      var email = $("#email").val();
      if(this.customerid  == "" && this.customName != "" && this.contactid == "" && this.contactName != "")
      {
        let customer = {CustomerName : this.customName , StatusId: 0, City : $("#city").val() , Phone : $("#phone").val(), CreatedDate : ""  };
          this._apicallservice.Add("http://localhost:61232/api/Customer/Create", customer).subscribe((result) => {
          if (result[0].Result == "True") {
            var custid = result[1].Value;
            let user = {  UserId: 0, Fullname: this.contactName, Email: email,StatusId : 0};
            this._apicallservice.Add("http://localhost:61232/api/User/Create", user).subscribe((resultt) => {
                    var userid = resultt[0].Result;
                    let contact =  {CustomerId : parseInt(result[1].Value) , Fullname : this.contactName , Phone : mphone , Email : email , UserId : userid   };
                    this._apicallservice.Add("http://localhost:61232/api/Customer/CreateContact",contact).subscribe((result) => {
                    var contactid=result[1].Value;    
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray[0].Result == "True") {
                      let lead={
                        LeadId: 0,
                        LeadDesc:  $("#desc").val(),
                        LeadName : $("#leadname").val(), 
                        Name : this.contactName,
                        Email : $("#email").val(),
                        CustomerId : custid,
                        Phone : $("#phone").val(),
                        MobilePhone : $("#mobilephone").val() ,
                        Town : $("#town").val() ,
                        Website : $("#website").val(),
                        Address : $("#address").val(),
                        City : $("#city").val(),
                        CreatedDate : $("#createdDate").val(),
                        SourceId : this.leadArray[0].selectedItem2,
                        UserId : this.leadArray[0].selectedItem,
                        StatusId : this.leadArray[0].selectedItem1,
                        CalledId : this.leadArray[0].selectedItem3,
                        Tags : this.items.toString(),
                        PossibilityId : this.leadArray[0].selectedItem4,
                        Position : $("#position").val(),
                        PrivateStatus : this.isChecked,
                        TeamId : this.leadArray[0].selectedItem5,
                        ContactId : parseInt(contactid),
                        CreatorId : this.leadArray[0].CreatorId
                      }
                      if(lead.Name == ""  || lead.SourceId == "" || lead.Phone == "" || lead.City == "" )
                      {
                        this.makeToast();
                        this.loading = false;
                      }
                      else 
                      {
                        if(lead.UserId == "")
                        {
                          lead.UserId = '0';
                        }
                      this._apicallservice.Add("http://localhost:61232/api/lead/Create",lead) 
                      .subscribe((result) => {
                        var leadId=parseInt(result[1].Value);
                        if(this.addedDynamicLineData.length != 0)
                        {
                          for(let i in this.addedDynamicLineData)
                          {
                              let _product = 0;
                              for(let j in this.productList)
                              {             
                                if(this.productList[j].label == this.addedDynamicLineData[i].ProductName)
                                {
                                  _product = this.productList[j].value;
                                }
                              }
                              let leadDetail = {Id : 0 ,LeadId : leadId , ProductId : _product,LineId : Number(i)+1, ProductName : this.addedDynamicLineData[i].ProductName , LineDesc : this.addedDynamicLineData[i].LineDesc  };
                              this._apicallservice.Add("http://localhost:61232/api/lead/CreateLeadDetail",leadDetail).subscribe((result) => {    
                                let resultArray = JSON.parse(JSON.stringify(result));
                                if (resultArray[0].Result == "True") {
                                  this.loading = false;
                                  this._router.navigate(['/pages/lead/get-lead']);
                                  this.makeToastSuccess();
                                }
                                else if (resultArray[0].Result == "Session_Expired") {
                                  this.loading = false;
                                  this._router.navigate(['/auth']);
                                  return;
                                }                    
                            }, )
                          }
                        }
                        else
                        {
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "True") {
                            this.loading = false;
                            this._router.navigate(['/pages/lead/get-lead']);
                            this.makeToastSuccess();
                          }
                          else if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }  
                        }                  
                      },)
                
                      }  

                    }                  
                },)
            }, )
          }
          else if (result[0].Result == "Session_Expired") {
            this.loading = false;
            this._router.navigate(['/auth']);
            return;
          }
        },
        )
      }
      else
      {
        if(this.contactid == "")
        {
          let user = {  UserId: 0, Fullname: this.contactName, Email: email,StatusId : 0};
          this._apicallservice.Add("http://localhost:61232/api/User/Create", user).subscribe((resultt) => {
              var userid = resultt[0].Result;
              let contact =  {CustomerId : this.customerid , Fullname : this.contactName , Phone : mphone , Email : email ,Position : $("#position").val() , UserId: userid  };
              this._apicallservice.Add("http://localhost:61232/api/Customer/CreateContact",contact).subscribe((result) => {
              var contactid=result[1].Value;
              let lead={
                LeadId: 0,
                LeadDesc:  $("#desc").val(),
                LeadName : $("#leadname").val(), 
                Name : this.contactName,
                Email : $("#email").val(),
                CustomerId : this.customerid,
                Phone : $("#phone").val(),
                MobilePhone : $("#mobilephone").val() ,
                Town : $("#town").val() ,
                Website : $("#website").val(),
                Address : $("#address").val(),
                City : $("#city").val(),
                CreatedDate : $("#createdDate").val(),
                SourceId : this.leadArray[0].selectedItem2,
                UserId : this.leadArray[0].selectedItem,
                StatusId : this.leadArray[0].selectedItem1,
                CalledId : this.leadArray[0].selectedItem3,
                Tags : this.items.toString(),
                PossibilityId : this.leadArray[0].selectedItem4,
                Position : $("#position").val(),
                PrivateStatus : this.isChecked,
                TeamId : this.leadArray[0].selectedItem5,
                ContactId : contactid,
                CreatorId : this.leadArray[0].CreatorId
              }
              if(lead.Name == ""  || lead.SourceId == "" || lead.Phone == "" || lead.City == "" )
              {
                this.makeToast();
                this.loading = false;
              }
              else 
              {
                if(lead.CustomerId == "")
                {
                  lead.CustomerId = '0';
                }
                if(lead.UserId == "")
                {
                  lead.UserId = '0';
                }
              this._apicallservice.Add("http://localhost:61232/api/lead/Create",lead) 
              .subscribe((result) => {
                var leadId=parseInt(result[1].Value);
                if(this.addedDynamicLineData.length != 0)
                {
                  for(let i in this.addedDynamicLineData)
                  {
                      let _product = 0;
                      for(let j in this.productList)
                      {             
                        if(this.productList[j].label == this.addedDynamicLineData[i].ProductName)
                        {
                          _product = this.productList[j].value;
                        }
                      }
                      let leadDetail = {Id : 0 ,LeadId : leadId , ProductId : _product,LineId : Number(i)+1, ProductName : this.addedDynamicLineData[i].ProductName , LineDesc : this.addedDynamicLineData[i].LineDesc  };
                      this._apicallservice.Add("http://localhost:61232/api/lead/CreateLeadDetail",leadDetail).subscribe((result) => {    
                        let resultArray = JSON.parse(JSON.stringify(result));
                        if (resultArray[0].Result == "True") {
                          this.loading = false;
                          this._router.navigate(['/pages/lead/get-lead']);
                          this.makeToastSuccess();
                        }
                        else if (resultArray[0].Result == "Session_Expired") {
                          this.loading = false;
                          this._router.navigate(['/auth']);
                          return;
                        }                    
                    }, )
                  }
                }
                else
                {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray[0].Result == "True") {
                    this.loading = false;
                    this._router.navigate(['/pages/lead/get-lead']);
                    this.makeToastSuccess();
                  }
                  else if (resultArray[0].Result == "Session_Expired") {
                    this.loading = false;
                    this._router.navigate(['/auth']);
                    return;
                  }  
                }                  
              },)
              }  
            }) 
          }) 
        }
        else
        {
          let lead={
            LeadId: 0,
            LeadDesc:  $("#desc").val(),
            LeadName : $("#leadname").val(), 
            Name : this._contactName,
            Email : $("#email").val(),
            CustomerId : this.customerid,
            Phone : $("#phone").val(),
            MobilePhone : $("#mobilephone").val() ,
            Town : $("#town").val() ,
            Website : $("#website").val(),
            Address : $("#address").val(),
            City : $("#city").val(),
            CreatedDate : $("#createdDate").val(),
            SourceId : this.leadArray[0].selectedItem2,
            UserId : this.leadArray[0].selectedItem,
            StatusId : this.leadArray[0].selectedItem1,
            CalledId : this.leadArray[0].selectedItem3,
            Tags : this.items.toString(),
            PossibilityId : this.leadArray[0].selectedItem4,
            Position : $("#position").val(),
            PrivateStatus : this.isChecked,
            TeamId : this.leadArray[0].selectedItem5,
            ContactId : this.contactid,
            CreatorId : this.leadArray[0].CreatorId
          }
          if(lead.Name == ""  || lead.SourceId == "" || lead.Phone == "" || lead.City == "" )
          {
            this.makeToast();
            this.loading = false;
          }
          else 
          {
            if(lead.CustomerId == "")
            {
              lead.CustomerId = '0';
            }
            if(lead.UserId == "")
            {
              lead.UserId = '0';
            }
          this._apicallservice.Add("http://localhost:61232/api/lead/Create",lead) 
          .subscribe((result) => {
            var leadId=parseInt(result[1].Value);
            if(this.addedDynamicLineData.length != 0)
            {
              for(let i in this.addedDynamicLineData)
              {
                  let _product = 0;
                  for(let j in this.productList)
                  {             
                    if(this.productList[j].label == this.addedDynamicLineData[i].ProductName)
                    {
                      _product = this.productList[j].value;
                    }
                  }
                  let leadDetail = {Id : 0 ,LeadId : leadId , ProductId : _product,LineId : Number(i)+1, ProductName : this.addedDynamicLineData[i].ProductName , LineDesc : this.addedDynamicLineData[i].LineDesc  };
                  this._apicallservice.Add("http://localhost:61232/api/lead/CreateLeadDetail",leadDetail).subscribe((result) => {    
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray[0].Result == "True") {
                      this.loading = false;
                      this._router.navigate(['/pages/lead/get-lead']);
                      this.makeToastSuccess();
                    }
                    else if (resultArray[0].Result == "Session_Expired") {
                      this.loading = false;
                      this._router.navigate(['/auth']);
                      return;
                    }                    
                }, )
              }
            }
            else
            {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray[0].Result == "True") {
                this.loading = false;
                this._router.navigate(['/pages/lead/get-lead']);
                this.makeToastSuccess();
              }
              else if (resultArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }  
            }                  
          },)
          }  
        }   
      }    
    }
    else if (this.title==this.sysLanguages.LeadEditText)
    {
      let editLead={
        LeadId: this.LeadID,
        LeadDesc:  $("#desc").val(),
        LeadName : $("#leadname").val(),
        Name :this._contactName,
        Email : $("#email").val(),
        CustomerId : this.customerid,
        Phone : $("#phone").val(),
        MobilePhone : $("#mobilephone").val() ,
        Town : $("#town").val() ,
        Website : $("#website").val(),
        Address : $("#address").val(),
        City : $("#city").val(),
        CreatedDate : $("#createdDate").val(),
        UserId : this.leadArray[0].selectedItem,
        StatusId : this.leadArray[0].selectedItem1,
        SourceId :this.leadArray[0].selectedItem2,
        CalledId : this.leadArray[0].selectedItem3,
        Tags : this.items.toString(),
        PossibilityId : this.leadArray[0].selectedItem4,
        Position : $("#position").val(),
        PrivateStatus :this.isChecked,
        TeamId : this.leadArray[0].selectedItem5,
        ContactId : this.contactid,
        CreatorId : this.leadArray[0].CreatorId
      }
      if(editLead.UserId == "")
      {
        editLead.UserId = '0';
      }
      if(editLead.Name == "" ||  editLead.UserId == "" || editLead.SourceId == "" || editLead.Phone == "" || editLead.City == "" )
      {
        this.makeToast();
        this.loading = false;
      }
      else 
      {
        this._apicallservice.Update("http://localhost:61232/api/lead/Edit",this.LeadID,editLead) 
         .subscribe((result) => {
          if(this.addedDynamicLineData.length != 0)
          {
            for(let i in this.addedDynamicLineData)
            {
                let _product = 0;
                for(let j in this.productList)
                {             
                  if(this.productList[j].label == this.addedDynamicLineData[i].ProductName)
                  {
                    _product = this.productList[j].value;
                  }
                }
                let leadDetail = {Id : 0 ,LeadId : this.LeadID , ProductId : _product,LineId : Number(i)+1, ProductName : this.addedDynamicLineData[i].ProductName , LineDesc : this.addedDynamicLineData[i].LineDesc  };
                this._apicallservice.Add("http://localhost:61232/api/lead/CreateLeadDetail",leadDetail).subscribe((result) => {    
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray[0].Result == "True") {
                    this.loading = false;
                    this._router.navigate(['/pages/lead/get-lead']);
                    this.makeToastSuccess();
                  }
                  else if (resultArray[0].Result == "Session_Expired") {
                    this.loading = false;
                    this._router.navigate(['/auth']);
                    return;
                  }                    
              }, )
            }
          }
          else
          {
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result == "True") {
              this.loading = false;
              this._router.navigate(['/pages/lead/get-lead']);
              this.makeToastSuccess();
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this.loading = false;
              this._router.navigate(['/auth']);
              return;
            } 
          }                   
         },
        )

      }

    }
  }

  createCustomer()
  {
    let editLead={
      LeadId: this.LeadID,
      LeadDesc:  $("#desc").val(),
      LeadName : $("#leadname").val(),
      Name :this._contactName,
      Email : $("#email").val(),
      CustomerId : this.customerid,
      Phone : $("#phone").val(),
      MobilePhone : $("#mobilephone").val() ,
      Town : $("#town").val() ,
      Website : $("#website").val(),
      Address : $("#address").val(),
      City : $("#city").val(),
      CreatedDate : $("#createdDate").val(),
      SourceId : this.leadArray[0].selectedItem2,
      UserId : this.leadArray[0].selectedItem,
      StatusId : 6,
      CalledId : this.leadArray[0].selectedItem3,
      Tags : this.items.toString(),
      PossibilityId : this.leadArray[0].selectedItem4,
      Position : $("#position").val(),
      PrivateStatus : this.isChecked,
      TeamId : this.leadArray[0].selectedItem5,
      ContactId : this.contactid,
      CreatorId : this.leadArray[0].CreatorId
    }

    if(editLead.LeadDesc == "" || editLead.Name == "" || editLead.Address == "" || editLead.Phone == "" || editLead.City == "" ||   editLead.SourceId == "" )
    {
      this.makeToast();
    }
    else 
    {
      this._apicallservice.Update("http://localhost:61232/api/lead/Edit",this.LeadID,editLead) 
       .subscribe((result) => {    
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this._router.navigate(['/pages/lead/get-lead']);
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }                    
       } )

    }

  }

  CallAgain()
  {
    let lead={
      LeadId: this.LeadID,
      LeadDesc:  $("#desc").val(),
      LeadName : $('leadname').val(),
      Name : this._contactName,
      Email : $("#email").val(),
      CustomerId :this.customerid,
      Phone : $("#phone").val(),
      MobilePhone : $("#mobilephone").val() ,
      Town : $("#town").val() ,
      Website : $("#website").val(),
      Address : $("#address").val(),
      City : $("#city").val(),
      CreatedDate : $("#createdDate").val(),
      SourceId : this.leadArray[0].selectedItem2,
      UserId : this.leadArray[0].selectedItem,
      StatusId : this.leadArray[0].selectedItem1,
      CalledId : this.leadArray[0].selectedItem3,
      Tags : this.items.toString(),
      PossibilityId : this.leadArray[0].selectedItem4,
      Position : $("#position").val(),
      PrivateStatus : this.isChecked,
      TeamId : this.leadArray[0].selectedItem5,
      ContactId : this.contactid,
      CreatorId : this.leadArray[0].CreatorId
    }
     this._apicallservice.Add("http://localhost:61232/api/lead/CreateCallTask",lead) 
     .subscribe((result) => {    
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this._router.navigate(['/pages/lead/get-lead']);
        }
        else if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }                    
     },)
 
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    this._apicallservice.Deactive("http://localhost:61232/api/Lead/RemoveLeadDetail", Number(this.LineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          document.getElementById("lineData_" + id).remove();
          this.LineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

}
