import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetLeadComponent } from './get-lead.component';

describe('GetLeadComponent', () => {
  let component: GetLeadComponent;
  let fixture: ComponentFixture<GetLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
