import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import {ExcelService} from "../../excel-service/excel.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'get-lead',
  templateUrl: './get-lead.component.html',
  providers:[ApicallService,HttpModule],
  styleUrls: ['./get-lead.component.scss'],
})
export class GetLeadComponent implements OnInit {

  filter =false;
  filteredLeadList = [];

  public LeadList = [];
  public allCustomers = [];
  public sysLanguages: any;
  public bargainCount = 0;
  public statusHot = 0;
  public statusStay = 0;
  public statusCustomer = 0;
  public statusCandidate = 0;
  public possibilityCount = 0;
  public callAgainCount = 0;
  public allLeadCount = 0;
  public statusLost =0;

  public LeadKanban = [{LeadId: "", LeadDesc: "", SourceDesc : "" , StatusId: "", assignedName:"", CustomerName: ""}];
  title : string = "";
  LeadID : number;
  public TaskList : any;
  public Statuss :any;

  public leadPrivate = [];
  public leadLost = [];

  leadStatusForFilter = "";

  ListToKanban = false;
  openKanbanText = "";
  CallAgainTaskCount = 0;
  isCheckedLost : any;
  statusCalled = [];
  userList = [];
  AdminUser = "";
  allUsers = [];
  selectedUser = 0;

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title, private excelService:ExcelService, private dialogService: NbDialogService) {
      
      this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
      this.titleService.setTitle(this.sysLanguages.LeadsText + " - Crmplus");

      this.settings.columns.LeadName.title = this.sysLanguages.LeadName;
      this.settings.columns.Name.title = this.sysLanguages.LeadTableNameText;
      this.settings.columns.Phone.title = this.sysLanguages.LeadTablePhoneText;
      this.settings.columns.CustomerName.title = this.sysLanguages.LeadTableCompanyText;
      this.settings.columns.Fullname.title = this.sysLanguages.LeadTableUserNameText;
      this.settings.columns.StatusDesc.title = this.sysLanguages.LeadTableStatusDescText;
      this.settings.columns.SourceDesc.title = this.sysLanguages.LeadTableSourceDescText;
      this.settings.columns.CreatedDate.title=this.sysLanguages.LeadCreatedDateText;
      this.settings.columns.PossibilityDesc.title=this.sysLanguages.LeadCalledStatus;
      this.settings.actions.columnTitle = this.sysLanguages.GrpActionText;
      this.settings.columns.taskCount.title = this.sysLanguages.CallStatusText;

      this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;

      this.getLeads();
      this.getStatus();
      this.getTasks();
      this.getCustomers();
      this.getUsers();

      this.statusCalled = [{value:0 , title : this.sysLanguages.CalledText },{value: 1 , title : this.sysLanguages.NotCalledText }]
      this.settings.columns.taskCount.filter.config.list = this.statusCalled;
      this.settings = Object.assign({}, this.settings);
      this.settings.columns.taskCount.filter.config.selectText = this.sysLanguages.BpAll;
      this.settings.columns.Fullname.filter.config.selectText = this.sysLanguages.BpAll;

      this.leadStatusForFilter = sessionStorage.getItem("leadStatus");
      sessionStorage.removeItem('leadStatus');

      this.AdminUser = localStorage.getItem("UserId");

  }

   settings = {
    columns: {
      LeadId: {
        title: 'ID',
        width: '2%',
      },
      LeadName: {
        title: ''
      },
      Name: {
        title: ''
      },
      Phone: {
        title: '',
      },
      CustomerName: {
        title: ''
      },
      Fullname: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: '',
                list: this.userList
              }
          }
      },
      StatusDesc: {
        title: '',
        width : '8%'
      },
      SourceDesc: {
        title: '',
        width : '4%'
      },
      CalledStatus: {
        title: 'Bound',
        width : '4%'
      },
      PossibilityDesc: {
        title: 'İhtimal',
        width: '3%'
      },
      Tags : {
        title : 'Tags', 
      },
      CreatedDate: {
        title: '',
        width: '5%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      taskCount : {
        title : '',
        type: 'html',
        valuePrepareFunction: (value) => {
          if(value == 0)
          {
            return '<p class="close">' + this.sysLanguages.CalledText + '</p>';
          }
          else if(value == 1)
          {
            return '<p class="open">' + this.sysLanguages.NotCalledText + '</p>';
          }
        },
        filter: {
          type: 'list',
            config: {
              selectText: '',
                list: this.statusCalled
              }
        }
      }
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };
   

  ngOnInit() {
    if(this.leadStatusForFilter != null)
    {
      if(parseInt(this.leadStatusForFilter) == 1)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 1);
          this.filteredLeadList=a;
        },1500);
        
      }
      else if(parseInt(this.leadStatusForFilter) == 2)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 2);
          this.filteredLeadList=a;
        },1500);
      }
      else if(parseInt(this.leadStatusForFilter) == 3)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 3);
          this.filteredLeadList=a;
        },1500);
      }
      else if(parseInt(this.leadStatusForFilter) == 4)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 4);
          this.filteredLeadList=a;
        },1500);
      }
      else if(parseInt(this.leadStatusForFilter) == 5)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 5);
          this.filteredLeadList=a;
        },1500);
      }
      else if(parseInt(this.leadStatusForFilter) == 6)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 6);
          this.filteredLeadList=a;
        },1500);
      }
      else if(parseInt(this.leadStatusForFilter) == 7)
      {
        setTimeout(()=>{
          this.filter = true;
          const a = this.LeadList.filter(x=>x.StatusId == 7);
          this.filteredLeadList=a;
        },1500);
      }
    }
  }

  getLeads() {
    this._apicallservice.GetAll("http://localhost:61232/api/lead/GetAllLead").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
        /*else if(resulttArray[0].Result == "Empty")
          {
            this.LeadList = [];
          }*/
        }
        else {
          this.leadLost=[];
          this.LeadList = [];
          this.leadPrivate = [];
          this.bargainCount = 0;
          this.statusCandidate = 0;
          this.statusHot = 0;
          this.statusStay = 0;
          this.statusCustomer = 0;
          this.possibilityCount = 0;
          this.callAgainCount = 0;
          this.statusLost = 0;
          var _onlineUser = resulttArray[0].onlineUser;
          this.allLeadCount = resulttArray.length;

          for(let i in resulttArray)
          {
            if(resulttArray[i].StatusId == 1)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartPossibility;
              this.possibilityCount++;
            }
            else if(resulttArray[i].StatusId == 2)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartCandidate;
              this.statusCandidate++;
            }
            else if(resulttArray[i].StatusId == 3)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartStatusStay;
              this.statusStay++;
            }
            else if(resulttArray[i].StatusId == 4)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartHotStatus;
              this.statusHot++;
            }
            else if(resulttArray[i].StatusId == 5)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartBargain;
              this.bargainCount++;
            }
            else if(resulttArray[i].StatusId == 6)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCustomerStatus;
              this.statusCustomer++;
            }
            else if(resulttArray[i].StatusId == 7)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartLost;
              this.statusLost++;
            }
            else if(resulttArray[i].StatusId == 8)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCallAgain;
            }

            if(resulttArray[i].SourceId == 1)
            {
              resulttArray[i].SourceDesc = this.sysLanguages.LeadSourceActivity;
            }
            else if(resulttArray[i].SourceId == 5)
            {
              resulttArray[i].SourceDesc = this.sysLanguages.LeadSourceReference;
            }
            else if(resulttArray[i].SourceId == 6)
            {
              resulttArray[i].SourceDesc = this.sysLanguages.LeadSourceWebForm;
            }
            else if(resulttArray[i].SourceId == 7)
            {
              resulttArray[i].SourceDesc = this.sysLanguages.LeadTablePhoneText;
            }

            if(resulttArray[i].PrivateStatus == 1 && resulttArray[i].UserId == _onlineUser || resulttArray[i].CreatorId == _onlineUser && resulttArray[i].StatusId != 7)
            {
              this.leadLost.push(resulttArray[i]);
            }
            else if(resulttArray[i].StatusId != 7 && resulttArray[i].PrivateStatus == 0)
            {
              this.leadLost.push(resulttArray[i]);
            }

            if(resulttArray[i].PrivateStatus == 1 && resulttArray[i].UserId == _onlineUser || resulttArray[i].CreatorId == _onlineUser )
            {
              this.leadPrivate.push(resulttArray[i]);
            }
            else if(resulttArray[i].PrivateStatus == 0)
            {
              this.leadPrivate.push(resulttArray[i]);
            }

          }

          /*this.possibilityCount = resulttArray.filter(x=>x.StatusId == 1).length;
          this.statusCandidate = resulttArray.filter(x=>x.StatusId == 2).length;
          this.statusStay = resulttArray.filter(x=>x.StatusId == 3).length;
          this.statusHot = resulttArray.filter(x=>x.StatusId == 4).length;
          this.bargainCount = resulttArray.filter(x=>x.StatusId == 5).length;
          this.statusCustomer = resulttArray.filter(x=>x.StatusId == 6).length;
          this.statusLost = resulttArray.filter(x=>x.StatusId == 7).length;*/
          /*const a = resulttArray.filter(x=>x.PrivateStatus == 0 && x.UserId == _onlineUser && x.StatusId != 7);
          const b = resulttArray.filter(x=>x.StatusId != 7 && x.PrivateStatus == 1);*/
          /*const c = resulttArray.filter(x=>x.PrivateStatus == 1);
          const d = resulttArray.filter(x=>x.PrivateStatus == 0 && x.UserId == _onlineUser );
          this.leadPrivate = c.concat(d);*/
          //this.leadLost = a.concat(b);

          this.LeadList=this.leadLost;  
        }
      }
    )
  }

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropLead(event)
  {
    var data = event.dataTransfer.getData("text2");

    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addLeadBtn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("lead_desc"))
    {
      //window.alert("You can't drop here");
    }   
    else if(event.target.className == "cardLeadDesc")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "assignedLeadCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "endDateleadCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "cardDetailButton")
    {
      //window.alert("You can't drop here");
    }
    else
    {
      var leadID = data.split('_')[1];
      var statusID = event.target.id.split('_')[1];
      
      let _lead = {LeadId: leadID, StatusId: statusID};

      this._apicallservice.Update("http://localhost:61232/api/Lead/ChangeLeadStatus", leadID, _lead).subscribe((result) => {
        this.getLeads();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  dragLead(event)
  {
     event.dataTransfer.setData("text", event.target.parentNode.id);
     event.dataTransfer.setData("text2", event.target.id);
  }

  getStatus()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/lead/GetStatusList").subscribe(
    (result) => {       
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          //
        }
      }
      else
      {
        for(let i in resulttArray)
        {
          if(resulttArray[i].StatusId == 1)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartPossibility;
          }
          else if(resulttArray[i].StatusId == 2)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartCandidate;
          }
          else if(resulttArray[i].StatusId == 3)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartStatusStay;
          }
          else if(resulttArray[i].StatusId == 4)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartHotStatus;
          }
          else if(resulttArray[i].StatusId == 5)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashboardLeadChartBargain;
          }
          else if(resulttArray[i].StatusId == 6)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCustomerStatus;
          }
          else if(resulttArray[i].StatusId == 7)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartLost;
          }
          else if(resulttArray[i].StatusId == 8)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.DashBoardLeadChartCallAgain;
          }          
        }
        
        this.Statuss = resulttArray;
      }
    },
    )
  }

  getTasks()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Task/GetLeadTasks").subscribe(
    (result) => {      
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          this.TaskList = [];
          this.CallAgainTaskCount = 0;
        }
      }
      else
      {
        this.TaskList=resulttArray;
        this.CallAgainTaskCount = resulttArray.length;
      }
    },
    )
  }

  getUsers()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {     
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if (resulttArray[0].Result == "Empty") {
          this.userList = [];
          this.allUsers = [];
        }
      }
      else
      { 
        for(var i = 0; i < resulttArray.length; i++ ){
          this.userList.push({value:resulttArray[i].Fullname,title:resulttArray[i].Fullname});
          let _users = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
          this.allUsers.push(_users);
        }
        this.settings.columns.Fullname.filter.config.list = this.userList;
        this.settings = Object.assign({}, this.settings);
      }
    },
    )
  }

  LostLead(event)
  {
    if(event == 1)
    {
      this.LeadList=this.leadPrivate;
    }
    else if (event == 0) 
    {
      this.LeadList=this.leadLost;
    }
  }

  openKanban()
  {
    if(this.openKanbanText == this.sysLanguages.TasksBackToKanbanButtonText)
    {
      this.ListToKanban = true;
      this.openKanbanText = this.sysLanguages.TasksListButtonText;
      this.LeadKanban = [];

      for(let i in this.Statuss) 
      {
        for(let j in this.LeadList)
        {
          if(this.LeadList[j].StatusId == this.Statuss[i].StatusId)
          {
            let leadlistkanban = {LeadId: this.LeadList[j].LeadId, LeadDesc: this.LeadList[j].LeadDesc, SourceDesc: this.LeadList[j].SourceDesc , StatusId: this.LeadList[j].StatusId, assignedName: this.LeadList[j].Fullname.substring(0, 2), CustomerName: this.LeadList[j].CustomerName};
            this.LeadKanban.push(leadlistkanban);
          }
        }
      }
    }
    else if (this.openKanbanText == this.sysLanguages.TasksListButtonText)
    {
      this.ListToKanban = false;
      this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;
      this.getLeads();
    }   
  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
        }
        else
        {
          this.allCustomers = resultArray;
        }
      }
    )
  }

  deleteLeadTask(event)
 {
   //var id = ${event.data.TaskId};
   var ans = confirm(this.sysLanguages.TpTaskDeleteText + event);
   if (ans) {
     this._apicallservice.Deactive("http://localhost:61232/api/Task/DeleteTask", event).subscribe((result) => {
       let resultArray = JSON.parse(JSON.stringify(result));
       if(resultArray[0].Result == "True")
       {
         this.getTasks();
         this._router.navigate(['/pages/lead/get-lead']);
       }
       else if(resultArray[0].Result == "Session_Expired")
       {
         this._router.navigate(['/auth']);
         return;
       }
     },)
   }
 }

  addLead()
  {
    this._router.navigate(['/pages/lead/add-edit-lead']);
  }

  editLead(event)
  {
    var leadid = `${event.data.LeadId}`;
    var companyname =  `${event.data.CustomerName}`;
    var status = `${event.data.StatusId}`;
    if(parseInt(status) == 6)
    { 
      for(let i in this.allCustomers)
      {
        if(this.allCustomers[i].CustomerName == companyname)
        {
          this._router.navigate(['/pages/customer/customer-overview',parseInt(this.allCustomers[i].CustomerId)]);
        }
      }
    }
    else
    {
      this._router.navigate(['/pages/lead/lead-overview',parseInt(leadid)]);
    }
  }
  
  GoLeadDetail(event)
  {
    this._router.navigate(['/pages/lead/add-edit-lead',event]);
  }

  createNewCallTask(taskid)
  {
    let task={
      TaskId: taskid,
      ParentId:0,
      Title: "",
      TaskDesc: "",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: 5,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0
    };
    this._apicallservice.Update("http://localhost:61232/api/Task/UpdateCallTask", parseInt(taskid), task).subscribe((result) => {
      if (result[0].Result == "True") {
        this.getTasks();
      }
      else if (result[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }
    },
    )
    

  }

  filterLead(value)
  {
    this.filter= true;
    if(value==1)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 1);
      this.filteredLeadList=a;
    }
    else if(value==2)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 2);
      this.filteredLeadList=a;
    }
    else if(value==3)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 3);
      this.filteredLeadList=a;
    }
    else if(value==4)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 4);
      this.filteredLeadList=a;
    }
    else if(value==5)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 5);
      this.filteredLeadList=a;
    }
    else if(value==6)
    {
      const a = this.LeadList.filter(x=>x.StatusId == 6);
      this.filteredLeadList=a;
    }
    else if(value==7)
    {
      const a = this.leadPrivate.filter(x=>x.StatusId == 7);
      this.filteredLeadList=a;
    }
    else if(value ==0)
    {
      this.filter = false;
    }
  }

  goToCustomerOverview(customerId)
  {
    this._router.navigate(['/pages/customer/customer-overview',customerId]);
  }

  openExportArea(dialog: TemplateRef<any>)
  {
    this.selectedUser = 0;
    $("#filterDate").val("");
    this.dialogService.open(dialog);
  }

  filterDate()
  {
    var x = $("#filterDatePublic").val().toString();
    var startDate = this.formatDate(x.split(" - ")[0]);
    var endDate = this.formatDate(x.split("-")[1].substring(1));
    const a = this.LeadList.filter(x=>x.CreatedDate.split(" ")[0] < endDate && x.CreatedDate.split(" ")[0] >= startDate);
    this.filter = true;
    this.filteredLeadList = a;
  }
  
  filterUserForExport(event)
  {
    this.filter = true;
    const a = this.LeadList.filter(x=>x.UserId == event);
    this.filteredLeadList = a;
  }

  exportExcel(ref)
  {
    var x = $("#filterDate").val().toString();
    if(x != "")
    {
      var startDate = this.formatDate(x.split(" - ")[0]);
      var endDate = this.formatDate(x.split("-")[1].substring(1));
      const a = this.LeadList.filter(x=>x.CreatedDate.split(" ")[0] < endDate && x.CreatedDate.split(" ")[0] >= startDate);
      this.filter = true;
      this.filteredLeadList = a;
    }

    if(this.filter == true)
    {
      this.excelService.exportAsExcelFile(this.filteredLeadList, this.sysLanguages.LeadsText);
      ref.close();
      this.filter = false;
    }
    else
    {
      this.excelService.exportAsExcelFile(this.LeadList, this.sysLanguages.LeadsText);
      ref.close();
      this.filter = false;
    }
  }

  exportPdf(ref)
  {
    var x = $("#filterDate").val().toString();
    if(x != "")
    {
      var startDate = this.formatDate(x.split(" - ")[0]);
      var endDate = this.formatDate(x.split("-")[1].substring(1));
      const a = this.LeadList.filter(x=>x.CreatedDate.split(" ")[0] < endDate && x.CreatedDate.split(" ")[0] >= startDate);
      this.filter = true;
      this.filteredLeadList = a;
    }

    var doc = new jsPDF("l","mm","a4");
    var col = ["LeadDesc", "Name","Phone","Responsible","StatusDesc","CratedDate","Customer","CalledStatus"];
    var rows = [];
    if(this.filter == true)
    {
      var itemNew = this.filteredLeadList;
    }
    else
    {
      var itemNew = this.LeadList;
    }
    
   itemNew.forEach(element => {
        if(element.taskCount == 0)
        {
          var calledStatus = this.sysLanguages.CalledText.toUpperCase();
        }
        else if(element.taskCount == 1)
        {
          var calledStatus = this.sysLanguages.NotCalledText.toUpperCase();
        }
        var temp = [element.LeadDesc, element.Name.toUpperCase(), element.Phone, element.Fullname.toUpperCase(), element.StatusDesc.toUpperCase(), element.CreatedDate.split(" ")[0], element.CustomerName.toUpperCase(), calledStatus];
        rows.push(temp);
    });        

    doc.autoTable(col, rows,{
        columnStyles: {
        0: {columnWidth: 60},
        1: {columnWidth: 40},
        2: {columnWidth: 30},
        3: {columnWidth: 30},
        4: {columnWidth: 35},
        5: {columnWidth: 25},
        6: {columnWidth: 30},
        7: {columnWidth: 25}
        },
        theme : "grid"
      }
    );
    doc.save(this.sysLanguages.LeadsText + ".pdf");
    ref.close();
    this.filter = false;
  }

  formatDate(date)
  {
    var splittedDate = date.split(" ");
    let val = "";
    if (splittedDate[0] == "Jan")
    {
        val = "01";
    }
    else if (splittedDate[0] == "Feb")
    {
        val = "02";
    }
    else if (splittedDate[0] == "Mar")
    {
        val = "03";
    }
    else if (splittedDate[0] == "Apr")
    {
        val = "04";
    }
    else if (splittedDate[0] == "May")
    {
        val = "05";
    }
    else if (splittedDate[0] == "Jun")
    {
        val = "06";
    }
    else if (splittedDate[0] == "Jul")
    {
        val = "07";
    }
    else if (splittedDate[0] == "Aug")
    {
        val = "08";
    }
    else if (splittedDate[0] == "Sep")
    {
        val = "09";
    }
    else if (splittedDate[0] == "Oct")
    {
        val = "10";
    }
    else if (splittedDate[0] == "Nov")
    {
        val = "11";
    }
    else if (splittedDate[0] == "Dec")
    {
        val = "12";
    }

    var year = splittedDate[2];
    var day = splittedDate[1].substring(0,splittedDate[1].length - 1);
    if(day.length == 1)
    {
      day = "0"+day
    }
    return year + "-" + val + "-" + day;

  }
  
}
