import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { NbDialogService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'lead-overview',
  templateUrl: './lead-overview.component.html',
  styleUrls: ['./lead-overview.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class LeadOverviewComponent implements OnInit {
  
  sysLanguages: any;
  public leadArray = [{ LeadDesc:' ', Name:' ', Email:' ', Company:' ', Address:' ', Phone:' ', City:' ', Website:' ', CreatedDate:' ', Fullname:'', CalledStatus:'', SourceDesc:' ', StatusDesc: '', MobilePhone : '',Town: '',PossibilityDesc : '', Tags:'' ,UserId :0, StatusId: 0, SourceId: 0 }];
  leadId : any;

  notcompletedTaskCount = 0;
  completedTaskCount = 0;
  notcompletedtaskList = [];
  completedTaskList = [] ;
  callAgainCount = 0;

  productList = [];
  loading = false;

  emptyComment = false;
  CommentList = [];

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router,private dialogService: NbDialogService, private titleService: Title, private toastrService: NbToastrService) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.LeadOverviewText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.leadId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title = this.sysLanguages.LeadInformation;
    this.tabs[1].title = this.sysLanguages.ProposalText;

    this.settings.columns.ProductName.title = this.sysLanguages.AddInvoiceTableProduct;
    this.settings.columns.LineDesc.title = this.sysLanguages.AddInvoiceTableDesc;

    this.getTasks();
    this.getLeadProdcut();
    this.getComments();

   }

   tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/lead/lead-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      route: ['/pages/lead/lead-proposals', this._avRoute.snapshot.params["id"]],
    },
  ];

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  settings = {
    columns: {
      ProductName: {
        title: ''
      },
      LineDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };
  
  ngOnInit() {

    this._apicallService.Get("http://localhost:61232/api/lead/DetailsForLeadOverview",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {

            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.leadArray[0].LeadDesc = resulttArray[0].LeadDesc;
            this.leadArray[0].Name = resulttArray[0].Name;
            this.leadArray[0].Address=resulttArray[0].Address;
            this.leadArray[0].Email=resulttArray[0].Email;
            this.leadArray[0].Phone=resulttArray[0].Phone;
            this.leadArray[0].City=resulttArray[0].City;
            this.leadArray[0].MobilePhone=resulttArray[0].MobilePhone;
            this.leadArray[0].Town=resulttArray[0].Town;
            this.leadArray[0].Company=resulttArray[0].CustomerName;
            this.leadArray[0].Website=resulttArray[0].Website;
            this.leadArray[0].StatusId = resulttArray[0].StatusId;
            this.leadArray[0].SourceId = resulttArray[0].SourceId;
            this.leadArray[0].CreatedDate= resulttArray[0].CreatedDate.split(" ")[0];
            this.leadArray[0].Fullname =resulttArray[0].Fullname;
            this.leadArray[0].CalledStatus=resulttArray[0].CalledStatus;
            this.leadArray[0].PossibilityDesc=resulttArray[0].PossibilityDesc.slice(1,4);
            this.leadArray[0].Tags = resulttArray[0].Tags;
            this.leadArray[0].UserId = resulttArray[0].UserId;
            
            if(this.leadArray[0].StatusId == 1)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashboardLeadChartPossibility;
            }
            else if(this.leadArray[0].StatusId == 2)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashboardLeadChartCandidate;
            }
            else if(this.leadArray[0].StatusId == 3)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashboardLeadChartStatusStay;
            }
            else if(this.leadArray[0].StatusId == 4)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashBoardLeadChartHotStatus;
            }
            else if(this.leadArray[0].StatusId == 5)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashboardLeadChartBargain;
            }
            else if(this.leadArray[0].StatusId == 6)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashBoardLeadChartCustomerStatus;
            }
            else if(this.leadArray[0].StatusId == 7)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashBoardLeadChartLost;
            }
            else if(this.leadArray[0].StatusId == 8)
            {
              this.leadArray[0].StatusDesc = this.sysLanguages.DashBoardLeadChartCallAgain;
            } 

            
            if(this.leadArray[0].SourceId == 1)
            {
              this.leadArray[0].SourceDesc = this.sysLanguages.LeadSourceActivity;
            }
            else if(this.leadArray[0].SourceId == 5)
            {
              this.leadArray[0].SourceDesc = this.sysLanguages.LeadSourceReference;
            }
            else if(this.leadArray[0].SourceId == 6)
            {
              this.leadArray[0].SourceDesc = this.sysLanguages.LeadSourceWebForm;
            }
            else if(this.leadArray[0].SourceId == 7)
            {
              this.leadArray[0].SourceDesc = this.sysLanguages.LeadTablePhoneText;
            }
            else
            {
              this.leadArray[0].SourceDesc = resulttArray[0].SourceDesc;
            }


            if(this.leadArray[0].LeadDesc == "")
            {
              this.leadArray[0].LeadDesc = this.sysLanguages.NotValueText;
            }
            if(this.leadArray[0].Address == "")
            {
              this.leadArray[0].Address = this.sysLanguages.NotValueText;
            }
            if(this.leadArray[0].Email == "")
            {
              this.leadArray[0].Email = this.sysLanguages.NotValueText;
            }
            if(this.leadArray[0].City == "")
            {
              this.leadArray[0].City = this.sysLanguages.NotValueText;
            }
            if(this.leadArray[0].Town == "")
            {
              this.leadArray[0].Town = this.sysLanguages.NotValueText;
            }
            if(this.leadArray[0].Company == "NoCustomer")
            {
              this.leadArray[0].Company = this.sysLanguages.NoCustomerText;
            }
            if(this.leadArray[0].Tags == "")
            {
              this.leadArray[0].Tags = this.sysLanguages.NotValueText;
            }
          }       
      },
      )
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/lead/add-edit-lead', this._avRoute.snapshot.params["id"]]);
  }

  getTasks()
  {
    this._apicallService.Get("http://localhost:61232/api/Lead/GetLeadByTasks",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if(resulttArray[0].Result == "Empty")
          {
            this.notcompletedtaskList = [];
            this.notcompletedTaskCount = 0;
            this.completedTaskList = [];
            this.completedTaskCount = 0;
          }
          else if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
        }
        } 
        else
        {
          this.callAgainCount=0;
          this.notcompletedtaskList= [];
          this.completedTaskList=[];

          for(var i in resulttArray)
          {
            if(resulttArray[i].StatusId != 5 && resulttArray[i].StatusId != 6)
            {
              this.notcompletedtaskList.push(resulttArray[parseInt(i)]);
            }
            else if (resulttArray[i].StatusId == 5)
            {
              this.completedTaskList.push(resulttArray[parseInt(i)]);
            }

            if (resulttArray[i].StatusId == 5 && resulttArray[i].TaskDesc == "Tekrar Ara" )
            {
              this.callAgainCount++;
            }
          }

          this.notcompletedTaskCount = this.notcompletedtaskList.length;
          this.completedTaskCount = this.completedTaskList.length;
        }       
    },
    )
  }

  getLeadProdcut()
  {
    this._apicallService.Get("http://localhost:61232/api/Lead/GetLeadProduct",this.leadId).subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productList = [];
            }
          }
          else
          {
            this.productList = resultArray;
          }
      }
    )
  }

  addTask()
  {
    sessionStorage.setItem("taskRelation","lead_"+this.leadId);
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  completeTask(id)
  {
    let task={ TaskId : id, TaskComplete : "0.5", StatusId : 5 };
    this._apicallService.Update("http://localhost:61232/api/Task/TaskComplete", id, task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getTasks();
      }
      else if (resultArray[0].Result == "False") {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.TasksToastrCantCompleted);
      }
      else if (resultArray[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }        
    }, 
    )
  }

  createNewCallTask(taskid)
  {
    let task={
      TaskId: taskid,
      ParentId:0,
      Title: "",
      TaskDesc: "",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: 5,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    this._apicallService.Update("http://localhost:61232/api/Task/UpdateCallTask", parseInt(taskid), task).subscribe((result) => {
      if (result[0].Result == "True") {
        this.getTasks();
      }
      else if (result[0].Result == "Session_Expired") {
        this._router.navigate(['/auth']);
        return;
      }
    },
    )
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Lead/LeadComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : this.leadId,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }


  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  
}
