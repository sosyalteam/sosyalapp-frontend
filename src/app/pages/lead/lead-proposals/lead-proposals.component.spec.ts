import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadProposalsComponent } from './lead-proposals.component';

describe('LeadProposalsComponent', () => {
  let component: LeadProposalsComponent;
  let fixture: ComponentFixture<LeadProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
