import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'lead-proposals',
  templateUrl: './lead-proposals.component.html',
  styleUrls: ['./lead-proposals.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class LeadProposalsComponent implements OnInit {

  sysLanguages: any;
  proposalList : any;
  leadId : any;

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.LeadsProposalText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.leadId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title=this.sysLanguages.LeadInformation;
    this.tabs[1].title = this.sysLanguages.ProposalText;

    this.settings.columns.Subject.title=this.sysLanguages.PcSubject;
    this.settings.columns.CreatedDate.title=this.sysLanguages.LogTableCreatedDate;
    this.settings.columns.ExpiredDate.title=this.sysLanguages.PcProposalExpirationDate;
    this.settings.columns.StatusDesc.title=this.sysLanguages.BoCardStatusText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
  }

  settings = {
    columns: {
      ProposalId: {
        title: 'ID',
        width: '2%'
      },
      Subject: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      ExpiredDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };


  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/lead/lead-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      route: ['/pages/lead/lead-proposals', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {

    this._apicallService.Get("http://localhost:61232/api/lead/GetLeadByProposals",this._avRoute.snapshot.params["id"])
    .subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if(resulttArray[0].Result == "Empty")
          {
            this.proposalList=[];
          }
          else if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
        }
        } 
        else
        {
          this.proposalList = resulttArray;

          for(let i in this.proposalList)
          {
            if(this.proposalList[i].StatusId == 1)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcDraftStatus;
            }
            else if(this.proposalList[i].StatusId == 2)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcSentStatus;
            }
            else if(this.proposalList[i].StatusId == 3)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcOpenStatus;
            }
            else if(this.proposalList[i].StatusId == 4)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcRevizeStatus;
            }
            else if(this.proposalList[i].StatusId == 5)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcRefusedStatus;
            }
            else if(this.proposalList[i].StatusId == 6)
            {
              this.proposalList[i].StatusDesc == this.sysLanguages.PcAcceptedStatus;
            }
          }
        }       
    },

    )
  }


  openEditUrl()
  {
    this._router.navigate(['/pages/lead/add-edit-lead', this._avRoute.snapshot.params["id"]]);
  }

  openProposal(event)
  {
    this._router.navigate(['/pages/proposals/add-edit-proposal', event.data.ProposalId]);
  }

  addProposal()
  {
    sessionStorage.setItem('leadId', this.leadId);
    this._router.navigate(['/pages/proposals/add-edit-proposal']);
  }

}
