import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetLeadComponent } from './get-lead/get-lead.component';
import { NbDatepickerModule } from '@nebular/theme';
import { LeadOverviewComponent } from './lead-overview/lead-overview.component';
import { AddEditLeadComponent } from './add-edit-lead/add-edit-lead.component';
import { LeadProposalsComponent } from './lead-proposals/lead-proposals.component';
import { LeadNotesComponent } from './lead-notes/lead-notes.component';

const routes: Routes = [
  {
    path: 'get-lead',
    component: GetLeadComponent,
  },
  {
    path: 'lead-overview/:id',
    component: LeadOverviewComponent,
  },
  {
    path: 'add-edit-lead',
    component: AddEditLeadComponent,
  },
  {
    path: 'add-edit-lead/:id',
    component: AddEditLeadComponent,
  },
  {
    path: 'lead-proposals/:id',
    component: LeadProposalsComponent,
  },
  {
    path: 'lead-notes/:id',
    component: LeadNotesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes),NbDatepickerModule.forRoot()],
  exports: [RouterModule]
})
export class LeadRoutingModule { }
