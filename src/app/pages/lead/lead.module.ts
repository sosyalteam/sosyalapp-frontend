import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeadRoutingModule } from './lead-routing.module';
import { GetLeadComponent } from './get-lead/get-lead.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbCardModule, NbInputModule, NbDatepickerModule, NbSelectModule, NbTabsetModule, NbListModule, NbContextMenuModule, NbRouteTabsetModule, NbProgressBarModule, NbDialogModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Select2Module } from 'ng2-select2';
import { TagInputModule } from 'ngx-chips';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { LeadOverviewComponent } from './lead-overview/lead-overview.component';
import { AddEditLeadComponent } from './add-edit-lead/add-edit-lead.component';
import { LeadProposalsComponent } from './lead-proposals/lead-proposals.component';
import { LeadNotesComponent } from './lead-notes/lead-notes.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [ GetLeadComponent, LeadOverviewComponent, AddEditLeadComponent, LeadProposalsComponent, LeadNotesComponent],
  imports: [
    CommonModule,
    LeadRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbDatepickerModule,
    TagInputModule, 
    DropdownModule,
    MultiSelectModule,
    NbTabsetModule,
    NbListModule,
    AutocompleteLibModule,
    NbContextMenuModule,
    NbRouteTabsetModule,
    NbProgressBarModule,
    NgxPaginationModule,
    NbDialogModule.forRoot(),
  ]
})
export class LeadModule { }
