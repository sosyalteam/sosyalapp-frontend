import { Component, OnInit } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { ApicallService } from '../../apicall.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'get-log',
  templateUrl: './get-log.component.html',
  styleUrls: ['./get-log.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class GetLogComponent implements OnInit {

  public sysLanguages: any;
  public LogList: any; 

  constructor(public http: Http, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) {
    
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle("Log - Crmplus");

    this.settings.columns.AdditionalInfo.title = this.sysLanguages.LogTableInfo;
    this.settings.columns.CreatedDate.title = this.sysLanguages.LogTableCreatedDate;
    this.settings.columns.Fullname.title = this.sysLanguages.LogTableUser;
    this.settings.columns.ModuleId.title = this.sysLanguages.LogTableModule;
    this.settings.columns.EventLogLevelId.title = this.sysLanguages.LogTableLevel;
    this.settings.actions.columnTitle = this.sysLanguages.ArActionText;
    this.getLogs();
  }

  settings = {
    columns: {
      EventTypeId: {
        title: 'ID',
        width:'2%',
      },
      AdditionalInfo: {
        title: ''
      },
      Fullname: {
        title: ''
      },
      CreatedDate: {
        title: '',
        width:'15%',
      },
      ModuleId: {
        title: '',
        width:'2%',
      },
      EventLogLevelId: {
        title: '',
        width:'2%',
      },
    },
    actions:{
      position:'right',
      add:false,
      edit:false,
      delete:false,
      columnTitle: ""
    },
  };
   
  ngOnInit() {
  }

  getLogs() {
    this._apicallservice.GetAll("http://localhost:61232/api/Log/Get").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.LogList = [];
          }
        }
        else {
          this.LogList = result;
        }
      }
    )
  }  

}
