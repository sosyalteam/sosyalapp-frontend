import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetLogComponent } from './get-log/get-log.component';

const routes: Routes = [
  {
    path: 'get-log',
    component: GetLogComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogRoutingModule { }
