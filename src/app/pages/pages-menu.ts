import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: '',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: '',
    icon: 'nb-star',
    link: 'customer/',
  },
  {
    title:'',
    icon: 'nb-bar-chart',
    children:[
      {
        title: '',
        icon: '',
        link: 'lead/get-lead',
      },
      {
        title:'',
        icon: '',
        link:'proposals/'
      },
      {
        title:'',
        icon: '',
        link:'proforms/'
      },
      {
        title:'',
        icon: '',
        link:'invoices/'
      },
      {
        title:'',
        icon: '',
        link:'products/'
      },
    ]
  },
  {
    title: '',
    icon: 'ion-document-text',
    link: '/pages/adcosts/',
  },
  {
    title: '',
    icon: 'ion-document-text',
    link: '/pages/costs/',
  },
  {
    title: '',
    icon: 'nb-list',
    link: '/pages/project/',
  },
  {
    title: '',
    icon: 'nb-compose',
    link: '/pages/task/tasks',
    children:[
      {
        title:'',
        icon: '',
        link: '/pages/task/tasks',
      },
      {
        title: '',
        icon: '',
        link: '/pages/activities',
      },
    ]
  },
  {
    title: '',
    icon: 'nb-help',
    link: 'support/',
  },
  {
    title: '',
    icon: 'nb-person',
    link: 'user/get-user',
  },
  {
    title: '',
    icon: 'nb-grid-a-outline',
    link: 'group/get-group',
  },
  {
    title: '',
    icon: 'nb-shuffle',
    link: 'accessright/get-right',
  },
  {
    title: '',
    icon: 'nb-power-circled',
    link: 'log/get-log',
  },
  
];
