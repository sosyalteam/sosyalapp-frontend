import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Syslanguages } from './syslanguages/syslanguages';
import { DashboardchartService} from './dashboard/dashboardchart.service';
import { DashboardProjectService } from './dashboard/dashboard-project.service';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  resolve: {
    lang: Syslanguages,
    lead: DashboardchartService,
    project :DashboardProjectService
   }, 
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'user',
      loadChildren: './user/user.module#UserModule'
    },
	{
      path: 'group',
      loadChildren: './group/group.module#GroupModule'
    },
    {
      path: 'log',
      loadChildren: './log/log.module#LogModule'
    },
    {
      path: 'accessright',
      loadChildren: './accessright/accessright.module#AccessrightModule' 
    },
    {
      path: 'task',
      loadChildren: './task/task.module#TaskModule'
    },
    {
      path: 'project',
      loadChildren: './project/project.module#ProjectModule'
    },
    {
      path: 'lead',
      loadChildren: './lead/lead.module#LeadModule'
    },
    {
      path: 'customer',
      loadChildren: './customer/customer.module#CustomerModule'
    },
    {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule'
    },
    {
      path: 'support',
      loadChildren: './support/support.module#SupportModule'
    },
    {
      path: 'invoices',
      loadChildren: './invoices/invoices.module#InvoicesModule'
    },
    {
      path: 'proforms',
      loadChildren: './proforms/proforms.module#ProformsModule'
    },
    {
      path: 'proposals',
      loadChildren: './proposals/proposals.module#ProposalsModule'
    },
    {
      path: 'products',
      loadChildren: './products/products.module#ProductsModule'
    },
    {
      path: 'activities',
      loadChildren: './activities/activities.module#ActivitiesModule'
    },
    {
      path: 'costs',
      loadChildren: './cost/cost.module#CostModule'
    },
    {
      path: 'adcosts',
      loadChildren: './adcosts/adcosts.module#AdcostsModule'
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [Syslanguages,DashboardchartService,DashboardProjectService],
})
export class PagesRoutingModule {
}
