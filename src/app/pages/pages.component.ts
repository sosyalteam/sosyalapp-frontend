import { Component } from '@angular/core';

import { Syslanguages } from './syslanguages/syslanguages';
import { ActivatedRoute } from '@angular/router';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu *ngIf = "showMenu == true" [items]="menu"></nb-menu>
      <nb-menu *ngIf = "showMenu == false" [items]="copyMenu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})

export class PagesComponent {

  public sysLanguages : any;
  menu = [];
  copyMenu = [];
  AdminUser = "";
  showMenu = false;

  MENU_ITEMS: NbMenuItem[] = [
  {
    title: '',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: '',
    icon: 'nb-star',
    link: 'customer/',
  },
  {
    title:'',
    icon: 'nb-bar-chart',
    children:[
      {
        title: '',
        icon: '',
        link: 'lead/get-lead',
      },
      {
        title:'',
        icon: '',
        link:'proposals/'
      },
      {
        title:'',
        icon: '',
        link:'proforms/'
      },
      {
        title:'',
        icon: '',
        link:'invoices/'
      },
      {
        title:'',
        icon: '',
        link:'products/'
      },
    ]
  },
  {
    title: '',
    icon: 'ion-document-text',
    link: '/pages/adcosts/',
  },
  {
    title: '',
    icon: 'ion-document-text',
    link: '/pages/costs/',
  },
  {
    title: '',
    icon: 'nb-list',
    link: '/pages/project/',
  },
  {
    title: '',
    icon: 'nb-compose',
    link: '/pages/task/tasks',
    children:[
      {
        title:'',
        icon: '',
        link: '/pages/task/tasks',
      },
      {
        title: '',
        icon: '',
        link: '/pages/activities',
      },
    ]
  },
  {
    title: '',
    icon: 'nb-help',
    link: 'support/',
  },
  {
    title: '',
    icon: 'nb-person',
    link: 'user/get-user',
  },
  {
    title: '',
    icon: 'nb-grid-a-outline',
    link: 'group/get-group',
  },
  {
    title: '',
    icon: 'nb-shuffle',
    link: 'accessright/get-right',
  },
  {
    title: '',
    icon: 'nb-power-circled',
    link: 'log/get-log',
  },
  
];

  constructor(private _sysLanguage: Syslanguages, private actr: ActivatedRoute) {

    this.sysLanguages = this.actr.snapshot.data['lang'];

    this.AdminUser = localStorage.getItem("UserId");

    if(this.AdminUser == "1" || this.AdminUser == "3" || this.AdminUser == "4" || this.AdminUser == "23" )
    {
      this.showMenu = true;
      this.menu = this.MENU_ITEMS;
      this.menu[0].title = this.sysLanguages.HomeText;
      this.menu[1].title = this.sysLanguages.CustomerText;
      this.menu[2].title = this.sysLanguages.SaleText;
      this.menu[2].children[0].title =this.sysLanguages.LeadText;
      this.menu[2].children[1].title=this.sysLanguages.ProposalText;
      this.menu[2].children[2].title=this.sysLanguages.ProformText;
      this.menu[2].children[3].title=this.sysLanguages.InvoiceText;
      this.menu[2].children[4].title=this.sysLanguages.ProductText;
      this.menu[3].title = this.sysLanguages.AdCosts;
      this.menu[4].title = this.sysLanguages.Costs;
      this.menu[5].title = this.sysLanguages.ProjectsSubscriptionText;
      this.menu[6].title = this.sysLanguages.TasksText;
      this.menu[6].children[0].title=this.sysLanguages.TasksText;
      this.menu[6].children[1].title =this.sysLanguages.ActivitiesText;
      this.menu[7].title = this.sysLanguages.SupportText;
      this.menu[8].title = this.sysLanguages.UserText;
      this.menu[9].title = this.sysLanguages.GroupText;
      this.menu[10].title = this.sysLanguages.AccessRightText;
      this.menu[11].title = this.sysLanguages.LogText;
    }
    else
    {
      this.showMenu = false;
      this.MENU_ITEMS[2].children.splice(0,1);
      this.copyMenu = this.MENU_ITEMS;
      this.copyMenu[0].title = this.sysLanguages.HomeText;
      this.copyMenu[1].title = this.sysLanguages.CustomerText;
      this.copyMenu[2].title = this.sysLanguages.SaleText;
      this.copyMenu[2].children[0].title=this.sysLanguages.ProposalText;
      this.copyMenu[2].children[1].title=this.sysLanguages.ProformText;
      this.copyMenu[2].children[2].title=this.sysLanguages.InvoiceText;
      this.copyMenu[2].children[3].title=this.sysLanguages.ProductText;
      this.copyMenu[3].title = this.sysLanguages.AdCosts;
      this.copyMenu[4].title = this.sysLanguages.Costs;
      this.copyMenu[5].title = this.sysLanguages.ProjectsSubscriptionText;
      this.copyMenu[6].title = this.sysLanguages.TasksText;
      this.copyMenu[6].children[0].title=this.sysLanguages.TasksText;
      this.copyMenu[6].children[1].title =this.sysLanguages.ActivitiesText;
      this.copyMenu[7].title = this.sysLanguages.SupportText;
      this.copyMenu[8].title = this.sysLanguages.UserText;
      this.copyMenu[9].title = this.sysLanguages.GroupText;
      this.copyMenu[10].title = this.sysLanguages.AccessRightText;
      this.copyMenu[11].title = this.sysLanguages.LogText;
    }
  }
}
