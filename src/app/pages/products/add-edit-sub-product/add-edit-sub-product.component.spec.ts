import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSubProductComponent } from './add-edit-sub-product.component';

describe('AddEditSubProductComponent', () => {
  let component: AddEditSubProductComponent;
  let fixture: ComponentFixture<AddEditSubProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditSubProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSubProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
