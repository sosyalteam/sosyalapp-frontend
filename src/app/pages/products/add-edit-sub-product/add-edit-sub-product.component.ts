import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'add-edit-sub-product',
  templateUrl: './add-edit-sub-product.component.html',
  styleUrls: ['./add-edit-sub-product.component.scss'],
  providers:[ApicallService, NbToastrService]
})
export class AddEditSubProductComponent implements OnInit {

  public sysLanguages : any;
  groupList = [];
  taxList = [];
  productList = [];
  allProductTaskList = [];
  currencyList = [];
  tasktimetypeList = [];
  productTaskList = [];
  subProductLineData = [];
  productID = 0;
  TeamList= [];
  repeatedTask = 0;


  productData = { ProductName: '', ProductDesc: '', Unit: '', Price: '', ProductGroup: 0, Tax1: 0, Tax2: 0, CurrencyId: 0 };
  selectedTask = { Title: '', Price: '', ProductTaskId: 0, CurrencyId: 1, TaskTimeType: 1, TaskTimeAmount: '', TeamId : 0 , DayCount : 0};

  addedDynamicProductData = [];
  selectedSubProduct = 0;
  productTaskIndexForAddPage = 0;
  isUpdateOrSave = "";

  loadingSave = false;
  loaddingSaveTask = false;

  SubTaskList = [];
  subTaskEmpty = false;

  //toastr
  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  statusDiscount : NbToastStatus = NbToastStatus.WARNING;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _router: Router, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private _apicallservice: ApicallService, private dialogService: NbDialogService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProductConfig + " - Crmplus");

    this.tasktimetypeList.push({ label: this.sysLanguages.ProductConfiguratorMinuteText, value: 1 }, { label: this.sysLanguages.ProductConfiguratorHourText, value: 2 }, { label: this.sysLanguages.ProductConfiguratorDayText, value: 3 });

    if (this._avRoute.snapshot.params["id"]) {
      this.productID = this._avRoute.snapshot.params["id"];
    }

    this.getProductGroups();
    this.getTax();
    this.getProducts();
    this.getCurrencies();
    this.getTasks();
    this.getTeams();

  }

  ngOnInit() 
  {
    if(this.productID > 0)
    {
      this._apicallservice.Get("http://localhost:61232/api/Product/ProductDetails", this.productID)
      .subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            setTimeout(()=>{
              if(resultArray[0].Price)
              {
                if(resultArray[0].Price.toString().includes(",") == true)
                {
                  this.productData.Price = resultArray[0].Price.toString().substring(0, resultArray[0].Price.toString().indexOf(",")); 
                  this.productData.Price = this.productData.Price.toString().replace(".","");
                }
                else
                {
                  this.productData.Price = resultArray[0].Price.toString().replace(".","");
                }
              }

              this.productData.ProductDesc = resultArray[0].ProductDesc;
              this.productData.ProductGroup = resultArray[0].ProductGroupId;
              this.productData.ProductName = resultArray[0].ProductName;
              this.productData.Tax1 = resultArray[0].Tax1;
              this.productData.Tax2 = resultArray[0].Tax2;
              this.productData.Unit = resultArray[0].Unit; 
              this.productData.CurrencyId = resultArray[0].CurrencyTypeId;
            }, 1000);
          }              
      }
      )

      this.getSubProducts();
      this.getProductTasks();
    }
    else
    {
      this.productTaskList = [];
      this.subProductLineData = [];
      this.addedDynamicProductData = [];
      this.productData.Price = "";
      this.productData.ProductDesc = "";
      this.productData.ProductGroup = 0;
      this.productData.ProductName = "";
      this.productData.Tax1 = 0;
      this.productData.Tax2 = 0;
      this.productData.Unit = "";
    }
  }

  openDialog(dialog: TemplateRef<any>, val, id)
  {
    if(val == "save")
    {
      this.isUpdateOrSave = "save";

      this.selectedTask.CurrencyId = 1;
      this.selectedTask.ProductTaskId = 0;
      this.selectedTask.Price = "";
      this.selectedTask.TaskTimeAmount = "";
      this.selectedTask.TaskTimeType = 1;
      this.selectedTask.Title = "";
      this.selectedTask.TeamId = 0;
      this.repeatedTask = 0;
      this.selectedTask.DayCount = 0;

      this.dialogService.open(dialog);
    }
    else if(val == "update")
    {
      this.isUpdateOrSave = "update";

      let index = this.productTaskList.findIndex(x => x.ProductTaskId == id);
      this.selectedTask.ProductTaskId = id;
      this.selectedTask.CurrencyId = this.productTaskList[index].CurrencyTypeId;
      this.selectedTask.Title = this.productTaskList[index].Title;
      this.selectedTask.TaskTimeAmount = this.productTaskList[index].TaskTimeAmount;
      this.selectedTask.Price = this.productTaskList[index].Price;
      this.selectedTask.TaskTimeType = this.productTaskList[index].TaskTimeType;
      this.selectedTask.TeamId = this.productTaskList[index].TeamId;
      this.repeatedTask = this.productTaskList[index].RepeatedTask;
      this.selectedTask.DayCount = this.productTaskList[index].DayCount;

      this.dialogService.open(dialog);
    }
    else if(val == "dialogAddSubTask")
    {
      this.getSubTask();
      this.dialogService.open(dialog);
    }
  }

  getSubProducts()
  {
    this.subProductLineData = [];

    this._apicallservice.Get("http://localhost:61232/api/Product/GetProductSubProducts", this.productID)
    .subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.subProductLineData = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            this.subProductLineData.push({ ProductId: resultArray[i].ProductId, ProductName: resultArray[i].ProductName, ProductRelationId: resultArray[i].ProductRelationId });
          }
        }              
    }
    )
  }
  
  getProductTasks()
  {
    this._apicallservice.Get("http://localhost:61232/api/Product/GetProductTasks", this.productID)
    .subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productTaskList = [];
          }
        }
        else
        {
          this.productTaskList = [];
          
          for(let i in resultArray)
          {
            if(resultArray[i].ProductTaskId == resultArray[i].ParentId )
            {
              this.productTaskList.push({ ProductId: resultArray[i].ProductId, ProductTaskId: resultArray[i].ProductTaskId, Price: resultArray[i].Price, CurrencyTypeId: resultArray[i].CurrencyTypeId, TaskTimeAmount: resultArray[i].TaskTimeAmount, TaskTimeType: resultArray[i].TaskTimeType, Title: resultArray[i].Title , TeamId : resultArray[i].TeamId , RepeatedTask : resultArray[i].RepeatedTask , DayCount : resultArray[i].DayCount });
            }
          }
        }              
    }
    )
  }

  getProductGroups()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProductGroup").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.groupList = [];
          }
        }
        else
        {
          this.groupList = [];
          for(let i in resultArray)
          {
            let _group = { label: resultArray[i].ProductGroupDesc, value: resultArray[i].ProductGroupId };
            this.groupList.push(_group);
          }
        }
    },
    )
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.currencyList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
            this.currencyList.push(_currency);
          }

          this.productData.CurrencyId = 1;
        }
    },
    )
  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetProductsWithoutPrice").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              let _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, desc: resultArray[i].ProductDesc };
              this.productList.push(_product);
            }
          }
        }
    },
    )
  }

  getTasks()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProductTasks").subscribe(
      (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.allProductTaskList = [];
          }
        }
        else
        {
          this.allProductTaskList = [];

          for(let i in resultArray)
          {
            this.allProductTaskList.push({ ProductId: resultArray[i].ProductId, ProductTaskId: resultArray[i].ProductTaskId, Price: resultArray[i].Price, CurrencyTypeId: resultArray[i].CurrencyTypeId, TaskTimeAmount: resultArray[i].TaskTimeAmount, TaskTimeType: resultArray[i].TaskTimeType, Title: resultArray[i].Title , TeamId : resultArray[i].TeamId , RepeatedTask : resultArray[i].RepeatedTask , DayCount : resultArray[i].DayCount  });
          }
        }
      },
      )
  }

  getTeams()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
    (result) => {         
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.TeamList = [];
        }
      }
      else
      {
        for(var i = 0; i < resultArray.length; i++ ){
          let _team = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
          this.TeamList.push(_team);
        }
      }
    },
    )
  }

  addNewProductLine()
  {
    if(this.productID > 0)
    {
      if(this.subProductLineData.findIndex(x => x.ProductId == this.selectedSubProduct) == -1)
      {
       
        let _subprod = { MasterProductId: this.productID, SubProductId: this.selectedSubProduct };

        this._apicallservice.Add("http://localhost:61232/api/Product/AddSubProduct", _subprod).subscribe(
        (result) =>{
          let _resultArray = JSON.parse(JSON.stringify(result));
          if (_resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else
          {
            this.getSubProducts();
          }
        }
        )

        for(let i in this.allProductTaskList)
        {
          if(this.allProductTaskList[i].ProductId == this.selectedSubProduct)
          {
            let _task = { Title: this.allProductTaskList[i].Title, Price: this.allProductTaskList[i].Price, CurrencyTypeId: this.allProductTaskList[i].CurrencyId, TaskTimeAmount: this.allProductTaskList[i].TaskTimeAmount, TaskTimeType: this.allProductTaskList[i].TaskTimeType, ProductId: this.productID , TeamId : this.allProductTaskList[i].TeamId , RepeatedTask : this.allProductTaskList[i].RepeatedTask , DayCount : this.allProductTaskList[i].DayCount  };

            this._apicallservice.Add("http://localhost:61232/api/Product/AddTaskToProduct", _task).subscribe(
            (result) =>{
              let _resultArray = JSON.parse(JSON.stringify(result));
              if (_resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
              else
              {
                this.getProductTasks();
              }
            }
            )
          }
        }
      }
      else
      {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ProductConfigAlreadyAddedProd);
      }
    }
    else
    {
      if(this.addedDynamicProductData.findIndex(x => x.ProductId == this.selectedSubProduct) == -1)
      {
        var line = <HTMLCollection> document.getElementsByClassName("productLine");
        var newid = line[0].childElementCount+1;
  
        let subProductData = this.productList.filter(x=> x.value == this.selectedSubProduct)[0];
  
        var productName = subProductData.label; //ürünler gelecek
        
        let mythis = this;
  
        var subLine = "<div id='Pline_" + newid + "' style='width:100%'> <p id='name" + newid + "' style='width: 87.8%; display: inline-block; margin-left: 2%; margin-top: 2%'>" + productName + " </p> <button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 10px;background-color:red;float: right;margin-right: 1%;' >x</button> <input type='hidden' id='subProductId' value='" + this.selectedSubProduct + "'> </div>"
        $('.productLine').append(subLine);
        var deletee = <HTMLInputElement> document.getElementById("Pline_" + newid);
  
        deletee.addEventListener("click", ( event: Event) => {
          let element = event.currentTarget;
          var id = (<any>element).id.split("Pline_")[1];
          let productid = (<any>element).children[2].value;
  
          document.getElementById("Pline_"+id).remove();
  
          mythis.addedDynamicProductData.splice(this.addedDynamicProductData.findIndex(x => x.ProductId == productid), 1); //product gelecek
          
        });
  
        let _lineData = { ProductId : this.selectedSubProduct };
        this.addedDynamicProductData.push(_lineData);

        for(let j in this.allProductTaskList)
        {
          if(this.allProductTaskList[j].ProductId == this.selectedSubProduct)
          {
            this.productTaskList.push({ ProductId: 0, ProductTaskId: this.productTaskIndexForAddPage, Price: this.allProductTaskList[j].Price, CurrencyTypeId: this.allProductTaskList[j].CurrencyId, TaskTimeAmount: this.allProductTaskList[j].TaskTimeAmount, TaskTimeType: this.allProductTaskList[j].TaskTimeType, Title: this.allProductTaskList[j].Title , TeamId : this.allProductTaskList[j].TeamId , RepeatedTask : this.allProductTaskList[j].RepeatedTask , DayCount : this.allProductTaskList[j].DayCount });
            this.productTaskIndexForAddPage++;
          }
        }
      }
      else
      {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ProductConfigAlreadyAddedProd);
      }
    }
  }

  deleteeditSubProductLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let prodRelationId = event.currentTarget.parentElement.children[2].value;
    
    this._apicallservice.Deactive("http://localhost:61232/api/Product/RemoveSubProduct", Number(prodRelationId)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getSubProducts();
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  toggle(event)
  {
    if(event.target.checked == true)
    {
      this.repeatedTask = 1;
    }
    else
    {
      this.repeatedTask = 0; 
    }
    console.log(this.repeatedTask);
  }

  addTaskToProduct(ref)
  {
    this.loaddingSaveTask = true;

    if(this.productID > 0)
    {
      let _task = { Title: $("#dialogTaskTitle").val(), Price: $("#dialogTaskPrice").val(), CurrencyTypeId: this.selectedTask.CurrencyId, TaskTimeAmount: $("#dialogTaskTimeCount").val(), TaskTimeType: this.selectedTask.TaskTimeType, ProductId: this.productID ,TeamId: this.selectedTask.TeamId, RepeatedTask : this.repeatedTask , DayCount : $("#dialogTaskDayCount").val()  };

      this._apicallservice.Add("http://localhost:61232/api/Product/AddTaskToProduct", _task).subscribe(
      (result) =>{
        let _resultArray = JSON.parse(JSON.stringify(result));
        if (_resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else
        {
          this.loaddingSaveTask = false;
          this.getProductTasks();
          ref.close();
        }
      }
      )
    }
    else
    {
      this.productTaskList.push({ ProductId: 0, ProductTaskId: this.productTaskIndexForAddPage, Price: $("#dialogTaskPrice").val(), CurrencyTypeId: this.selectedTask.CurrencyId, TaskTimeAmount: $("#dialogTaskTimeCount").val(), TaskTimeType: this.selectedTask.TaskTimeType, Title: $("#dialogTaskTitle").val(),TeamId: this.selectedTask.TeamId, RepeatedTask : this.repeatedTask , DayCount : $("#dialogTaskDayCount").val() });
      this.productTaskIndexForAddPage++;
      this.loaddingSaveTask = false;
      ref.close();
    }
  }
  
  deleteeditProductTask(event)
  {
    if(this.productID > 0)
    {
      let prodTaskId = event.currentTarget.parentElement.children[0].id;

      this._apicallservice.Deactive("http://localhost:61232/api/Product/RemoveProductTask", Number(prodTaskId)).subscribe(
        (result) =>{
          let resultArray = JSON.parse(JSON.stringify(result));
          if(resultArray[0].Result == "True")
          {
            this.getProductTasks();
          }
          else if(resultArray[0].Result == "Session_Expired")
          {
            this._router.navigate(['/auth']);
            return;
          }
        }
      )
    }
    else
    {
      let prodTaskId = event.currentTarget.parentElement.children[0].id;
      let selectedIndex = this.productTaskList.findIndex(x => x.ProductTaskId == prodTaskId);
      this.productTaskList.splice(selectedIndex, 1);
    }
  }

  updateProductTask(ref)
  {
    this.loaddingSaveTask = true;

    if(this.productID > 0)
    {
      let _task = {  ProductTaskId: this.selectedTask.ProductTaskId, Title: $("#dialogTaskTitle").val(), Price: $("#dialogTaskPrice").val(), CurrencyTypeId: this.selectedTask.CurrencyId, TaskTimeAmount: $("#dialogTaskTimeCount").val(), TaskTimeType: this.selectedTask.TaskTimeType, ProductId: this.productID ,TeamId : this.selectedTask.TeamId , RepeatedTask : this.repeatedTask , DayCount : $("#dialogTaskDayCount").val() };
      this._apicallservice.Update("http://localhost:61232/api/Product/UpdateProductTask", this.selectedTask.ProductTaskId, _task)
      .subscribe((result) => {
        if(result[0].Result == "True")
        {
          this.loaddingSaveTask = false;
          this.getProductTasks();
          ref.close(); 
        }
        else if(result[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
      )
    }
    else
    {

      let index = this.productTaskList.findIndex(x => x.ProductTaskId == this.selectedTask.ProductTaskId);
      this.productTaskList[index].Price = $("#dialogTaskPrice").val().toString();
      this.productTaskList[index].TaskTimeAmount = $("#dialogTaskTimeCount").val().toString();
      this.productTaskList[index].Title = $("#dialogTaskTitle").val().toString();
      this.productTaskList[index].CurrencyTypeId = this.selectedTask.CurrencyId;
      this.productTaskList[index].TaskTimeType = this.selectedTask.TaskTimeType;
      this.productTaskList[index].TeamId = this.selectedTask.TeamId ; 
      this.productTaskList[index].RepeatedTask = this.repeatedTask;
      this.productTaskList[index].DayCount = $("#dialogTaskDayCount").val();

      this.loaddingSaveTask = false;
      ref.close();
    }
  }

  saveProduct()
  {
    this.loadingSave = true;

    if(this.productID > 0)
    {
      let _prod = {  ProductId: this.productID, ProductName: $("#_name").val(), Price: $("#_price").val().toLocaleString(), ProductDesc: $("#_productDesc").val(), ProductGroupId: this.productData.ProductGroup, Tax1: this.productData.Tax1, Tax2: this.productData.Tax2, Unit: $("#_unit").val(), CurrencyTypeId: this.productData.CurrencyId };
      this._apicallservice.Update("http://localhost:61232/api/Product/EditProduct", this.productID, _prod)
      .subscribe((result) => {
        if(result[0].Result == "True")
        {
          this.loadingSave = false;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.ProductConfigUpdatedProd);
          this._router.navigate(['/pages/products']);
        }
        else if(result[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
      )
    }
    else
    {
      let _prod = { ProductName: $("#_name").val(), Price: $("#_price").val().toLocaleString(), ProductDesc: $("#_productDesc").val(), ProductGroupId: this.productData.ProductGroup, Tax1: this.productData.Tax1, Tax2: this.productData.Tax2, Unit: $("#_unit").val(), CurrencyTypeId: this.productData.CurrencyId };
      this._apicallservice.Add("http://localhost:61232/api/Product/CreateProduct", _prod)
      .subscribe((result) => {
        if(result[0].Result == "True")
        {
          if(this.addedDynamicProductData.length > 0)
          {
            let _prodRel;
            for(let i in this.addedDynamicProductData)
            {
              _prodRel = { MasterProductId: result[1].Result, SubProductId: this.addedDynamicProductData[i].ProductId };
      
              this._apicallservice.Add("http://localhost:61232/api/Product/AddSubProduct", _prodRel).subscribe(
              (result) =>{
                let _resultArray = JSON.parse(JSON.stringify(result));
                if (_resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
            }
          }
          if(this.productTaskList.length > 0)
          {
            let _task;
            for(let i in this.productTaskList)
            {
              _task = { Title: this.productTaskList[i].Title, Price: this.productTaskList[i].Price, CurrencyTypeId: this.productTaskList[i].CurrencyTypeId, TaskTimeAmount: this.productTaskList[i].TaskTimeAmount, TaskTimeType: this.productTaskList[i].TaskTimeType, ProductId: result[1].Result , TeamId : this.productTaskList[i].TeamId , RepeatedTask : this.repeatedTask , DayCount : this.productTaskList[i].DayCount };
      
              this._apicallservice.Add("http://localhost:61232/api/Product/AddTaskToProduct", _task).subscribe(
              (result) =>{
                let _resultArray = JSON.parse(JSON.stringify(result));
                if (_resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
            }
          }

          this.loadingSave = false;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.ProductConfigSavedProd);
          this._router.navigate(['/pages/products']);
        }
        else if(result[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
      )
    }
  }

  addSubTask()
  {
    let subProductTask={
      ProductTaskId: 0,
      Title: $("#subs").val(),
      Price : "",
      CurrencyTypeId : 1,
      TaskTimeAmount : "",
      TaskTimeType : 1 ,
      ProductId : this.productID,
      TeamID :  this.selectedTask.TeamId ,
      RepeatedTask : 0 ,
      DayCount : 0,
      ParentId : this.selectedTask.ProductTaskId,
    };

    this._apicallservice.Add("http://localhost:61232/api/Product/CreateSubProductTask", subProductTask) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
        $("#subs").val("");
      }          
      },
    )
    
  }

  editSubTask(taskId)
  {
    var newSubTaskDesc = $("#subTaskk_"+taskId).val();
    let subProductTask={
      ProductTaskId: taskId,
      Title: newSubTaskDesc,
      Price : "",
      CurrencyTypeId : 1,
      TaskTimeAmount : "",
      TaskTimeType : 1 ,
      ProductId : this.productID,
      TeamID :  this.selectedTask.TeamId ,
      RepeatedTask : 0 ,
      DayCount : 0,
      ParentId : this.selectedTask.ProductTaskId,
    };
    this._apicallservice.Add("http://localhost:61232/api/Product/UpdateProductSubTask", subProductTask) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)
  }

  getSubTask()
  {
    this._apicallservice.Get("http://localhost:61232/api/Product/GetProductSubTask/",this.selectedTask.ProductTaskId).subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.SubTaskList = [];
            this.subTaskEmpty = true;
          }
        }
        else
        {
          this.SubTaskList=resultArray;
          this.subTaskEmpty = false;
        }
    },
    )
  }

  deleteProductTask(event)
  {
    this._apicallservice.Deactive("http://localhost:61232/api/Product/RemoveProductSubTask", event).subscribe(
        (result) =>{
          let resultArray = JSON.parse(JSON.stringify(result));
          if(resultArray[0].Result == "True")
          {
            this.getSubTask();
          }
          else if(resultArray[0].Result == "Session_Expired")
          {
            this._router.navigate(['/auth']);
            return;
          }
        }
      )
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
