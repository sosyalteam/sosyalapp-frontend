import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { AddEditSubProductComponent } from './add-edit-sub-product/add-edit-sub-product.component';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent
  },
  {
    path: 'add-edit-sub-product',
    component: AddEditSubProductComponent
  },
  {
    path: 'add-edit-sub-product/:id',
    component: AddEditSubProductComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
