import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products/products.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbDialogModule, NbContextMenuModule, NbSelectModule, NbTabsetModule, NbListModule ,NbCheckboxModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { AddEditSubProductComponent } from './add-edit-sub-product/add-edit-sub-product.component';

@NgModule({
  declarations: [ProductsComponent, AddEditSubProductComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    NbListModule,
    NbCheckboxModule
  ]
})
export class ProductsModule { }
