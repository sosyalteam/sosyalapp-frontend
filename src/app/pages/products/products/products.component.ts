import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class ProductsComponent implements OnInit {

  public sysLanguages : any;
  productList : any;
  //public productData = [{  ProductName:' ', Price:' ', ProductDesc : '', Unit : '' , Tax1 : '', Tax2:'',ProductGroup : ''}];
  //title:any;
  ProductId:any;
  taxList : any;
  groupList : any;
  GroupList:any;

  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService,private dialogService: NbDialogService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProductText + " - Crmplus");
    this.getProducts();
    this.getTax();
    this.getProductGroups();

    this.settings.columns.ProductName.title = this.sysLanguages.AddInvoiceProducts;
    this.settings.columns.CopyPrice.title = this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.ProductDesc.title = this.sysLanguages.ProductLongDesc;
    this.settings.columns.TaxDesc.title = this.sysLanguages.AddInvoiceTableTax + " 1";
    this.settings.columns.Tax2.title = this.sysLanguages.AddInvoiceTableTax + " 2";
    this.settings.columns.Unit.title = this.sysLanguages.Unit;
    this.settings.columns.GroupDesc.title = this.sysLanguages.GroupText;
    this.settings.columns.CurrencyDesc.title = this.sysLanguages.AddInvoiceCurrencyType;
    this.settingsGroupTable.columns.ProductGroupDesc.title = this.sysLanguages.GroupText;   

  }

  settings = {
    columns: {
      ProductId: {
        title: 'ID',
        width: '2%'
      },
      ProductName: {
        title: ''
      },
      CopyPrice: {
        title: ''
      },
      CurrencyDesc: {
        title: ''
      },
      ProductDesc : {
        title:''
      },
      TaxDesc:{
        title:''
      },
      Tax2:{
        title:''
      },
      Unit:{
        title:''
      },
      GroupDesc:{
        title:''
      }
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  settingsGroupTable = {
    columns: {
      ProductGroupId: {
        title: 'ID',
        width: '2%'
      },
      ProductGroupDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    }
  };

  ngOnInit() {
  }


  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;
          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }

                  }
                }

                _product= { ProductId: resultArray[i].ProductId, ProductName: resultArray[i].ProductName, Price: resultArray[i].Price, CopyPrice: Number(resultArray[i].Price).toLocaleString(), ProductDesc: resultArray[i].ProductDesc, TaxDesc: resultArray[i].Tax1, Tax2: resultArray[i].Tax2, Unit: resultArray[i].Unit, GroupDesc: resultArray[i].GroupDesc, CurrencyDesc: resultArray[i].CurrencyDesc };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }

            /*if(resultArray[i].ProductId != 0)
            {
              let _product= { ProductId: resultArray[i].ProductId, ProductName: resultArray[i].ProductName, Price: resultArray[i].Price, CopyPrice: Number(resultArray[i].Price.split(" ")[0]).toLocaleString(), ProductDesc: resultArray[i].ProductDesc, TaxDesc: resultArray[i].Tax1, Tax2: resultArray[i].Tax2, Unit: resultArray[i].Unit, GroupDesc: resultArray[i].GroupDesc, CurrencyDesc: resultArray[i].CurrencyDesc };
              this.productList.push(_product);
            }*/
          }
        }
    },
    )
  }

  /*openDialogSave(dialog: TemplateRef<any>)
  {
    this.title = this.sysLanguages.NewProduct;
    this.productData[0].ProductName="";
    this.productData[0].Price="";
    this.productData[0].ProductDesc="";
    this.productData[0].ProductGroup="";
    this.productData[0].Tax1="";
    this.productData[0].Tax2="";
    this.productData[0].Unit="";

    this.dialogService.open(dialog);
  }*/

  openDialogSaveGroup(dialog: TemplateRef<any>)
  {
    this.dialogService.open(dialog);
  }

  /*openDialogEdit(dialog: TemplateRef<any>,event)
  {
    this.ProductId=parseInt(`${event.data.ProductId}`);
      this._apicallservice.Get("http://localhost:61232/api/Product/ProductDetails",parseInt(this.ProductId)).subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              this.productList = [];
            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.productData[0].ProductName = resulttArray[0].ProductName;
            this.productData[0].Price = resulttArray[0].Price;
            this.productData[0].ProductDesc=resulttArray[0].ProductDesc;
            this.productData[0].Tax1=resulttArray[0].Tax1;  
            this.productData[0].Tax2=resulttArray[0].Tax2;  
            this.productData[0].Unit=resulttArray[0].Unit;  
            this.productData[0].ProductGroup=resulttArray[0].ProductGroupId;  

          }       
      },

      ),
      this.title=this.sysLanguages.EditProduct;
      this.dialogService.open(dialog);
      
  }*/

  /*save(ref)
  {
    if(this.title == this.sysLanguages.NewProduct)
    {
      let product={ProductName:  $("#name").val(), Price : $("#price").val(),ProductDesc : $("#ProductDesc").val() ,Unit: $('#unit').val(),Tax1 :this.productData[0].Tax1 , Tax2 : this.productData[0].Tax2 ,ProductGroupId : this.productData[0].ProductGroup };
      this._apicallservice.Add("http://localhost:61232/api/Product/CreateProduct",product) 
          .subscribe((result) => {    
             let resultArray = JSON.parse(JSON.stringify(result));
             if (resultArray[0].Result == "True") {
               ref.close();
               this.getProducts();
               this._router.navigate(['/pages/sale/products']);
             }
             else if (resultArray[0].Result == "Session_Expired") {
               this._router.navigate(['/auth']);
               return;
             }                    
          },)

    }
    else if (this.title==this.sysLanguages.EditProduct)
    {
      let editProduct={ProductId: this.ProductId, ProductName:  $("#name").val(), Price : $("#price").val(),ProductDesc : $("#ProductDesc").val(),Unit: $('#unit').val(),Tax1 :this.productData[0].Tax1 , Tax2 : this.productData[0].Tax2 ,ProductGroupId : this.productData[0].ProductGroup};
      this._apicallservice.Update("http://localhost:61232/api/Product/EditProduct",this.ProductId,editProduct) 
      .subscribe((result) => {    
         let resultArray = JSON.parse(JSON.stringify(result));
         if (resultArray[0].Result == "True") {
           ref.close();
           this.getProducts();
         }
         else if (resultArray[0].Result == "Session_Expired") {
           this._router.navigate(['/auth']);
           return;
         }                    
      }, )

    }
  }*/

  onDeleteConfirm(event)
  {
    var id=parseInt(`${event.data.ProductId}`);
    var ans = confirm(this.sysLanguages.DeleteProduct + id);
    if (ans) 
    {
      this._apicallservice.Deactive("http://localhost:61232/api/Product/DeleteProduct",id) 
      .subscribe((result) => {    
         let resultArray = JSON.parse(JSON.stringify(result));
         if (resultArray[0].Result == "True") {
           this.getProducts();
         }
         else if (resultArray[0].Result == "Session_Expired") {
           this._router.navigate(['/auth']);
           return;
         }                    
      }, )
    }
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          this.taxList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  getProductGroups()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProductGroup").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.groupList = [];
          }
        }
        else
        {
          this.GroupList=result;
          this.groupList = [];
          for(let i in resultArray)
          {
            let _group = { label: resultArray[i].ProductGroupDesc, value: resultArray[i].ProductGroupId };
            this.groupList.push(_group);
          }
        }
    },
    )

  }


  onDeleteConfirmGroup(event)
  {

    var id = `${event.data.ProductGroupId}`;
      var ans = confirm(this.sysLanguages.DeleteProductGroup + parseInt(id));
     if (ans) {
       this._apicallservice.Deactive("http://localhost:61232/api/Product/DeleteProductGroup", parseInt(id)).subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getProductGroups();
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
       
       }, error => console.error(error))
     }

  }

  onSaveConfirmGroup(event)
  {

    var id = `${event.data.ProductGroupId}`;
      var name=`${event.newData.ProductGroupDesc}`;
      let groupArray = {  ProductGroupId: parseInt(id), ProductGroupDesc:name };
             this._apicallservice.Update("http://localhost:61232/api/Product/UpdateProductGroup", parseInt(id), groupArray)
               .subscribe((result) => {
                 if(result[0].Result == "True")
                 {
                   this.getProductGroups();
                 }
                 else if(result[0].Result == "Session_Expired")
                 {
                   this._router.navigate(['/auth']);
                   return;
                 }
               },)

  }

  onCreateConfirm(event)
  {
      var name=`${event.newData.ProductGroupDesc}`;
      let groupArray = {  ProductGroupId: 0, ProductGroupDesc:name };
      this._apicallservice.Add("http://localhost:61232/api/Product/CreateProductGroup",groupArray) 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.getProductGroups();
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )

  }

  openUrl(link, val)
  {
    if(val == 0)
    {
      this._router.navigate([link]);
    }
    else
    {
      this._router.navigate([link, val.data.ProductId]);
    }
  }

}
