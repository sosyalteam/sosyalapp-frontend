import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { NbDatepickerModule } from '@nebular/theme';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes),NbDatepickerModule.forRoot()],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
