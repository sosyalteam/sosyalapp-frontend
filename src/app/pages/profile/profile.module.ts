
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile/profile.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbDialogModule, NbContextMenuModule, NbTabsetModule, NbRouteTabsetModule, NbProgressBarModule, NbListModule, NbStepperModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbTabsetModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    NbRouteTabsetModule,
    DropdownModule,
    MultiSelectModule,
    NbListModule,
    NbStepperModule
  ]
})
export class ProfileModule { }
