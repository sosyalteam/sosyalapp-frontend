import { Component, OnInit, TemplateRef, HostListener } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { NbDialogService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  providers: [ApicallService,HttpModule],
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  linearMode = true;
  clientsecret :any;
  clientid :any;

  public sysLanguages: any;
  public userArray = [{ Username :' ',Fullname :' ', Email : '' }];
  public userId : any;
  public profileHeader : any;
  public projectHeader : any;
  boardList: any;
  leadList:any;
  statusList = [{ value: '', title: '' }];
  LeadstatusList = [{ value: '', title: '' }];
  taskData = {TaskId :0 , Title: "", BeginDate: "", EndDate: "", Fullname: "", StatusDesc: "", TaskDesc: "", ResponsibleId: 0, StatusId: 0 , Creator : "", TaskTime : ""};
  selectedStatus: any;
  selectedType: any;

  isShowingNotDefaultTeamTask = false;
  isSaveorUpdate = "";

  relationList = [];
  isSelectedRelation: any;
  customerList = [];
  multiLeadList = [];
  invoiceList = [];
  projectList = [];
  isSelectedInvoice=0;
  isSelectedCustomer=0;
  isSelectedLead=0;
  isSelectedProject = 0;
  RelationText  = "";
  allUsers = [];
  StatusList = [];
  date:string;
  taskTabid :any;

  taskTypeList = [];

  taskListCount = 0;
  completedTaskCount = 0;
  notCompletedtaskList = [];
  completedTaskList = [] ;
  copynotCompletedTaskList = [];
  copyCompletedTaskList = [];

  pageNumfornotCompleted = 1;
  pageCountfornotCompleted = 8;

  pageNumforCompleted = 1;
  pageCountforCompleted = 8;

  loading = false;

   //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  title:any;
  content:any;

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  constructor(public http: HttpModule, private _router: Router, private _apicallService: ApicallService,private dialogService: NbDialogService,private toastrService: NbToastrService, private titleService: Title) {

    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProfileText + " - Crmplus");

    this.taskTabid = parseInt(sessionStorage.getItem('tasksTab'));
    
    this.relationList.push({ label: this.sysLanguages.ProjectText, value: 4 }, { label: this.sysLanguages.CustomerText, value: 1 },  { label: this.sysLanguages.LeadText, value: 3 }, { label: this.sysLanguages.InvoiceText, value: 2} );
    
    this.profileHeader = this.sysLanguages.ProfileText;
    this.projectHeader  = this.sysLanguages.ProjectText;
    this.settings.columns.BoardName.title=this.sysLanguages.BoardTableNameText;
    this.settings.columns.BeginDate.title=this.sysLanguages.BoardTableStartDateText;
    this.settings.columns.EndDate.title=this.sysLanguages.BoardTableEndDateText;
    this.settings.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;

    this.settingsLead.columns.LeadDesc.title = this.sysLanguages.LeadTableLeadDescText;
    this.settingsLead.columns.Name.title = this.sysLanguages.LeadTableNameText;
    this.settingsLead.columns.Company.title = this.sysLanguages.LeadTableCompanyText;
    this.settingsLead.columns.StatusDesc.title = this.sysLanguages.LeadTableStatusDescText;
    this.settingsLead.columns.CreatedDate.title=this.sysLanguages.LeadCreatedDateText;
    this.settingsLead.columns.StatusDesc.filter.config.selectText=this.sysLanguages.TasksTableFilterSelectText;
    this.getStatus();
    this.getLead();
    this.getLeadStatus();
    //this.getUsers();
    //this.getTaskStatusList();
    //this.getCustomers();
    //this.getInvoices();
    //this.getTeamTasks();
    this.getMyTasks();
    //this.getTaskType();
    this.getProjects();
    
   }

   settings = {
    columns: {
      BoardId: {
        title: 'ID',
        width: '3%'
      },
      BoardName: {
        title: ''
      },
      CustomerName:{
        title:''
      },
      BeginDate: {
        title: '',
        width: '5%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      EndDate: {
        title: '',
        width: '5%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },

    },
    actions:{
      position:'right',
      columnTitle:"",
      add:false,
      edit:false,
      delete:false
    },
    mode:"external",
  };

  settingsLead = {
    columns: {
      LeadId: {
        title: 'ID',
        width: '3%'
      },
      LeadDesc:
      {
        title:''
      },
      Name:{
        title: ''
      },
      Company: {
        title: ''
      },
      StatusDesc: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: 'Select',
                list: this.LeadstatusList
              }
            }
      },
      CreatedDate: {
        title: '',
        width: '5%',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
    },
    actions:{
      position:'right',
      columnTitle:"",
      add:false,
      edit:false,
      delete:false
    },
    mode:"external",
  };

  ngOnInit() {
    this._apicallService.GetAll("http://localhost:61232/api/user/GetProfileDetail").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.userArray = [];
          }
        }
        else {
            this.userArray[0].Fullname = resulttArray[0].Fullname;
            this.userArray[0].Email = resulttArray[0].Email;
            this.userId = resulttArray[0].UserId;
        }
      }
    ),
    this._apicallService.GetAll("http://localhost:61232/api/project/GetTaskBoards").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.boardList = [];
          }
        }
        else {
          this.boardList = result;
        }
      }
    )
  }

  @HostListener('window:scroll', ['$event']) 
  lazyLoadingforNotCompleted(event)
  {
    if ($("#notCompletedTaskList").scrollTop() >= (document.getElementById("notCompletedTaskList").scrollHeight - document.getElementById("notCompletedTaskList").offsetHeight)) 
    {
      this.pageNumfornotCompleted++;
      this.getNotCompletedTasksforLazyLoading();
    }
  }

  @HostListener('window:scroll', ['$event']) 
  lazyLoadingforCompleted(event)
  {
    if ($("#completedTaskList").scrollTop() >= (document.getElementById("completedTaskList").scrollHeight - document.getElementById("completedTaskList").offsetHeight)) 
    {
      this.pageNumforCompleted++;
      this.getCompletedTasksforLazyLoading();
    }
  }

  projectDetail(event){
    var id = `${event.data.BoardId}`;
    this._router.navigate(['/pages/task/board-overview',id]);
  }

  getStatus()
  {
  this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length !=0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resulttArray[0].Result == "Empty")
        {
          this.statusList = [];
        }
      }
      else
      {
        for(var i = 0; i < resulttArray.length; i++ ){
          this.statusList.push({value:resulttArray[i].StatusDesc,title:resulttArray[i].StatusDesc});
        }
        this.statusList.splice(0,1);
      }
    },
    )
  }
  
  getLead()
  {
    this._apicallService.GetAll("http://localhost:61232/api/User/GetLeadByUser").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.leadList=[];
          }
        }
        else
        {
          this.leadList=result;
          for(let i in resulttArray)
            {
              if(resulttArray[i].LeadId != 0)
              {
                let _lead = { label: resulttArray[i].LeadName +" ( " +resulttArray[i].CustomerName + " )", value: resulttArray[i].LeadId, name: resulttArray[i].Name };
                this.multiLeadList.push(_lead);
              }
            }
        }
      }
    )
  }

  getLeadStatus()
  {
  this._apicallService.GetAll("http://localhost:61232/api/lead/GetStatusList").subscribe(
    (result) => {       
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          this.LeadstatusList = [];
        }
      }
      else
      {
        for(var i = 0; i < resulttArray.length; i++ ){
          this.LeadstatusList.push({value:resulttArray[i].StatusDesc,title:resulttArray[i].StatusDesc});
        }
        this.settingsLead.columns.StatusDesc.filter.config.list = this.LeadstatusList;
        this.LeadstatusList.splice(0,1);
        this.settingsLead = Object.assign({}, this.settingsLead);
      }
    },
    )
  }

  addTask()
  {
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

 /* openDialogs(dialog: TemplateRef<any>, event, val)
  {
    if(val == "teamTask")
    {
      let index = this.teamTasksList.findIndex(x => x.TaskId == event.data.TaskId);

      this.RelationText = "";
      this.taskData.StatusDesc = this.teamTasksList[index].StatusDesc;
      this.taskData.Fullname = this.teamTasksList[index].Fullname;
      this.taskData.TaskId = this.teamTasksList[index].TaskId;
      this.taskData.Title = this.teamTasksList[index].Title;
      this.taskData.TaskDesc = this.teamTasksList[index].TaskDesc;
      this.taskData.BeginDate = this.formatIsoDateToCrypted(this.teamTasksList[index].BeginDate.split(" ")[0]);
      this.taskData.EndDate = this.formatIsoDateToCrypted(this.teamTasksList[index].EndDate.split(" ")[0]);
      this.taskData.ResponsibleId = this.teamTasksList[index].ResponsibleId;
      this.taskData.StatusId = this.teamTasksList[index].StatusId;
      this.selectedStatus = this.teamTasksList[index].StatusId;
      this.selectedType = this.teamTasksList[index].TypeId;
      this.isSelectedInvoice = this.teamTasksList[index].InvoiceId;
      this.isSelectedCustomer = this.teamTasksList[index].CustomerId;
      this.isSelectedLead = this.teamTasksList[index].LeadId;
      this.isSelectedProject = this.teamTasksList[index].BoardId;
      this.taskData.Creator = this.allUsers[this.allUsers.findIndex(x=>x.value == this.teamTasksList[index].CreatorId)].label;
      this.taskData.TaskTime = this.teamTasksList[index].TaskTime;

      if(this.isSelectedCustomer != 0)
      {
        this.isSelectedRelation = 1;
        this.RelationText = this.sysLanguages.ProfileCustomerRelatedText;
      }
      else if(this.isSelectedInvoice != 0)
      {
        this.isSelectedRelation = 2;
        this.RelationText = this.sysLanguages.ProfileInvoiceRelatedText;
      }
      else if(this.isSelectedLead != 0)
      {
        this.isSelectedRelation = 3;
        this.RelationText = this.sysLanguages.ProfileLeadRelatedText;
      }
      else if(this.isSelectedProject != 0)
      {
        this.isSelectedRelation = 4;
        this.RelationText = this.sysLanguages.ProfileBoardRelatedText;
      }
      else
      {
        this.isSelectedRelation = null;
      }

      this.isSaveorUpdate = "update";
      this.dialogService.open(dialog);
    }
    else if(val == "addTodo")
    {
      
      this.RelationText = "";
      this.selectedType = 0;
      this.taskData.Title = "";
      this.taskData.TaskDesc = "";
      this.taskData.TaskId = 0;
      this.taskData.StatusId = 0;
      this.taskData.BeginDate = "";
      this.taskData.EndDate = "";
      this.taskData.ResponsibleId = 0;
      this.taskData.TaskTime = "";

      this.isSelectedRelation = 0;
      this.selectedStatus = 0;
      this.isSelectedInvoice = 0;
      this.isSelectedCustomer = 0;
      this.isSelectedLead = 0;
      this.isSelectedProject = 0;

      this.isSaveorUpdate = "save";
      this.dialogService.open(dialog);  
    }

  }*/

 /* getUsers()
  {
  this._apicallService.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {     
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if (resulttArray[0].Result == "Empty") {
          this.allUsers = [];
        }
      }
      else
      { 
        for(var i = 0; i < resulttArray.length; i++ ){
          if(resulttArray[i].UserId != 0)
          {
            let _users = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
            this.allUsers.push(_users);
          }
          
        }
      }
    },
    )
  }*/

  /*getTaskStatusList()
  {
  this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.StatusList = [];
          }
        }
        else
        {
          for(var i = 0; i < resultArray.length; i++ ){
            let _status = { label: resultArray[i].StatusDesc, value: resultArray[i].StatusId };
            this.StatusList.push(_status);
          }
        }
    },
    )
  }*/

  /*getInvoices()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.invoiceList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].InvoiceId != 0)
              {
                let _invoice = { label: resultArray[i].InvoiceNumber + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].InvoiceId};
                this.invoiceList.push(_invoice);
              }
            }
          }
      }
    )
  }/*

 /* getCustomers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }*/

  /*editTask(ref)
  {
    this.loading = true;
    var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
    var endDate = <HTMLInputElement> document.getElementById("EndDate");
     let task={
      TaskId: this.taskData.TaskId,
      ParentId: 0,
      Title: $("#taskTitle").val(),
      TaskDesc: $("#taskDesc").val(),
      TypeId: this.selectedType,
      CreatedDate: "",
      ResponsibleId: 1,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: this.isSelectedProject,
      ContractId: 1,
      BeginDate: beginDate.value,
      EndDate: endDate.value,
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 1,
      CustomerId: this.isSelectedCustomer,
      LeadId: this.isSelectedLead,
      InvoiceId: this.isSelectedInvoice,
      TaskTime : $("#time").val(),
    }

    if(task.Title == "" || task.EndDate == "" || task.BeginDate == "" || this.selectedStatus == 0 )
     {
       this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
       this.loading = false;
     }
     else
     {
      this._apicallService.Add("http://localhost:61232/api/Task/Update", task) 
      .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.loading = false;
        ref.close();
        this.getTeamTasks();     
      }          
      }
      )
    } 
  }*/

  /*takeTaskOnMe(ref)
  {   
    this.loading = true;
    this._apicallService.Update("http://localhost:61232/api/Task/TakeTaskOnMe", this.taskData.TaskId, { TaskId : 1, TaskComplete : "0.5", StatusId : 5 })
    .subscribe((result) => {
      if(result[0].Result == "True")
      {
        this.loading = false;
        ref.close();
        this.getTeamTasks();   
      }
      else if(result[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        this.loading = false;
        return;
      }
    },)
  }*/

  getMyTasks()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskByUser") 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length !=0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.notCompletedtaskList = [];
              this.taskListCount = 0;
              this.completedTaskList = [];
              this.completedTaskCount = 0;
              this.copyCompletedTaskList = [];
              this.copynotCompletedTaskList = [];
            }
          }
          else
          {
            this.notCompletedtaskList = [];
            this.taskListCount = 0;
            this.completedTaskList = [];
            this.completedTaskCount = 0;
            this.copyCompletedTaskList = [];
            this.copynotCompletedTaskList = [];
            
            for(var i in resultArray)
            {
              if(resultArray[i].StatusId != 5 && resultArray[i].StatusId != 6)
              {
                if(this.taskListCount < this.pageCountfornotCompleted * this.pageNumfornotCompleted)
                {
                  this.notCompletedtaskList.push(resultArray[parseInt(i)]);
                  this.taskListCount++;
                }
    
                this.copynotCompletedTaskList.push(resultArray[parseInt(i)]);
              }
              else if (resultArray[i].StatusId == 5)
              {
                if(this.completedTaskCount < this.pageCountforCompleted * this.pageNumforCompleted)
                {
                  this.completedTaskList.push(resultArray[parseInt(i)]);
                  this.completedTaskCount++;
                }
    
                this.copyCompletedTaskList.push(resultArray[parseInt(i)]);
              }     
            }          
          }     
        }, )
  }


  /*saveTask(ref)
  {
    this.loading = true;
    let _today = new Date().toISOString(); 

    let task = {
      TaskId : 0,
      ParentId: 0,
      Title : $("#taskTitle").val(),
      TaskDesc : $("#taskDesc").val(),
      TypeId : this.selectedType,
      CreatedDate : _today.split("T")[0],
      CustomerId : this.isSelectedCustomer,
      ResponsibleId : 1,
      StatusId : this.selectedStatus,
      BeginCheckListId:1,
      EndCheckListId:  1,
      BoardId: this.isSelectedProject,
      ContractId:1,
      BeginDate: $("#BeginDate").val(),
      EndDate: $("#EndDate").val(),
      CreatorId: 1,
      TeamId: 1,
      LeadId: this.isSelectedLead,
      InvoiceId: this.isSelectedInvoice,
      TaskTime : $("#time").val(),
    };

    if(task.EndDate == "" || task.TypeId == 0 || task.BeginDate == "" )
    {
      this.makeToast(this.status,this.sysLanguages.AddTaskAlertText,this.sysLanguages.PcToastrErrorTitle);
      this.loading = false;
    }
    else
    {
      if(!task.Title)
      {
        task.Title = _today.split("T")[0] + " " + this.taskTypeList[this.taskTypeList.findIndex(x => x.value == this.selectedType)].label;
      }
      this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskForMe", task) .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          this.loading = false;
          return;
        }
        else 
        {
          this.getMyTasks();
          this.loading = false;
          ref.close();
        }         
      }, 
    )
    }
  }*/

  completeTask(id)
  {
    let task={ TaskId : id, TaskComplete : "0.5", StatusId : 5 };
    this._apicallService.Update("http://localhost:61232/api/Task/TaskComplete", id, task) 
        .subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.getMyTasks();
          }
          else if (resultArray[0].Result == "False") {
            this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.TasksToastrCantCompleted);
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )

  }

  /*getTaskType()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskType").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.taskTypeList = [];
            }
          }
          else
          { 
            this.taskTypeList = [];
            for(let i in resultArray)
            {
              let _types = { label: resultArray[i].TypeDesc, value: resultArray[i].TypeId };
              this.taskTypeList.push(_types);
            }
          }
      }
    )
  }*/

  getNotCompletedTasksforLazyLoading()
  {
    this.notCompletedtaskList = [];
    this.taskListCount = 0;

    for(let i in this.copynotCompletedTaskList)
    {
      if(this.taskListCount == this.pageCountfornotCompleted * this.pageNumfornotCompleted)
      {
        break;
      }
      else
      {
        this.notCompletedtaskList.push(this.copynotCompletedTaskList[parseInt(i)]);
        this.taskListCount++;
      }
    }
  }

  getCompletedTasksforLazyLoading()
  {
    this.completedTaskList = [];
    this.completedTaskCount = 0;

    for(let i in this.copyCompletedTaskList)
    {
      if(this.completedTaskCount == this.pageCountforCompleted * this.pageNumforCompleted)
      {
        break;
      }
      else
      {
        this.completedTaskList.push(this.copyCompletedTaskList[parseInt(i)]);
        this.completedTaskCount++;
      }
    }
  }

  getProjects()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Project/GetTaskBoards").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.projectList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].BoardId != 0)
              {
                let _projects = { label: resultArray[i].BoardName + " ( " +resultArray[i].CustomerName + " )" , value: resultArray[i].BoardId };
                this.projectList.push(_projects);
              }
            }
          }
      }
    )
  }

  saveClient(val)
  {
    if(val == 0)
    { 
      this.clientid = $("#clientid").val();
    }
    else
    {
      this.clientsecret = $("#clientsecret").val();
      let userAuth = { ClientId : this.clientid , ClientSecret : this.clientsecret  };
      this._apicallService.Add("http://localhost:61232/api/User/AddUserClient",userAuth).subscribe((result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.SuccessMessage, this.sysLanguages.SuccessMessage1);
          }
          else if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }        
        }, )

    }
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }
}
