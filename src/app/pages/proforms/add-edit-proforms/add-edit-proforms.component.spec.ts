import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditProformsComponent } from './add-edit-proforms.component';

describe('AddEditProformsComponent', () => {
  let component: AddEditProformsComponent;
  let fixture: ComponentFixture<AddEditProformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditProformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditProformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
