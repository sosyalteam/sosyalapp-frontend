import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-proforms',
  templateUrl: './add-edit-proforms.component.html',
  styleUrls: ['./add-edit-proforms.component.scss'],
  providers: [ApicallService, NbToastrService]
})
export class AddEditProformsComponent implements OnInit {

  sysLanguages: any;
  _proformid: any;
  proformData = { Amount: "", CreatedDate: "", ExpiredDate: "", ProformId: "", Status: 0, Discount: 0, Currency: 0, Customer: 0, Project: 0, ProformNumber: "", Salesman: 0, AdminNote: "", CustomerNote: "", Conditions: "", Contact: 0, Language: 0, Product: 0 , TotalDiscount : "0"};
  lineData = [{ Id: '', ProductName: '', LineDesc: '', ProductCount: 1, ProductPrice: 0, LineAmount: "0", updatedefaultTax: "", LineDiscount : '0' }];
  addedDynamicLineData = [];
  proformstatusList = [];
  discountList = [];
  currencyList = [];
  productList = [];
  taxList = [];
  customerList = [];
  customerData = { TaxNumber: "", CustomerDiscount: ""};
  contactList = [];
  customerContact = [];
  userList = [];
  projectList = [];
  languageList = [];
  selectedCustomerContacts = [];
  copyselectedCustomerContacts = [];
  tax: any;
  TaxDesc: string;
  sendMail = false;

  _total = 0;
  topTax = 0;
  topTaxArray = [];
  _totalDiscount = 0;
  loading = false;

  currency = "TL";

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _router: Router, private _apicallservice: ApicallService, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  { 
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProformText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this._proformid = this._avRoute.snapshot.params["id"];
    }

    this.languageList.push({ label: this.sysLanguages.LanguageOptionTurkish, value: 0 }, { label: this.sysLanguages.LanguageOptionEnglish, value: 1 });

    this.getProformStatusses();
    this.getDiscountTypes();
    this.getCurrencies();
    this.getProducts();
    this.getTax();
    this.getCustomers();
    this.getUsers();
    this.getAllContact();
  }


  ngOnInit() 
  {
    if(this._proformid>0)
    {
      setTimeout(()=>{
      this._apicallservice.Get("http://localhost:61232/api/Proform/GetProform", this._proformid)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            this.proformData.Status = Number(resulttArray[0].StatusId);
            this.proformData.Currency = Number(resulttArray[0].CurrencyTypeId);
            this.proformData.Customer = Number(resulttArray[0].CustomerId);
            this.proformData.Discount = Number(resulttArray[0].DiscountTypeId);
            this.proformData.Product = 0;
            this.proformData.Salesman = Number(resulttArray[0].SalesmanId);
            this.proformData.Amount = resulttArray[0].Amount;   
            this.proformData.ExpiredDate = this.formatIsoDateToCrypted(resulttArray[0].ExpirementDate.split(" ")[0]);
            this.proformData.CreatedDate = this.formatIsoDateToCrypted(resulttArray[0].CreatedDate.split(" ")[0]);
            this.proformData.AdminNote = resulttArray[0].AdminNote;
            this.proformData.Conditions = resulttArray[0].Conditions;
            this.proformData.CustomerNote = resulttArray[0].CustomerNote;
            this.proformData.ProformNumber = resulttArray[0].ProformNumber;
            this.proformData.Project = resulttArray[0].ProjectId;
            this.proformData.ProformId = resulttArray[0].ProformId;
            this.proformData.TotalDiscount = resulttArray[0].Discount;

            this.getProject(this.proformData.Customer);
            
            this.customerContact = [];

            var customerDesc = document.getElementById("customerDesc");
            customerDesc.style.visibility="visible";

            for(let i in this.contactList)
            {
              if(this.contactList[i].customerId == this.proformData.Customer)
              {
                this.customerContact.push(this.contactList[i]);
              }
            }

            for(let i in this.customerList)
            {
              if(this.customerList[i].value == this.proformData.Customer)
              {
                this.customerData.TaxNumber = this.customerList[i].taxNumber; 
                break;
              }
            }

            this._apicallservice.Get("http://localhost:61232/api/Proform/GetProformContacts", this._proformid).subscribe(
              (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(resultArray[0].Result == "Empty")
                    {
                      this.selectedCustomerContacts = [];
                    }
                  }
                  else
                  { 
                    this.selectedCustomerContacts = resultArray.map(x=> x.ContactId);
                    this.copyselectedCustomerContacts = this.selectedCustomerContacts.slice();
                  }
              }
            )

          }              
      }
      )

      this._apicallservice.Get("http://localhost:61232/api/Proform/GetProformLines", this._proformid).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            debugger;
            /*for(var i in resultArray)
            {
              let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName };
              this.lineData.push(_lineData);
            }*/
            let _lineData;          
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }

            this.updatedTotal();
          }
        }
      )
      }, 1500);
    }
    else
    {
      let _today = new Date(); 
      let _expiredDate = new Date();
      _expiredDate.setDate(_today.getDate()+30);

      this.proformData.Status = 0;
      this.proformData.Currency = 0;
      this.proformData.Customer = 0;
      this.proformData.Discount = 0;
      this.proformData.Product = 0;
      this.proformData.Salesman = 0;
      this.proformData.Amount = "";
      this.proformData.ExpiredDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
      this.proformData.CreatedDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
      this.proformData.Language = 0;
      this.proformData.AdminNote = "";
      this.proformData.CustomerNote = "";
      this.proformData.Conditions = "";
      this.proformData.ProformNumber = "";
      this.selectedCustomerContacts = []; 
    }
  }

  getProformStatusses()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proform/GetAllStatus").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.proformstatusList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _status;

            if(resultArray[i].StatusId == 1)    
            {
              _status = { label: this.sysLanguages.PcDraftStatus, value: resultArray[i].StatusId };
              this.proformstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 2)
            {
              _status = { label: this.sysLanguages.PcSentStatus, value: resultArray[i].StatusId };
              this.proformstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 3)
            {
              _status = { label: this.sysLanguages.PcExpiredStatus, value: resultArray[i].StatusId };
              this.proformstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 4)
            {
              _status = { label: this.sysLanguages.PcRefusedStatus, value: resultArray[i].StatusId };
              this.proformstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 5)
            {
              _status = { label: this.sysLanguages.PcAcceptedStatus, value: resultArray[i].StatusId };
              this.proformstatusList.push(_status);
            }      
          }
          this.proformData.Status = 1;
        }
    }
    )
  }

  getDiscountTypes()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetDiscountTypes").subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.discountList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _discount;

              if(resultArray[i].DiscountTypeId == 1)
              {
                _discount = { label: this.sysLanguages.DiscountTypeNoDiscount, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
              else if(resultArray[i].DiscountTypeId == 2)
              {
                _discount = { label: this.sysLanguages.DiscountTypeBeforeTax, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
              else if(resultArray[i].DiscountTypeId == 3)
              {
                _discount = { label: this.sysLanguages.DiscountTypeAfterTax, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
            }
            this.proformData.Discount = 1;
          }
      },
      )
  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.currencyList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
              this.currencyList.push(_currency);
            }
            this.proformData.Currency = 1;
          }
      }
      )
  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }

                  }
                }

                _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, price: resultArray[i].Price, desc: resultArray[i].ProductDesc };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }
          }
        }
    },
    )
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  getProject(_customerid)
  {
    this._apicallservice.Get("http://localhost:61232/api/Invoice/GetBoardDetail", _customerid).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if (resultArray[0].Result == "Empty") {
              this.projectList = [];
            }
          }
          else
          {
            this.projectList = [];

            for(let i in resultArray)
            {
              let _projectlist = { label: resultArray[i].BoardName, value: resultArray[i].BoardId };
              this.projectList.push(_projectlist);
            }
          }
        }
      )

  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId, address: resultArray[i].Address, city: resultArray[i].City, discount: resultArray[i].CustomerDiscount, taxNumber: resultArray[i].TaxNumber };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }

  getUsers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].UserId != "0")
            {
              let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
              this.userList.push(_user);
            }
          }
        }        
      },
      )
  }

  getAllContact()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].ContactId != 0)
              {
                let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId, customerId: resultArray[i].CustomerId, email: resultArray[i].Email };
                this.contactList.push(_contact);
              }
            }
          }
      }
    )
  }

  getProductDetail(event)
  {
    if(!this._proformid)
    {
        this.lineData = [];
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "", LineDiscount : "" };
            this.lineData.push(_lineData);
          }
        }
    }
    else
    {
        this.lineData.shift();
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
           let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "", LineDiscount : "" };
            this.lineData.unshift(_lineData);
          }
        }
    }
  }

  getCustomerDetail(event)
  {
    var customerDesc = document.getElementById("customerDesc");
    customerDesc.style.visibility="visible";
    
    var customerid = event;
    this.customerContact = [];

    for(let i in this.customerList)
    {
      if(this.customerList[i].value == customerid)
      {
        this.customerData.TaxNumber = this.customerList[i].taxNumber;

        if(this.customerList[i].discount == null)
        {
          this.customerData.CustomerDiscount= null;
        }
        else
        {
          this.customerData.CustomerDiscount = this.customerList[i].discount.slice(1,4);
        }
        
        break;
      }
    }

    for(let i in this.contactList)
    {
      if(this.contactList[i].customerId == this.proformData.Customer)
      {
        this.customerContact.push(this.contactList[i]);
      }
    }
     
    this.selectedCustomerContacts = [];
    this.projectList = [];
    this.getProject(customerid);

  }

  saveProform()
  {
    this.loading = true;
    if(!this._proformid)
    {
      var x = document.getElementById("total");

      let proform = {
        ProformId: 0,
        StatusId: this.proformData.Status,
        SalesmanId: this.proformData.Salesman,
        CustomerId: this.proformData.Customer,
        ProjectId: this.proformData.Project,
        ProposalId: 0,
        ProformNumber: $("#proformNumber").val(),
        CreatedDate:  $("#CratedDate").val(),
        ExpirementDate: $("#LastDate").val(),
        DiscountTypeId: this.proformData.Discount,
        CurrencyTypeId: this.proformData.Currency,
        AdminNote: $("#adminNote").val(),
        CustomerNote: $("#CustomerNote").val(),
        Conditions: $("#Conditions").val(),
        Amount: x.innerText,
        Discount : $("#totaldiscount").val()
      };

      if(proform.Amount == "0.00 " + this.currency  || proform.Amount == "0 " + this.currency)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ErrorNoProduct);
        return;
      }
      else if(proform.ProformNumber == "" || proform.ExpirementDate == "" || proform.CreatedDate == "" || proform.StatusId == 0 || proform.SalesmanId == 0 || proform.CustomerId == 0 || proform.DiscountTypeId == 0 || proform.CurrencyTypeId == 0)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        this._apicallservice.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
        (result) => {
          var proformId = result[0].Result;
          proform.ProformId = proformId;
          let proformdetail;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);

          for(let i = 0; i < this.addedDynamicLineData.length; i++)
          {
            let _product = 0;
            
            for(let j in this.productList)
            {
              if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
              {
                _product = this.productList[j].value;
              }
            }

            if(_product == 0)
              proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
            else
              proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
            
            this._apicallservice.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
              else
              {
                this.loading = false;
                //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                this._router.navigate(['/pages/proforms']);
              }
            }
            )
          }   
          
          this.addedDynamicLineData = [];   

          let proformcontact;

          for(let j in this.selectedCustomerContacts)
          {
            proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
          
            this._apicallservice.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
            (result) =>{
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )

            if(this.sendMail == true)
            {
              proform.ProformNumber = this.contactList[this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])].email;
              this._apicallservice.Update("http://localhost:61232/api/Proform/SendProform", this.proformData.Language, proform).subscribe(
                (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this.loading = false;
                      this._router.navigate(['/auth']);
                      return;
                    }
                  }
                }
              )
            }
          }       
          }
        )
      }

    }
    else
    {
      var x =document.getElementById("total");

      if(x.innerHTML == "0 " + this.currency)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ErrorNoProduct);
        return;
      }
      else if($("#proformNumber").val() == "" || $("#CratedDate").val() == "" || $("#LastDate").val() == "" || this.proformData.Status == 0 || this.proformData.Salesman == 0 || this.proformData.Customer == 0 || this.proformData.Discount == 0 || this.proformData.Currency == 0)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        let proform = {
          ProformId: this._proformid,
          StatusId: this.proformData.Status,
          SalesmanId: this.proformData.Salesman,
          CustomerId: this.proformData.Customer,
          ProjectId: this.proformData.Project,
          ProposalId: 0,
          ProformNumber: $("#proformNumber").val(),
          CreatedDate:  $("#CratedDate").val(),
          ExpirementDate: $("#LastDate").val(),
          DiscountTypeId: this.proformData.Discount,
          CurrencyTypeId: this.proformData.Currency,
          AdminNote: $("#adminNote").val(),
          CustomerNote: $("#CustomerNote").val(),
          Conditions: $("#Conditions").val(),
          Amount: x.innerText,
          Discount : $("#totaldiscount").val()
        };
  
        this._apicallservice.Add("http://localhost:61232/api/Proform/EditProform", proform).subscribe(
          (result) => {
            let total = 0;
            let lineCount = 0;
  
            let resultArray = JSON.parse(JSON.stringify(result));
  
            if(this.lineData != [])
            {
              for(let i in this.lineData)
              {
                if(this.lineData[i].LineAmount != "0")
                {
                  total += Number(this.lineData[i].LineAmount.split(" ")[0]);
                }
              }
            }
  
            if(this.addedDynamicLineData != [])
            {
              for(let j in this.addedDynamicLineData)
              {
                if(this.addedDynamicLineData[j].LineAmount != "0")
                {
                  total += Number(this.addedDynamicLineData[j].LineAmount.split(" ")[0]);
                }
              }
            }
  
            if(total != Number($("#total").val().toString().split(" ")[0]))
            {
                if(this.lineData.length != 0)
                {
                  for(let i = 0; i < this.lineData.length; i++)
                  {
                    if(this.lineData[i].LineAmount != "0")
                    {
                      lineCount++;   
                      let _line = { Id: this.lineData[i].Id, LineId: lineCount };                               
                      this._apicallservice.Update("http://localhost:61232/api/Proform/EditProformLines", Number(this.lineData[i].Id), _line).subscribe(
                        (resultt) =>{
                        }
                      )              
                    }                
                  }
                }
                if(this.addedDynamicLineData != [])
                {
                  let _product = 0;
                  for(let i in this.addedDynamicLineData)
                  {
                    for(let j in this.productList)
                    {
                      if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                      {
                        _product = this.productList[j].value;
                      }
                    }             
                    lineCount++;
                    let _proformdetail;
  
                    if(_product == 0)
                      _proformdetail = { ProformId: this._proformid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].updatedefaultTax, ProductId: _product, LineId: lineCount, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                    else
                      _proformdetail = { ProformId: this._proformid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].updatedefaultTax, ProductId: _product, LineId: lineCount, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};
  
                    this._apicallservice.Add("http://localhost:61232/api/Proform/CreateProformDetail", _proformdetail).subscribe(
                      (resultt) =>{
                      }
                    )
                  }
                }
  
                this.loading = false;
                this.addedDynamicLineData = [];
                this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrEdited);
                this._router.navigate(['/pages/proforms']);
            }
  
            //---------- eğer eski seçilmiş kontak varsa onları sil
            for(let i in this.copyselectedCustomerContacts)
            {
              if(this.selectedCustomerContacts.findIndex(x => x.value == this.copyselectedCustomerContacts[i]) == -1)
              {
                let proformcontact = { ProformId: this._proformid, ContactId: this.copyselectedCustomerContacts[i] }; 
                this._apicallservice.Add("http://localhost:61232/api/Proform/RemoveProformContact", proformcontact).subscribe(
                  (result) =>{
                  }
                )
              }
            }
  
            //----------- yeni eklenen varsada onları ekle
            for(let j in this.selectedCustomerContacts)
            {
              if(this.copyselectedCustomerContacts.findIndex(x => x.value == this.selectedCustomerContacts[j]) == -1)
              {
                let proformcontact = { ProformId: this._proformid, ContactId: this.selectedCustomerContacts[j] };   
                this._apicallservice.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                  (result) =>{
                  }
                )
              }
  
              if(this.sendMail == true)
              {
                let index = this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j]);
                proform.ProformNumber = this.contactList[index].email;
                this._apicallservice.Update("http://localhost:61232/api/Proform/SendProform", this.proformData.Language, proform).subscribe(
                  (result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray.length != 0 && resultArray[0].Result) {
                      if (resultArray[0].Result == "Session_Expired") {
                        this.loading = false;
                        this._router.navigate(['/auth']);
                        return;
                      }
                    }
                  }
                )
              }
            } 
        
            }
         )
      }
    }
  }

  saveandSent()
  {
    this.sendMail = true;
    this.saveProform();
  }

  //---------- Line Functions ------------------------
  updatedTotal()
  {
    var total = 0;
    var z =document.getElementById("total");
    for(var i = 1; i < this.lineData.length; i++)
    {
      if(this.lineData[i].LineAmount != "0")
      {
        var sub=parseFloat(this.lineData[i].LineAmount);
        total=total+sub;
        z.innerHTML=Number(total).toLocaleString()+" " + this.currency;
        this._total = total;
        if(this.lineData[i].LineDiscount)
        {
            this._totalDiscount += Number(this.lineData[i].LineDiscount);
        }

        if(this.lineData[i].updatedefaultTax == "%18 KDV")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.18, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice});
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);   
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
      }
    }
    
    var discount = this.proformData.TotalDiscount;
    if(discount)
    {
      this._total = this._total - parseFloat(discount);
    }
    z.innerHTML=Number(this._total).toLocaleString()+" " + this.currency;
  }

  total(id)
  {
    var y = document.getElementById("f"+id).innerHTML.split(" ")[0];
    var x =document.getElementById("total");
    this._total = parseFloat(y) + this._total;
    x.innerText=Number(this._total).toLocaleString()+" " + this.currency;
  }

  createLine()
  {
    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid= line[0].childElementCount+1;
    var a1 =$("#a1").val();
    var b1 =$("#b1").val();
    var c1 =$("#c1").val().toString();
    var d1 =$("#d1").val().toString();
    var dis1=$("#dis1").val().toString();
    if(dis1 == "")
    {
      var total = (parseFloat(c1)*parseFloat(d1));
      dis1 = '0'; 
    }
    else
    {
      this._totalDiscount += parseFloat(dis1);
      var total = (parseFloat(c1)*parseFloat(d1))-parseFloat(dis1);
    }

    if(!this.tax || this.tax.length == 0)
    {
      this.TaxDesc="Vergi seçimi yapılmadı.";
    }
    else if(this.tax.length == 1)
    {
      if(this.tax[0] == 1)
      {
        this.TaxDesc="%18 KDV";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18), amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.18, amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18);
        }

        total=total+(total*0.18);
      }
      else if(this.tax[0] == 2)
      {
        this.TaxDesc="%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total*17.65/100);
      }
      else if(this.tax[0] == 3)
      {
        this.TaxDesc="%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += (total * 0.15);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15);
        }

        total=total+(total*0.15);
      }
    }
    else if(this.tax.length == 2)
    {
      if((this.tax[0] == 1 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 1))
      {
        this.TaxDesc="%18 KDV,%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100);   
        }

        total=total+(total*0.18)+(total*17.65/100);
      }
      else if((this.tax[0] == 1 && this.tax[1] == 3) || (this.tax[0] == 3 && this.tax[1] == 1))
      {
        this.TaxDesc="%18 KDV,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18);
        }

        total=total+(total*0.15)+(total*0.18);
      }
      else if((this.tax[0] == 3 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 3))
      {
        this.TaxDesc="%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total*0.15)+(total*17.65/100);
      }
    }
    else if(this.tax.length == 3)
    {
        this.TaxDesc="%18 KDV,%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total*0.18)+(total*0.15)+(total*17.65/100);
    }

    if (total.toString()=="NaN")
    {
      total=0;
    }
    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><p id='c"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;''>" + c1 + "</p></td><td><p id='d"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + d1 + "</p></td><td><p id='dis"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + dis1 + "</p></td><td><p id='updatedefaultTax' style='float: left;margin-top: 26px;margin-left:5px;' >"+this.TaxDesc+"</p></td><td><p id='f"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>"+total+" "+this.currency+" </p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red ;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_"+newid);
    let mythis = this;

    this.total(newid);
    
    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let priceValue = element.parentElement.parentElement.children[6].children[0].innerHTML;
      let priceforTaxValue = element.parentElement.parentElement.children[3].children[0].innerHTML;
      let discounttext = element.parentElement.parentElement.children[4].children[0].innerHTML;
      let counttext = element.parentElement.parentElement.children[2].children[0].innerHTML;

      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue && x.LineAmount == priceValue), 1);

      var id = element.id.split("_")[1];
      var minus = $("#f"+id).text().split(" ")[0];
      document.getElementById(id).remove();
      var a = document.getElementById("total");
      this._total = this._total - parseFloat(minus);
      a.innerText = Number(this._total).toLocaleString() + " " + this.currency; 

      for(let i in this.topTaxArray)
      {
        if(this.topTaxArray[i].amount == priceValue.split(" ")[0] && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
        {
          this.topTax -= this.topTaxArray[i].value;
          if(this.topTax < 0)
          {
            this.topTax = 0;
          }
          break;
        }
      }

      this._totalDiscount -= parseFloat(discounttext);
      
    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString(), ProductCount: Number(c1), ProductPrice: Number(d1), LineAmount: total + " " + this.currency, updatedefaultTax: "", LineTax: this.TaxDesc , LineDiscount : dis1 };
    this.addedDynamicLineData.push(_lineData);
    
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let priceforTaxValue = event.currentTarget.parentElement.parentElement.children[3].children[0].innerText;
    let priceValue = event.currentTarget.parentElement.parentElement.children[6].children[0].innerText;
    let discounttext = event.currentTarget.parentElement.parentElement.children[4].children[0].innerText;
    let counttext = event.currentTarget.parentElement.parentElement.children[2].children[0].innerText;

    for(let i in this.topTaxArray)
    {
      if(this.topTaxArray[i].amount == priceValue && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
      {
        this.topTax -= this.topTaxArray[i].value;
        if(this.topTax < 0)
        {
          this.topTax = 0;
        }
        break;
      }
    }

    this._totalDiscount -= parseFloat(discounttext);

    this._apicallservice.Deactive("http://localhost:61232/api/Proform/RemoveProformLine", Number(this.lineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          var x = this.lineData[id].LineAmount.split(" ")[0];
          var total =document.getElementById("total");
          var newTotal = this._total-parseFloat(x);
          this._total = newTotal;
          total.innerHTML=Number(newTotal).toLocaleString()+" " + this.currency;
          document.getElementById("lineData_" + id).remove();
          this.lineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  totalDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total - parseFloat(y);
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
  }

  cleanDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total + parseFloat(y);
    this._total = discountedTotal;
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
    $("#totaldiscount").val("0");
  }

  changeCurrencyType(event)
  {
    if(event == 1)
    {
      this.currency = "TL";
    }
    else if(event == 2)
    {
      this.currency = "$";
    }
    else if(event == 3)
    {
      this.currency = "€";
    }
    else if(event == 4)
    {
      this.currency = "£";
    }
  }
  //-------------------------------------------------------

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
