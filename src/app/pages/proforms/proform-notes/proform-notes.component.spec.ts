import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformNotesComponent } from './proform-notes.component';

describe('ProformNotesComponent', () => {
  let component: ProformNotesComponent;
  let fixture: ComponentFixture<ProformNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProformNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProformNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
