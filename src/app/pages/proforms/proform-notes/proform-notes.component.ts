import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proform-notes',
  templateUrl: './proform-notes.component.html',
  styleUrls: ['./proform-notes.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class ProformNotesComponent implements OnInit {

  sysLanguages: any;
  proformId : any;

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProformNotesText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.proformId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title = this.sysLanguages.ProformInformation;
    this.tabs[1].title = this.sysLanguages.BoardsNoteText;

    this.getComments();
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/proforms/proform-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'ion-ios-copy-outline',
      route: ['/pages/proforms/proform-notes', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/proforms/add-edit-proforms', this._avRoute.snapshot.params["id"]]);
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Proform/ProformComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : this.proformId,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

}
