import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformOverviewComponent } from './proform-overview.component';

describe('ProformOverviewComponent', () => {
  let component: ProformOverviewComponent;
  let fixture: ComponentFixture<ProformOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProformOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProformOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
