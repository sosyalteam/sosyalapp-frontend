import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proform-overview',
  templateUrl: './proform-overview.component.html',
  styleUrls: ['./proform-overview.component.scss'],
  providers: [ApicallService, NbToastrService]
})
export class ProformOverviewComponent implements OnInit {

  sysLanguages: any;
  proformArray = { ProformId: 0, ProformNumber:'', Customer:'', AdminNote: '', CustomerNote:'', Conditions:'', Status:'', User:'', Project:'', CreatedDate:'', ExpiredDate: '', Amount : '', CopyAmount: '', CustomerId: 0, StatusId: 0, ProjectId: 0, DiscountTypeId: 0, CurrencyTypeId: 0, UserId: 0 , Discount : "" };
  invoiceList = [];
  productList = [];

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProformOverviewText + " - Crmplus");

    this.tabs[0].title = this.sysLanguages.ProformInformation;

    this.settings.columns.ProductName.title = this.sysLanguages.AddInvoiceTableProduct;
    this.settings.columns.Description.title = this.sysLanguages.AddInvoiceTableDesc;
    this.settings.columns.LineAmount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settings.columns.LineTax.title = this.sysLanguages.AddInvoiceTableTax;
    this.settings.columns.LineDiscount.title = this.sysLanguages.AddInvoiceDiscount;


    this.getInvoices();
    this.getProformProduct();
    this.getComments();
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/proforms/proform-overview', this._avRoute.snapshot.params["id"]],
    },
  ];

  settings = {
    columns: {
      ProductName: {
        title: ''
      },
      Description: {
        title: ''
      },
      LineAmount: {
        title: ''
      },
      LineTax: {
        title: ''
      },
      LineDiscount: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/Proform/GetProform",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {

            }
            else if (resultArray.length != 0 && resultArray[0].Result) {
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.proformArray.AdminNote = resultArray[0].AdminNote;
            this.proformArray.Amount =  resultArray[0].Amount;
            this.proformArray.CopyAmount = resultArray[0].Amount;
            this.proformArray.Conditions = resultArray[0].Conditions;
            this.proformArray.CreatedDate = resultArray[0].CreatedDate.toString().split(" ")[0];
            this.proformArray.CurrencyTypeId = resultArray[0].CurrencyTypeId;
            this.proformArray.Customer = resultArray[0].CustomerName;
            this.proformArray.CustomerId = resultArray[0].CustomerId;
            this.proformArray.CustomerNote = resultArray[0].CustomerNote;
            this.proformArray.DiscountTypeId = resultArray[0].DiscountTypeId;
            this.proformArray.ExpiredDate = resultArray[0].ExpirementDate.toString().split(" ")[0];
            this.proformArray.ProformId = resultArray[0].ProformId;
            this.proformArray.ProformNumber = resultArray[0].ProformNumber;
            this.proformArray.Project = resultArray[0].BoardName;
            this.proformArray.ProjectId = resultArray[0].ProjectId;
            this.proformArray.StatusId = resultArray[0].StatusId;
            this.proformArray.User = resultArray[0].Fullname;
            this.proformArray.UserId = resultArray[0].UserId;
            this.proformArray.Discount = resultArray[0].Discount;

            if(this.proformArray.AdminNote == "")
            {
              this.proformArray.AdminNote = this.sysLanguages.PcNoNote;
            }
            if(this.proformArray.CustomerNote == "")
            {
              this.proformArray.CustomerNote = this.sysLanguages.PcNoNote;
            }
            if(this.proformArray.Conditions == "")
            {
              this.proformArray.Conditions = this.sysLanguages.PcNoNote;
            }
            if(this.proformArray.Project == "NoBoard")
            {
              this.proformArray.Project = this.sysLanguages.NotValueText;
            }

            if(resultArray[0].StatusId == 1)
            {
              this.proformArray.Status = this.sysLanguages.PcProformDraftStatus;
            }
            else if(resultArray[0].StatusId == 2)
            {
              this.proformArray.Status = this.sysLanguages.PcProformSentStatus;
            }
            else if(resultArray[0].StatusId == 3)
            {
              this.proformArray.Status = this.sysLanguages.PcProformExpiredStatus;
            }
            else if(resultArray[0].StatusId == 4)
            {
              this.proformArray.Status = this.sysLanguages.PcProformRefuseStatus;
            }
            else if(resultArray[0].StatusId == 5)
            {
              this.proformArray.Status = this.sysLanguages.PcProformAcceptedStatus;
            }
            else if(resultArray[0].StatusId == 6)
            {
              this.proformArray.Status = this.sysLanguages.PcConvertedStatus;
            }

          }       
      }

      )

  }

  openUrl(link)
  {
    if(link == "/proform")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else if(link == "/proform/pdf")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else
    {
      this._router.navigate([link, this._avRoute.snapshot.params["id"]]);
    }
  }

  getInvoices()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.invoiceList = [];
          }
        }
        else 
        {
          this.invoiceList = resultArray;
        }
      }
    )
  }

  getProformProduct()
  {
    this._apicallService.Get("http://localhost:61232/api/Proform/GetProformLines",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              if(resultArray[i].ProductId == 0)
              {
                resultArray[i].ProductName = resultArray[i].LineName;
              }
            }

            this.productList = resultArray.slice();
          }
      }
    )
  }

  convertToInvoice()
  {
    let index = this.invoiceList.findIndex(x => x.ProformId == this.proformArray.ProformId);
    if(index != -1)
    {
      this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.PcToastrConvertedBefore);
    }
    else
    {
      this._apicallService.Update("http://localhost:61232/api/Proform/ConvertProform", this.proformArray.ProformId , "notImportantWhatYouSent").subscribe(
        (result) => {
        }
      )

      // eğer oluştuğu sipariş öncesinde bir tekliften oluşuyosa önemli değil neyden oluştuğu biz sadece sipariş ile bağlıyoruz.
      let invoice = {
        InvoiceId: 0,
        CustomerId: this.proformArray.CustomerId,
        ProjectId: this.proformArray.ProjectId,
        InvoiceNumber: this.proformArray.ProformNumber,
        CreatedDate: this.proformArray.CreatedDate + " 12:00:00",
        LastPaymentDate: this.proformArray.ExpiredDate + " 12:00:00",
        PayingMethodId: 1,
        ProposalId: 0,
        ProformId: this.proformArray.ProformId,
        SalesmanId: this.proformArray.UserId,
        DiscountTypeId: this.proformArray.DiscountTypeId,
        CurrencyTypeId: this.proformArray.CurrencyTypeId,
        AdminNote: "--Proform--///" + this.proformArray.AdminNote,
        CustomerNote: this.proformArray.CustomerNote,
        Conditions: this.proformArray.Conditions,
        StatusId: 5,
        Amount : this.proformArray.CopyAmount,
        Discount : this.proformArray.Discount
      };
      
      this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice)
      .subscribe((result) => {
        if (result[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else
        {
          var invoiceId = (result[1].Value);
          this._router.navigate(['/pages/invoices/add-edit-invoices',invoiceId]);
        }
      }
      )

      this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrConverted);
      this.ngOnInit();
    }
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Proform/ProformComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : this._avRoute.snapshot.params["id"],
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  convertToProSub(value)
  {
    if(value == "project")
    {
      let projectArray = {ProformId : this._avRoute.snapshot.params["id"] , ProformName : this.proformArray.ProformNumber, CreatedDate : this.proformArray.CreatedDate , UserId : this.proformArray.UserId , CustomerId : this.proformArray.CustomerId , ProjectorSub : value };
      sessionStorage.setItem("projectSubsData",JSON.stringify(projectArray));
      this._router.navigate(['/pages/project/add-edit-project']);
    }
    else if(value == "subs")
    {
      let subsArray = {ProformId : this._avRoute.snapshot.params["id"] , ProformName : this.proformArray.ProformNumber, CreatedDate : this.proformArray.CreatedDate , UserId : this.proformArray.UserId , CustomerId : this.proformArray.CustomerId , ProjectorSub : value };
      sessionStorage.setItem("projectSubsData",JSON.stringify(subsArray));
      this._router.navigate(['/pages/project/add-edit-project']);
    }
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
