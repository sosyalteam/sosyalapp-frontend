import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProformsComponent } from './proforms/proforms.component';
import { AddEditProformsComponent } from './add-edit-proforms/add-edit-proforms.component';
import { ProformOverviewComponent } from './proform-overview/proform-overview.component';
import { ProformNotesComponent } from './proform-notes/proform-notes.component';

const routes: Routes = [
  {
    path: '',
    component: ProformsComponent,
  },
  {
    path: 'add-edit-proform',
    component: AddEditProformsComponent,
  },
  {
    path: 'add-edit-proform/:id',
    component: AddEditProformsComponent,
  },
  {
    path: 'proform-overview/:id',
    component: ProformOverviewComponent,
  },
  {
    path: 'proform-notes/:id',
    component: ProformNotesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProformsRoutingModule { }
