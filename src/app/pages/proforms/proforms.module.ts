import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProformsRoutingModule } from './proforms-routing.module';
import { ProformsComponent } from './proforms/proforms.component';
import { AddEditProformsComponent } from './add-edit-proforms/add-edit-proforms.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbDialogModule, NbContextMenuModule, NbSelectModule, NbTabsetModule, NbRouteTabsetModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProformOverviewComponent } from './proform-overview/proform-overview.component';
import { ProformNotesComponent } from './proform-notes/proform-notes.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [ProformsComponent, AddEditProformsComponent, ProformOverviewComponent, ProformNotesComponent],
  imports: [
    CommonModule,
    ProformsRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule,
    NbTabsetModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NgxPaginationModule
  ]
})
export class ProformsModule { }
