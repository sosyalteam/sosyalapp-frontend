import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proforms',
  templateUrl: './proforms.component.html',
  styleUrls: ['./proforms.component.scss'],
  providers: [ApicallService]
})
export class ProformsComponent implements OnInit {

  public sysLanguages : any;
  public proformList = [];
  public convertedProformList = [];
  public allProformCount = 0;
  public draftCount = 0;
  public sendCount = 0;
  public expiredCount = 0;
  public deniedCount = 0;
  public checkedCount = 0;
  public convertedCount = 0;
  filter =false;
  filteredProformList = [];
  public Statuss :any;
  public ProformKanban = [{ProformId: "", ProformNumber: "", AdminNote : "" , StatusId: "", BoardName:"", Company: ""}];

  ListToKanban = false;
  openKanbanText = "";

  constructor(private _router: Router, private _apicallservice: ApicallService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProformText + " - Crmplus");

    this.settings.columns.ProformNumber.title=this.sysLanguages.ProformNumberText;
    this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settings.columns.CreatedDate.title=this.sysLanguages.CreatedDateText;
    this.settings.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;
    this.settings.columns.BoardName.title=this.sysLanguages.BoardNameText;
    this.settings.columns.ExpirementDate.title=this.sysLanguages.ProfcExpiredDate;
    this.settings.columns.StatusDesc.title=this.sysLanguages.AddTaskStatusText;   
    this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;

    this.getProform();
    this.getStatus();

   }

   settings = {
    columns: {
      ProformId: {
        title: 'ID',
        width: '2%'
      },
      ProformNumber: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CustomerName: {
        title: ''
      },
      BoardName: {
        title: ''
      },
      ExpirementDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
      Conditions: {
        title: 'Faturalama Tarihi',
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  getProform()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proform/GetAllProforms").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.proformList = [];
          }
        }
        else 
        {  
          this.allProformCount = resulttArray.length;
          this.draftCount = 0;
          this.sendCount = 0;
          this.expiredCount = 0;
          this.deniedCount = 0;
          this.checkedCount = 0;
          this.convertedCount = 0;

          for(let i in resulttArray)
          {
            if(resulttArray[i].StatusId == 1)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformDraftStatus;
              this.draftCount++;
            }  
            else if(resulttArray[i].StatusId == 2)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformSentStatus;
              this.sendCount++;
            }
            else if(resulttArray[i].StatusId == 3)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformExpiredStatus;
              this.expiredCount++;
            }
            else if(resulttArray[i].StatusId == 4)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformRefuseStatus;
              this.deniedCount++;
            }
            else if(resulttArray[i].StatusId == 5)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformAcceptedStatus;
              this.checkedCount++;
            }
            else if(resulttArray[i].StatusId == 6)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcConvertedStatus;
              this.convertedCount++;
            }
          }
          this.proformList = resulttArray.filter(x=>x.StatusId != 6);
          this.convertedProformList = resulttArray.filter(x=>x.StatusId == 6);
        }
      }
    )
    

  }

  filterProform(value)
  {
    this.filter = true;
    if(value == 1)
    {
      const a = this.proformList.filter(x=>x.StatusId == 1);
      this.filteredProformList=a;
    }
    else if(value == 2)
    {
      const a = this.proformList.filter(x=>x.StatusId == 2);
      this.filteredProformList=a;
    }
    else if(value == 3)
    {
      const a = this.proformList.filter(x=>x.StatusId == 3);
      this.filteredProformList=a;
    }
    else if(value == 4)
    {
      const a = this.proformList.filter(x=>x.StatusId == 4);
      this.filteredProformList=a;
    }
    else if(value == 5)
    {
      const a = this.proformList.filter(x=>x.StatusId == 5);
      this.filteredProformList=a;
    }
    else if(value == 6)
    {
      this.filteredProformList = this.convertedProformList;
    }
    else if(value == 0)
    {
      this.filter = false;
    }
  }

  openKanban()
  {
    if(this.openKanbanText == this.sysLanguages.TasksBackToKanbanButtonText)
    {
      this.ListToKanban = true;
      this.openKanbanText = this.sysLanguages.TasksListButtonText;
      this.ProformKanban = [];

      for(let i in this.Statuss) 
      {
        for(let j in this.proformList)
        {
          if(this.proformList[j].StatusId == this.Statuss[i].StatusId)
          {
            let proformlistkanban = {ProformId: this.proformList[j].ProformId, AdminNote: this.proformList[j].AdminNote.substring(0, 10), ProformNumber: this.proformList[j].ProformNumber , StatusId: this.proformList[j].StatusId, BoardName: this.proformList[j].BoardName, Company: this.proformList[j].CustomerName};
            this.ProformKanban.push(proformlistkanban);
          }
        }
      }
    }
    else if (this.openKanbanText == this.sysLanguages.TasksListButtonText)
    {
      this.ListToKanban = false;
      this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;
      this.getProform();
    }   
  }

  getStatus()
  {
  this._apicallservice.GetAll("http://localhost:61232/api/Proform/GetAllStatus").subscribe(
    (result) => {       
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if(resulttArray[0].Result == "Empty")
        {
          //
        }
      }
      else
      {
        for(let i in resulttArray)
        {
          if(resulttArray[i].StatusId == 1)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.PcProformDraftStatus;
          }
          else if(resulttArray[i].StatusId == 2)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.PcProformSentStatus;
          }
          else if(resulttArray[i].StatusId == 3)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.PcProformExpiredStatus;
          }
          else if(resulttArray[i].StatusId == 4)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.PcProformRefuseStatus;
          }
          else if(resulttArray[i].StatusId == 5)
          {
            resulttArray[i].StatusDesc = this.sysLanguages.PcProformAcceptedStatus;
          }
        }
        
        this.Statuss = result;
      }
    },
    )
  }

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropProform(event)
  {
    var data = event.dataTransfer.getData("text2");

    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addProformBtn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("proform_desc"))
    {
      //window.alert("You can't drop here");
    }   
    else if(event.target.className == "cardProformDesc")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "assignedProformCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "endDateProformCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "cardDetailButton")
    {
      //window.alert("You can't drop here");
    }
    else
    {
      var proformID = data.split('_')[1];
      var statusID = event.target.id.split('_')[1];
      
      let _proform = {ProformId: proformID, StatusId: statusID};

      this._apicallservice.Update("http://localhost:61232/api/Proform/ChangeProformStatus", proformID, _proform).subscribe((result) => {
        this.getProform();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  dragProform(event)
  {
     event.dataTransfer.setData("text", event.target.parentNode.id);
     event.dataTransfer.setData("text2", event.target.id);
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  FilterDate()
  {
    var x = $("#filterDate").val().toString();
    var startDate = this.formatDate(x.split(" - ")[0]);
    var endDate = this.formatDate(x.split("-")[1].substring(1));
    const a = this.proformList.filter(x=>x.CreatedDate.split(" ")[0] < endDate && x.CreatedDate.split(" ")[0] >= startDate);
    this.filter = true;
    this.filteredProformList = a;
  }

  formatDate(date)
  {
    var splittedDate = date.split(" ");
    let val = "";
    if (splittedDate[0] == "Jan")
    {
        val = "01";
    }
    else if (splittedDate[0] == "Feb")
    {
        val = "02";
    }
    else if (splittedDate[0] == "Mar")
    {
        val = "03";
    }
    else if (splittedDate[0] == "Apr")
    {
        val = "04";
    }
    else if (splittedDate[0] == "May")
    {
        val = "05";
    }
    else if (splittedDate[0] == "Jun")
    {
        val = "06";
    }
    else if (splittedDate[0] == "Jul")
    {
        val = "07";
    }
    else if (splittedDate[0] == "Aug")
    {
        val = "08";
    }
    else if (splittedDate[0] == "Sep")
    {
        val = "09";
    }
    else if (splittedDate[0] == "Oct")
    {
        val = "10";
    }
    else if (splittedDate[0] == "Nov")
    {
        val = "11";
    }
    else if (splittedDate[0] == "Dec")
    {
        val = "12";
    }

    var year = splittedDate[2];
    var day = splittedDate[1].substring(0,splittedDate[1].length - 1);
    if(day.length == 1)
    {
      day = "0"+day
    }
    return year + "-" + val + "-" + day;

  }

}
