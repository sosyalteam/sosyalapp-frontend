import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';
import { last } from '@angular/router/src/utils/collection';

@Component({
  selector: 'add-edit-project',
  templateUrl: './add-edit-project.component.html',
  styleUrls: ['./add-edit-project.component.scss'],
  providers: [ApicallService, NbToastrService]
})
export class AddEditProjectComponent implements OnInit {

  _boardId: any;
  sysLanguages: any;
  selectedRelation: any;

  projectData = { BoardId: 0, BoardName: "", BeginDate: "", EndDate: "", CustomerId: 0, StatusId: 10, Proposal: 0, Invoice: 0, Proform: 0 , PeriodCount : "0" , TotalDiscount : "0" , CurrencyTypeId :0 , RepetitionDate : "" , ResponsibleId : 0 };
  lineData = [{ Id: '', ProductName: '', LineDesc: '', ProductCount: 1, ProductPrice: 0, LineAmount: "0", updatedefaultTax: "" , LineDiscount : '0'}];
  customerList = [];
  relationList = [];
  invoiceList = [];
  proposalList = [];
  proformList = [];
  projectstatusList = [];
  productList = [];
  addedDynamicLineData = [];
  selectedCustomerContacts = [];
  copyselectedCustomerContacts = [];
  customerContact = [];
  contactList = [];
  currencyList = [];
  serviceList = [];
  userList = [];
  productTask = [];

  subsArea = false;
  subscriptionPeriodList = [];
  copysubscriptionPeriodList = [];
  selectedSubPeriod = 0;
  SubsRelationList = [];
  customSelectedSubPeriod = 0;
  selectedService = 0;
  repetitionDateText = "";

  _total = 0;
  topTax = 0;
  topTaxArray = [];
  _totalDiscount = 0;
  currency = "TL";
  tax = [];
  TaxDesc:string;
  taxList = [];
  convertedProforomData : any;
  
  loading = false;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _router: Router, private _apicallService: ApicallService, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProjectText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this._boardId = this._avRoute.snapshot.params["id"];
    }

    this.relationList.push({ label: this.sysLanguages.BpProposal, value: 1 }, { label: this.sysLanguages.BpProform, value: 2 }, { label: this.sysLanguages.BpInvoice, value: 3 });
    //this.SubsRelationList.push({ label: this.sysLanguages.BpProform, value: 1 }, { label: this.sysLanguages.BpInvoice, value: 2 });
    this.serviceList.push({label : "Hizmet Başında", value : 1 },{label : "Hizmet Sonunda", value : 2 });

    this.getCustomers();
    this.getProforms();
    this.getInvoices();
    this.getProposals();
    this.getBoardStatusses();
    this.getProducts();
    this.getAllContact();
    this.getSubscriptonPeriod();
    this.getTax();
    this.getCurrencies();
    this.getUsers();

    this.convertedProforomData = JSON.parse(sessionStorage.getItem("projectSubsData"));
    sessionStorage.removeItem('projectSubsData');

  }

  ngOnInit() 
  {
    setTimeout(()=>{
      if(this._boardId>0)
      {
        this._apicallService.Get("http://localhost:61232/api/Project/GetBoardDetail", this._boardId)
        .subscribe(
          (result) => {
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray.length != 0 && resultArray[0].Result) {
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
              else if(resultArray[0].Result == "Empty")
              {
                // do smth.
              }
            }
            else
            {
              let found = false;

              this.projectData.BoardId = Number(resultArray[0].BoardId);
              this.projectData.BoardName = resultArray[0].BoardName;
              this.projectData.BeginDate = this.formatIsoDateToCrypted(resultArray[0].BeginDate.split(" ")[0]);
              this.projectData.EndDate = this.formatIsoDateToCrypted(resultArray[0].EndDate.split(" ")[0]); 
              this.projectData.CustomerId = Number(resultArray[0].CustomerId);
              this.projectData.StatusId = Number(resultArray[0].StatusId);
              this.projectData.TotalDiscount = resultArray[0].Discount;
              this.projectData.CurrencyTypeId = resultArray[0].CurrencyTypeId;
              this.projectData.ResponsibleId = resultArray[0].ResponsibleId;
              if(resultArray[0].RepetitionDate)
              {
                this.projectData.RepetitionDate = this.formatIsoDateToCrypted(resultArray[0].RepetitionDate.split(" ")[0]);
              }
              else
              {
                this.projectData.RepetitionDate = "";
              }

              if(this.projectData.CurrencyTypeId == 1)
              {
                this.currency = "TL";
              }
              else if (this.projectData.CurrencyTypeId == 2)
              {
                this.currency = "$";
              }
              else if(this.projectData.CurrencyTypeId == 3)
              {
                this.currency = "€";
              }
              else if(this.projectData.CurrencyTypeId == 4)
              {
                this.currency = "£";
              }

              if(resultArray[0].PeriodId != 0)
              {
                this.subsArea = true;
                if(resultArray[0].PeriodId == 1 )
                {
                  this.selectedSubPeriod = resultArray[0].PeriodId;
                  this.projectData.PeriodCount = '1';
                }
                else if(resultArray[0].PeriodId == 2)
                {
                  this.selectedSubPeriod = resultArray[0].PeriodId;
                  this.projectData.PeriodCount = '3';
                }
                else if(resultArray[0].PeriodId == 3)
                {
                  this.selectedSubPeriod = resultArray[0].PeriodId;
                  this.projectData.PeriodCount = '6';
                }
                else if(resultArray[0].PeriodId == 4)
                {
                  this.selectedSubPeriod = resultArray[0].PeriodId;
                  this.projectData.PeriodCount = '12';
                }
                this.repetitionDateText = "Her ayın " + resultArray[0].RepetitionDate.split(" ")[0].split("-")[2] + ". gününde tekrar eder.";
              }

              this.customerContact = [];

              for(let i in this.contactList)
              {
                if(this.contactList[i].customerId == this.projectData.CustomerId)
                {
                  this.customerContact.push(this.contactList[i]);
                }
              }

              this._apicallService.Get("http://localhost:61232/api/Project/GetProjectContacts", this.projectData.BoardId).subscribe(
                (result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray.length != 0 && resultArray[0].Result) {
                      if (resultArray[0].Result == "Session_Expired") {
                        this._router.navigate(['/auth']);
                        return;
                      }
                      else if(resultArray[0].Result == "Empty")
                      {
                        this.selectedCustomerContacts = [];
                      }
                    }
                    else
                    { 
                      this.selectedCustomerContacts = resultArray.map(x=> x.ContactId);
                      this.copyselectedCustomerContacts = this.selectedCustomerContacts.slice();
                    }
                }
              )

              for(let i in this.invoiceList)
              {
                if(this.invoiceList[i].project == this.projectData.BoardId)
                {         
                  this.selectedRelation = 3;
                  this.projectData.Invoice = this.invoiceList[i].value;      
                  found = true;
                  break;
                }
              }

              if(found == false)
              {
                for(let j in this.proformList)
                {
                  if(this.proformList[j].project == this.projectData.BoardId)
                  {         
                    this.selectedRelation = 2;
                    this.projectData.Proform = this.proformList[j].value;      
                    found = true;
                    break;
                  }
                }

                if(found == false)
                {
                  for(let j in this.proposalList)
                  {
                    if(this.proposalList[j].project == this.projectData.BoardId)
                    {         
                      this.selectedRelation = 1;
                      this.projectData.Proposal = this.proposalList[j].value;      
                      found = true;
                      break;
                    }
                  }
                }
              }
            }              
        }
        )

        this._apicallService.Get("http://localhost:61232/api/Project/GetProjectLines", this._boardId).subscribe(
          (result) => {
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray.length != 0 && resultArray[0].Result) {
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
              else if(resultArray[0].Result == "Empty")
              {
                this.lineData = [];
              }
            }
            else
            {
              let _lineData;          
              for(var i in resultArray)
              {
                if(resultArray[i].Description.toString().includes("<br/>"))
                {
                  var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                  let _desc = [];

                  for(let j in splittedDesc)
                  {
                    _desc.push({desc: splittedDesc[j]});
                  }

                  if(resultArray[i].ProductId == 0)
                  {
                    _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                    this.lineData.push(_lineData);
                  }
                  else
                  {
                    _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                    this.lineData.push(_lineData);
                  }

                }
                else
                {
                  if(resultArray[i].ProductId == 0)
                  {
                    _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                    this.lineData.push(_lineData);
                  }
                  else
                  {
                    _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                    this.lineData.push(_lineData);
                  }
                }
              }
              this.updatedTotal();
            }
          }
        )
      }
      else
      {
        if(this.convertedProforomData != null)
        {
          if(this.convertedProforomData.ProjectorSub == "project")
          {
            let _today = new Date(this.convertedProforomData.CreatedDate); 
            let _expiredDate = new Date();
            _expiredDate.setDate(_today.getDate()+30);
            this.projectData.StatusId = 10;
            this.projectData.BoardName = "";
            this.selectedCustomerContacts = [];   
            this.projectData.CurrencyTypeId = 0;
            this.projectData.EndDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
            this.projectData.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
            this.subsArea = false;
            this.selectedRelation = 2;
            this.projectData.BoardName = this.convertedProforomData.ProformName;
            this.selectedService = 1;
            
            setTimeout(()=>{
              this.projectData.Proform = parseInt(this.convertedProforomData.ProformId);
              this.projectData.CustomerId = this.convertedProforomData.CustomerId;
              this.projectData.ResponsibleId = this.convertedProforomData.UserId;
              for(let i in this.contactList)
              {
                if(this.contactList[i].customerId == this.projectData.CustomerId)
                {
                  this.customerContact.push(this.contactList[i]);
                }
              }
            },1500);

              this._apicallService.Get("http://localhost:61232/api/Proform/GetProformLines", parseInt(this.convertedProforomData.ProformId)).subscribe(
              (result) => {
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray.length != 0 && resultArray[0].Result) {
                  if (resultArray[0].Result == "Session_Expired") {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else if(resultArray[0].Result == "Empty")
                  {
                    this.lineData = [];
                    this.addedDynamicLineData = [];
                  }
                }
                else
                {
                  let _lineData;
                  let _line;        
                  for(var i in resultArray)
                  {
                    if(resultArray[i].Description.toString().includes("<br/>"))
                    {
                      var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                      let _desc = [];

                      for(let j in splittedDesc)
                      {
                        _desc.push({desc: splittedDesc[j]});
                      }

                      if(resultArray[i].ProductId == 0)
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                      else
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }

                    }
                    else
                    {
                      if(resultArray[i].ProductId == 0)
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                      else
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                    }
                  }
                  this.updatedTotal();
                }
              }
            )
          }
          else if(this.convertedProforomData.ProjectorSub == "subs")
          {
            let _today = new Date(this.convertedProforomData.CreatedDate); 
            let _expiredDate = new Date();
            _expiredDate.setDate(_today.getDate()+30);
            this.projectData.StatusId = 10;
            this.projectData.BoardName = "";
            this.selectedCustomerContacts = [];   
            this.projectData.CurrencyTypeId = 0;
            this.projectData.EndDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
            this.projectData.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
            this.subsArea = true;
            this.selectedRelation = 2;
            this.projectData.BoardName = this.convertedProforomData.ProformName;
            this.selectedService = 1;

            setTimeout(()=>{
              this.projectData.Proform = parseInt(this.convertedProforomData.ProformId);
              this.projectData.CustomerId = this.convertedProforomData.CustomerId;
              this.projectData.ResponsibleId = this.convertedProforomData.UserId;
              for(let i in this.contactList)
              {
                if(this.contactList[i].customerId == this.projectData.CustomerId)
                {
                  this.customerContact.push(this.contactList[i]);
                }
              }
            },1500);

            this._apicallService.Get("http://localhost:61232/api/Proform/GetProformLines", parseInt(this.convertedProforomData.ProformId)).subscribe(
              (result) => {
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray.length != 0 && resultArray[0].Result) {
                  if (resultArray[0].Result == "Session_Expired") {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else if(resultArray[0].Result == "Empty")
                  {
                    this.lineData = [];
                    this.addedDynamicLineData = [];
                  }
                }
                else
                {
                  let _lineData;   
                  let _line;       
                  for(var i in resultArray)
                  {
                    if(resultArray[i].Description.toString().includes("<br/>"))
                    {
                      var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                      let _desc = [];

                      for(let j in splittedDesc)
                      {
                        _desc.push({desc: splittedDesc[j]});
                      }

                      if(resultArray[i].ProductId == 0)
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                      else
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }

                    }
                    else
                    {
                      if(resultArray[i].ProductId == 0)
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description , ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                      else
                      {
                        _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                        this.lineData.push(_lineData);
                        _line = { ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description , ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].LineAmount, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, LineTax : resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                        this.addedDynamicLineData.push(_line);
                      }
                    }
                  }
                  this.updatedTotal();
                }
              }
            )
          }
        }
        else
        {
          let _today = new Date(); 
          let _expiredDate = new Date();
          _expiredDate.setDate(_today.getDate()+30);

          this.projectData.StatusId = 10;
          this.projectData.CustomerId = 0;
          this.projectData.BoardName = "";
          this.selectedCustomerContacts = [];   
          this.projectData.CurrencyTypeId = 0;
          this.projectData.EndDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
          this.projectData.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
          this.subsArea = false;
        }


      }
    }, 1000);
  }

  getUsers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {       
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resulttArray[0].Result == "Empty") {
            this.userList = []; 
          }
        }
        else
        {
          this.userList = []; 
          for(let i in resulttArray)
          {
            if(resulttArray[i].UserId != 0)
            {
              let _userlist = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
              this.userList.push(_userlist);
            }    
          }
        }
      },
      )
  }

  getCurrencies()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.currencyList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
              this.currencyList.push(_currency);
            }
            this.projectData.CurrencyTypeId = 1;
          }
      }
      )
  }

  getCustomers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _customerList = { value: resultArray[i].CustomerId, label: resultArray[i].CustomerName };
            this.customerList.push(_customerList);
          }
        }
      }
    )
  }

  getAllContact()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].ContactId != 0)
              {
                let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId, customerId: resultArray[i].CustomerId, email: resultArray[i].Email };
                this.contactList.push(_contact);
              }
            }
          }
      }
    )
  }

  getCustomerContacts()
  {
    this.customerContact = [];

    for(let i in this.contactList)
    {
      if(this.contactList[i].customerId == this.projectData.CustomerId)
      {
        this.customerContact.push(this.contactList[i]);
      }
    }

    this.selectedCustomerContacts = [];
  }

  getProforms()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Proform/GetAllProforms").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.proformList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _proformList = { value: resultArray[i].ProformId, label: resultArray[i].ProformNumber, customer: resultArray[i].CustomerId, begindate: resultArray[i].CreatedDate, enddate: resultArray[i].ExpirementDate, project: resultArray[i].ProjectId };
            this.proformList.push(_proformList);
          }
        }
      }
    )
  }

  getInvoices()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.invoiceList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _invoiceList = { value: resultArray[i].InvoiceId, label: resultArray[i].InvoiceNumber, customer: resultArray[i].CustomerId, begindate: resultArray[i].CreatedDate, enddate: resultArray[i].LastPaymentDate, project: resultArray[i].ProjectId };
            this.invoiceList.push(_invoiceList);
          }
        }
      }
    )
  }

  getProposals()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Proposal/GetAllProposals").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.proposalList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _proposalList = { value: resultArray[i].ProposalId, label: resultArray[i].Subject, customer: resultArray[i].CustomerId, begindate: resultArray[i].CreatedDate, enddate: resultArray[i].ExpiredDate, project: resultArray[i].ProjectId };
            this.proposalList.push(_proposalList);
          }
        }
      }
    )
  }

  getRelationData(id, val)
  {
    if(val == 1)
    {
      for(let i in this.proposalList)
      {
        if(this.proposalList[i].value == id)
        {
          this.projectData.CustomerId = this.proposalList[i].customer;
          this.projectData.BoardName = this.proposalList[i].label;
          this.projectData.BeginDate = this.formatIsoDateToCrypted(this.proposalList[i].begindate.split(" ")[0]);
          this.projectData.EndDate = this.formatIsoDateToCrypted(this.proposalList[i].enddate.split(" ")[0]);

          this.customerContact = [];

          for(let i in this.contactList)
          {
            if(this.contactList[i].customerId == this.projectData.CustomerId)
            {
              this.customerContact.push(this.contactList[i]);
            }
          }
      
          this.selectedCustomerContacts = [];

          break;
        }
      }
    }
    else if(val == 2)
    {
      for(let i in this.proformList)
      {
        if(this.proformList[i].value == id)
        {
          this.projectData.CustomerId = this.proformList[i].customer;
          this.projectData.BoardName = this.proformList[i].label;
          this.projectData.BeginDate = this.formatIsoDateToCrypted(this.proformList[i].begindate.split(" ")[0]);
          this.projectData.EndDate = this.formatIsoDateToCrypted(this.proformList[i].enddate.split(" ")[0]);

          this.customerContact = [];

          for(let i in this.contactList)
          {
            if(this.contactList[i].customerId == this.projectData.CustomerId)
            {
              this.customerContact.push(this.contactList[i]);
            }
          }
      
          this.selectedCustomerContacts = [];

          break;
        }
      }
    }
    else if(val == 3)
    {
      for(let i in this.invoiceList)
      {
        if(this.invoiceList[i].value == id)
        {
          this.projectData.CustomerId = this.invoiceList[i].customer;
          this.projectData.BoardName = this.invoiceList[i].label;
          this.projectData.BeginDate = this.formatIsoDateToCrypted(this.invoiceList[i].begindate.split(" ")[0]);
          this.projectData.EndDate = this.formatIsoDateToCrypted(this.invoiceList[i].enddate.split(" ")[0]);

          this.customerContact = [];

          for(let i in this.contactList)
          {
            if(this.contactList[i].customerId == this.projectData.CustomerId)
            {
              this.customerContact.push(this.contactList[i]);
            }
          }
      
          this.selectedCustomerContacts = [];

          break;
        }
      }
    }
  }

  getBoardStatusses()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Project/GetBoardStatus").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.projectstatusList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _status = { value: resultArray[i].StatusId, label: resultArray[i].StatusDesc };
            this.projectstatusList.push(_status);
          }
          this.projectData.StatusId = 0;
        }
      }
    )
  }

  getProducts()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }

                  }
                }

                _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, price: resultArray[i].Price, desc: resultArray[i].ProductDesc , tax : resultArray[i].Tax1 };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }
          }
        }
    },
    )
  }

  getProductDetail(event)
  {
    if(!this._boardId)
    {
      this.lineData = [];
      this.tax = [];
      for(let i=0; i<this.productList.length; i++)
      {
        if(event == this.productList[i].value)
        {
          if(this.productList[i].tax == "NoTax")
          {
            this.tax = [];
          }
          else if(this.productList[i].tax == "KDV %18")
          {
            this.tax[0] = 1;
          }
          else if(this.productList[i].tax == "Stopaj")
          {
            this.tax[0] = 2; 
          }
          else if(this.productList[i].tax == "Stopaj 15")
          {
            this.tax[0] = 3;
          }
          let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" , LineDiscount : "" };
          this.lineData.push(_lineData);
        }
      }
    }
    else
    {
      this.tax = [];
      this.lineData.shift();
      for(let i=0; i<this.productList.length; i++)
      {
        if(event == this.productList[i].value)
        {
          if(this.productList[i].tax == "NoTax")
          {
            this.tax = [];
          }
          else if(this.productList[i].tax == "KDV %18")
          {
            this.tax[0] = 1;
          }
          else if(this.productList[i].tax == "Stopaj")
          {
            this.tax[0] = 2; 
          }
          else if(this.productList[i].tax == "Stopaj 15")
          {
            this.tax[0] = 3;
          }
          let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" , LineDiscount : "" };
          this.lineData.unshift(_lineData);
        }
      }
    }
  }

  getTax()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          this.taxList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  saveBoard()
  {
    this.loading = true;
    if(!this._boardId)
    {
      var x = document.getElementById("total");
      var amount = x.innerText.split(" ")[0] + " " + this.currency;
      if(this.subsArea == true)
      {
        if(this.selectedSubPeriod == 1 )
        {
          this.projectData.PeriodCount = '1';
        }
        else if(this.selectedSubPeriod == 2)
        {
          this.projectData.PeriodCount = '3';
        }
        else if(this.selectedSubPeriod == 3)
        {
          this.projectData.PeriodCount = '6';
        }
        else if(this.selectedSubPeriod == 4)
        {
          this.projectData.PeriodCount = '12';
        }
      }

      let board  = { BoardId: 0, BoardName: $("#boardName").val(), BeginDate: $("#CratedDate").val(), EndDate: $("#LastDate").val(), StatusId: this.projectData.StatusId, CustomerId: this.projectData.CustomerId , PeriodId : this.selectedSubPeriod , PeriodCount : this.projectData.PeriodCount , Amount : amount , Discount : $("#totaldiscount").val() , CurrencyTypeId : this.projectData.CurrencyTypeId , RepetitionDate : $("#repetitionDate").val(), ResponsibleId : this.projectData.ResponsibleId  };

      if(board.BoardName == "" || board.CustomerId == 0 || this.selectedCustomerContacts.length == 0 )
      {
        this.makeToast(NbToastStatus.DANGER,this.sysLanguages.ErrorMessage,this.sysLanguages.ErrorMessage1);
        this.loading = false;
      }
      else
      {
          this._apicallService.Add("http://localhost:61232/api/project/CreateTaskBoard", board).subscribe((result) => {
          var boardId = result[0].Result;
          board.BoardId = boardId;
          let boarddetail;
          debugger;

          //seçilen ürünlere göre otomatik görev oluşturulması
          var now = new Date().toISOString();
          var subProductTaskList = this.productTask.filter(x=>x.ProductTaskId != x.ParentId );
          var productTaskList = this.productTask.filter(x=>x.ProductTaskId == x.ParentId );
          for(let i in productTaskList)
          {
            let taskDate  = new Date($("#CratedDate").val().toString());
            taskDate.setDate(taskDate.getDate()+ productTaskList[i].DayCount+1 );
            var _taskDate = taskDate.toISOString().split("T")[0];

            let task={
              ParentId:0,
              Title: productTaskList[i].Title,
              TaskDesc: "",
              TypeId: 1,
              CreatedDate: now.split("T")[0],
              ResponsibleId: this.projectData.ResponsibleId ,
              BeginCheckListId:1,
              EndCheckListId:1,
              BoardId: boardId,
              ContractId:1,
              BeginDate: this.formatIsoDateToCrypted(_taskDate),
              EndDate: this.formatIsoDateToCrypted(_taskDate),
              StatusId: 0,
              CreatorId: 0,
              TeamId: productTaskList[i].TeamId,
              CustomerId:this.projectData.CustomerId,
              LeadId: 0,
              InvoiceId: 0,
              TaskTime : "",
            }

            this._apicallService.Add("http://localhost:61232/api/Task/Create",task) 
            .subscribe((result) => {
              let resultArray = JSON.parse(JSON.stringify(result));    
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }  
              else
              {
                var taskIdForSub = result[0].Result; 
                let taskmember = { TaskId : result[0].Result , UserId : this.projectData.ResponsibleId } ;
                this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember",taskmember).subscribe((resultt) => {} );
                var checkSubTask = subProductTaskList.filter(x=>x.ParentId == productTaskList[i].ProductTaskId );
                if(checkSubTask.length > 0)
                {
                  for(let i in checkSubTask )
                  {
                    let _subtask={
                      TaskId: taskIdForSub,
                      ParentId : 0,
                      Title: checkSubTask[i].Title,
                      TaskDesc: "SubTask",
                      TypeId: 1,
                      CreatedDate: now.split("T")[0],
                      ResponsibleId: this.projectData.ResponsibleId ,
                      BeginCheckListId:1,
                      EndCheckListId:1,
                      BoardId: boardId,
                      ContractId:1,
                      BeginDate: "",
                      EndDate: "",
                      StatusId: 0,
                      CreatorId: 0,
                      TeamId: checkSubTask[i].TeamId,
                      CustomerId:this.projectData.CustomerId,
                      LeadId: 0,
                      InvoiceId: 0,
                      TaskTime : "",
                    }
                    this._apicallService.Add("http://localhost:61232/api/Task/CreateSubTask",_subtask) .subscribe((result) => {});
                  }
                }
              }
            },) 
          }

          for(let i = 0; i < this.addedDynamicLineData.length; i++)
          {
            let _product = 0;
            
            for(let j in this.productList)
            {
              if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
              {
                _product = this.productList[j].value;
              }
            }

            if(_product == 0)
              boarddetail = { BoardId: boardId, LineId: Number(i)+1, ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineName: this.addedDynamicLineData[i].ProductName , LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
            else
            boarddetail = { BoardId: boardId, LineId: Number(i)+1, ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineName: "" , LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
            
            this._apicallService.Add("http://localhost:61232/api/Project/CreateProjectDetail", boarddetail).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          } 
          if(this.projectData.Invoice != 0)
          {
            // BoardId = boardid, customerid = invoiceid, statusid for distinction
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Invoice, StatusId: 1}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }
          else if(this.projectData.Proform != 0)
          {
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Proform, StatusId: 2}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }
          else if(this.projectData.Proposal != 0)
          {
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Proposal, StatusId: 3}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }

          if(this.projectData.CustomerId != 0)
          {
            let projectcontact;

            for(let j in this.selectedCustomerContacts)
            {
              projectcontact = { BoardId: boardId, ContactId: this.selectedCustomerContacts[j]};   
            
              this._apicallService.Add("http://localhost:61232/api/Project/CreateProjectContact", projectcontact).subscribe(
              (result) =>{
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray[0].Result == "Session_Expired") {
                  this.loading = false;
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
            }
          }
          debugger;
          //faturalama seçeneği tekrarlı olsa da olmasa da seçilecek.
          if(this.selectedService == 1)
          {
            if(this.subsArea == false)
            {
              //yani bu bi projeyse
              if(this.projectData.Proform == 0 )
              {
                let date  = new Date($("#CratedDate").val().toString());
                date.setDate(date.getDate()+1);
                let flag =  date.toISOString().split("T")[0];

               let proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "",
                          Amount: amount,
                          Discount : $("#totaldiscount").val()
                       };
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )
              }
              /*
              else if(this.selectedSubRelation == 2 && this.projectData.Invoice == 0 )
              {
                //var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                let date  = new Date($("#CratedDate").val().toString());
                date.setDate(date.getDate()+1);
                let flag =  date.toISOString().split("T")[0];

                //bu bır proje oldugu ıcın sadece basında veya sonunda olusturcak. for dobngusune gerek yok.
                //hızmet basındaysa tarıh bası startdate hizmet sonundaysa tarih sonu end date
                //
                  let invoice = {
                    InvoiceId:0,
                    CustomerId: this.projectData.CustomerId,
                    ProjectId: boardId,
                    InvoiceNumber: "Inv-" + $("#boardName").val(),
                    CreatedDate: this.formatIsoDateToCrypted(flag),
                    LastPaymentDate: this.formatIsoDateToCrypted(flag),
                    PayingMethodId: 1,
                    SalesmanId: 0,
                    ProposalId: 0,
                    ProformId: 0,
                    DiscountTypeId: 0,
                    CurrencyTypeId: this.projectData.CurrencyTypeId,
                    AdminNote: "",
                    CustomerNote: "",
                    Conditions: "",
                    StatusId: 5,
                    Amount : amount,
                    Discount : $("#totaldiscount").val()
                  };

                  
                  this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                  (result) => {
                    var invoiceID = result[0].Result;
                    let invoiceDetail;           

                    for(let i = 0; i < this.addedDynamicLineData.length; i++)
                    {
                      let _product = 0;
                      
                      for(let j in this.productList)
                      {
                        if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                        {
                          _product = this.productList[j].value;
                        }
                      }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                        let resulttArray = JSON.parse(JSON.stringify(resultt));
                        if (resulttArray[0].Result == "Session_Expired") {
                          this.loading = false;
                          this._router.navigate(['/auth']);
                          return;
                        }
                        else
                        {
                          this.loading = false;    
                        }
                        }
                        )
                      }     
                      
                      this.addedDynamicLineData = [];
                      let invoicecontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }
                    }
                  )
              }*/
            }
            else
            {
              //abonelıkse hizmet başında
                  let proform;
                  let date  = new Date($("#repetitionDate").val().toString());
                  date.setDate(date.getDate()+1);
                  var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                  var repetitionCount = parseInt(monthCount+1) / parseInt(this.projectData.PeriodCount);
                  let startDate = new Date($("#CratedDate").val().toString());
                  startDate.setDate(startDate.getDate()+1);
                  let _startDate = startDate.toISOString().split("T")[0];

                  let flag =  date.toISOString().split("T")[0];
                  let x = new Date(flag);
                  x.setDate(x.getDate()+30);
                  let _flag = x.toISOString().split("T")[0];

                  let lastDate = new Date($("#LastDate").val().toString());
                  lastDate.setDate(lastDate.getDate()+1);
                  let _lastDate = lastDate.toISOString().split("T")[0];

                  if(_startDate < flag)
                  {
                    let _amount = amount.split(" ")[0].toString().replace(".","");;
                    let diffDays = new Date(flag).getTime() - new Date(_startDate).getTime();
                    let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                    proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(_startDate),
                          ExpirementDate: this.formatIsoDateToCrypted(flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi: " + _startDate,
                          Amount: Number(partialAmount).toLocaleString() + " " + this.currency,
                          Discount : $("#totaldiscount").val()
                       };
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )
                  }
                  for(var i = 0; i < repetitionCount; i++)
                  {
                    if(i == repetitionCount-1)
                    {
                      let _amount = amount.split(" ")[0].toString().replace(".","");;
                      let diffDays = new Date(_lastDate).getTime() - new Date(flag).getTime();
                      let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                      proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(_lastDate) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi : " + flag ,
                          Amount: Number(partialAmount).toLocaleString() + " " + this.currency,
                          Discount : $("#totaldiscount").val()
                       };
                    }
                    else
                    {
                      proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(_flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi : " + flag ,
                          Amount: amount,
                          Discount : $("#totaldiscount").val()
                       };
                    }
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )

                    let xx = new Date(flag);
                    new Date(xx.setMonth(xx.getMonth()+parseInt(this.projectData.PeriodCount)));
                    flag = xx.toISOString().split("T")[0];
                    let yy = new Date(flag);
                    new Date(yy.setDate(yy.getDate()+30));
                    _flag = yy.toISOString().split("T")[0];
                    
                  }
              /*
              else if(this.selectedSubRelation == 2)
              {
                //ilk tekrarlama tarihini alıyoruz ve ilk oluşacak invoice için expirement date 1 ay sonrasına yazıyoruz. Daha sonra
                  // oluşacak her invoice için döngü içinde oluşturuulan tarihten sonra ekrandan aldıgımız deger kadar artırıyoruz. tekrar sayısı kadar
                  //invoice olusturuyoruz.
                  // toastr verilecek bölünemeyen aylar için
                  let invoice;
                  let date  = new Date($("#repetitionDate").val().toString());
                  date.setDate(date.getDate()+1);
                  var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                  var repetitionCount = parseInt(monthCount+1) / parseInt(this.projectData.PeriodCount);
                  let startDate = new Date($("#CratedDate").val().toString());
                  startDate.setDate(startDate.getDate()+1);
                  let _startDate = startDate.toISOString().split("T")[0];

                  let flag =  date.toISOString().split("T")[0];
                  let x = new Date(flag);
                  x.setDate(x.getDate()+30);
                  let _flag = x.toISOString().split("T")[0];

                  if(_startDate < flag)
                  {
                    let _amount = amount.split(" ")[0].toString().replace(".","");;
                    let diffDays = new Date(flag).getTime() - new Date(_startDate).getTime();
                    let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                    invoice = {
                        InvoiceId:0,
                        CustomerId: this.projectData.CustomerId,
                        ProjectId: boardId,
                        InvoiceNumber: "Inv-" + $("#boardName").val(),
                        CreatedDate: this.formatIsoDateToCrypted(_startDate),
                        LastPaymentDate: this.formatIsoDateToCrypted(flag),
                        PayingMethodId: 1,
                        SalesmanId: 0,
                        ProposalId: 0,
                        ProformId: 0,
                        DiscountTypeId: 0,
                        CurrencyTypeId: this.projectData.CurrencyTypeId,
                        AdminNote: "",
                        CustomerNote: "",
                        Conditions: "",
                        StatusId: 5,
                        Amount :  Number(partialAmount).toLocaleString() + " " + this.currency,
                        Discount : $("#totaldiscount").val()
                      };

                      
                    
                    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                    (result) => {
                      var invoiceID = result[0].Result;
                      let invoiceDetail;           

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                          
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;    
                          }
                        }
                        )
                        }   
                        
                        let invoicecontact;

                        for(let j in this.selectedCustomerContacts)
                        {
                          invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                        
                          this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                          (result) =>{
                            let resultArray = JSON.parse(JSON.stringify(result));
                            if (resultArray[0].Result == "Session_Expired") {
                              this.loading = false;
                              this._router.navigate(['/auth']);
                              return;
                            }
                          }
                          )
                        }
                      }
                    )
                  }
                  for(var i = 0; i < repetitionCount; i++)
                  {
                      invoice = {
                        InvoiceId:0,
                        CustomerId: this.projectData.CustomerId,
                        ProjectId: boardId,
                        InvoiceNumber: "Inv-" + $("#boardName").val(),
                        CreatedDate: this.formatIsoDateToCrypted(flag),
                        LastPaymentDate: this.formatIsoDateToCrypted(_flag),
                        PayingMethodId: 1,
                        SalesmanId: 0,
                        ProposalId: 0,
                        ProformId: 0,
                        DiscountTypeId: 0,
                        CurrencyTypeId: this.projectData.CurrencyTypeId,
                        AdminNote: "",
                        CustomerNote: "",
                        Conditions: "",
                        StatusId: 5,
                        Amount : amount,
                        Discount : $("#totaldiscount").val()
                      };

                      
                    
                    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                    (result) => {
                      var invoiceID = result[0].Result;
                      let invoiceDetail;           

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                          
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;    
                          }
                        }
                        )
                        }   
                        
                        let invoicecontact;

                        for(let j in this.selectedCustomerContacts)
                        {
                          invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                        
                          this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                          (result) =>{
                            let resultArray = JSON.parse(JSON.stringify(result));
                            if (resultArray[0].Result == "Session_Expired") {
                              this.loading = false;
                              this._router.navigate(['/auth']);
                              return;
                            }
                          }
                          )
                        }
                      }
                    )

                    let xx = new Date(flag);
                    new Date(xx.setMonth(xx.getMonth()+parseInt(this.projectData.PeriodCount)));
                    flag = xx.toISOString().split("T")[0];
                    let yy = new Date(flag);
                    new Date(yy.setDate(yy.getDate()+30));
                    _flag = yy.toISOString().split("T")[0];
                    
                  }
              }*/
            } 
          }
          else if(this.selectedService == 2)
          {
            if(this.subsArea == false)
            {
              //proje
              if(this.projectData.Proform == 0 )
              {
                let date  = new Date($("#LastDate").val().toString());
                date.setDate(date.getDate()+1);
                let flag =  date.toISOString().split("T")[0];

               let proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "",
                          Amount: amount,
                          Discount : $("#totaldiscount").val()
                       };
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )
              }
              /*
              else if(this.selectedSubRelation == 2 && this.projectData.Invoice == 0)
              {
                //var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                let date  = new Date($("#LastDate").val().toString());
                date.setDate(date.getDate()+1);
                let flag =  date.toISOString().split("T")[0];

                //bu bır proje oldugu ıcın sadece basında veya sonunda olusturcak. for dobngusune gerek yok.
                //hızmet basındaysa tarıh bası startdate hizmet sonundaysa tarih sonu end date
                //
                  let invoice = {
                    InvoiceId:0,
                    CustomerId: this.projectData.CustomerId,
                    ProjectId: boardId,
                    InvoiceNumber: "Inv-" + $("#boardName").val(),
                    CreatedDate: this.formatIsoDateToCrypted(flag),
                    LastPaymentDate: this.formatIsoDateToCrypted(flag),
                    PayingMethodId: 1,
                    SalesmanId: 0,
                    ProposalId: 0,
                    ProformId: 0,
                    DiscountTypeId: 0,
                    CurrencyTypeId: this.projectData.CurrencyTypeId,
                    AdminNote: "",
                    CustomerNote: "",
                    Conditions: "",
                    StatusId: 5,
                    Amount : amount,
                    Discount : $("#totaldiscount").val()
                  };

                  
                  this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                  (result) => {
                    var invoiceID = result[0].Result;
                    let invoiceDetail;           

                    for(let i = 0; i < this.addedDynamicLineData.length; i++)
                    {
                      let _product = 0;
                      
                      for(let j in this.productList)
                      {
                        if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                        {
                          _product = this.productList[j].value;
                        }
                      }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                        let resulttArray = JSON.parse(JSON.stringify(resultt));
                        if (resulttArray[0].Result == "Session_Expired") {
                          this.loading = false;
                          this._router.navigate(['/auth']);
                          return;
                        }
                        else
                        {
                          this.loading = false;    
                        }
                        }
                        )
                      }     
                      
                      this.addedDynamicLineData = [];
                      let invoicecontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }
                    }
                  )
              }*/
            }
            else
            {
              debugger;
              //abonelik hizmet sonunda
              let proform;
                  let date  = new Date($("#repetitionDate").val().toString());
                  date.setDate(date.getDate()+1);
                  var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                  var repetitionCount = parseInt(monthCount+1) / parseInt(this.projectData.PeriodCount);
                  let startDate = new Date($("#CratedDate").val().toString());
                  startDate.setDate(startDate.getDate()+1);
                  let _startDate = startDate.toISOString().split("T")[0];

                  let invoiceDate = new Date($("#CratedDate").val().toString());
                  invoiceDate.setDate(invoiceDate.getDate()+1);
                  invoiceDate.setMonth(invoiceDate.getMonth()+parseInt(this.projectData.PeriodCount));
                  let _invoiceDate = invoiceDate.toISOString().split("T")[0];

                  let flag =  date.toISOString().split("T")[0];
                  let x = new Date(flag);
                  x.setDate(x.getDate()+30);
                  let _flag = x.toISOString().split("T")[0];
                  
                  let lastDate = new Date($("#LastDate").val().toString());
                  lastDate.setDate(lastDate.getDate()+1);
                  let _lastDate = lastDate.toISOString().split("T")[0];

                  if(_startDate < flag)
                  {
                    let _amount = amount.split(" ")[0].toString().replace(".","");;
                    let diffDays = new Date(flag).getTime() - new Date(_startDate).getTime();
                    let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                    proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(_startDate),
                          ExpirementDate: this.formatIsoDateToCrypted(flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi: " + _startDate,
                          Amount: Number(partialAmount).toLocaleString() + " " + this.currency,
                          Discount : $("#totaldiscount").val()
                       };
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )
                  }
                  for(var i = 0; i < repetitionCount; i++)
                  {
                    if(i == repetitionCount-1)
                    {
                      let _amount = amount.split(" ")[0].toString().replace(".","");;
                      let diffDays = new Date(_lastDate).getTime() - new Date(flag).getTime();
                      let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                      proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(_lastDate) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi : " + flag ,
                          Amount: Number(partialAmount).toLocaleString() + " " + this.currency,
                          Discount : $("#totaldiscount").val()
                       };
                    }
                    else
                    {
                      proform = {
                         ProformId: 0,
                          StatusId: 1,
                          SalesmanId: 0,
                          CustomerId: this.projectData.CustomerId,
                          ProjectId: boardId,
                          ProposalId: 0,
                          ProformNumber: "Pro-" + $("#boardName").val() ,
                          CreatedDate:  this.formatIsoDateToCrypted(flag),
                          ExpirementDate: this.formatIsoDateToCrypted(_flag) ,
                          DiscountTypeId: 0,
                          CurrencyTypeId: this.projectData.CurrencyTypeId,
                          AdminNote: "",
                          CustomerNote: "",
                          Conditions: "Faturalama tarihi : " + _invoiceDate ,
                          Amount: amount,
                          Discount : $("#totaldiscount").val()
                       };
                    }
                    
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform).subscribe(
                    (result) => {
                      var proformId = result[0].Result;
                      proform.ProformId = proformId;
                      let proformdetail;

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName, LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          proformdetail = { ProformId: proformId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount};   
                        
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformDetail", proformdetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;
                            //this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcProformToastrSaved);
                          }
                        }
                        )
                      }   

                      let proformcontact;

                      for(let j in this.selectedCustomerContacts)
                      {
                        proformcontact = { ProformId: proformId, ContactId: this.selectedCustomerContacts[j]};   
                      
                        this._apicallService.Add("http://localhost:61232/api/Proform/CreateProformContact", proformcontact).subscribe(
                        (result) =>{
                          let resultArray = JSON.parse(JSON.stringify(result));
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                        )
                      }       
                      }
                    )

                    let xx = new Date(flag);
                    new Date(xx.setMonth(xx.getMonth()+parseInt(this.projectData.PeriodCount)));
                    flag = xx.toISOString().split("T")[0];

                    let yy = new Date(flag);
                    new Date(yy.setDate(yy.getDate()+30));
                    _flag = yy.toISOString().split("T")[0];

                    let zz = new Date(_invoiceDate);
                    new Date(zz.setMonth(zz.getMonth()+parseInt(this.projectData.PeriodCount)));
                    _invoiceDate = zz.toISOString().split("T")[0];
                    
                  }
              /*
              else if(this.selectedSubRelation == 2)
              {
                //ilk tekrarlama tarihini alıyoruz ve ilk oluşacak invoice için expirement date 1 ay sonrasına yazıyoruz. Daha sonra
                  // oluşacak her invoice için döngü içinde oluşturuulan tarihten sonra ekrandan aldıgımız deger kadar artırıyoruz. tekrar sayısı kadar
                  //invoice olusturuyoruz.
                  // toastr verilecek bölünemeyen aylar için
                  let invoice;
                  let date  = new Date($("#repetitionDate").val().toString());
                  date.setDate(date.getDate()+1);
                  //date.setMonth(date.getMonth()+1);
                  var monthCount = this.monthDiff(new Date($("#CratedDate").val().toString()),new Date ($("#LastDate").val().toString()));
                  var repetitionCount = parseInt(monthCount+1) / parseInt(this.projectData.PeriodCount);

                  let endDate = new Date($("#LastDate").val().toString());
                  endDate.setDate(endDate.getDate()+1);
                  let _endDate = endDate.toISOString().split("T")[0];

                  let flag =  date.toISOString().split("T")[0];
                  let x = new Date(flag);
                  x.setDate(x.getDate()+30);
                  let _flag = x.toISOString().split("T")[0];
                  let createdDate = new Date($("#CratedDate").val().toString());
                  createdDate.setDate(createdDate.getDate()+1);
                  let _createdDate = createdDate.toISOString().split("T")[0];

                  createdDate.setMonth(createdDate.getMonth()+1);
                  let confirmDate = createdDate.toISOString().split("T")[0];

                  if(_createdDate < flag && confirmDate != flag  )
                  {
                    let _amount = amount.split(" ")[0].toString().replace(".","");;
                    let diffDays = new Date(flag).getTime() - new Date(_createdDate).getTime();
                    let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                    invoice = {
                      InvoiceId:0,
                      CustomerId: this.projectData.CustomerId,
                      ProjectId: boardId,
                      InvoiceNumber: "Inv-" + $("#boardName").val(),
                      CreatedDate: this.formatIsoDateToCrypted(_createdDate),
                      LastPaymentDate: this.formatIsoDateToCrypted(flag),
                      PayingMethodId: 1,
                      SalesmanId: 0,
                      ProposalId: 0,
                      ProformId: 0,
                      DiscountTypeId: 0,
                      CurrencyTypeId: this.projectData.CurrencyTypeId,
                      AdminNote: "",
                      CustomerNote: "",
                      Conditions: "",
                      StatusId: 5,
                      Amount :  Number(partialAmount).toLocaleString() + " " + this.currency,
                      Discount : $("#totaldiscount").val()
                    };

                    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                    (result) => {
                      var invoiceID = result[0].Result;
                      let invoiceDetail;           

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                          
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;    
                          }
                        }
                        )
                        }   
                        
                        let invoicecontact;

                        for(let j in this.selectedCustomerContacts)
                        {
                          invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                        
                          this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                          (result) =>{
                            let resultArray = JSON.parse(JSON.stringify(result));
                            if (resultArray[0].Result == "Session_Expired") {
                              this.loading = false;
                              this._router.navigate(['/auth']);
                              return;
                            }
                          }
                          )
                        }
                      }
                    )
                  }
                  else if (confirmDate >= flag )
                  {
                    flag = confirmDate;
                    let x = new Date(flag);
                    x.setDate(x.getDate()+30);
                    let _flag = x.toISOString().split("T")[0];
                  }

                  for(var i = 0; i < repetitionCount; i++)
                  {
                    if(i == repetitionCount -1 && _flag > _endDate )
                    {
                      _flag = _endDate;
                      let _amount = amount.split(" ")[0].toString().replace(".","");;
                      let diffDays = new Date(_flag).getTime() - new Date(flag).getTime();
                      let partialAmount = (parseInt(_amount)/30)*(diffDays/(1000*3600*24));
                      amount =  Number(partialAmount).toLocaleString() + " " + this.currency;
                    }
                    
                    invoice = {
                      InvoiceId:0,
                      CustomerId: this.projectData.CustomerId,
                      ProjectId: boardId,
                      InvoiceNumber: "Inv-" + $("#boardName").val(),
                      CreatedDate: this.formatIsoDateToCrypted(flag),
                      LastPaymentDate: this.formatIsoDateToCrypted(_flag),
                      PayingMethodId: 1,
                      SalesmanId: 0,
                      ProposalId: 0,
                      ProformId: 0,
                      DiscountTypeId: 0,
                      CurrencyTypeId: this.projectData.CurrencyTypeId,
                      AdminNote: "",
                      CustomerNote: "",
                      Conditions: "",
                      StatusId: 5,
                      Amount : amount,
                      Discount : $("#totaldiscount").val()
                    };

                    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice).subscribe(
                    (result) => {
                      var invoiceID = result[0].Result;
                      let invoiceDetail;           

                      for(let i = 0; i < this.addedDynamicLineData.length; i++)
                      {
                        let _product = 0;
                        
                        for(let j in this.productList)
                        {
                          if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                          {
                            _product = this.productList[j].value;
                          }
                        }

                        if(_product == 0)
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                        else
                          invoiceDetail = {ID: 0,InvoiceId: invoiceID, LineID: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                          
                        this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceDetail", invoiceDetail).subscribe(
                        (resultt) =>{
                          let resulttArray = JSON.parse(JSON.stringify(resultt));
                          if (resulttArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                          else
                          {
                            this.loading = false;    
                          }
                        }
                        )
                        }   
                        
                        let invoicecontact;

                        for(let j in this.selectedCustomerContacts)
                        {
                          invoicecontact = { InvoiceID: invoiceID, ContactId: this.selectedCustomerContacts[j]};   
                        
                          this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoiceContact", invoicecontact).subscribe(
                          (result) =>{
                            let resultArray = JSON.parse(JSON.stringify(result));
                            if (resultArray[0].Result == "Session_Expired") {
                              this.loading = false;
                              this._router.navigate(['/auth']);
                              return;
                            }
                          }
                          )
                        }
                      }
                    )

                    let xx = new Date(flag);
                    new Date(xx.setMonth(xx.getMonth()+parseInt(this.projectData.PeriodCount)));
                    flag = xx.toISOString().split("T")[0];
                    let yy = new Date(flag);
                    new Date(yy.setDate(yy.getDate()+30));
                    _flag = yy.toISOString().split("T")[0];
                    
                  }
              }*/
            }
          }
          this.loading = false;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.BpToastrAddedBoard);
          this._router.navigate(['/pages/project/']);  
        }
        )
      }
    }
    else
    {
      var x = document.getElementById("total");
      var amount = x.innerText.split(" ")[0] + " " + this.currency;

      if(this.subsArea == true)
      {
        if(this.selectedSubPeriod == 5)
        {
          this.projectData.PeriodCount = $("#customSubNumber").val().toString();
        }
        else
        {
          this.projectData.PeriodCount = '1';
        }
      }

      let board  = { BoardId: this._boardId, BoardName: $("#boardName").val(), BeginDate: $("#CratedDate").val(), EndDate: $("#LastDate").val(), StatusId: this.projectData.StatusId, CustomerId: this.projectData.CustomerId ,PeriodId : this.selectedSubPeriod , PeriodCount : this.projectData.PeriodCount , Amount : amount ,  Discount : $("#totaldiscount").val(), CurrencyTypeId : this.projectData.CurrencyTypeId , RepetitionDate : this.projectData.RepetitionDate, ResponsibleId : this.projectData.ResponsibleId  };

      if(board.BoardName == "" || board.CustomerId == 0 )
      {
        this.makeToast(NbToastStatus.DANGER,this.sysLanguages.ErrorMessage,this.sysLanguages.ErrorMessage1);
        this.loading = false;
      }
      else
      {
          this._apicallService.Update("http://localhost:61232/api/project/UpdateTaskBoard", this._boardId, board).subscribe((result) => {

          if(this.addedDynamicLineData.length != 0)
          {
            for(let i in this.addedDynamicLineData)
            {
                let _product = 0;
                for(let j in this.productList)
                {
                  if(this.productList[j].label == this.addedDynamicLineData[i].ProductName)
                  {
                    _product = this.productList[j].value;
                  }
                }

                let boarddetail;
                if(_product == 0)
                  boarddetail = { BoardId: this._boardId, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: Number(i)+1, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                else
                  boarddetail = { BoardId: this._boardId, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, ProductId: _product, LineId: Number(i)+1, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
    
                this._apicallService.Add("http://localhost:61232/api/Project/CreateProjectDetail", boarddetail).subscribe((result) => {
                  let resulttArray = JSON.parse(JSON.stringify(result));
                  if (resulttArray[0].Result == "Session_Expired") {
                    this.loading = false;
                    this._router.navigate(['/auth']);
                    return;
                  }
                })
            }
          }

          if(this.projectData.Invoice != 0)
          {
            // BoardId = boardid, customerid = invoiceid, statusid for distinction
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: this._boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Invoice, StatusId: 1}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }
          else if(this.projectData.Proform != 0)
          {
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: this._boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Proform, StatusId: 2}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }
          else if(this.projectData.Proposal != 0)
          {
            this._apicallService.Update("http://localhost:61232/api/Project/ConnectProject", 1, { BoardId: this._boardId, BoardName: "Nomatterwhatyousent", BeginDate: "Nomatterwhatyousent", EndDate: "Nomatterwhatyousent", CustomerId: this.projectData.Proposal, StatusId: 3}).subscribe(
            (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
            }
            )
          }

          if(this.projectData.CustomerId != 0)
          {
            //---------- eğer eski seçilmiş kontak varsa onları sil
            for(let i in this.copyselectedCustomerContacts)
            {
              if(this.selectedCustomerContacts.findIndex(x => x.value == this.copyselectedCustomerContacts[i]) == -1)
              {
                let projectcontact = { BoardId: this._boardId, ContactId: this.copyselectedCustomerContacts[i] }; 
                this._apicallService.Add("http://localhost:61232/api/Project/RemoveProjectContact", projectcontact).subscribe(
                  (result) =>{
                  }
                )
              }
            }

            //----------- yeni eklenen varsada onları ekle
            for(let j in this.selectedCustomerContacts)
            {
              if(this.copyselectedCustomerContacts.findIndex(x => x.value == this.selectedCustomerContacts[j]) == -1)
              {
                let projectcontact = { BoardId: this._boardId, ContactId: this.selectedCustomerContacts[j] };   
                this._apicallService.Add("http://localhost:61232/api/Project/CreateProjectContact", projectcontact).subscribe(
                  (result) =>{
                  }
                )
              }
            } 
          }

          this.loading = false;
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.BpToastrEditedBoard);
          this.addedDynamicLineData = [];
          this._router.navigate(['/pages/project/']);

        })
      } 
    }
  }

  createLine()
  {
    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid = line[0].childElementCount+1;
    var a1 = $("#a1").val();
    var b1 = $("#b1").val();
    var c1 = $("#c1").val().toString();
    var d1 = $("#d1").val().toString();
    var dis1=$("#dis1").val().toString();
    if(dis1 == "")
    {
      var total = (parseFloat(c1)*parseFloat(d1));
      dis1 = '0'; 
    }
    else
    {
      this._totalDiscount += parseFloat(dis1);
      var total = (parseFloat(c1)*parseFloat(d1))-parseFloat(dis1);
    }

    if(!this.tax || this.tax.length == 0)
    {
      this.TaxDesc = "Vergi seçimi yapılmadı.";
    }
    else if(this.tax.length == 1)
    {
      if(this.tax[0] == 1)
      {
        this.TaxDesc = "%18 KDV";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18), amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.18, amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18);
        }

        total = total + (total*0.18);
      }
      else if(this.tax[0] == 2)
      {
        this.TaxDesc = "%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*17.65/100);
      }
      else if(this.tax[0] == 3)
      {
        this.TaxDesc = "%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += (total * 0.15);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15);
        }

        total = total + (total*0.15);
      }
    }
    else if(this.tax.length == 2)
    {
      if((this.tax[0] == 1 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100);   
        }

        total = total + (total*0.18)+(total*17.65/100);
      }
      else if((this.tax[0] == 1 && this.tax[1] == 3) || (this.tax[0] == 3 && this.tax[1] == 1))
      {
        this.TaxDesc = "%18 KDV,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18);
        }

        total = total + (total*0.15) + (total*0.18);
      }
      else if((this.tax[0] == 3 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 3))
      {
        this.TaxDesc = "%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*0.15) + (total*17.65/100);
      }
    }
    else if(this.tax.length == 3)
    {
        this.TaxDesc = "%18 KDV,%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total = total + (total*0.18) + (total*0.15) + (total*17.65/100);
    }

    if (total.toString() == "NaN")
    {
      total = 0;
    }

    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><p id='c"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;''>" + c1 + "</p></td><td><p id='d"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + d1 + "</p></td><td><p id='dis"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + dis1 + "</p></td><td><p id='updatedefaultTax' style='float: left;margin-top: 26px;margin-left:5px;' >"+this.TaxDesc+"</p></td><td><p id='f"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>"+total+" "+this.currency+"</p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red ;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_" + newid);
    let mythis = this;

    this.total(newid);

    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let priceValue = element.parentElement.parentElement.children[6].children[0].innerHTML;
      let priceforTaxValue = element.parentElement.parentElement.children[3].children[0].innerHTML;
      let discounttext = element.parentElement.parentElement.children[4].children[0].innerHTML;
      let counttext = element.parentElement.parentElement.children[2].children[0].innerHTML;

      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue && x.LineAmount == priceValue), 1);

      var id = element.id.split("_")[1];
      var minus = $("#f"+id).text().split(" ")[0];
      document.getElementById(id).remove();
      var a = document.getElementById("total");
      this._total = this._total - parseFloat(minus);
      a.innerText = Number(this._total).toLocaleString() + " " + this.currency; 

      for(let i in this.topTaxArray)
      {
        if(this.topTaxArray[i].amount == priceValue.split(" ")[0] && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
        {
          this.topTax -= this.topTaxArray[i].value;
          if(this.topTax < 0)
          {
            this.topTax = 0;
          }
          break;
        }
      }

      this._totalDiscount -= parseFloat(discounttext);

      var productID = this.productList.filter(x=> x.label == nameValue)[0].value;
      var deletedElement = this.productTask.filter(x=>x.ProductId == productID);
      for(let i in deletedElement)
      {
        this.productTask.splice(this.productTask.findIndex(x=>x.ProductID == productID),1) ;
      }

    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString(), ProductCount: Number(c1), ProductPrice: Number(d1), LineAmount: total + " " + this.currency, updatedefaultTax: "", LineTax: this.TaxDesc , LineDiscount : dis1 };
    this.addedDynamicLineData.push(_lineData);

    if(!this._boardId)
    {
      var productID = this.productList.filter(x=> x.label == a1.toString())[0].value;
      this._apicallService.Get("http://localhost:61232/api/Product/GetProductTasks",productID).subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productTask = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _productTask = {ProductTaskId : resultArray[i].ProductTaskId , Title : resultArray[i].Title , TeamId : resultArray[i].TeamId , ProductId : resultArray[i].ProductId , DayCount : resultArray[i].DayCount , ParentId : resultArray[i].ParentId };
              this.productTask.push(_productTask);
            }
          }
      },
      )
    }
    
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let priceforTaxValue = event.currentTarget.parentElement.parentElement.children[3].children[0].innerText;
    let priceValue = event.currentTarget.parentElement.parentElement.children[6].children[0].innerText;
    let discounttext = event.currentTarget.parentElement.parentElement.children[4].children[0].innerText;
    let counttext = event.currentTarget.parentElement.parentElement.children[2].children[0].innerText;

    
    for(let i in this.topTaxArray)
    {
      if(this.topTaxArray[i].amount == priceValue && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
      {
        this.topTax -= this.topTaxArray[i].value;
        if(this.topTax < 0)
        {
          this.topTax = 0;
        }
        break;
      }
    }

    this._totalDiscount -= parseFloat(discounttext);

    this._apicallService.Deactive("http://localhost:61232/api/Project/RemoveProjectLine", Number(this.lineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          var x = this.lineData[id].LineAmount.split(" ")[0];
          var total =document.getElementById("total");
          var newTotal = this._total-parseFloat(x);
          this._total = newTotal;
          total.innerHTML=Number(newTotal).toLocaleString()+" " + this.currency;
          document.getElementById("lineData_" + id).remove();
          this.lineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  getSubscriptonPeriod()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Project/GetSubscriptionPeriod").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.subscriptionPeriodList = [];
          }
        }
        else 
        {
          for(var i in resultArray)
          {
            let _period = { value: resultArray[i].PeriodId, label: resultArray[i].PeriodDesc };
            this.subscriptionPeriodList.push(_period);
          }
          this.copysubscriptionPeriodList = [{value: 1, label: "Ayda"},{value: 2, label: "Yılda"}];
        }
      }
    )
  }

  toggle(event)
  {
    if(event.target.checked == true)
    {
      this.subsArea = true;
    }
    else
    {
      this.subsArea = false; 
    }
  }

  updatedTotal()
  {
    var total = 0;
    var z = document.getElementById("total");
    for(var i = 1; i < this.lineData.length; i++)
    {
      if(this.lineData[i].LineAmount != "0")
      {
        var sub=parseFloat(this.lineData[i].LineAmount);
        total=total+sub;
        z.innerHTML=Number(total).toLocaleString()+" " + this.currency;
        this._total = total;

        if(this.lineData[i].LineDiscount)
        {
            this._totalDiscount += Number(this.lineData[i].LineDiscount);
        }

        if(this.lineData[i].updatedefaultTax == "%18 KDV")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.18, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice});
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);   
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
      }
    }
    var discount = this.projectData.TotalDiscount;
    if(discount)
    {
      this._total = this._total - parseFloat(discount);
    }
    z.innerHTML=Number(this._total).toLocaleString()+" " + this.currency;
  }

  total(id)
  {
    var y = document.getElementById("f"+id).innerHTML.split(" ")[0];
    var x =document.getElementById("total");
    this._total = parseFloat(y) + this._total;
    x.innerText=Number(this._total).toLocaleString()+" " + this.currency;  
  }

  totalDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total - parseFloat(y);
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
  }

  cleanDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total + parseFloat(y);
    this._total = discountedTotal;
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
    $("#totaldiscount").val("0");
  }

  changeCurrencyType(event)
  {
    if(event == 1)
    {
      this.currency = "TL";
    }
    else if(event == 2)
    {
      this.currency = "$";
    }
    else if(event == 3)
    {
      this.currency = "€";
    }
    else if(event == 4)
    {
      this.currency = "£";
    }
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01" || splittedDate[1] == "1")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02" || splittedDate[1] == "2")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03" || splittedDate[1] == "3")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04" || splittedDate[1] == "4")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05" || splittedDate[1] == "5")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06" || splittedDate[1] == "6")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07" || splittedDate[1] == "7")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08" || splittedDate[1] == "8")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09" || splittedDate[1] == "9")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

   monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
