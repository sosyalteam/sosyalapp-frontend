import { Component, OnInit, TemplateRef, ElementRef } from '@angular/core';
import '../../../../../node_modules/ionicons/css/ionicons.css';
import { ApicallService } from '../../apicall.service';
import * as $ from "jquery";
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';
import {ExcelService} from "../../excel-service/excel.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'board-panel',
  templateUrl: './board-panel.component.html',
  styleUrls: ['./board-panel.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class BoardPanelComponent implements OnInit {

   customerList = [];
   boardStatusList = [];
   productgroupList = [];
   userList = [];
   boardProductGroupList = [];
   boardUserList = [];

   boardList: any;
   filteredBoardList = [];
   copyboardList: any;
   sysLanguages: any;
   selectedGroup = "all";
   selectedUser = "all";
   teams = "";

   allProjectsCount = 0;
   notStartCount = 0;
   continuesCount = 0;
   stayCount = 0;
   canceledCount = 0;
   completedCount = 0;
   filter = false;
   filterSelectCount =0;
   selectedfilter0 = 0;
   selectedfilter1 = 0;
   selectedfilter2 = 0;
   selectedfilter3 = 0;
   selectedfilter4 = 0;


  constructor(private _router: Router, private _apicallService: ApicallService, private dialogService: NbDialogService, private titleService: Title,private _eref: ElementRef, private excelService:ExcelService) 
  { 
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProjectText + " - Crmplus");

    this.getBoards("noMatterWhatYouSent");
    this.getCustomers();
    this.getBoardStatus();
    this.getProductGroups();
    this.getAllUsers();
    this.getBoardDetails();
    this.getBoardUserList();

    this.settings.columns.Name.title=this.sysLanguages.BoardTableNameText;
    this.settings.columns.BeginDate.title=this.sysLanguages.BoardTableStartDateText;
    this.settings.columns.EndDate.title=this.sysLanguages.BoardTableEndDateText;
    this.settings.columns.Status.title=this.sysLanguages.BoardTableStatusText;
    this.settings.columns.Customer.title = this.sysLanguages.BoCardCustomerText;
    this.settings.columns.teams.title = this.sysLanguages.IncludesTeamsText;
    this.settings.actions.columnTitle = this.sysLanguages.ArActionText;
    this.settings.columns.Amount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settings.columns.Fullname.title = this.sysLanguages.TasksResponsible;

  }

  settings = {
    columns: {
      Name: {
        title: ''
      },
      Customer: {
        title: ''
      },
      BeginDate: {
        title: ''
      },
      EndDate: {
        title: ''
      },
      Status: {
        title: '',
      },
      teams :{
        title : '',
      },
      Amount :{
        title : ''
      },
      Fullname: {
        title: ''
      }
    },
    actions:{
      columnTitle: '',
      position:'right',
      add:false
     // custom: [{ title: '<i class="nb-menu"></i>' }]
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
    edit:{
      editButtonContent: '<i class="nb-edit"></i>'
    },

    mode: 'external',
  };

  ngOnInit() {
  }

  getBoards(boardList)
  {
    // tüm bu saçmalık ng2smarttable sadece bu fonksiyonda güncellendiği için normal çağrılarda statik bi parametre gönderip normal işini yaptırıyorum, filtreden geldiğinde ise listeyi göndertip içini doldur boşalt yapıyorum.
    if(boardList == "noMatterWhatYouSent")
    {
      this._apicallService.GetAll("http://localhost:61232/api/project/GetTaskBoards").subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.boardList = [];
            }
          }
          else {
            this.boardList = [];
            for(let j = 0; j < resultArray.length; j++)
            {
              this.teams = "";
              for(let i in resultArray[j].teamNames)
              {
                this.teams += resultArray[j].teamNames[i].Key + ","
              }
              let _boardList = { BoardId: resultArray[j].BoardId, Name: resultArray[j].BoardName, Customer: resultArray[j].CustomerName, BeginDate: resultArray[j].BeginDate.split(' ')[0], EndDate: resultArray[j].EndDate.split(' ')[0], Status: resultArray[j].StatusDesc , StatusId : resultArray[j].StatusId , Amount : resultArray[j].Amount, teams : this.teams.substring(0,this.teams.length-1),PeriodCount : resultArray[j].PeriodCount, Fullname : resultArray[j].Fullname };
              this.boardList.push(_boardList);
            }
            this.copyboardList = this.boardList.slice();
            this.boardList = this.boardList.filter(x=>x.StatusId != 4 && x.StatusId !=3);
            this.allProjectsCount = resultArray.length;
            this.notStartCount = resultArray.filter(x=>x.StatusId == 0).length;
            this.continuesCount = resultArray.filter(x=>x.StatusId == 1).length;
            this.stayCount = resultArray.filter(x=>x.StatusId == 2).length;
            this.canceledCount = resultArray.filter(x=>x.StatusId == 3).length;
            this.completedCount = resultArray.filter(x=>x.StatusId == 4).length;
          }
        }
      )
    }
    else
    {
      let copyList = this.boardList.slice();
      this.boardList = [];
      this.boardList = copyList.slice();
    }
  }

  getBoardDetails()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Project/GetBoardsDetails").subscribe(
      (result) =>
      {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.boardProductGroupList = [];
          }
        }
        else
        {
          for(var i in resultArray)
          {
            this.boardProductGroupList.push({ BoardId: resultArray[i].BoardId, ProductGroupId: resultArray[i].ProductGroupId });
          }
        }
      }
      ) 
  }

  getBoardUserList()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Project/GetBoardsUsers").subscribe(
      (result) =>
      {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.boardUserList = [];
          }
        }
        else
        {
          let flag = 0;
          let copyArray = [];

          for(var i in resultArray)
          {
            //copyArray = resultArray.slice();
            flag = 0;

            if(Number(i) == 0)
            {
              copyArray.push(resultArray[0]);
            }
            else
            {
              if(copyArray.findIndex(x => x.BoardId == resultArray[i].BoardId && x.UserId == resultArray[i].UserId) != -1)
              {
                flag++;
              }

              if(flag == 0)
                copyArray.push({ BoardId: resultArray[i].BoardId, UserId: resultArray[i].UserId });
            }          
          }

          this.boardUserList = copyArray.slice();
        }
      }
      ) 
  }

  getBoardStatus()
  {
    this._apicallService.GetAll("http://localhost:61232/api/project/GetBoardStatus").subscribe(
      (result) =>
      {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resultArray[0].Result == "Empty") {
            this.boardStatusList = [];
          }
        }
        else
        {
          for(var i in resultArray)
          {
            let _statusList = { value: resultArray[i].StatusId, label: resultArray[i].StatusDesc };
            this.boardStatusList.push(_statusList);
          }
        }
      }
      ) 
  }

  getCustomers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerList = [];
          }
        }
        else {
          for(var i in resultArray)
          {
            let _customerList = { value: resultArray[i].CustomerId, label: resultArray[i].CustomerName };
            this.customerList.push(_customerList);
          }
        }
      }
    )
  }

  getProductGroups()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Product/GetAllProductGroup").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productgroupList = [];
          }
        }
        else {
          this.productgroupList = [];
          this.productgroupList.push({ value: "all", label: this.sysLanguages.BpAll });

          for(let j = 0; j < resultArray.length; j++)
          {
            let _pgList = { value: resultArray[j].ProductGroupId, label: resultArray[j].ProductGroupDesc };
            this.productgroupList.push(_pgList);
          }
        }
      }
    )
  }

  getAllUsers()
  {
    this._apicallService.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if(resultArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else {
          this.userList = [];
          this.userList.push({ value: "all", label: this.sysLanguages.BpAll });

          for(let j = 0; j < resultArray.length; j++)
          {
            let _userList = { value: resultArray[j].UserId, label: resultArray[j].Fullname };
            this.userList.push(_userList);
          }
        }
      }
    )
  }

  deleteBoard(event)
  {
    this._apicallService.Deactive("http://localhost:61232/api/project/DeleteTaskBoard", event.data.BoardId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getBoards("noMatterWhatYouSent");
      }
      else if(resultArray[0].Result == "HasColumns")
      {
        alert(this.sysLanguages.CantDeleteThisBoardAlert);
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
      }, error => console.error(error)) 
  }

  onCustomAction(event){
    var id = `${event.data.BoardId}`;
    this._router.navigate(['/pages/task/board-overview', id]);
  }


  filterProductGroup(_productgroupid)
  {
    this.selectedGroup = _productgroupid;
    if(_productgroupid == "all")
    {
      this.boardList = this.copyboardList.slice();

      if(this.selectedUser != "all")
      {
        this.filterUser(this.selectedUser);  
      }
      else
      {
        this.boardList = this.copyboardList.slice();
        this.getBoards(this.boardList);
      }     
    }
    else
    {
      if(this.selectedUser != "all")
        this.filterGeneral("all", this.selectedUser);

      let filteredBoardIds = [];
      for(let i in this.boardProductGroupList) //--------------- önce ürün gurubunu içeren board idler bir diziye alınıyor
      {
        if(this.boardProductGroupList[i].ProductGroupId == _productgroupid)
        {
          filteredBoardIds.push({ BoardId: this.boardProductGroupList[i].BoardId });
        }
      }

      if(typeof this.boardList[0] !== 'undefined') //---------------- sonra boardlistteki elemanlar eğer bir önceki dizide yoksa onlar siliniyor
      {
        let copyList = this.boardList.slice();
        for(let j=0; j<copyList.length; j++)
        {
          if(filteredBoardIds.findIndex(x => x.BoardId == copyList[j].BoardId) == -1)
          {
            let index = this.boardList.findIndex(x => x.BoardId == copyList[j].BoardId);
            this.boardList.splice(index, 1);
          }
        }
      }

      if(this.selectedUser == "all")
      {
        for(let k in filteredBoardIds) //--------------- enson boardlistte olmayan fakat ilk dizide olan bir eleman varmı diye bakıyoruz yoksa ekliyoruz falan filan
        {
          if(typeof this.boardList[0] === 'undefined')
          {
            this.boardList.splice(0, 1);
            let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
            this.boardList.push(element[0]);
          }
          else
          {
            if(this.boardList.findIndex(x => x.BoardId == filteredBoardIds[k].BoardId) == -1)
            {
              let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
              this.boardList.push(element[0]);
            }
          }
        }
      }

      this.getBoards(this.boardList);
    }
  }

  filterUser(userid)
  {
    this.selectedUser = userid;
    if(userid == "all")
    {
      if(this.selectedGroup != "all")
      {
        this.filterProductGroup(this.selectedGroup);  
      }
      else
      {
        this.boardList = this.copyboardList.slice();
        this.getBoards(this.boardList);
      }  
    }
    else
    {
      if(this.selectedGroup != "all")
        this.filterGeneral(this.selectedGroup, "all");

      let filteredBoardIds = [];
      for(let i in this.boardUserList)
      {
        if(this.boardUserList[i].UserId == userid)
        {
          filteredBoardIds.push({ BoardId: this.boardUserList[i].BoardId });
        }
      }

      if(typeof this.boardList[0] !== 'undefined')
      {
        let copyList = this.boardList.slice();
        for(let j=0; j<copyList.length; j++)
        {
          if(filteredBoardIds.findIndex(x => x.BoardId == copyList[j].BoardId) == -1)
          {
            let index = this.boardList.findIndex(x => x.BoardId == copyList[j].BoardId);
            this.boardList.splice(index, 1);
          }
        }
      }

      if(this.selectedGroup == "all")
      {
        for(let k in filteredBoardIds)
        {
          if(typeof this.boardList[0] === 'undefined')
          {
            this.boardList.splice(0, 1);
            let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
            this.boardList.push(element[0]);
          }
          else
          {
            if(this.boardList.findIndex(x => x.BoardId == filteredBoardIds[k].BoardId) == -1)
            {
              let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
              this.boardList.push(element[0]);
            }
          }
        }
      }

      this.getBoards(this.boardList);
    }
  }

  filterGeneral(_productgroupid, user_id)
  {
    if(_productgroupid != "all")
    {
      let filteredBoardIds = [];
      for(let i in this.boardProductGroupList)
      {
        if(this.boardProductGroupList[i].ProductGroupId == _productgroupid)
        {
          filteredBoardIds.push({ BoardId: this.boardProductGroupList[i].BoardId });
        }
      }

      if(typeof this.boardList[0] !== 'undefined')
      {
        let copyList = this.boardList.slice();
        for(let j=0; j<copyList.length; j++)
        {
          if(filteredBoardIds.findIndex(x => x.BoardId == copyList[j].BoardId) == -1)
          {
            let index = this.boardList.findIndex(x => x.BoardId == copyList[j].BoardId);
            this.boardList.splice(index, 1);
          }
        }
      }

      for(let k in filteredBoardIds)
      {
        if(typeof this.boardList[0] === 'undefined')
        {
          this.boardList.splice(0, 1);
          let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
          this.boardList.push(element[0]);
        }
        else
        {
          if(this.boardList.findIndex(x => x.BoardId == filteredBoardIds[k].BoardId) == -1)
          {    
            let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
            this.boardList.push(element[0]);
          }
        }
      }
    }
    if(user_id != "all")
    {
      let filteredBoardIds = [];
      for(let i in this.boardUserList)
      {
        if(this.boardUserList[i].UserId == user_id)
        {
          filteredBoardIds.push({ BoardId: this.boardUserList[i].BoardId });
        }
      }

      if(typeof this.boardList[0] !== 'undefined')
      {
        let copyList = this.boardList.slice();
        for(let j=0; j<copyList.length; j++)
        {
          if(filteredBoardIds.findIndex(x => x.BoardId == copyList[j].BoardId) == -1)
          {
            let index = this.boardList.findIndex(x => x.BoardId == copyList[j].BoardId);
            this.boardList.splice(index, 1);
          }
        }
      }

      for(let k in filteredBoardIds)
      {
        if(typeof this.boardList[0] === 'undefined')
        {
          this.boardList.splice(0, 1);
          let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
          this.boardList.push(element[0]);
        }
        else
        {
          if(this.boardList.findIndex(x => x.BoardId == filteredBoardIds[k].BoardId) == -1)
          {
            let element = this.copyboardList.filter(x => x.BoardId == filteredBoardIds[k].BoardId);
            this.boardList.push(element[0]);
          }
        }
      }
    }
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  filterProject(value)
  {
    this.filter = true;
    if(value == "0")
    {
      this.filteredBoardList = this.copyboardList.filter(x=>x.StatusId == 0);
    }
    else if(value == "1")
    {
      this.filteredBoardList = this.copyboardList.filter(x=>x.StatusId == 1);
    }
    else if(value == "2")
    {
      this.filteredBoardList = this.copyboardList.filter(x=>x.StatusId == 2);
    }
    else if(value == "3")
    {
      this.filteredBoardList = this.copyboardList.filter(x=>x.StatusId == 3);
    }
    else if(value == "4")
    {
      this.filteredBoardList = this.copyboardList.filter(x=>x.StatusId == 4);
    }
    else if(value == "All")
    {
      this.filter = false;
      this.filteredBoardList = [];
      this.filterSelectCount = 0;
      this.selectedfilter0 = 0;
      this.selectedfilter1 = 0;
      this.selectedfilter2 = 0;
      this.selectedfilter3 = 0;
      this.selectedfilter4 = 0;
    }
  }

  openFilterArea()
  {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  openFilterProjectArea()
  {
    document.getElementById("myDropdownProject").classList.toggle("show-project");

    setTimeout(()=>{
      var myDropdown = document.getElementById("myDropdownProject");
      if (myDropdown.classList.contains('show-project')) {
        myDropdown.classList.remove('show-project');
      }
    },3000);
  }

  onClick(event) {
    var classname = event.target.className;
    if(classname == "btn ng-star-inserted" || classname == "filterStatus ng-star-inserted" || classname == "filterStatusSelected ng-star-inserted" || classname == "ion-ios-flag" )
    {
      //menu kapanmasın dıye
    }
    else
    {
      //menu kapatma
      var myDropdown = document.getElementById("myDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
    }
      
  }

  filterStatus(value)
  {
    var x = document.getElementById("status"+value);
    if(x.classList[0] == "filterStatus")
    {
      this.filter = true;
      const a = this.copyboardList.filter(x=>x.StatusId == value);
      this.filteredBoardList = this.filteredBoardList.concat(a);
      this.filterSelectCount++;
      if(value == 0)
      {
        this.selectedfilter0 = 1;
      }
      else if(value == 1)
      {
        this.selectedfilter1 = 1;
      }
      else if(value == 2)
      {
        this.selectedfilter2 = 1;
      }
      else if(value == 3)
      {
        this.selectedfilter3 = 1;
      }
      else if(value == 4)
      {
        this.selectedfilter4 = 1;
      }

    }
    else if (x.classList[0] == "filterStatusSelected")
    {
      this.filteredBoardList = this.filteredBoardList.filter(x=>x.StatusId != value );
      this.filterSelectCount--;
      if(value == 0)
      {
        this.selectedfilter0 = 0;
      }
      else if(value == 1)
      {
        this.selectedfilter1 = 0;
      }
      else if(value == 2)
      {
        this.selectedfilter2 = 0;
      }
      else if(value == 3)
      {
        this.selectedfilter3 = 0;
      }
      else if(value == 4)
      {
        this.selectedfilter4 = 0;
      }
    }
    if(this.filterSelectCount == 0)
    {
      this.filter = false;
      this.filteredBoardList = [];
    }
  }

  filterBoard(value)
  {
    debugger;
    if(value == 1)
    {
      this.filter = true;
      const a = this.copyboardList.filter(x=>x.PeriodCount == 0);
      this.filteredBoardList = a;
    }
    else if(value == 2)
    {
      this.filter = true;
      const a = this.copyboardList.filter(x=>x.PeriodCount == 1);
      this.filteredBoardList = a;
    }
    else if(value == 0)
    {
      this.filter = false;
    }

  }

  openExportArea()
  {
    document.getElementById("myExportDropdown").classList.toggle("show");

    setTimeout(()=>{
      var myDropdown = document.getElementById("myExportDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
    },2500);
  }

  exportExcel()
  {
    if(this.filter == true)
    {
      this.excelService.exportAsExcelFile(this.filteredBoardList, this.sysLanguages.ProjectText);
    }
    else
    {
      this.excelService.exportAsExcelFile(this.boardList, this.sysLanguages.ProjectText);
    }
  }

  exportPdf()
  {
    var doc = new jsPDF("l","mm","a4");
    var col = ["Name", "Customer","Begin Date","End Date","Status","Amount","Teams"];
    var rows = [];
    var itemNew = this.boardList;

   itemNew.forEach(element => {      
        var temp = [element.Name.toUpperCase(), element.Customer.toUpperCase(), element.BeginDate.split(" ")[0], element.EndDate.split(" ")[0], element.Status.toUpperCase(),element.Amount,element.teams.toUpperCase()];
        rows.push(temp);
    });        

    doc.autoTable(col, rows,{
        columnStyles: {
        0: {columnWidth: 75},
        1: {columnWidth: 45},
        2: {columnWidth: 22},
        3: {columnWidth: 22},
        4: {columnWidth: 30},
        5: {columnWidth: 25},
        6: {columnWidth: 50}
        },
        theme : "grid"
      }
    );
    doc.save(this.sysLanguages.ProjectText + ".pdf");
  }

}
