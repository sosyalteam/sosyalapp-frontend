import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardPanelComponent } from './board-panel/board-panel.component';
import { AddEditProjectComponent } from './add-edit-project/add-edit-project.component';

const routes: Routes = [
  {
    path: '',
    component: BoardPanelComponent,
  },
  {
    path: 'add-edit-project',
    component: AddEditProjectComponent,
  },
  {
    path: 'add-edit-project/:id',
    component: AddEditProjectComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
