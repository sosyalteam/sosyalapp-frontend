import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRoutingModule } from './project-routing.module';
import { BoardPanelComponent } from '../project/board-panel/board-panel.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbDialogModule, NbCheckboxModule, NbPopoverModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DropdownModule } from 'primeng/dropdown';
import { AddEditProjectComponent } from './add-edit-project/add-edit-project.component';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  declarations: [BoardPanelComponent, AddEditProjectComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    NbDialogModule.forRoot(),
    DropdownModule,
    MultiSelectModule,
    NbCheckboxModule,
    NbPopoverModule,
  ]
})
export class ProjectModule { }
