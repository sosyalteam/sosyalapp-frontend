import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-proposals',
  templateUrl: './add-edit-proposals.component.html',
  styleUrls: ['./add-edit-proposals.component.scss'],
  providers:[ApicallService, NbToastrService]
})
export class AddEditProposalsComponent implements OnInit {

  sysLanguages: any;
  Proposalid: any;
  proposalData = { Address: "", Amount: "", City: "", CreatedDate: "", ExpiredDate: "", Phone: "", PostCode: "", ProposalId: "", Subject: "", Town: "", Status: 0, Lead: 0, Customer: 0, Currency: 0, Discount: 0, Product: 0, User: 0, Language: 0, AdminNote : "", TotalDiscount : "0"  };
  lineData = [{ Id: '', ProductName: '', LineDesc: '', ProductCount: 1, ProductPrice: 0, LineAmount: "0", updatedefaultTax: "", LineDiscount : '0' }];
  addedDynamicLineData = [];
  proposalstatusList = [];
  discountList = [];
  currencyList = [];
  productList = [];
  taxList = [];
  customerList = [];
  contactList = [];
  customerContact = [];
  leadList = [];
  userList = [];
  relationList = [];
  languageList = [];
  isSelectedLeadOrCust: any;
  tax: any;
  TaxDesc: string;
  sendMail = false;
  selectedCustomerContacts = [];
  copyselectedCustomerContacts = [];

  _total = 0;
  topTax = 0;
  topTaxArray = [];
  _totalDiscount = 0;

  leadid : any;
  loading = false;

  currency = "TL";

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  
  constructor(private _router: Router, private _apicallservice: ApicallService, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.ProposalsText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.Proposalid = this._avRoute.snapshot.params["id"];
    }

    this.relationList.push({ label: this.sysLanguages.LeadText, value: 1 }, { label: this.sysLanguages.CustomerText, value: 2 });
    this.languageList.push({ label: this.sysLanguages.LanguageOptionTurkish, value: 0 }, { label: this.sysLanguages.LanguageOptionEnglish, value: 1 });

    this.getProposalStatusses();
    this.getDiscountTypes();
    this.getCurrencies();
    this.getProducts();
    this.getTax();
    this.getCustomers();
    this.getUsers();
    this.getLeads();
    this.getAllContact();

    this.leadid = parseInt(sessionStorage.getItem('leadId'));
    sessionStorage.removeItem('leadId');


  }

  ngOnInit() 
  {
    if(this.Proposalid>0)
    {
      setTimeout(()=>{
      this._apicallservice.Get("http://localhost:61232/api/Proposal/GetProposal",this.Proposalid)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            this.proposalData.Status = Number(resulttArray[0].StatusId);
            this.proposalData.Currency = Number(resulttArray[0].CurrencyTypeId);
            this.proposalData.Customer = Number(resulttArray[0].CustomerId);
            this.proposalData.Discount = Number(resulttArray[0].DiscountTypeId);
            this.proposalData.Lead = Number(resulttArray[0].LeadId);
            this.proposalData.Phone = resulttArray[0].Phone;
            this.proposalData.PostCode = resulttArray[0].PostCode;
            this.proposalData.Product = 0;
            this.proposalData.Subject = resulttArray[0].Subject;
            this.proposalData.Town = resulttArray[0].Town;
            this.proposalData.User = Number(resulttArray[0].UserId);
            this.proposalData.Address = resulttArray[0].Address;
            this.proposalData.Amount = resulttArray[0].Amount;
            this.proposalData.City = resulttArray[0].City;      
            this.proposalData.ExpiredDate = this.formatIsoDateToCrypted(resulttArray[0].ExpiredDate.split(" ")[0]);
            this.proposalData.CreatedDate = this.formatIsoDateToCrypted(resulttArray[0].CreatedDate.split(" ")[0]);
            this.proposalData.AdminNote = resulttArray[0].AdminNote;
            this.proposalData.TotalDiscount = resulttArray[0].Discount;
            
            if(Number(this.proposalData.Customer) != 0)
            {
              this.isSelectedLeadOrCust = 2;

              this.customerContact = [];

              for(let i in this.contactList)
              {
                if(this.contactList[i].customerId == this.proposalData.Customer)
                {
                  this.customerContact.push(this.contactList[i]);
                }
              }

            }
            else if(Number(this.proposalData.Lead) != 0)
            {
              this.isSelectedLeadOrCust = 1;
            }
            else
            {
              this.isSelectedLeadOrCust = 0;
            }
            
            this._apicallservice.Get("http://localhost:61232/api/Proposal/GetProposalContacts", this.Proposalid).subscribe(
              (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(resultArray[0].Result == "Empty")
                    {
                      this.selectedCustomerContacts = [];
                    }
                  }
                  else
                  { 
                    this.selectedCustomerContacts = resultArray.map(x=> x.ContactId);
                    this.copyselectedCustomerContacts = this.selectedCustomerContacts.slice();
                  }
              }
            )

          }              
      }
      )

      this._apicallservice.Get("http://localhost:61232/api/Proposal/GetProposalLines", this.Proposalid).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            /*for(var i in resultArray)
            {
              let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: resultArray[i].Description, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName };
              this.lineData.push(_lineData);
            }*/
            let _lineData;          
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }

            this.updatedTotal();
          }
        }
      )
      }, 1500);
    }
    else
    {
      let _today = new Date(); 
      let _expiredDate = new Date();
       _expiredDate.setDate(_today.getDate()+30);
      if(this.leadid > 0)
      {
        setTimeout(()=>{
          const lead = this.leadList.filter(x=> x.value == this.leadid);
          this.isSelectedLeadOrCust = 1;
          this.proposalData.Lead = this.leadid;
          this.proposalData.Address = lead[0].address;
          this.proposalData.City = lead[0].city;
          this.proposalData.Town = lead[0].town;
          this.proposalData.ExpiredDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
          this.proposalData.CreatedDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
          this.proposalData.Status = 1;
          this.proposalData.Currency = 1;
          this.proposalData.Customer = 0;
          this.proposalData.Discount = 1;
          this.proposalData.Phone = "";
          this.proposalData.PostCode = "";
          this.proposalData.Product = 0;
          this.proposalData.Subject = "";
          this.proposalData.User = 0;
          this.selectedCustomerContacts = [];
          this.proposalData.Language = 0;
          this.proposalData.AdminNote = "";
          this.proposalData.Amount = "";
        },3000);
      }
      else
      {
        this.proposalData.Status = 0;
        this.proposalData.Currency = 0;
        this.proposalData.Customer = 0;
        this.proposalData.Discount = 0;
        this.proposalData.Lead = 0;
        this.proposalData.Phone = "";
        this.proposalData.PostCode = "";
        this.proposalData.Product = 0;
        this.proposalData.Subject = "";
        this.proposalData.Town = "";
        this.proposalData.User = 0;
        this.proposalData.Address = "";
        this.proposalData.Amount = "";
        this.proposalData.City = ""; 
        this.selectedCustomerContacts = [];   
        this.proposalData.ExpiredDate = this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0]);
        this.proposalData.CreatedDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
        this.proposalData.Language = 0;
        this.proposalData.AdminNote = "";
      }

    }

  }

  getProposalStatusses()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proposal/GetAllStatus").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.proposalstatusList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _status;

            if(resultArray[i].StatusId == 1)    
            {
              _status = { label: this.sysLanguages.PcDraftStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 2)
            {
              _status = { label: this.sysLanguages.PcSentStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 3)
            {
              _status = { label: this.sysLanguages.PcOpenStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 4)
            {
              _status = { label: this.sysLanguages.PcRevizeStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 5)
            {
              _status = { label: this.sysLanguages.PcRefusedStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 6)
            {
              _status = { label: this.sysLanguages.PcAcceptedStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            }
            else if(resultArray[i].StatusId == 7)
            {
              _status = { label: this.sysLanguages.PcConvertedStatus, value: resultArray[i].StatusId };
              this.proposalstatusList.push(_status);
            } 
          }

          this.proposalData.Status = 1;
        }
    }
    )
  }

  getDiscountTypes()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetDiscountTypes").subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.discountList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _discount;

              if(resultArray[i].DiscountTypeId == 1)
              {
                _discount = { label: this.sysLanguages.DiscountTypeNoDiscount, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
              else if(resultArray[i].DiscountTypeId == 2)
              {
                _discount = { label: this.sysLanguages.DiscountTypeBeforeTax, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
              else if(resultArray[i].DiscountTypeId == 3)
              {
                _discount = { label: this.sysLanguages.DiscountTypeAfterTax, value: resultArray[i].DiscountTypeId };
                this.discountList.push(_discount);
              }
            }
            this.proposalData.Discount = 1;
          }
      },
      )
  }

  getCurrencies()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetCurrencies").subscribe(
      (result) => {        
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.currencyList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              let _currency = { label: resultArray[i].CurrencyDesc, value: resultArray[i].CurrencyId };
              this.currencyList.push(_currency);
            }
            this.proposalData.Currency = 1;
          }
      }
      )
  }

  getProducts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Product/GetAllProducts").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.productList = [];
          }
        }
        else
        {
          this.productList = [];
          let _product;

          for(let i in resultArray)
          {
            if(resultArray[i].ProductId != 0)
            {
              if(resultArray[i].prodtask.ProductTaskId == 0)  
              {
                if(resultArray[i].Price.toString().includes(",") == true)
                {
                  resultArray[i].Price = resultArray[i].Price.toString().substring(0, resultArray[i].Price.toString().indexOf(",")); 
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }
                else
                {
                  resultArray[i].Price = resultArray[i].Price.toString().replace(".","");
                }

                for(let j in resultArray)
                {
                  if(resultArray[i].ProductId == resultArray[j].ProductId)
                  {
                    if(resultArray[j].prodtask.ProductTaskId != 0)
                    {
                      if(resultArray[j].prodtask.Price.toString().includes(",") == true)
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().substring(0, resultArray[j].prodtask.Price.toString().indexOf(",")); 
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }
                      else
                      {
                        resultArray[j].prodtask.Price = resultArray[j].prodtask.Price.toString().replace(".","");
                      }

                      resultArray[i].Price = (Number(resultArray[j].prodtask.Price) + Number(resultArray[i].Price));
                    }
                  }
                }

                _product= { label: resultArray[i].ProductName, value: resultArray[i].ProductId, price: resultArray[i].Price, desc: resultArray[i].ProductDesc };
                this.productList.push(_product);
              }
              else
              {
                break;
              }
            }
          }
        }
    },
    )
  }

  getTax()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Invoice/GetTax").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.taxList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].TaxId != 0)
            {
              let _tax = { label: resultArray[i].TaxRate, value: resultArray[i].TaxId, desc: resultArray[i].TaxDesc };
              this.taxList.push(_tax);
            }
          }
        }
    },
    )

  }

  getCustomers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId, address: resultArray[i].Address, city: resultArray[i].City };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }

  getUsers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            if(resultArray[i].UserId != "0")
            {
              let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
              this.userList.push(_user);
            }
          }
        }        
      },
      )
  }

  getLeads()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Lead/GetAllLead").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.leadList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].LeadId != 0)
              {
                let _lead = { label: resultArray[i].LeadName  + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].LeadId, name: resultArray[i].Name, address: resultArray[i].Address, email: resultArray[i].Email, city: resultArray[i].City, town: resultArray[i].Town, customerid: resultArray[i].CustomerId, contactid: resultArray[i].ContactId };
                this.leadList.push(_lead);
              }
            }
          }
      }
    )
  }

  getAllContact()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.contactList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].ContactId != 0)
              {
                let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId, customerId: resultArray[i].CustomerId, email: resultArray[i].Email };
                this.contactList.push(_contact);
              }
            }
          }
      }
    )
  }

  getProposalUser(event, flag)
  {
    if(flag == 0)
    {
      for(let i in this.leadList)
      {
        if(this.leadList[i].value == event)
        {
          this.proposalData.Address = this.leadList[i].address;
          this.proposalData.City = this.leadList[i].city;
          this.proposalData.Town = this.leadList[i].town;
          break;
        }
      }

      this.selectedCustomerContacts = [];
      this.proposalData.Customer = 0;
    }
    else if(flag == 1)
    {
      this.customerContact = [];

      for(let i in this.contactList)
      {
        if(this.contactList[i].customerId == event)
        {
          this.customerContact.push(this.contactList[i]);
        }
      }

      for(let j in this.customerList)
      {
        if(this.customerList[j].value == event)
        {
          this.proposalData.Address = this.customerList[j].address;
          this.proposalData.City = this.customerList[j].city;
          break;
        }
      }
      
      this.selectedCustomerContacts = [];
      this.proposalData.Lead = 0;
    }
  }

  saveProposal()
  {
    this.loading = true;
    if(!this.Proposalid)
    {
      var x = document.getElementById("total");
      let proposal;

      if(this.proposalData.Lead == 0 && this.proposalData.Customer == 0)
      {
        proposal = {
          StatusId: this.proposalData.Status,
          UserId: this.proposalData.User,
          CustomerId: 0,
          LeadId: 0,
          Subject: $("#proposalSubject").val(),
          CreatedDate:  $("#CratedDate").val(),
          ExpiredDate: $("#LastDate").val(),
          DiscountTypeId: this.proposalData.Discount,
          CurrencyTypeId: this.proposalData.Currency,
          City: $("#proposalCity").val(),
          Town: $("#proposalTown").val(),
          PostCode: $("#proposalPost").val(),
          Address :$("#proposalAddress").val(),
          Phone: $("#proposalPhone").val(),
          Amount: x.innerText,
          ProjectId: 0,
          AdminNote : $("#AdminNote").val(),
          Discount : $("#totaldiscount").val()
        };
      }
      else if(this.proposalData.Lead == 0 && this.proposalData.Customer != 0)
      {
        proposal = {
          StatusId: this.proposalData.Status,
          UserId: this.proposalData.User,
          CustomerId: this.proposalData.Customer,
          LeadId: 0,
          Subject: $("#proposalSubject").val(),
          CreatedDate:  $("#CratedDate").val(),
          ExpiredDate: $("#LastDate").val(),
          DiscountTypeId: this.proposalData.Discount,
          CurrencyTypeId: this.proposalData.Currency,
          City: $("#proposalCity").val(),
          Town: $("#proposalTown").val(),
          PostCode: $("#proposalPost").val(),
          Address :$("#proposalAddress").val(),
          Phone: $("#proposalPhone").val(),
          Amount: x.innerText,
          ProjectId: 0,
          AdminNote : $("#AdminNote").val(),
          Discount : $("#totaldiscount").val()
        };
      }
      else if(this.proposalData.Lead != 0 && this.proposalData.Customer == 0)
      {
        proposal = {
          StatusId: this.proposalData.Status,
          UserId: this.proposalData.User,
          CustomerId: 0,
          LeadId: this.proposalData.Lead,
          Subject: $("#proposalSubject").val(),
          CreatedDate:  $("#CratedDate").val(),
          ExpiredDate: $("#LastDate").val(),
          DiscountTypeId: this.proposalData.Discount,
          CurrencyTypeId: this.proposalData.Currency,
          City: $("#proposalCity").val(),
          Town: $("#proposalTown").val(),
          PostCode: $("#proposalPost").val(),
          Address :$("#proposalAddress").val(),
          Phone: $("#proposalPhone").val(),
          Amount: x.innerText,
          ProjectId: 0,
          AdminNote : $("#AdminNote").val(),
          Discount : $("#totaldiscount").val()
        };
      }
      if(proposal.StatusId == 0 )
      {
        proposal.StatusId = 1
      }

      if(proposal.Amount == "0.00 " + this.currency || proposal.Amount == "0 " + this.currency )
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ErrorNoProduct);
        return;
      }
      else if(proposal.Subject == "" || proposal.ExpiredDate == "" || proposal.CreatedDate == "" || proposal.UserId == 0 || proposal.StatusId == 0 || proposal.CurrencyTypeId == 0 || proposal.DiscountTypeId == 0)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposal", proposal).subscribe(
        (result) => {
          var proposalId = result[0].Result;
          proposal.ProposalId = proposalId;
          let proposaldetail;

          for(let i = 0; i < this.addedDynamicLineData.length; i++)
          {
            let _product = 0;
            
            for(let j in this.productList)
            {
              if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
              {
                _product = this.productList[j].value;
              }
            }

            if(_product == 0)
              proposaldetail = { ProposalId: proposalId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount };
            else
              proposaldetail = { ProposalId: proposalId, LineId: Number(i)+1 ,ProductId: _product, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount:this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].LineTax, LineName: "", LineDiscount : this.addedDynamicLineData[i].LineDiscount  };   
            
              this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalDetail",proposaldetail).subscribe(
              (resultt) =>{
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if (resulttArray[0].Result == "Session_Expired") {
                this.loading = false;
                this._router.navigate(['/auth']);
                return;
              }
              else
              {
                this.loading = false;
                this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrSavedProposal);
                this.addedDynamicLineData = [];
                this._router.navigate(['/pages/proposals']);
              }
            }
            )
          }
          
          if(proposal.CustomerId != 0)
          {
            let proposalcontact;

            for(let j in this.selectedCustomerContacts)
            {
              proposalcontact = { ProposalId: proposalId, ContactId: this.selectedCustomerContacts[j]};   
            
              this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalContact", proposalcontact).subscribe(
              (result) =>{
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray[0].Result == "Session_Expired") {
                  this.loading = false;
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
              
              if(this.sendMail == true)
              {
                this._router.navigate([]).then(result => {  window.open('/proposal/pdf' + "/" + proposalId, '_blank'); });
                setTimeout(()=>{
                  proposal.Subject = this.contactList[this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])].email;
                  this._apicallservice.Update("http://localhost:61232/api/Proposal/SendProposal", this.proposalData.Language, proposal).subscribe(
                    (result) => {
                      let resultArray = JSON.parse(JSON.stringify(result));
                      if (resultArray.length != 0 && resultArray[0].Result) {
                        if (resultArray[0].Result == "Session_Expired") {
                          this.loading = false;
                          this._router.navigate(['/auth']);
                          return;
                        }
                      }
                    }
                  )
                },10000);
                
              }
            }
          }
          else
          {    
            if(this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid != 0)
            {
              let proposalcontact = { ProposalId: proposalId, ContactId: this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid };   

              this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalContact", proposalcontact).subscribe(
              (result) =>{
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray[0].Result == "Session_Expired") {
                  this.loading = false;
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )

              if(this.sendMail == true)
              {
                proposal.Subject = this.contactList[this.contactList.findIndex(x => x.value == this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid)].email;
                this._apicallservice.Update("http://localhost:61232/api/Proposal/SendProposal", this.proposalData.Language, proposal).subscribe(
                  (result) => {
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray.length != 0 && resultArray[0].Result) {
                      if (resultArray[0].Result == "Session_Expired") {
                        this.loading = false;
                        this._router.navigate(['/auth']);
                        return;
                      }
                    }
                  }
                )
              }
            }
          }      

          }
        )
      }
    }
    else
    {
      var x = document.getElementById("total");
      if(x.innerHTML == "0 " + this.currency )
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.ErrorNoProduct);
        return;
      }
      else if($("#proposalSubject").val() == "" || $("#CratedDate").val() == "" || $("#LastDate").val() == "" || this.proposalData.User == 0 || this.proposalData.Status == 0 || this.proposalData.Currency == 0 || this.proposalData.Discount == 0)
      {
        this.loading = false;
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        return;
      }
      else
      {
        let proposal;

        if(this.proposalData.Lead == 0 && this.proposalData.Customer == 0)
        {
          proposal = {
            ProposalId: this.Proposalid,
            StatusId: this.proposalData.Status,
            UserId: this.proposalData.User,
            CustomerId: 0,
            LeadId: 0,
            Subject: $("#proposalSubject").val(),
            CreatedDate:  $("#CratedDate").val(),
            ExpiredDate: $("#LastDate").val(),
            DiscountTypeId: this.proposalData.Discount,
            CurrencyTypeId: this.proposalData.Currency,
            City: $("#proposalCity").val(),
            Town: $("#proposalTown").val(),
            PostCode: $("#proposalPost").val(),
            Address :$("#proposalAddress").val(),
            Phone: $("#proposalPhone").val(),
            Amount: x.innerText,
            ProjectId: 0,
            AdminNote : $("#AdminNote").val(),
            Discount : $("#totaldiscount").val()
          };
        }
        else if(this.proposalData.Lead == 0 && this.proposalData.Customer != 0)
        {
          proposal = {
            ProposalId: this.Proposalid,
            StatusId: this.proposalData.Status,
            UserId: this.proposalData.User,
            CustomerId: this.proposalData.Customer,
            LeadId: 0,
            Subject: $("#proposalSubject").val(),
            CreatedDate:  $("#CratedDate").val(),
            ExpiredDate: $("#LastDate").val(),
            DiscountTypeId: this.proposalData.Discount,
            CurrencyTypeId: this.proposalData.Currency,
            City: $("#proposalCity").val(),
            Town: $("#proposalTown").val(),
            PostCode: $("#proposalPost").val(),
            Address :$("#proposalAddress").val(),
            Phone: $("#proposalPhone").val(),
            Amount: x.innerText,
            ProjectId: 0,
            AdminNote : $("#AdminNote").val(),
            Discount : $("#totaldiscount").val()
          };
        }
        else if(this.proposalData.Lead != 0 && this.proposalData.Customer == 0)
        {
          proposal = {
            ProposalId: this.Proposalid,
            StatusId: this.proposalData.Status,
            UserId: this.proposalData.User,
            CustomerId: 0,
            LeadId: this.proposalData.Lead,
            Subject: $("#proposalSubject").val(),
            CreatedDate:  $("#CratedDate").val(),
            ExpiredDate: $("#LastDate").val(),
            DiscountTypeId: this.proposalData.Discount,
            CurrencyTypeId: this.proposalData.Currency,
            City: $("#proposalCity").val(),
            Town: $("#proposalTown").val(),
            PostCode: $("#proposalPost").val(),
            Address :$("#proposalAddress").val(),
            Phone: $("#proposalPhone").val(),
            Amount: x.innerText,
            ProjectId: 0,
            AdminNote : $("#AdminNote").val(),
            Discount : $("#totaldiscount").val()
          };
        }
        this._apicallservice.Add("http://localhost:61232/api/Proposal/EditProposal", proposal).subscribe(
          (result) => {
              let total = 0;
              let lineCount = 0;

              let resultArray = JSON.parse(JSON.stringify(result));

              if(this.lineData != [])
              {
                for(let i in this.lineData)
                {
                  if(this.lineData[i].LineAmount != "0")
                  {
                    total += Number(this.lineData[i].LineAmount.split(" ")[0]);
                  }
                }
              }

              if(this.addedDynamicLineData != [])
              {
                for(let j in this.addedDynamicLineData)
                {
                  if(this.addedDynamicLineData[j].LineAmount != "0")
                  {
                    total += Number(this.addedDynamicLineData[j].LineAmount.split(" ")[0]);
                  }
                }
              }

              if(total != Number($("#total").val().toString().split(" ")[0]))
              {
                  if(this.lineData.length != 0)
                  {
                    for(let i = 0; i < this.lineData.length; i++)
                    {
                      if(this.lineData[i].LineAmount != "0")
                      {
                        lineCount++;   
                        let _line = { Id: this.lineData[i].Id, LineId: lineCount };                               
                        this._apicallservice.Update("http://localhost:61232/api/Proposal/EditProposalLines", Number(this.lineData[i].Id), _line).subscribe(
                          (resultt) =>{
                          }
                        )              
                      }                
                    }
                  }
                  if(this.addedDynamicLineData != [])
                  {
                    let _product = 0;
                    for(let i in this.addedDynamicLineData)
                    {
                      for(let j in this.productList)
                      {
                        if(this.productList[j].label == this.addedDynamicLineData[i].ProductName && this.productList[j].price == this.addedDynamicLineData[i].ProductPrice)
                        {
                          _product = this.productList[j].value;
                        }
                      }             
                      lineCount++;
                      let _proposaldetail;

                      if(_product == 0)
                        _proposaldetail = { ProposalId: this.Proposalid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].updatedefaultTax, ProductId: _product, LineId: lineCount, LineName: this.addedDynamicLineData[i].ProductName , LineDiscount : this.addedDynamicLineData[i].LineDiscount};
                      else
                        _proposaldetail = { ProposalId: this.Proposalid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].updatedefaultTax, ProductId: _product, LineId: lineCount, LineName: "" , LineDiscount : this.addedDynamicLineData[i].LineDiscount};

                      //let _invdetail = { ProposalId: this.Proposalid, Description: this.addedDynamicLineData[i].LineDesc, LineAmount: this.addedDynamicLineData[i].LineAmount, ProductCount: this.addedDynamicLineData[i].ProductCount, LineTax: this.addedDynamicLineData[i].updatedefaultTax, ProductId: _product, LineId: lineCount};
                      this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalDetail", _proposaldetail).subscribe(
                        (resultt) =>{
                        }
                      )
                    }
                  }  
                
              }

              if(proposal.CustomerId != 0)
              {
                //---------- eğer eski seçilmiş kontak varsa onları sil
                for(let i in this.copyselectedCustomerContacts)
                {
                  if(this.selectedCustomerContacts.findIndex(x => x.value == this.copyselectedCustomerContacts[i]) == -1)
                  {
                    let proposalcontact = { ProposalId: this.Proposalid, ContactId: this.copyselectedCustomerContacts[i] }; 
                    this._apicallservice.Add("http://localhost:61232/api/Proposal/RemoveProposalContact", proposalcontact).subscribe(
                      (result) =>{
                      }
                    )
                  }
                }

                //----------- yeni eklenen varsada onları ekle
                for(let j in this.selectedCustomerContacts)
                {
                  if(this.copyselectedCustomerContacts.findIndex(x => x.value == this.selectedCustomerContacts[j]) == -1)
                  {
                    let proposalcontact = { ProposalId: this.Proposalid, ContactId: this.selectedCustomerContacts[j] };   
                    this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalContact", proposalcontact).subscribe(
                      (result) =>{
                      }
                    )
                  }

                  if(this.sendMail == true)
                  {
                    let index = this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])
                    proposal.Subject = this.contactList[index].email;
                    this._apicallservice.Update("http://localhost:61232/api/Proposal/SendProposal", this.proposalData.Language, proposal).subscribe(
                      (result) => {
                        let resultArray = JSON.parse(JSON.stringify(result));
                        if (resultArray.length != 0 && resultArray[0].Result) {
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                      }
                    )
                  }
                } 
              }
              else
              {
                if(this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid != 0) //yeni leadin kontağı varsa
                {
                  let flag = 0;
      
                  //---------- eğer eski seçilmiş kontak varsa onları sil
                  for(let i in this.copyselectedCustomerContacts)
                  {
                    if(this.copyselectedCustomerContacts[i] == this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid)
                    {
                      flag++;
                    }
                    else
                    {
                      let proposalcontact = { ProposalId: this.Proposalid, ContactId: this.copyselectedCustomerContacts[i] }; 
                      this._apicallservice.Add("http://localhost:61232/api/Proposal/RemoveProposalContact", proposalcontact).subscribe(
                        (result) =>{
                        }
                      )
                    }
                  }
    
                  if(flag == 0)
                  {
                    let proposalcontact = { ProposalId: this.Proposalid, ContactId: this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid };   
                    this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalContact", proposalcontact).subscribe(
                      (result) =>{
                      }
                    )
                  }
    
                  if(this.sendMail == true)
                  {
                    proposal.Subject = this.contactList[this.contactList.findIndex(x => x.value == this.leadList[this.leadList.findIndex(x => x.value == proposal.LeadId)].contactid)].email;
                    this._apicallservice.Update("http://localhost:61232/api/Proposal/SendProposal", this.proposalData.Language, proposal).subscribe(
                      (result) => {
                        let resultArray = JSON.parse(JSON.stringify(result));
                        if (resultArray.length != 0 && resultArray[0].Result) {
                          if (resultArray[0].Result == "Session_Expired") {
                            this.loading = false;
                            this._router.navigate(['/auth']);
                            return;
                          }
                        }
                      }
                    ) 
                  }
                }
                else // yeni leadin kontağı yoksa eskileri sil
                {
                  for(let i in this.copyselectedCustomerContacts)
                  {
                    let proposalcontact = { ProposalId: this.Proposalid, ContactId: this.copyselectedCustomerContacts[i] }; 
                    this._apicallservice.Add("http://localhost:61232/api/Proposal/RemoveProposalContact", proposalcontact).subscribe(
                      (result) =>{
                      }
                    )
                  }

                }
              }  
              
              this.loading = false;
              this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrEditedProposal);
              this.addedDynamicLineData = [];
              this._router.navigate(['/pages/proposals']);
            }
        )
      } 
    }
  }

  saveandSent()
  {
    this.sendMail = true;
    this.saveProposal();
  }

  getProductDetail(event)
  {
    if(!this.Proposalid)
    {
        this.lineData = [];
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
            let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "" , LineDiscount : "" };
            this.lineData.push(_lineData);
          }
        }
    }
    else
    {
        this.lineData.shift();
        for(let i=0; i<this.productList.length; i++)
        {
          if(event == this.productList[i].value)
          {
           let _lineData = { Id: '', ProductName: this.productList[i].label, LineDesc: this.productList[i].desc, ProductCount: 1, ProductPrice: this.productList[i].price, LineAmount: "0", updatedefaultTax: "", LineDiscount : "" };
            this.lineData.unshift(_lineData);
          }
        }
    }
  }

  //---------- Line Functions ------------------------
  updatedTotal()
  {
    var total = 0;
    var z =document.getElementById("total");
    for(var i = 1; i < this.lineData.length; i++)
    {
      if(this.lineData[i].LineAmount != "0")
      {
        var sub=parseFloat(this.lineData[i].LineAmount);
        total=(total+sub);
        z.innerHTML=Number(total).toLocaleString()+" "+this.currency;
        this._total = total;
        if(this.lineData[i].LineDiscount)
        {
            this._totalDiscount += Number(this.lineData[i].LineDiscount);
        }
        
        if(this.lineData[i].updatedefaultTax == "%18 KDV")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.18, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 17.65/100, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: total * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15);
          }
          else
          {
            this.topTaxArray.push({ value: (total + Number(this.lineData[i].LineDiscount)) * 0.15, amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice});
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);   
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 0.18);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 0.18);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
        else if(this.lineData[i].updatedefaultTax == "%18 KDV,%17.65 Stopaj,%15 Stopaj")
        {
          if(this.lineData[i].LineDiscount == '0')
          {
            this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
          }
          else
          {
            this.topTaxArray.push({ value: ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100), amount: this.lineData[i].LineAmount, count: this.lineData[i].ProductCount, price: this.lineData[i].ProductPrice });
            this.topTax += ((total + Number(this.lineData[i].LineDiscount)) * 0.18) + ((total + Number(this.lineData[i].LineDiscount)) * 0.15) + ((total + Number(this.lineData[i].LineDiscount)) * 17.65/100);
          }
        }
      }
    }

    var discount = this.proposalData.TotalDiscount;
    if(discount)
    {
      this._total = this._total - parseFloat(discount);
    }

    z.innerHTML=Number(this._total).toLocaleString()+ " " + this.currency;
  }

  total(id)
  {
    var y = document.getElementById("f"+id).innerHTML.split(" ")[0];
    var x =document.getElementById("total");
    this._total = parseFloat(y) + this._total;
    x.innerText=Number(this._total).toLocaleString()+ " " + this.currency;
    
  }

  createLine()
  {

    var line = <HTMLCollection> document.getElementsByClassName("line");
    var newid= line[0].childElementCount+1;
    var a1 =$("#a1").val();
    var b1 =$("#b1").val();
    var c1 =$("#c1").val().toString();
    var d1 =$("#d1").val().toString();
    var dis1=$("#dis1").val().toString();
    if(dis1 == "")
    {
      var total = (parseFloat(c1)*parseFloat(d1));
      dis1 = '0'; 
    }
    else
    {
      this._totalDiscount += parseFloat(dis1);
      var total = (parseFloat(c1)*parseFloat(d1))-parseFloat(dis1);
    }

    if(!this.tax || this.tax.length == 0)
    {
      this.TaxDesc="Vergi seçimi yapılmadı.";
    }
    else if(this.tax.length == 1)
    {
      if(this.tax[0] == 1)
      {
        this.TaxDesc="%18 KDV";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18), amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.18, amount: total + (total * 0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18);
        }

        total=total+(total*0.18);
      }
      else if(this.tax[0] == 2)
      {
        this.TaxDesc="%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 17.65/100, amount: total+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total*17.65/100);
      }
      else if(this.tax[0] == 3)
      {
        this.TaxDesc="%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: total * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += (total * 0.15);
        }
        else
        {
          this.topTaxArray.push({ value: (total + Number(dis1)) * 0.15, amount: total+(total*0.15), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15);
        }

        total=total+(total*0.15);
      }
    }
    else if(this.tax.length == 2)
    {
      if((this.tax[0] == 1 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 1))
      {
        this.TaxDesc="%18 KDV,%17.65 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.18)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 17.65/100);   
        }

        total=total+(total*0.18)+(total*17.65/100);
      }
      else if((this.tax[0] == 1 && this.tax[1] == 3) || (this.tax[0] == 3 && this.tax[1] == 1))
      {
        this.TaxDesc="%18 KDV,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 0.18);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18), amount: total+(total*0.15)+(total*0.18), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 0.18);
        }
        
        total=total+(total*0.15)+(total*0.18);
      }
      else if((this.tax[0] == 3 && this.tax[1] == 2) || (this.tax[0] == 2 && this.tax[1] == 3))
      {
        this.TaxDesc="%17.65 Stopaj,%15 Stopaj";

        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.15) + (total * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total*0.15)+(total*17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total*0.15)+(total*17.65/100);
      }
    }
    else if(this.tax.length == 3)
    {
        this.TaxDesc="%18 KDV,%17.65 Stopaj,%15 Stopaj";
        
        if(dis1 == '0')
        {
          this.topTaxArray.push({ value: (total * 0.18) + (total * 0.15) + (total * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += (total * 0.18) + (total * 0.15) + (total * 17.65/100);
        }
        else
        {
          this.topTaxArray.push({ value: ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100), amount: total+(total * 0.18) + (total * 0.15) + (total * 17.65/100), count: c1, price: d1});
          this.topTax += ((total + Number(dis1)) * 0.18) + ((total + Number(dis1)) * 0.15) + ((total + Number(dis1)) * 17.65/100);
        }

        total=total+(total * 0.18) + (total * 0.15) + (total * 17.65/100);
    }

    if (total.toString()=="NaN")
    {
      total=0;
    }
    var subLine = "<tr id='"+ newid+ "'><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='a"+newid+"' >"+a1+"</p></td><td><p style='float: left;margin-top: 26px;margin-left:5px;' id='b"+newid+"' >"+b1+"</p></td><td><p id='c"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;''>" + c1 + "</p></td><td><p id='d"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + d1 + "</p></td><td><p id='dis"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>" + dis1 + "</p></td><td><p id='updatedefaultTax' style='float: left;margin-top: 26px;margin-left:5px;' >"+this.TaxDesc+"</p></td><td><p id='f"+newid+"' style='float: left;margin-top: 26px;margin-left:5px;'>"+total+ " "+this.currency+"</p></td><td><button id='_" + newid+"' class='btn' (click)='deleteLine($event)' style='margin-top: 15px;background-color:red ;' >x</button></td></tr>";
    $('.line').append(subLine);
    var deletee = <HTMLInputElement> document.getElementById("_"+newid);
    let mythis = this;

    this.total(newid);

    deletee.addEventListener("click", ( event: Event) => {

      let element = event.currentTarget as HTMLInputElement;

      let nameValue = element.parentElement.parentElement.children[0].children[0].innerHTML;
      let priceValue = element.parentElement.parentElement.children[6].children[0].innerHTML;
      let priceforTaxValue = element.parentElement.parentElement.children[3].children[0].innerHTML;
      let discounttext = element.parentElement.parentElement.children[4].children[0].innerHTML;
      let counttext = element.parentElement.parentElement.children[2].children[0].innerHTML;
      
      mythis.addedDynamicLineData.splice(this.addedDynamicLineData.findIndex(x => x.ProductName == nameValue && x.LineAmount == priceValue), 1);

      var id = element.id.split("_")[1];
      var minus = $("#f"+id).text().split(" ")[0];
      document.getElementById(id).remove();
      var a = document.getElementById("total");
      this._total = this._total - parseFloat(minus);
      a.innerText = Number(this._total).toLocaleString() + " " + this.currency; 

      for(let i in this.topTaxArray)
      {
        if(this.topTaxArray[i].amount == priceValue.split(" ")[0] && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
        {
          this.topTax -= this.topTaxArray[i].value;
          if(this.topTax < 0)
          {
            this.topTax = 0;
          }
          break;
        }
      }

      this._totalDiscount -= parseFloat(discounttext);

    });

    let _lineData = { ProductName: a1.toString(), LineDesc: b1.toString(), ProductCount: Number(c1), ProductPrice: Number(d1), LineAmount: total + " " + this.currency, updatedefaultTax: "", LineTax: this.TaxDesc , LineDiscount : dis1 };
    this.addedDynamicLineData.push(_lineData);
    
  }

  deleteeditLine(event)
  {
    var id = event.currentTarget.id.split("_")[1];
    let priceforTaxValue = event.currentTarget.parentElement.parentElement.children[3].children[0].innerText;
    let priceValue = event.currentTarget.parentElement.parentElement.children[6].children[0].innerText;
    let discounttext = event.currentTarget.parentElement.parentElement.children[4].children[0].innerText;
    let counttext = event.currentTarget.parentElement.parentElement.children[2].children[0].innerText;

    for(let i in this.topTaxArray)
    {
      if(this.topTaxArray[i].amount == priceValue && this.topTaxArray[i].price == priceforTaxValue && this.topTaxArray[i].count == counttext)
      {
        this.topTax -= this.topTaxArray[i].value;
        if(this.topTax < 0)
        {
          this.topTax = 0;
        }
        break;
      }
    }

    this._totalDiscount -= parseFloat(discounttext);

    this._apicallservice.Deactive("http://localhost:61232/api/Proposal/RemoveProposalLine", Number(this.lineData[id].Id)).subscribe(
      (result) =>{
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          var x = this.lineData[id].LineAmount.split(" ")[0];
          var total =document.getElementById("total");
          var newTotal = this._total-parseFloat(x);
          this._total = newTotal;
          total.innerHTML=Number(newTotal).toLocaleString()+" " + this.currency;
          document.getElementById("lineData_" + id).remove();
          this.lineData.splice(id, 1);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      }
    )
  }

  totalDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total - parseFloat(y);
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
  }

  cleanDiscount()
  {
    var y = $("#totaldiscount").val().toString();
    var x = document.getElementById("total");
    var discountedTotal = this._total + parseFloat(y);
    this._total = discountedTotal;
    x.innerText=Number(discountedTotal).toLocaleString()+" " + this.currency;
    $("#totaldiscount").val("0");
  }

  changeCurrencyType(event)
  {
    if(event == 1)
    {
      this.currency = "TL";
    }
    else if(event == 2)
    {
      this.currency = "$";
    }
    else if(event == 3)
    {
      this.currency = "€";
    }
    else if(event == 4)
    {
      this.currency = "£";
    }
  }
  //-------------------------------------------------------

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
