import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalNotesComponent } from './proposal-notes.component';

describe('ProposalNotesComponent', () => {
  let component: ProposalNotesComponent;
  let fixture: ComponentFixture<ProposalNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposalNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
