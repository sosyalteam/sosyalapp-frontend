import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proposal-notes',
  templateUrl: './proposal-notes.component.html',
  styleUrls: ['./proposal-notes.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class ProposalNotesComponent implements OnInit {

  sysLanguages: any;
  proposalId : any;

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.PcProposalNotes + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.proposalId = this._avRoute.snapshot.params["id"];
    }

    this.tabs[0].title = this.sysLanguages.PcProposalInformation;
    this.tabs[1].title = this.sysLanguages.BoardsNoteText;

    this.getComments();
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/proposals/proposal-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'ion-ios-copy-outline',
      route: ['/pages/proposals/proposal-notes', this._avRoute.snapshot.params["id"]],
    },
  ];


  ngOnInit() {
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Proposal/ProposalComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : this.proposalId,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Proposal/CreateProposalComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/proposals/add-edit-proposal', this._avRoute.snapshot.params["id"]]);
  }

}
