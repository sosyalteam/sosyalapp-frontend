import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proposal-overview',
  templateUrl: './proposal-overview.component.html',
  styleUrls: ['./proposal-overview.component.scss'],
  providers: [ApicallService, NbToastrService]
})
export class ProposalOverviewComponent implements OnInit {

  sysLanguages: any;
  proposalArray = { ProposalId: 0, Subject:'', Phone:'', Customer:'', LeadName: '', Address:'', City:'', Town:'', Status:'', User:'', Project:'', CreatedDate:'', ExpiredDate: '', Amount : '', CopyAmount: '', LeadId: 0, CustomerId: 0, StatusId: 0, ProjectId: 0, DiscountTypeId: 0, CurrencyTypeId: 0, UserId: 0 , Discount : "" };
  proformList = [];
  invoiceList = [];
  productList = [];

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  emptyComment = false;
  CommentList = [];
  loading = false;

  //pagination 
  p: number = 1;
  collection = [];

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.PcProposalOverview + " - Crmplus");

    this.tabs[0].title = this.sysLanguages.PcProposalInformation;

    this.settings.columns.ProductName.title = this.sysLanguages.AddInvoiceTableProduct;
    this.settings.columns.Description.title = this.sysLanguages.AddInvoiceTableDesc;
    this.settings.columns.LineAmount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settings.columns.LineTax.title = this.sysLanguages.AddInvoiceTableTax;
    this.settings.columns.LineDiscount.title = this.sysLanguages.AddInvoiceDiscount;

    this.getInvoices();
    this.getProforms();
    this.getProposalProduct();
    this.getComments();

  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/proposals/proposal-overview', this._avRoute.snapshot.params["id"]],
    },
  ];

  settings = {
    columns: {
      ProductName: {
        title: ''
      },
      Description: {
        title: ''
      },
      LineAmount: {
        title: ''
      },
      LineTax: {
        title: ''
      },
      LineDiscount: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {

    this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposal",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {

            }
            else if (resultArray.length != 0 && resultArray[0].Result) {
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.proposalArray.Address = resultArray[0].Address;
            this.proposalArray.Amount =  resultArray[0].Amount;
            this.proposalArray.CopyAmount = resultArray[0].Amount;
            this.proposalArray.City = resultArray[0].City;
            this.proposalArray.CreatedDate = resultArray[0].CreatedDate.toString().split(" ")[0];
            this.proposalArray.Customer = resultArray[0].CustomerName;
            this.proposalArray.LeadName = resultArray[0].LeadName;
            this.proposalArray.ExpiredDate = resultArray[0].ExpiredDate.toString().split(" ")[0];
            this.proposalArray.Phone = resultArray[0].Phone;
            this.proposalArray.Project = resultArray[0].BoardName;
            this.proposalArray.Subject = resultArray[0].Subject;
            this.proposalArray.Town = resultArray[0].Town;
            this.proposalArray.User = resultArray[0].Fullname;
            this.proposalArray.LeadId = resultArray[0].LeadId; 
            this.proposalArray.CustomerId = resultArray[0].CustomerId;
            this.proposalArray.StatusId = resultArray[0].StatusId;
            this.proposalArray.ProjectId = resultArray[0].ProjectId;
            this.proposalArray.DiscountTypeId = resultArray[0].DiscountTypeId;
            this.proposalArray.CurrencyTypeId = resultArray[0].CurrencyTypeId;
            this.proposalArray.ProposalId = resultArray[0].ProposalId;
            this.proposalArray.UserId = resultArray[0].UserId;
            this.proposalArray.Discount = resultArray[0].Discount;

            if(resultArray[0].StatusId == 1)
            {
              this.proposalArray.Status = this.sysLanguages.PcDraftStatus;
            }
            else if(resultArray[0].StatusId == 2)
            {
              this.proposalArray.Status = this.sysLanguages.PcSentStatus;
            }
            else if(resultArray[0].StatusId == 3)
            {
              this.proposalArray.Status = this.sysLanguages.PcOpenStatus;
            }
            else if(resultArray[0].StatusId == 4)
            {
              this.proposalArray.Status = this.sysLanguages.PcRevizeStatus;
            }
            else if(resultArray[0].StatusId == 5)
            {
              this.proposalArray.Status = this.sysLanguages.PcRefusedStatus;
            }
            else if(resultArray[0].StatusId == 6)
            {
              this.proposalArray.Status = this.sysLanguages.PcAcceptedStatus;
            }
            else if(resultArray[0].StatusId == 7)
            {
              this.proposalArray.Status = this.sysLanguages.PcConvertedStatus;
            }

            if(this.proposalArray.Phone == "")
            {
              this.proposalArray.Phone = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Address == "")
            {
              this.proposalArray.Address = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.City == "")
            {
              this.proposalArray.City = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Subject == "")
            {
              this.proposalArray.Subject = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Customer == "NoCustomer")
            {
              this.proposalArray.Customer = this.sysLanguages.NoCustomerText;
            }
            if(this.proposalArray.LeadName == "")
            {
              this.proposalArray.LeadName = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Town == "")
            {
              this.proposalArray.Town = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Project == "NoBoard")
            {
              this.proposalArray.Project = this.sysLanguages.NotValueText;
            }
            if(this.proposalArray.Amount == "")
            {
              this.proposalArray.Amount = this.sysLanguages.NotValueText;
            }

          }       
      },

      )

  }

  openUrl(link)
  {
    if(link == "/proposal")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else if(link == "/proposal/pdf")
    {
      this._router.navigate([]).then(result => {  window.open(link + "/" + this._avRoute.snapshot.params["id"], '_blank'); });
    }
    else
    {
      this._router.navigate([link, this._avRoute.snapshot.params["id"]]);
    }
  }

  getInvoices()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.invoiceList = [];
          }
        }
        else 
        {
          this.invoiceList = resultArray;
        }
      }
    )
  }

  getProforms()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Proform/GetAllProforms").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.proformList = [];
          }
        }
        else 
        {
          this.proformList = resultArray;
        }
      }
    )
  }

  getProposalProduct()
  {
    this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposalLines",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              if(resultArray[i].ProductId == 0)
              {
                resultArray[i].ProductName = resultArray[i].LineName;
              }
            }

            this.productList = resultArray.slice();
          }
      }
    )
  }

  convertProposal(event)
  {
    let indexprof = this.proformList.findIndex(x => x.ProposalId == this.proposalArray.ProposalId);
    let indexinv = this.invoiceList.findIndex(x => x.ProposalId == this.proposalArray.ProposalId);
    if(indexprof != -1 || indexinv != -1)
    {
      this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.PcToastrCantConvert);
    }
    else
    {
      if(event.currentTarget.value == 0)
      {
        if(this.proposalArray.LeadId != 0 && this.proposalArray.CustomerId == 0)
        {
          let _lead = { LeadId: this.proposalArray.LeadId, StatusId: 6 };
          this._apicallService.Update("http://localhost:61232/api/Lead/ChangeLeadStatus", this.proposalArray.LeadId, _lead)
          .subscribe((result) => {
            if (result[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else
            {
              this._apicallService.Update("http://localhost:61232/api/Proposal/ConvertProposal", this.proposalArray.ProposalId, "notImportantWhatYouSent").subscribe(
                (result) => {
                }
              )
              
              let _data = { LeadId: this.proposalArray.LeadId };
              this._apicallService.Update("http://localhost:61232/api/Proposal/ChangeProposalRelation", this.proposalArray.ProposalId, _data).subscribe(
                (result) => {
                  if (result[0].Result == "Session_Expired") 
                  {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else
                  {
                    let proform = {
                      ProformId: 0,
                      StatusId: 1,
                      SalesmanId: this.proposalArray.UserId,
                      CustomerId: result[0].Result,
                      ProjectId: this.proposalArray.ProjectId,
                      ProposalId: this.proposalArray.ProposalId,
                      ProformNumber: this.proposalArray.Subject,
                      CreatedDate:  this.proposalArray.CreatedDate + " 12:00:00",
                      ExpirementDate: this.proposalArray.ExpiredDate + " 12:00:00",
                      DiscountTypeId: this.proposalArray.DiscountTypeId,
                      CurrencyTypeId: this.proposalArray.CurrencyTypeId,
                      AdminNote:"--Proposal--",
                      CustomerNote: "--Lead--",
                      Conditions: this.proposalArray.LeadId.toString(),
                      Amount: this.proposalArray.CopyAmount,
                      Discount : this.proposalArray.Discount
                    };
              
                    this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform)
                    .subscribe((result) => {
                      if (result[0].Result == "Session_Expired") {
                        this._router.navigate(['/auth']);
                        return;
                      }
                      else
                      {
                        var proformId = (result[1].Value);
                        this._router.navigate(['/pages/proforms/add-edit-proform',proformId]);
                      }
                    }
                    )
                  }
                }
              )
            }
          }
          )
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrConvertedProposal);
        }
        else
        {
          this._apicallService.Update("http://localhost:61232/api/Proposal/ConvertProposal", this.proposalArray.ProposalId, "notImportantWhatYouSent").subscribe(
            (result) => {
            }
          )
    
          let proform = {
            ProformId: 0,
            StatusId: 1,
            SalesmanId: this.proposalArray.UserId,
            CustomerId: this.proposalArray.CustomerId,
            ProjectId: this.proposalArray.ProjectId,
            ProposalId: this.proposalArray.ProposalId,
            ProformNumber: this.proposalArray.Subject,
            CreatedDate:  this.proposalArray.CreatedDate + " 12:00:00",
            ExpirementDate: this.proposalArray.ExpiredDate + " 12:00:00",
            DiscountTypeId: this.proposalArray.DiscountTypeId,
            CurrencyTypeId: this.proposalArray.CurrencyTypeId,
            AdminNote:"--Proposal--",
            CustomerNote: "",
            Conditions: "",
            Amount: this.proposalArray.CopyAmount,
            Discount : this.proposalArray.Discount
          };
    
          this._apicallService.Add("http://localhost:61232/api/Proform/CreateProform", proform)
          .subscribe((result) => {
            if (result[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else
            {
              var proformId = (result[1].Value);
              this._router.navigate(['/pages/proforms/add-edit-proform',proformId]);
            }
          }
          )
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrConvertedProposal);
        }
      }
      else if(event.currentTarget.value == 1)
      {
        if(this.proposalArray.LeadId != 0 && this.proposalArray.CustomerId == 0)
        {
          let _lead = {LeadId: this.proposalArray.LeadId, StatusId: 6};

          this._apicallService.Update("http://localhost:61232/api/Lead/ChangeLeadStatus", this.proposalArray.LeadId, _lead)
          .subscribe((result) => {
            if (result[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else
            {
              this._apicallService.Update("http://localhost:61232/api/Proposal/ConvertProposal", this.proposalArray.ProposalId, "notImportantWhatYouSent").subscribe(
                (result) => {
                }
              )

              let _data = { LeadId: this.proposalArray.LeadId };
              this._apicallService.Update("http://localhost:61232/api/Proposal/ChangeProposalRelation", this.proposalArray.ProposalId, _data).subscribe(
                (result) => {
                  if (result[0].Result == "Session_Expired") 
                  {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else
                  {
                    let invoice = {
                      InvoiceId: 0,
                      CustomerId: result[0].Result,
                      ProjectId: this.proposalArray.ProjectId,
                      InvoiceNumber: this.proposalArray.Subject,
                      CreatedDate: this.proposalArray.CreatedDate + " 12:00:00",
                      LastPaymentDate: this.proposalArray.ExpiredDate + " 12:00:00",
                      PayingMethodId: 1,
                      ProposalId: this.proposalArray.ProposalId,
                      ProformId: 0,
                      SalesmanId: this.proposalArray.UserId,
                      DiscountTypeId: this.proposalArray.DiscountTypeId,
                      CurrencyTypeId: this.proposalArray.CurrencyTypeId,
                      AdminNote: "--Proposal--",
                      CustomerNote: "--Lead--",
                      Conditions: this.proposalArray.LeadId.toString(),
                      StatusId: 5,
                      Amount : this.proposalArray.CopyAmount,
                      Discount : this.proposalArray.Discount
                    };
                    
                    this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice)
                    .subscribe((result) => {
                      if (result[0].Result == "Session_Expired") {
                        this._router.navigate(['/auth']);
                        return;
                      }
                      else
                      {
                        var invoiceId = (result[1].Value);
                        this._router.navigate(['/pages/invoices/add-edit-invoices',invoiceId]);
                      }
                    }
                    )
                  }
                }
              )
            }
          }
          )
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrConvertedProposal);
        }
        else
        {
          this._apicallService.Update("http://localhost:61232/api/Proposal/ConvertProposal", this.proposalArray.ProposalId, "notImportantWhatYouSent").subscribe(
            (result) => {
            }
          )
    
          let invoice = {
            InvoiceId: 0,
            CustomerId: this.proposalArray.CustomerId,
            ProjectId: this.proposalArray.ProjectId,
            ProformId: 0,
            ProposalId: this.proposalArray.ProposalId,
            InvoiceNumber: this.proposalArray.Subject,
            CreatedDate: this.proposalArray.CreatedDate + " 12:00:00",
            LastPaymentDate: this.proposalArray.ExpiredDate + " 12:00:00",
            PayingMethodId: 1,
            SalesmanId: this.proposalArray.UserId,
            DiscountTypeId: this.proposalArray.DiscountTypeId,
            CurrencyTypeId: this.proposalArray.CurrencyTypeId,
            AdminNote: "--Proposal--",
            CustomerNote: "",
            Conditions: "",
            StatusId: 5,
            Amount : this.proposalArray.CopyAmount,
            Discount : this.proposalArray.Discount
          };
          
          this._apicallService.Add("http://localhost:61232/api/Invoice/CreateInvoice", invoice)
          .subscribe((result) => {
            if (result[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else
            {
              var invoiceId = (result[1].Value);
              this._router.navigate(['/pages/invoices/add-edit-invoices',invoiceId]);
            }
          }
          )
          this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrConvertedProposal);
        }
      }

      $("#convertProposal").val("5");
      this.ngOnInit();
    }
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Proposal/ProposalComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : this._avRoute.snapshot.params["id"],
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
