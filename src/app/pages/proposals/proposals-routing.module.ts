import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProposalsComponent } from './proposals/proposals.component';
import { AddEditProposalsComponent } from './add-edit-proposals/add-edit-proposals.component';
import { ProposalOverviewComponent } from './proposal-overview/proposal-overview.component';
import { ProposalNotesComponent } from './proposal-notes/proposal-notes.component';

const routes: Routes = [
  {
    path: '',
    component: ProposalsComponent,
  },
  {
    path: 'add-edit-proposal',
    component: AddEditProposalsComponent,
  },
  {
    path: 'add-edit-proposal/:id',
    component: AddEditProposalsComponent,
  },
  {
    path: 'proposal-overview/:id',
    component: ProposalOverviewComponent,
  },
  {
    path: 'proposal-notes/:id',
    component: ProposalNotesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProposalsRoutingModule { }
