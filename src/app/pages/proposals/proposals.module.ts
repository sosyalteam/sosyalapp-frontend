import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProposalsRoutingModule } from './proposals-routing.module';
import { ProposalsComponent } from './proposals/proposals.component';
import { AddEditProposalsComponent } from './add-edit-proposals/add-edit-proposals.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbProgressBarModule, NbDialogModule, NbContextMenuModule, NbTabsetModule, NbSelectModule, NbRouteTabsetModule, NbPopoverModule } from '@nebular/theme';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProposalOverviewComponent } from './proposal-overview/proposal-overview.component';
import { ProposalNotesComponent } from './proposal-notes/proposal-notes.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [ProposalsComponent, AddEditProposalsComponent, ProposalOverviewComponent, ProposalNotesComponent],
  imports: [
    CommonModule,
    ProposalsRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule,
    NbTabsetModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NgxPaginationModule,
    NbPopoverModule
  ]
})
export class ProposalsModule { }
