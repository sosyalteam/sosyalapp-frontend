import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { NbToastrService, NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'proposals',
  templateUrl: './proposals.component.html',
  styleUrls: ['./proposals.component.scss'],
  providers:[ApicallService, NbToastrService]
})
export class ProposalsComponent implements OnInit {

  sysLanguages : any;

  proposalList = [];
  convertedProposalList = [];
  proposalStatusList = [];
  filteredProposalList = [];
  isShowingKanban = false;

  isFiltered = false;
  proposalsCount = 0;
  draftProposalsCount = 0;
  sentProposalsCount = 0;
  openedProposalsCount = 0;
  revisedProposalsCount = 0;
  refusedProposalsCount = 0;
  acceptedProposalsCount = 0;
  convertedProposalsCount = 0;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  
  constructor(private _router: Router, private _apicallservice: ApicallService, private toastrService: NbToastrService, private titleService: Title) 
  {
     this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
     this.titleService.setTitle(this.sysLanguages.ProposalsText + " - Crmplus");

     this.settings.columns.Subject.title=this.sysLanguages.PcSubject;
     this.settings.columns.CreatedDate.title=this.sysLanguages.LogTableCreatedDate;
     this.settings.columns.ExpiredDate.title=this.sysLanguages.PcProposalExpirationDate;
     this.settings.columns.StatusDesc.title=this.sysLanguages.BoCardStatusText;
     this.settings.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;

     this.settingsforFilter.columns.Subject.title=this.sysLanguages.PcSubject;
     this.settingsforFilter.columns.CreatedDate.title=this.sysLanguages.LogTableCreatedDate;
     this.settingsforFilter.columns.ExpiredDate.title=this.sysLanguages.PcProposalExpirationDate;
     this.settingsforFilter.columns.StatusDesc.title=this.sysLanguages.BoCardStatusText;
     this.settingsforFilter.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;

     this.getProposals();
     this.getProposalStatusses();
  }

  settings = {
    columns: {
      ProposalId: {
        title: 'ID',
        width: '2%'
      },
      Subject: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      ExpiredDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  settingsforFilter = {
    columns: {
      ProposalId: {
        title: 'ID',
        width: '2%'
      },
      Subject: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      ExpiredDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  }; 

  ngOnInit() {
  }

  filterProposal(value)
  {
    this.filteredProposalList = [];
    if(value == 0)
    {
      this.isFiltered = false;
    }
    else if(value == 1)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 1);
    }
    else if(value == 2)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 2);
    }
    else if(value == 3)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 3);
    }
    else if(value == 4)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 4);
    }
    else if(value == 5)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 5);
    }
    else if(value == 6)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.proposalList.filter(x=>x.StatusId == 6);
    }
    else if(value == 7)
    {
      this.isFiltered = true;
      this.filteredProposalList = this.convertedProposalList;
    }
  }

  openUrl(link, val)
  {
    if(val > 0)
    {
      this._router.navigate([link, val]);
    }
    else
    {
      this._router.navigate([link]);
    }
  }

  getProposals()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proposal/GetAllProposals").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.proposalList = [];
          }
        }
        else 
        {

          for(let i in resulttArray)
          {
            this.proposalsCount++;

            if(resulttArray[i].StatusId == 1)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformDraftStatus;
              this.draftProposalsCount++;
            }
            else if(resulttArray[i].StatusId == 2)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcProformSentStatus;
              this.sentProposalsCount++;
            }
            else if(resulttArray[i].StatusId == 3)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcOpenStatus;
              this.openedProposalsCount++;
            }
            else if(resulttArray[i].StatusId == 4)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcRevizeStatus;
              this.revisedProposalsCount++;
            }
            else if(resulttArray[i].StatusId == 5)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcRefusedStatus; 
              this.refusedProposalsCount++; 
            }
            else if(resulttArray[i].StatusId == 6)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcAcceptedStatus;
              this.acceptedProposalsCount++;
            }
            else if(resulttArray[i].StatusId == 7)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcConvertedStatus;
              this.convertedProposalsCount++;
            }
          }

          this.proposalList = resulttArray.filter(x=>x.StatusId != 7);
          this.convertedProposalList = resulttArray.filter(x=>x.StatusId == 7);   
        }
      }
    )
  }

  getProposalStatusses()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Proposal/GetAllStatus").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.proposalStatusList = [];
          }
        }
        else 
        {
          for(let i in resulttArray)
          {
            if(resulttArray[i].StatusId == 1)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcDraftStatus;
            }
            else if(resulttArray[i].StatusId == 2)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcSentStatus;
            }
            else if(resulttArray[i].StatusId == 3)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcOpenStatus;
            }
            else if(resulttArray[i].StatusId == 4)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcRevizeStatus;
            }
            else if(resulttArray[i].StatusId == 5)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcRefusedStatus;
            }
            else if(resulttArray[i].StatusId == 6)
            {
              resulttArray[i].StatusDesc = this.sysLanguages.PcAcceptedStatus;
            } 
          }

          this.proposalStatusList = resulttArray;
        }
      }
    )
  }

  showKanban()
  {
    this.isShowingKanban = true;
  }

  showList()
  {
    this.isShowingKanban = false;
  }

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropProposal(event)
  {
    var data = event.dataTransfer.getData("text2");

    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addProposalBtn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("prop_desc"))
    {
      //window.alert("You can't drop here");
    }   
    else if(event.target.className == "cardProposalDesc")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "assignedProposalCard")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "amountProposalCard")
    {
      //window.alert("You can't drop here");
    }
    else
    {    
      let _prop = {ProposalId: data.split('_')[1], StatusId: event.target.id.split('_')[1]};

      this._apicallservice.Update("http://localhost:61232/api/Proposal/ChangeProposalStatus", Number(data.split('_')[1]), _prop).subscribe((result) => {
        this.getProposals();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  dragProposal(event)
  {
     event.dataTransfer.setData("text", event.target.parentNode.id);
     event.dataTransfer.setData("text2", event.target.id);
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
