import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSupportComponent } from './add-edit-support.component';

describe('AddEditSupportComponent', () => {
  let component: AddEditSupportComponent;
  let fixture: ComponentFixture<AddEditSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
