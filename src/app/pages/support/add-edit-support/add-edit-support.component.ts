import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from "jquery";
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-edit-support',
  templateUrl: './add-edit-support.component.html',
  styleUrls: ['./add-edit-support.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class AddEditSupportComponent implements OnInit {

  public sysLanguages: any;
  supportId:any;
  userList: any;
  customerList :any;
  groupList:any;
  priorityList:any;
  statusList:any;
  name : any;
  email:any;
  supportArray = [{Subject : "", CustomerId : 0 , GroupId: 0, ResponsibleId : 0, PriorityId : 0, SupportStatusId  :1, ProjectId : 0, RelationId: 0 }];
  items = [];
  title="";
  boardList:any;
  copyboardList:any;
  addOption = true;
  selectedCustomerContacts = [];
  copyselectedCustomerContacts = [];
  customerContact = [];
  contactList = [];
  relationList = [];

  loading = false;

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;
  status1: NbToastStatus = NbToastStatus.SUCCESS;

  title1 = '';
  content = '';


  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast() {
    this.showToast(this.status, this.title1, this.content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  //


  html = ``;
  config: any = {
    height: 250,
    theme: 'modern',
    plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image imagetools link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount contextmenu colorpicker textpattern',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
  };

  constructor(private _avRoute: ActivatedRoute,public http: HttpModule, private _router: Router, private _apicallservice: ApicallService,private toastrService: NbToastrService, private titleService: Title) {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.SupportText + " - Crmplus");

    this.relationList.push({ label: this.sysLanguages.RelationProjectText, value: 1 });

    if (this._avRoute.snapshot.params["id"]) {
      this.supportId = this._avRoute.snapshot.params["id"];
    }


    this.getUsers();
    this.getCustomer();
    this.getGroups();
    this.getPriority();
    this.getStatus();
    this.getContacts();
    this.getProjects();
    this.title=this.sysLanguages.NewSupport;
    this.title1=this.sysLanguages.ErrorMessage;
    this.content=this.sysLanguages.ErrorMessage1;
   }

  ngOnInit() {
    if(this.supportId>0)
    {
      this._apicallservice.Get("http://localhost:61232/api/Support/GetSupportById",this.supportId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              //do smth.
            }
          }
          else
          {
            this.addOption = false;
            this.supportArray[0].Subject = resultArray[0].Subject;
            this.supportArray[0].GroupId = resultArray[0].GroupId;
            this.supportArray[0].ResponsibleId = resultArray[0].ResponsibleId;
            this.supportArray[0].PriorityId = resultArray[0].PriorityId;
            this.supportArray[0].SupportStatusId = resultArray[0].SupportStatusId;
            var stringTags = resultArray[0].Tags;
            this.html = resultArray[0].SupportDesc;
            this.supportArray[0].ProjectId = resultArray[0].ProjectId;
            this.supportArray[0].CustomerId = resultArray[0].CustomerId;

            if(this.supportArray[0].ProjectId != 0)
            {
              this.supportArray[0].RelationId = 1;
            }

            if(stringTags == "")
            {
              this.items = [];
            }
            else
            {
              this.items = stringTags.split(",");
            }

            this.title=this.sysLanguages.EditSupport;

            
            this.customerContact = [];

            for(let i in this.contactList)
            {
              if(this.contactList[i].customerId == this.supportArray[0].CustomerId)
              {
                this.customerContact.push(this.contactList[i]);
              }
            }

            this._apicallservice.Get("http://localhost:61232/api/Support/GetSupportContacts", this.supportId).subscribe(
              (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(resultArray[0].Result == "Empty")
                    {
                      this.selectedCustomerContacts = [];
                    }
                  }
                  else
                  { 
                    this.selectedCustomerContacts = resultArray.map(x=> x.ContactId);
                    this.copyselectedCustomerContacts = this.selectedCustomerContacts.slice();
                  }
              }
            )
          }
        }
      )

    }
    
  }

  getUsers()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else
        {
          this.userList = [];
          for(let i in resultArray)
          {
            let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
            this.userList.push(_user);
          }
        }        
      },
      )
  }

  getPriority()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Support/GetPriority").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.priorityList = [];
          }
        }
        else
        {
          this.priorityList = [];
          for(let i in resultArray)
          {
            let _priority = { label: resultArray[i].PriorityStatusDesc, value: resultArray[i].PriorityStatusId };
            this.priorityList.push(_priority);
          }
        }        
      },
      )

  }

  getContacts()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/GetContact").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.contactList = [];
          }
        }
        else
        {
          this.contactList = [];
          for(let i in resultArray)
          {
            let _contact = { label: resultArray[i].Fullname, value: resultArray[i].ContactId , email: resultArray[i].Email , customerId : resultArray[i].CustomerId };
            this.contactList.push(_contact);
          }
        }        
      },
      )
  }

  getStatus()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Support/GetSupportStatus").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.statusList = [];
          }
        }
        else
        {
          this.statusList = [];
          for(let i in resultArray)
          {
            let _status = { label: resultArray[i].SupportStatusDesc, value: resultArray[i].SupportStatusId };
            this.statusList.push(_status);
          }
        }        
      },
      )

  }


  getCustomer()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.customerList = [];
          }
        }
        else
        {
          this.customerList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].CustomerId != 0)
            {
              let customer = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
              this.customerList.push(customer);
            }
            
          }
        }        
      },
      )

  }

  onChangeCustomer(event)
  {
    var id = event.value;

    this.customerContact = [];

    for(let i in this.contactList)
    {
      if(this.contactList[i].customerId == id)
      {
        this.customerContact.push(this.contactList[i]);
      }
    }

    this.selectedCustomerContacts = [];

    const a = this.boardList.filter(x=>x.CustomerId == id);
    this.copyboardList = a;
    
  }


  getGroups()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Group/Get").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.groupList = [];
          }
        }
        else
        {
          this.groupList = [];
          for(let i in resultArray)
          {
            let _group = { label: resultArray[i].GroupDesc, value: resultArray[i].GroupId };
            this.groupList.push(_group);
          }
        }        
      },
      )
  }

  save()
  {
    this.loading = true;
    if(this.title == this.sysLanguages.NewSupport)
    {
      let support = {
        Subject : $("#Subject").val(),
        CustomerId : this.supportArray[0].CustomerId,
        GroupId : this.supportArray[0].GroupId,
        Tags : this.items.toString(),
        ResponsibleId : this.supportArray[0].ResponsibleId,
        PriorityId : this.supportArray[0].PriorityId,
        SupportStatusId : this.supportArray[0].SupportStatusId,
        SupportDesc : this.html,
        ProjectId : this.supportArray[0].ProjectId
      }
      if(support.Subject == "" || support.CustomerId == 0 || support.GroupId == 0 ||support.ResponsibleId == 0 || support.PriorityId == 0 )
      {
        this.loading = false;
        this.makeToast();
      }
      else
      {
        this._apicallservice.Add("http://localhost:61232/api/Support/Create", support) 
        .subscribe((result) => {   
            let resultArray = JSON.parse(JSON.stringify(result));
            if (resultArray[0].Result != "Session_Expired") {
              let supportId = resultArray[0].Result;

              if(this.supportArray[0].CustomerId != 0)
              {
                let supportcontact;
      
                for(let j in this.selectedCustomerContacts)
                {
                  supportcontact = { SupportId: supportId, ContactId: this.selectedCustomerContacts[j]};   
                
                  this._apicallservice.Add("http://localhost:61232/api/Support/CreateSupportContact", supportcontact).subscribe(
                  (result) =>{
                    let resultArray = JSON.parse(JSON.stringify(result));
                    if (resultArray[0].Result == "Session_Expired") {
                      this.loading = false;
                      this._router.navigate(['/auth']);
                      return;
                    }
                  }
                  )
                  support.SupportDesc = this.contactList[this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])].email;
                  this._apicallservice.Add("http://localhost:61232/api/Support/SendSupport", support).subscribe(
                  (result) => {
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray.length != 0 && resultArray[0].Result) {
                    if (resultArray[0].Result == "Session_Expired") {
                      this.loading = false;
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(resultArray[0].Result == "True")
                    {
                      this.loading = false;
                      this._router.navigate(['/pages/support']);
                    }
                  }
                }
              )
                }
              }
            }
            else if (resultArray[0].Result == "Session_Expired") {
              this.loading = false;
              this._router.navigate(['/auth']);
              return;
            }                    
        },) 
      }
    }
    else if (this.title == this.sysLanguages.EditSupport)
    {
      let support = {
        SupportId : this.supportId,
        Subject : $("#Subject").val(),
        CustomerId : this.supportArray[0].CustomerId,
        GroupId : this.supportArray[0].GroupId,
        Tags : this.items.toString(),
        ResponsibleId : this.supportArray[0].ResponsibleId,
        PriorityId : this.supportArray[0].PriorityId,
        SupportStatusId : this.supportArray[0].SupportStatusId,
        SupportDesc : this.html,
        ProjectId : this.supportArray[0].ProjectId
      }
      if(support.Subject == "" || support.CustomerId == 0 || support.GroupId == 0 ||support.ResponsibleId == 0 || support.PriorityId == 0 )
      {
        this.loading = false;
        this.makeToast();
      }
      else
      {
        this._apicallservice.Update("http://localhost:61232/api/Support/Update",parseInt(this.supportId),support) 
        .subscribe((result) => {    
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray[0].Result == "True") {

            if(this.supportArray[0].CustomerId != 0)
            {
              //---------- eğer eski seçilmiş kontak varsa onları sil
              for(let i in this.copyselectedCustomerContacts)
              {
                if(this.selectedCustomerContacts.findIndex(x => x.value == this.copyselectedCustomerContacts[i]) == -1)
                {
                  let supportcontact = { SupportId: this.supportId, ContactId: this.copyselectedCustomerContacts[i] }; 
                  this._apicallservice.Add("http://localhost:61232/api/Support/RemoveSupportContact", supportcontact).subscribe(
                    (result) =>{
                    }
                  )
                }
              }
    
              //----------- yeni eklenen varsada onları ekle
              for(let j in this.selectedCustomerContacts)
              {
                if(this.copyselectedCustomerContacts.findIndex(x => x.value == this.selectedCustomerContacts[j]) == -1)
                {
                  let supportcontact = { SupportId: this.supportId, ContactId: this.selectedCustomerContacts[j] };   
                  this._apicallservice.Add("http://localhost:61232/api/Support/CreateSupportContact", supportcontact).subscribe(
                    (result) =>{
                    }
                  )
                }
              } 
            }

            this.loading = false;
            this._router.navigate(['/pages/support']);

          }
          else if (resultArray[0].Result == "Session_Expired") {
            this.loading = false;
            this._router.navigate(['/auth']);
            return;
          }                    
        },)
        }
    }
  }

  addProposalandSaveSupport()
  {
    this.loading = true;
    let _today = new Date();
    let _expiredDate = new Date();
    _expiredDate.setDate(_today.getDate()+30);

    let proposal = {
      ProposalId : 0,
      StatusId : 1,
      UserId : this.supportArray[0].ResponsibleId,
      CustomerId : this.supportArray[0].CustomerId,
      LeadId : 0,
      ProjectId : this.supportArray[0].ProjectId,
      CurrencyTypeId : 1,
      Amount : "0 TL",
      Subject : $("#Subject").val(),
      CreatedDate : this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]),
      ExpiredDate : this.formatIsoDateToCrypted(_expiredDate.toISOString().split("T")[0])
    };

    let support = {
      Subject : $("#Subject").val(),
      CustomerId : this.supportArray[0].CustomerId,
      GroupId : this.supportArray[0].GroupId,
      Tags : this.items.toString(),
      ResponsibleId : this.supportArray[0].ResponsibleId,
      PriorityId : this.supportArray[0].PriorityId,
      SupportStatusId : this.supportArray[0].SupportStatusId,
      SupportDesc : this.html,
      ProjectId : this.supportArray[0].ProjectId
    };
    if(support.Subject == "" || support.CustomerId == 0 || support.GroupId == 0 ||support.ResponsibleId == 0 || support.PriorityId == 0 )
    {
      this.loading = false;
      this.makeToast();
    }
    else
    {
        this._apicallservice.Add("http://localhost:61232/api/Support/Create",support) 
        .subscribe((result) => {   
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result != "Session_Expired") {
        let supportId = resultArray[0].Result;
        this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposal", proposal).subscribe(
          (result) => {
            var proposalId = result[0].Result;
            proposal.ProposalId = proposalId;
            let proposaldetail = {
              ProposalId: proposalId, 
              LineId: 1 ,
              ProductId: 0,
              Description: "Destek Paketi", 
              LineAmount: "0 TL",
              ProductCount : "0", 
              LineTax : "Vergi Yok", 
              LineName : ""
            };
            this._apicallservice.Add("http://localhost:61232/api/Proposal/CreateProposalDetail",proposaldetail).subscribe(
              (resultt) =>{
                let resulttArray = JSON.parse(JSON.stringify(resultt));
                if (resulttArray[0].Result == "Session_Expired") {
                  this.loading = false;
                  this._router.navigate(['/auth']);
                  return;
                }
              }
              )
            if(this.supportArray[0].CustomerId != 0)
            {
              let supportcontact;
    
              for(let j in this.selectedCustomerContacts)
              {
                supportcontact = { SupportId: supportId, ContactId: this.selectedCustomerContacts[j]};   
              
                this._apicallservice.Add("http://localhost:61232/api/Support/CreateSupportContact", supportcontact).subscribe(
                (result) =>{
                  let resultArray = JSON.parse(JSON.stringify(result));
                  if (resultArray[0].Result == "Session_Expired") {
                    this.loading = false;
                    this._router.navigate(['/auth']);
                    return;
                  }
                }
                )
                proposal.Subject = this.contactList[this.contactList.findIndex(x => x.value == this.selectedCustomerContacts[j])].email;
                this._apicallservice.Update("http://localhost:61232/api/Proposal/SendProposal", 0, proposal).subscribe(
                (result) => {
                let resultArray = JSON.parse(JSON.stringify(result));
                if (resultArray.length != 0 && resultArray[0].Result) {
                  if (resultArray[0].Result == "Session_Expired") {
                    this.loading = false;
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else if(resultArray[0].Result == "True")
                  {
                    this.loading = false;
                    this._router.navigate(['/pages/support']);
                  }
                }
              }
            )
              }
            }  
            }
          )
        }
        else if (resultArray[0].Result == "Session_Expired") {
          this.loading = false;
          this._router.navigate(['/auth']);
          return;
        }                    
    },)  
    }
  }


  getProjects()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/project/GetTaskBoards").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.boardList = [];
          }
        }
        else {
          this.boardList = [];
          for(let i in resultArray)
          {
            if(resultArray[i].BoardId != 0)
            {
              let board = { label: resultArray[i].BoardName, value: resultArray[i].BoardId , CustomerId:resultArray[i].CustomerId  };
              this.boardList.push(board);
            }
          }
        }
      }
    )
  }

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03")
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05")
    {
        val = "May";
    }
    else if (splittedDate[1] == "06")
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07")
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08")
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09")
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }
  
}
