import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'support-overview',
  templateUrl: './support-overview.component.html',
  styleUrls: ['./support-overview.component.scss'],
  providers:[ApicallService,HttpModule]
})
export class SupportOverviewComponent implements OnInit {

  sysLanguages: any;
  public SupportArray = [{ Subject: "" ,CustomerName : "" , Tags:"" , Fullname : "" , PriorityStatusDesc : "", CreatedDate : "", SupportStatusDesc: "" , BoardName: ""   }];

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.SupportOverviewText + " - Crmplus");
    this.tabs[0].title = this.sysLanguages.SupportOverviewHeader;
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/support/support-overview', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/support/DetailsForSupportOverview",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {

            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {
            this.SupportArray[0].Subject = resulttArray[0].Subject;
            this.SupportArray[0].Tags=resulttArray[0].Tags;
            this.SupportArray[0].PriorityStatusDesc=resulttArray[0].PriorityStatusDesc;
            this.SupportArray[0].SupportStatusDesc=resulttArray[0].SupportStatusDesc;
            this.SupportArray[0].Fullname=resulttArray[0].Fullname;
            this.SupportArray[0].CustomerName=resulttArray[0].CustomerName;
            this.SupportArray[0].BoardName=resulttArray[0].BoardName;
            this.SupportArray[0].CreatedDate=resulttArray[0].CreatedDate.split(" ")[0];

            if(this.SupportArray[0].Subject == "")
            {
              this.SupportArray[0].Subject = this.sysLanguages.NotValueText;
            }
            if(this.SupportArray[0].Tags == "")
            {
              this.SupportArray[0].Tags = this.sysLanguages.NotValueText;
            }
            if(this.SupportArray[0].CustomerName == "NoCustomer")
            {
              this.SupportArray[0].CustomerName = this.sysLanguages.NoCustomerText;
            }
            if(this.SupportArray[0].BoardName == "NoBoard")
            {
              this.SupportArray[0].BoardName = this.sysLanguages.NotValueText;
            }

          }       
      },

      )
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/support/add-edit-support', this._avRoute.snapshot.params["id"]]);
  }

}
