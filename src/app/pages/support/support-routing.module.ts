import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupportComponent } from './support/support.component';
import { AddEditSupportComponent } from './add-edit-support/add-edit-support.component';
import { SupportOverviewComponent } from './support-overview/support-overview.component';

const routes: Routes = [
  {
    path: '',
    component: SupportComponent,
  },
  {
    path: 'add-edit-support',
    component: AddEditSupportComponent,
  },
  {
    path: 'add-edit-support/:id',
    component: AddEditSupportComponent,
  },
  {
    path: 'support-overview/:id',
    component: SupportOverviewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
