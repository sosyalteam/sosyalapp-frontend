import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupportRoutingModule } from './support-routing.module';
import { SupportComponent } from './support/support.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbCardModule, NbInputModule, NbDatepickerModule, NbTabsetModule, NbListModule, NbRouteTabsetModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { AddEditSupportComponent } from './add-edit-support/add-edit-support.component';
import { NgxTinymceModule } from 'ngx-tinymce';
import { SupportOverviewComponent } from './support-overview/support-overview.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  declarations: [SupportComponent, AddEditSupportComponent, SupportOverviewComponent],
  imports: [
    CommonModule,
    SupportRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NbCardModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbDatepickerModule,
    TagInputModule, 
    DropdownModule,
    MultiSelectModule,
    NbTabsetModule,
    AutocompleteLibModule,
    NbListModule,
    NbRouteTabsetModule,
    NgxTinymceModule.forRoot({
      baseURL: '//cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.0/',
    })
  ]
})
export class SupportModule { }
