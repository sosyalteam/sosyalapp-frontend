import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import html2canvas from 'html2canvas';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class SupportComponent implements OnInit {

  public sysLanguages: any;
  TicketList : any;
  filteredTicketList : any;
  filter = false;
  public allSupportCount  = 0;
  public openCount  = 0;
  public continueCount  = 0;
  public answeredCount  = 0;
  public stayCount  = 0;
  public closedCount  = 0;


  constructor(public http: HttpModule, private _router: Router, private _apicallservice: ApicallService, private titleService: Title) { 

    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.SupportText + " - Crmplus");

    this.settings.columns.Subject.title=this.sysLanguages.PcSubject;
    this.settings.columns.CustomerName.title=this.sysLanguages.CustomerText;
    this.settings.columns.Tags.title=this.sysLanguages.BoardTableTagText;
    this.settings.columns.GroupDesc.title=this.sysLanguages.GroupText;
    this.settings.columns.SupportStatusDesc.title=this.sysLanguages.AddTaskStatusText;
    this.settings.columns.PriorityStatusDesc.title=this.sysLanguages.Priority;
    this.settings.columns.CreatedDate.title=this.sysLanguages.LogTableCreatedDate;
    
    this.getTickets();
  }

  settings = {
    columns: {
      SupportId: {
        title: 'ID',
        width: '2%'
      },
      Subject: {
        title: ''
      },
      CustomerName: {
        title: ''
      },
      Tags:{
        title : ''
      },
      GroupDesc : {
        title : ''
      },
      SupportStatusDesc :  {
        title : '',
        type: 'html',
        valuePrepareFunction: (value) => {
          if(value === this.sysLanguages.StatusOpen)
          {
            return '<p class="open">' + value + '</p>'; 
          }
          else if(value === this.sysLanguages.StatusContinue )
          {
            return '<p class="continue">' + value + '</p>'; 
          }
          else if(value === this.sysLanguages.StatusAnswered )
          {
            return '<p class="answered">' + value + '</p>'; 
          }
          else if(value === this.sysLanguages.StatusStay )
          {
            return '<p class="stay">' + value + '</p>'; 
          }
          else if(value === this.sysLanguages.StatusClosed )
          {
            return '<p class="close">' + value + '</p>'; 
          }
        },
      },
      PriorityStatusDesc : {
        title : ''
      },
      CreatedDate : {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      }
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  addTicket()
  {
    this._router.navigate(['/pages/support/add-edit-support']);
  }

  editTicket(event)
  {
    var id =parseInt(`${event.data.SupportId}`);
    this._router.navigate(['/pages/support/support-overview',id]);
  }

  getTickets()
  {
    this._apicallservice.GetAll("http://localhost:61232/api/Support/GetAllSupport").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.TicketList = [];
          }
        }
        else
        {
          this.TicketList = result;
          this.allSupportCount = resultArray.length;
          for(let i in resultArray)
          {
            if(resultArray[i].SupportStatusId == 1)
            {
              this.TicketList[i].SupportStatusDesc = this.sysLanguages.StatusOpen;
              this.openCount++;
            }
            else if(resultArray[i].SupportStatusId == 2)
            {
              this.TicketList[i].SupportStatusDesc = this.sysLanguages.StatusContinue;
              this.continueCount++;
            }
            else if(resultArray[i].SupportStatusId == 3)
            {
              this.TicketList[i].SupportStatusDesc = this.sysLanguages.StatusAnswered;
              this.answeredCount++;
            }
            else if(resultArray[i].SupportStatusId == 4)
            {
              this.TicketList[i].SupportStatusDesc = this.sysLanguages.StatusStay;
              this.stayCount++;
            }
            else if(resultArray[i].SupportStatusId == 5)
            {
              this.TicketList[i].SupportStatusDesc = this.sysLanguages.StatusClosed;
              this.closedCount++;
            }
          }
        }        
      },
      )
  }

  onDeleteConfirm(event)
  {
    var id =parseInt(`${event.data.SupportId}`);
    var ans = confirm(this.sysLanguages.DeleteSupport + id);
     if (ans) {
       this._apicallservice.Deactive("http://localhost:61232/api/Support/Delete/", id).subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getTickets();
          this._router.navigate(['/pages/support']);
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
       
       },)
     }

  }

  filterSupport(value)
  {
    this.filter = true;
    if(value == "1")
    {
      const a  = this.TicketList.filter(x=>x.SupportStatusId == 1);
      this.filteredTicketList = a;
    }
    else if(value == "2")
    {
      const a  = this.TicketList.filter(x=>x.SupportStatusId == 2);
      this.filteredTicketList = a;
    }
    else if(value == "3")
    {
      const a  = this.TicketList.filter(x=>x.SupportStatusId == 3);
      this.filteredTicketList = a;
    }
    else if(value == "4")
    {
      const a  = this.TicketList.filter(x=>x.SupportStatusId == 4);
      this.filteredTicketList = a;
    }
    else if(value == "5")
    {
      const a  = this.TicketList.filter(x=>x.SupportStatusId == 5);
      this.filteredTicketList = a;
    }
    else if(value == "All")
    {
      this.filter = false;
    }
  }

}
