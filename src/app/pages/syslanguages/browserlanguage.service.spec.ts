import { TestBed } from '@angular/core/testing';

import { BrowserlanguageService } from './browserlanguage.service';

describe('BrowserlanguageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrowserlanguageService = TestBed.get(BrowserlanguageService);
    expect(service).toBeTruthy();
  });
});
