import { Injectable, NgModule } from '@angular/core';
import{ Turkish } from '../syslanguages/turkish';
import{ English } from '../syslanguages/english';
import { Resolve } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})

@NgModule({
  providers: [HttpClient],
})

export class BrowserlanguageService implements Resolve<any> {

  constructor(private _http: HttpClient) { }

  resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): Observable<any> {
      return this._http.get("http://localhost:61232/api/Proposal/GetBrowserLanguage").pipe(
        map( (dataFromApi) => {
          let resultArray = JSON.parse(JSON.stringify(dataFromApi));
          if(dataFromApi[0].Result == "tr-TR")
          {
            return Turkish;
          }
          else
          {
            return English;
          }
        } ),
      );
  }
}
