import{ Turkish } from '../syslanguages/turkish';
import{ English } from '../syslanguages/english';
import { Injectable, NgModule } from '@angular/core';
import { Router, Resolve } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})

@NgModule({
    providers: [HttpClient],
  })

export class Syslanguages implements Resolve<any>{

  public Language: any;


  resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): Observable<any> {
    debugger;
    if(localStorage.length == 0)
    {
      this.Language = Turkish;
      sessionStorage.setItem("langFile", JSON.stringify(Turkish));
      return this.Language;
    }
    else
    {
      const headers = new HttpHeaders({'Content-Type':'application/json','EuToken': localStorage.getItem("EuToken")});
      return this._http.get("http://localhost:61232/api/User/GetUserLanguage", { headers: headers }).pipe(
        map( (dataFromApi) => {
          let resultArray = JSON.parse(JSON.stringify(dataFromApi));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "NullLanguage") // eğer herhangi bir dil seçili ise otomatik türkçe getirr
            {
              sessionStorage.setItem("langFile", JSON.stringify(Turkish));
              return this.Language = Turkish;
            }
            else if (resultArray[0].Result == "Session_Expired")
            {
              sessionStorage.setItem("langFile", JSON.stringify(Turkish));
              return this.Language = Turkish;
            }
          }
          else
          {
              if(dataFromApi == "Turkish")
              {
                this.Language = Turkish;
              }
              else if(dataFromApi == "English")
              {
                this.Language = English;
              }
  
              sessionStorage.setItem("langFile", JSON.stringify(this.Language));
  
              return this.Language;
          }
        } ),
      );
    }
  }

    constructor(private _http: HttpClient, private _router: Router) {
    
    }
    
}

