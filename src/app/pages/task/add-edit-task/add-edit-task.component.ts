import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule, Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NbDialogService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ThemeSettingsComponent } from '../../../@theme/components';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'add-edit-task',
  templateUrl: './add-edit-task.component.html',
  styleUrls: ['./add-edit-task.component.scss'],
  providers: [ApicallService,HttpModule],
})
export class AddEditTaskComponent implements OnInit {

  sysLanguages : any;
  taskID : any;
  loading = false;

  SubTaskList : any;
  subTaskEmpty= false;
  assignTaskId : any;

  taskData = {TaskId : 0 , Title: "", BeginDate: "", EndDate: "", Fullname: "", StatusId : 0 , TaskDesc: "", ResponsibleId: 0, TeamId: 0, TaskTime : "" , Creator : ""};
  StatusList = [];
  allUsers = [];
  TeamList= [];
  selectedResponsible =  0;
  selectedTeam = 0;
  selectedStatus = 1;
  selectedType = 0;
  assigneeUsers = [];
  _copyassigneeUsers = [];
  TeamuserList = [];

  CommentList = [];
  filteredCommentList = [];
  filteredComment = false;

  relationList = [];
  isSelectedRelation: any;
  customerList = [];
  leadList = [];
  invoiceList = [];
  projectList = [];
  taskTypeList = [];
  isSelectedInvoice = 0;
  isSelectedCustomer = 0;
  isSelectedLead = 0;
  isSelectedProject = 0;
  RelationText  = "";
  isDialogSaveOrDetail: any;

  taskRelation = "";

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  //


  constructor(private datePipe:DatePipe,private _avRoute: ActivatedRoute,public http: Http, private _router: Router, private _apiCallservice: ApicallService,private toastrService: NbToastrService, private titleService:Title)
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.getUsers();
    this.getTeams();
    this.getStatusList();
    this.getTaskType();
    this.titleService.setTitle(this.sysLanguages.AddTaskHeaderText + " - Crmplus");

    this.relationList.push({ label: this.sysLanguages.LeadText, value: 0 }, { label: this.sysLanguages.CustomerText, value: 1 } ,  { label: this.sysLanguages.InvoiceText, value : 2 } , { label : this.sysLanguages.ProjectText , value : 3}  );

    this.getLeads();
    this.getCustomers();
    this.getInvoices();
    this.getProjects();
    this.getTeamuser();

    if (this._avRoute.snapshot.params["id"]) {
      this.taskID = this._avRoute.snapshot.params["id"];  
    }

    this.taskRelation = sessionStorage.getItem("taskRelation");
    sessionStorage.removeItem('taskRelation');
  }

  ngOnInit() {
    if(this.taskID > 0)
    {
      //düzenleme
      this.assigneeUsers = [];
      this.isDialogSaveOrDetail = "detail";
      this._apiCallservice.Get("http://localhost:61232/api/Task/Details",this.taskID)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              return;
            }
            else if (resulttArray.length != 0 && resulttArray[0].Result) {
              if (resulttArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
          }
          } 
          else
          {

            setTimeout(()=>{
              debugger;
              this.RelationText = "";
              this.selectedType = resulttArray[0].TypeId;
              this.taskData.TaskId = resulttArray[0].TaskId;
              this.taskData.Title = resulttArray[0].Title;
              this.taskData.TaskDesc = resulttArray[0].TaskDesc;
              this.taskData.BeginDate = this.formatIsoDateToCrypted(resulttArray[0].BeginDate.split(" ")[0]);
              this.taskData.EndDate = this.formatIsoDateToCrypted(resulttArray[0].EndDate.split(" ")[0]);
              this.taskData.ResponsibleId = resulttArray[0].ResponsibleId;
              this.taskData.TeamId = resulttArray[0].TeamId;
              this.selectedResponsible = resulttArray[0].ResponsibleId;
              this.selectedStatus = resulttArray[0].StatusId;
              this.taskData.StatusId = resulttArray[0].StatusId;
              this.selectedTeam = resulttArray[0].TeamId;
              this.isSelectedInvoice = resulttArray[0].InvoiceId;
              this.isSelectedCustomer = resulttArray[0].CustomerId;
              this.isSelectedLead = resulttArray[0].LeadId;
              this.isSelectedProject = resulttArray[0].BoardId;
              this.taskData.TaskTime = resulttArray[0].TaskTime;
              this.taskData.Creator = this.allUsers[this.allUsers.findIndex(x=>x.value == resulttArray[0].CreatorId)].label;

              this._apiCallservice.Get("http://localhost:61232/api/Task/GetTaskMembers", this.taskID).subscribe(
                (result) => {
                  let _resultArray = JSON.parse(JSON.stringify(result));
                  if (_resultArray.length != 0 && _resultArray[0].Result) {
                    if (_resultArray[0].Result == "Session_Expired") {
                      this._router.navigate(['/auth']);
                      return;
                    }
                    else if(_resultArray[0].Result == "Empty")
                    {
                      this.assigneeUsers = [];
                      this._copyassigneeUsers = [];
                    }
                  }
                  else
                  { 
                    this.assigneeUsers = _resultArray.map(x=> x.UserId);
                    this._copyassigneeUsers = this.assigneeUsers.slice();
                  }
                }
              )

              if(this.isSelectedCustomer != 0)
              {
                this.isSelectedRelation=1;
                this.RelationText = this.sysLanguages.ProfileCustomerRelatedText;
              }
              else if(this.isSelectedInvoice != 0)
              {
                this.isSelectedRelation=2;
                this.RelationText = this.sysLanguages.ProfileInvoiceRelatedText;
              }
              else if(this.isSelectedLead !=0 )
              {
                this.isSelectedRelation=0;
                this.RelationText = this.sysLanguages.ProfileLeadRelatedText;
              }
              else if (this.isSelectedProject != 0)
              {
                this.isSelectedRelation=3;
                this.RelationText = this.sysLanguages.ProfileBoardRelatedText;
              }
              else
              {
                this.isSelectedRelation=null;
              }
              this.getComments();
              this.getSubTask();
            }, 2000);
          }       
      },
      ) 
    }
    else
    {
      //ekleme
      this.isDialogSaveOrDetail = "save";

      if(this.taskRelation != null)
      {
        var splittedTaskRelation = this.taskRelation.split("_");
        if(splittedTaskRelation[0] == "lead")
        {
          //leadden görev ekleniyorsa ilişki seçili gelsin diye
          setTimeout(()=>{
            debugger;
            this.selectedStatus = 0;
            this.taskData.Title = this.leadList.filter(x=>x.value == parseInt(splittedTaskRelation[1]) )[0].label;
            this.isSelectedLead = parseInt(splittedTaskRelation[1]);
            this.isSelectedRelation = 0;
            this.RelationText = this.sysLanguages.ProfileBoardRelatedText;
          },2500);
        }
        else if(splittedTaskRelation[0] == "project")
        {
          //projeden gorev eklenıyorsa
          setTimeout(()=>{
            this.isSelectedProject = parseInt(splittedTaskRelation[1]);
            this.isSelectedRelation = 3;
            this.RelationText = this.sysLanguages.ProfileBoardRelatedText;
          },2500);
        }
      }
      else
      {
        this.isSelectedRelation = null;
        this.isSelectedCustomer = 0;
        this.isSelectedLead = 0;
        this.isSelectedProject = 0;
        this.RelationText = "";
      }

      let _today = new Date(); 
      this.taskData.Title = "";
      this.taskData.TaskDesc = "";
      this.taskData.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);;
      this.taskData.EndDate = "";
      this.taskData.ResponsibleId = 0;
      this.taskData.TeamId = 0;
      this.selectedStatus= 1;
      this.selectedResponsible = 0;
      this.selectedType = 0;
      this.selectedTeam = 0;
      this.assigneeUsers = [];    
      this.taskData.TaskTime = "";
    }
  }

  getUsers()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {     
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if (resulttArray[0].Result == "Empty") {
          this.allUsers = [];
        }
      }
      else
      { 
        for(var i = 0; i < resulttArray.length; i++ ){
          let _users = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
          this.allUsers.push(_users);
        }
      } 
    },
    )
  }

  getTeams()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
    (result) => {         
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.TeamList = [];
        }
      }
      else
      {
        for(var i = 0; i < resultArray.length; i++ ){
          let _team = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
          this.TeamList.push(_team);
        }
      }
    },
    )
  }

  getStatusList()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.StatusList = [];
          }
        }
        else
        {
          for(var i = 0; i < resultArray.length; i++ ){
            let _status = { label: resultArray[i].StatusDesc, value: resultArray[i].StatusId };
            this.StatusList.push(_status);
          }
        }
    },
    )
  }

  getTaskType()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskType").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.taskTypeList = [];
            }
          }
          else
          { 
            this.taskTypeList = [];
            for(let i in resultArray)
            {
              let _types = { label: resultArray[i].TypeDesc, value: resultArray[i].TypeId };
              this.taskTypeList.push(_types);
            }
            this.selectedType = 1;
          }
      }
    )
  }
  getCustomers()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Customer/Get").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.customerList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].CustomerId != 0)
              {
                let _customers = { label: resultArray[i].CustomerName, value: resultArray[i].CustomerId };
                this.customerList.push(_customers);
              }
            }
          }
      }
    )
  }

  getLeads()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Lead/GetAllLead").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.leadList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].LeadId != 0)
              {
                let _lead = { label: resultArray[i].LeadName + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].LeadId, name: resultArray[i].Name };
                this.leadList.push(_lead);
              }
            }
          }
      }
    )
  }

  getInvoices()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Invoice/GetAllInvoices").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.invoiceList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].InvoiceId != 0)
              {
                let _invoice = { label: resultArray[i].InvoiceNumber + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].InvoiceId};
                this.invoiceList.push(_invoice);
              }
            }
          }
      }
    )
  }

  getProjects()
  {
    this._apiCallservice.GetAll("http://localhost:61232/api/Project/GetTaskBoards").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.projectList = [];
            }
          }
          else
          { 
            for(let i in resultArray)
            {
              if(resultArray[i].BoardId != 0)
              {
                let _projects = { label: resultArray[i].BoardName + " ( " +resultArray[i].CustomerName + " )", value: resultArray[i].BoardId };
                this.projectList.push(_projects);
              }
            }
          }
      }
    )
  }

  getTeamuser()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/User/GetTeamuser").subscribe(
    (result) => {         
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.TeamuserList = [];
        }
      }
      else
      {
        this.TeamuserList = resultArray;
      }
    },
    )
  }

  getComments()
  {
    this._apiCallservice.Get("http://localhost:61232/api/Task/TaskComments",this.taskID)
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
        }
      }
      else
      {
        this.CommentList = resultArray;
      }
    })
  }

  addTask()
  {
    this.loading = true;
    var now = new Date().toISOString();
    var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
    var endDate = <HTMLInputElement> document.getElementById("EndDate");
    let task={
      ParentId:0,
      Title: $("#taskTitle").val(),
      TaskDesc: $("#taskDesc").val(),
      TypeId: this.selectedType,
      CreatedDate: now.split("T")[0],
      ResponsibleId: this.assigneeUsers[0] ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this.isSelectedProject,
      ContractId:1,
      BeginDate:beginDate.value,
      EndDate:endDate.value,
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: this.selectedTeam,
      CustomerId:this.isSelectedCustomer,
      LeadId:this.isSelectedLead,
      InvoiceId:this.isSelectedInvoice,
      TaskTime : $("#time").val(),
    }

     if(task.Title == "" || task.EndDate == "" || task.BeginDate == "" )
     {
      this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
      this.loading = false;
     }
     else
     {
      this._apiCallservice.Add("http://localhost:61232/api/Task/Create",task) 
      .subscribe((result) => {
         let resultArray = JSON.parse(JSON.stringify(result));    
         if (resultArray[0].Result == "Session_Expired") {
           this.loading = false;
           this._router.navigate(['/auth']);
           return;
         }  
         else
         {
            for(let i in this.assigneeUsers)
            {
              let taskmember = { TaskId : result[0].Result , UserId : this.assigneeUsers[i] } ;
              this._apiCallservice.Add("http://localhost:61232/api/Task/CreateTaskMember",taskmember)
                .subscribe((resultt) => {          
              }                  
              )
            }
          this.loading = false;
          this._router.navigate(['/pages/task/add-edit-task',result[0].Result]);
         }
      },) 
     }
  }

  editTask()
  {
    this.loading = true;
    var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
    var endDate = <HTMLInputElement> document.getElementById("EndDate");
    let task={
      TaskId: this.taskData.TaskId,
      ParentId:0,
      Title: $("#taskTitle").val(),
      TaskDesc: $("#taskDesc").val(),
      TypeId: this.selectedType,
      CreatedDate: "fromTasksComponent",
      ResponsibleId: this.assigneeUsers[0],
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this.isSelectedProject,
      ContractId:1,
      BeginDate:beginDate.value,
      EndDate:endDate.value,
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: this.selectedTeam,
      CustomerId:this.isSelectedCustomer,
      LeadId:this.isSelectedLead,
      InvoiceId:this.isSelectedInvoice,
      TaskTime : $("#time").val(),
    }

    if(task.Title == "" || task.EndDate == "" || task.BeginDate == ""  )
     {
      this.loading = false;
      this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
     }
     else
     {
      this._apiCallservice.Add("http://localhost:61232/api/Task/Update", task) 
      .subscribe((result) => {
        for(let i in this._copyassigneeUsers)
        {
          if(this.assigneeUsers.findIndex(x => x == this._copyassigneeUsers[i]) == -1)
          {
            let _taskmember = { TaskId: this.taskID, UserId: this._copyassigneeUsers[i] }; 
            this._apiCallservice.Add("http://localhost:61232/api/Task/DeleteTaskMembers", _taskmember).subscribe(
              (result) =>{
              }
            )
          }
        }

        //----------- yeni eklenen varsada onları ekle
        for(let j in this.assigneeUsers)
        {
          if(this._copyassigneeUsers.findIndex(x => x == this.assigneeUsers[j]) == -1)
          {
            let _taskmember = { TaskId: this.taskID, UserId: this.assigneeUsers[j] };   
            this._apiCallservice.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
              (result) =>{
              }
            )
          }
        } 

      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") 
      {
        this.loading = false;
        this.makeToast(NbToastStatus.SUCCESS,this.sysLanguages.SuccessEditTask, this.sysLanguages.SuccessMessage);
      }          
      }
    )
     } 
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId: this.taskID,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : 0,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apiCallservice.Add("http://localhost:61232/api/Task/CreateTaskComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apiCallservice.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }
  
  getSubTask()
  {
    this._apiCallservice.Get("http://localhost:61232/api/Task/GetSubTask/",this.taskID).subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.SubTaskList = [];
            this.subTaskEmpty = true;
          }
        }
        else
        {
          this.SubTaskList=resultArray;
          this.subTaskEmpty = false;
        }
    },
    )
  }

  addSubTask()
  {
    let task={
      TaskId: this.taskID,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: 0,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    if(task.TaskDesc == "")
    {
      return;
    }
    else
    {
      this._apiCallservice.Add("http://localhost:61232/api/Task/CreateSubTask", task) 
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this.getSubTask();
          $("#subs").val("");
        }          
        },
      )
    }
    
  }

  editSubTask(taskId)
  {
    var newSubTaskDesc = $("#subTaskk_"+taskId).val();
    let editTask={
      TaskId: taskId,
      ParentId:0,
      Title: newSubTaskDesc,
      TaskDesc: "newSubTaskDesc",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: 0,
      ContractId: 1,
      BeginDate: "",
      EndDate: "",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId: 0,
      LeadId: 0,
      InvoiceId: 0,
    };
    this._apiCallservice.Add("http://localhost:61232/api/Task/UpdateSubTask", editTask) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)
  }


  completeSubTask(taskId)
   {
    this._apiCallservice.Deactive("http://localhost:61232/api/Task/CompleteSubTask", taskId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getSubTask();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
     
     }, )
   }

   selectSubAssignee(id)
   {
     this.assignTaskId = id;
   }

   subAssignTo(event)
   {
     var userId = event.value;
     let task={
      TaskId: this.assignTaskId,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: userId ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    this._apiCallservice.Add("http://localhost:61232/api/Task/AssigneeSubTask", task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)

      this._apiCallservice.Deactive("http://localhost:61232/api/Task/DeleteSubTaskMember", this.assignTaskId).subscribe(
        (result) =>{
        }
      )

      let _taskmember = { TaskId: this.assignTaskId, UserId: userId };   
      this._apiCallservice.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
        (result) =>{
        }
      )

   }

   userChange(event)
   {
     if(event.value.length == 1)
     {
       this.selectedTeam = this.TeamuserList.filter(x=>x.UserId == event.itemValue)[0].TeamId;
     }
     else if(event.value.length == 0)
     {
       this.selectedTeam = 0;
     }
   }


  formatIsoDateToCrypted(date)
   {
     var splittedDate = date.split("-");
     let val = "";
     if (splittedDate[1] == "01")
     {
         val = "Jan";
     }
     else if (splittedDate[1] == "02")
     {
         val = "Feb";
     }
     else if (splittedDate[1] == "03")
     {
         val = "Mar";
     }
     else if (splittedDate[1] == "04")
     {
         val = "Apr";
     }
     else if (splittedDate[1] == "05")
     {
         val = "May";
     }
     else if (splittedDate[1] == "06")
     {
         val = "Jun";
     }
     else if (splittedDate[1] == "07")
     {
         val = "Jul";
     }
     else if (splittedDate[1] == "08")
     {
         val = "Aug";
     }
     else if (splittedDate[1] == "09")
     {
         val = "Sep";
     }
     else if (splittedDate[1] == "10")
     {
         val = "Oct";
     }
     else if (splittedDate[1] == "11")
     {
         val = "Nov";
     }
     else if (splittedDate[1] == "12")
     {
         val = "Dec";
     }
 
     return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
   }

   makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
