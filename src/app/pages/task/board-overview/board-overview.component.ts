import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { NbDialogService, NbToastrService, NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'board-overview',
  templateUrl: './board-overview.component.html',
  styleUrls: ['./board-overview.component.scss']
})
export class BoardOverviewComponent implements OnInit {

  remainingDays:any;
  allDays:any;
  remainingDaysValue:any;
  allTask = 0;
  notCompletedTask = 0;
  taskCountValue = 0;
  sysLanguages: any;
  team = "";
  public statusList = [{ value: '', title: '' }];
  public userList = [{ value: '', title: '' }];
  public TaskList : any;
  taskData = {TaskId : 0 , Title: "", BeginDate: "", EndDate: "", Fullname: "", StatusDesc: "", TaskDesc: "", ResponsibleId: 0, TeamId: 0, StatusId: 0, TaskTime : ""};
  isDialogSaveOrDetail: any;
  selectedResponsible: any;
  selectedTeam = 0;
  selectedStatus = 0;
  selectedType = 0;
  _copySelectedFollower : any;
  assigneeUsers: any;
  _copyassigneeUsers :any;
  taskID : any;
  SubTaskList : any;
  subTaskEmpty= "";
  assignTaskId : any;
  date:string;

  StatusList = [];
  allUsers = [];
  TeamList= [];
  taskTypeList = [];

  costList = [];
  totalExpenses : any;
  billiableExpenses : any;
  billedExpenses : any;
  nonInvoicedExpenses : any;

  subsArea = false;
  proformArea = false;
  ProformList = [];
  productList = [];

  emptyComment = false;
  CommentList = [];

  //pagination 
  p: number = 1;
  collection = [];


  loading = false;

  public overviewData = { projectName: "", statusDesc: "", startDate: "", endDate: "", CustomerName: "",CustomerId:"",Amount : ""};

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  

  constructor(private datePipe:DatePipe,private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title , private dialogService: NbDialogService, private toastrService: NbToastrService ) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));

    this.tabs[0].title = this.sysLanguages.BoTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.BoTabsProjectTasksText;
    this.settings.columns.Title.title=this.sysLanguages.TasksTableTitleText;
    this.settings.columns.TaskDesc.title=this.sysLanguages.TasksTableTascDescText;
    this.settings.columns.StatusDesc.title=this.sysLanguages.TasksTableStatusText;
    this.settings.columns.StatusDesc.filter.config.selectText=this.sysLanguages.TasksTableFilterSelectText;
    this.settings.columns.BeginDate.title=this.sysLanguages.TasksTableStartDateText;
    this.settings.columns.EndDate.title=this.sysLanguages.TasksTableEndDateText;
    this.settings.columns.Fullname.title=this.sysLanguages.TasksTableAssignText;
    this.settings.columns.Fullname.filter.config.selectText=this.sysLanguages.TasksTableFilterSelectText;

    this.settingsProform.columns.ProformNumber.title=this.sysLanguages.ProformNumberText;
    this.settingsProform.columns.Amount.title=this.sysLanguages.AddInvoiceTablePrice;
    this.settingsProform.columns.CreatedDate.title=this.sysLanguages.CreatedDateText;
    this.settingsProform.columns.CustomerName.title=this.sysLanguages.BoCardCustomerText;
    this.settingsProform.columns.ExpirementDate.title=this.sysLanguages.ProfcExpiredDate;
    this.settingsProform.columns.StatusDesc.title=this.sysLanguages.AddTaskStatusText;

    this.settingsProduct.columns.ProductName.title = this.sysLanguages.AddInvoiceTableProduct;
    this.settingsProduct.columns.Description.title = this.sysLanguages.AddInvoiceTableDesc;
    this.settingsProduct.columns.LineAmount.title = this.sysLanguages.AddInvoiceTableQuantity;
    this.settingsProduct.columns.LineTax.title = this.sysLanguages.AddInvoiceTableTax;
    this.settingsProduct.columns.LineDiscount.title = this.sysLanguages.AddInvoiceDiscount;

    this.titleService.setTitle(this.sysLanguages.BoTabsProjectOverviewText + " - Crmplus");

    this.getStatus();
    this.getUsers();
    this.getTaskType();
    this.getTeams();
    this.getCosts();
    this.getBoardsProduct();
 
  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/task/board-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/task/tasks-panel', this._avRoute.snapshot.params["id"]],
    },
  ];

  settings = {
    columns: {
      TaskId: {
        title: 'ID',
        width: '2%'
      },
      Title: {
        title: ''
      },
      TaskDesc: {
        title: ''
      },
      StatusDesc: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: 'Select',
                list: this.statusList
              }
            }
      },
      BeginDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },       
      },
      EndDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      Fullname: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: 'Select',
                list: this.userList
              }
            }
      },
    },
    actions:{
      position:'right',
      edit:false,
      delete:false,
      add:false,
    },
    mode: 'external',
  };

  settingsProform = {
    columns: {
      ProformId: {
        title: 'ID',
        width: '2%'
      },
      ProformNumber: {
        title: '',
      },
      Amount: {
        title: ''
      },
      CreatedDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      CustomerName: {
        title: ''
      },
      ExpirementDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      StatusDesc: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    mode:"external",
  };

  settingsProduct = {
    columns: {
      ProductName: {
        title: ''
      },
      Description: {
        title: ''
      },
      LineAmount: {
        title: ''
      },
      LineTax: {
        title: ''
      },
      LineDiscount: {
        title: ''
      },
    },
    actions:{
      position:'right',
      columnTitle: "",
      add:false,
      delete:false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
    this._apicallService.Get("http://localhost:61232/api/project/GetBoardDetail",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resulttArray[0].Result == "Empty") {
            this.overviewData.projectName = '';
            this.overviewData.statusDesc = '';
            this.overviewData.startDate = '';
            this.overviewData.endDate = '';
            this.overviewData.CustomerName = '';
            this.overviewData.CustomerId= '';
            this.overviewData.Amount = "";
          }
        }
        else {
          var myDate = this.datePipe.transform(new Date,"yyyy-MM-dd");

          this.overviewData.projectName = resulttArray[0].BoardName;
          this.overviewData.statusDesc = resulttArray[0].StatusDesc
          this.overviewData.startDate = resulttArray[0].BeginDate.split(" ")[0];
          this.overviewData.endDate = resulttArray[0].EndDate.split(" ")[0];
          this.overviewData.CustomerName = resulttArray[0].CustomerName;
          this.overviewData.CustomerId = resulttArray[0].CustomerId;
          this.overviewData.Amount = resulttArray[0].Amount;

          var sd = resulttArray[0].BeginDate.split(" ")[0];
          var ed = resulttArray[0].EndDate.split(" ")[0];
          var diff=new Date(ed).getTime() - new Date(sd).getTime();
          this.allDays=Math.ceil(diff/(1000*3600*24));
          var diff2=new Date(ed).getTime() - new Date(myDate).getTime();
          
          if(diff2>0){
            this.remainingDays=Math.ceil(diff2/(1000*3600*24));
            if(this.remainingDays <= this.allDays)
            {
              this.remainingDaysValue=(this.allDays-this.remainingDays)/this.allDays*100;              
            }
            else
            {
              this.remainingDaysValue = 0;
            }
            var x = document.getElementById("days");
            x.style.display = "block";
          }

          if(resulttArray[0].PeriodId != 0)
          {
              this.subsArea = true;
              this._apicallService.Get("http://localhost:61232/api/project/GetSubsProform",this._avRoute.snapshot.params["id"]).subscribe(
              (result) => {
                let resulttArray = JSON.parse(JSON.stringify(result));
                if (resulttArray.length != 0 && resulttArray[0].Result) {
                  if (resulttArray[0].Result == "Session_Expired") {
                    this._router.navigate(['/auth']);
                    return;
                  }
                  else if (resulttArray[0].Result == "Empty") {
                    this.proformArea = false;
                  }
                }
                else 
                {
                  this.proformArea = true;
                  this.ProformList = resulttArray;
                  this.overviewData.Amount = resulttArray[0].TotalAmount;          
                }
              }
            )
          }
        }
      }
    ),
    this._apicallService.Get("http://localhost:61232/api/project/GetBoardTeam",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if (resulttArray[0].Result == "Empty") {
            this.team = "";
          }
        }
        else 
        {
          for(let i = 0; i < resulttArray.length; i++)
          {
            if(resulttArray[i].TeamName != "NoTeam")
            {
              this.team = this.team + resulttArray[i].TeamName + " ";
            }
                           
          }               
        }
      }
    ),
    this._apicallService.Get("http://localhost:61232/api/Task/GetBoardTask",this._avRoute.snapshot.params["id"])
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              this.TaskList = [];
            }
          }
          else {
            this.TaskList = result;
            this.allTask = this.TaskList.length;
            this.notCompletedTask = this.TaskList.filter(x=>x.StatusId != 5 && x.StatusId != 6 ).length;
            this.taskCountValue = 100 - ((this.notCompletedTask) / (this.allTask) * 100 );

          }         
      },

      )
  }

  getStatus()
  {
  this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length !=0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resulttArray[0].Result == "Empty")
        {
          this.statusList = [];
        }
      }
      else
      {
        for(var i = 0; i < resulttArray.length; i++ ){
          this.statusList.push({value:resulttArray[i].StatusDesc,title:resulttArray[i].StatusDesc});
          let _status = { label: resulttArray[i].StatusDesc, value: resulttArray[i].StatusId };
          this.StatusList.push(_status);
        }
        this.settings.columns.StatusDesc.filter.config.list = this.statusList;
        this.statusList.splice(0,1);
        this.settings = Object.assign({}, this.settings);
      }
    },
    )
  }

  getUsers()
  {
  this._apicallService.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {     
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if (resulttArray[0].Result == "Empty") {
          this.userList = [];
        }
      }
      else
      { 
        for(var i = 0; i < resulttArray.length; i++ ){
          this.userList.push({value:resulttArray[i].Fullname,title:resulttArray[i].Fullname});
          let _users = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
          this.allUsers.push(_users);
        }
        this.settings.columns.Fullname.filter.config.list = this.userList;
        this.userList.splice(0,1);
        this.settings = Object.assign({}, this.settings);
      }
    },
    )
  }

  getTeams()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
      (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.TeamList = [];
          }
        }
        else
        {
          for(var i = 0; i < resultArray.length; i++ ){
            let _team = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
            this.TeamList.push(_team);
          }
        }
      },
      )
  }

  getCosts()
  {
    this._apicallService.Get("http://localhost:61232/api/Cost/GetCostsForProject",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.costList = [];
          }
        }
        else
        {
          debugger;
          this.costList = resultArray;
          const billiableCost  = this.costList.filter(x=> x.IsBilliable == 1);
          const billedCost = this.costList.filter(x=> x.InvoiceId != 0 );
          const nonbilledCost = this.costList.filter(x=> x.InvoiceId == 0);
          let amountArray = {tlTotal : 0, dolarTotal : 0 , euroTotal : 0 , sterlinTotal : 0 , tlBilliable : 0 , dolarBilliable : 0 , euroBilliable : 0 , sterlinBilliable : 0 , tlBilled : 0 , dolarBilled: 0 , euroBilled : 0, sterlinBilled : 0 , tlNon : 0 , dolarNon : 0 , euroNon : 0 , sterlinNon : 0  };


          for(let i in resultArray)
          {
            if(resultArray[i].CurrencyTypeId == 1)
            {
               amountArray.tlTotal += Number(resultArray[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(resultArray[i].CurrencyTypeId == 2)
            {
              amountArray.dolarTotal += Number(resultArray[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(resultArray[i].CurrencyTypeId == 3)
            {
              amountArray.euroTotal += Number(resultArray[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(resultArray[i].CurrencyTypeId == 4)
            {
              amountArray.sterlinTotal += Number(resultArray[i].Amount.split(" ")[0].replace(".","")); 
            } 
          }

          for(let i in billiableCost)
          {
            if(billiableCost[i].CurrencyTypeId == 1)
            {
               amountArray.tlBilliable += Number(billiableCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billiableCost[i].CurrencyTypeId == 2)
            {
              amountArray.dolarBilliable += Number(billiableCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billiableCost[i].CurrencyTypeId == 3)
            {
              amountArray.euroBilliable += Number(billiableCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billiableCost[i].CurrencyTypeId == 4)
            {
              amountArray.sterlinBilliable += Number(billiableCost[i].Amount.split(" ")[0].replace(".","")); 
            }
          }
          for(let i in billedCost)
          {
            if(billedCost[i].CurrencyTypeId == 1)
            {
               amountArray.tlBilled += Number(billedCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billedCost[i].CurrencyTypeId == 2)
            {
              amountArray.dolarBilled += Number(billedCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billedCost[i].CurrencyTypeId == 3)
            {
              amountArray.euroBilled += Number(billedCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(billedCost[i].CurrencyTypeId == 4)
            {
              amountArray.sterlinBilled += Number(billedCost[i].Amount.split(" ")[0].replace(".","")); 
            } 
          }
          for(let i in nonbilledCost)
          {
            if(nonbilledCost[i].CurrencyTypeId == 1)
            {
               amountArray.tlNon += Number(nonbilledCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(nonbilledCost[i].CurrencyTypeId == 2)
            {
              amountArray.dolarNon += Number(nonbilledCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(nonbilledCost[i].CurrencyTypeId == 3)
            {
              amountArray.euroNon += Number(nonbilledCost[i].Amount.split(" ")[0].replace(".","")); 
            }
            else if(nonbilledCost[i].CurrencyTypeId == 4)
            {
              amountArray.sterlinNon += Number(nonbilledCost[i].Amount.split(" ")[0].replace(".","")); 
            }
          }

          this.totalExpenses = amountArray.tlTotal + " TL," + amountArray.dolarTotal + " $,"+amountArray.euroTotal + " €," + amountArray.sterlinTotal + " £" ;
          this.billiableExpenses = amountArray.tlBilliable + " TL," + amountArray.dolarBilliable + " $,"+amountArray.euroBilliable + " €," + amountArray.sterlinBilliable + " £" ;
          this.billedExpenses = amountArray.tlBilled + " TL," + amountArray.dolarBilled + " $,"+amountArray.euroBilled + " €," + amountArray.sterlinBilled + " £" ;
          this.nonInvoicedExpenses = amountArray.tlNon + " TL," + amountArray.dolarNon + " $,"+amountArray.euroNon + " €," + amountArray.sterlinNon + " £" ;
        }
      },
      )
  }

  getTaskType()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskType").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.taskTypeList = [];
            }
          }
          else
          { 
            this.taskTypeList = [];
            for(let i in resultArray)
            {
              let _types = { label: resultArray[i].TypeDesc, value: resultArray[i].TypeId };
              this.taskTypeList.push(_types);
            }
          }
      }
    )
  }

  getBoardsProduct()
  {
    this._apicallService.Get("http://localhost:61232/api/Project/GetProjectLines",this._avRoute.snapshot.params["id"]).subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.productList = [];
            }
          }
          else
          {
            for(let i in resultArray)
            {
              if(resultArray[i].ProductId == 0)
              {
                resultArray[i].ProductName = resultArray[i].LineName;
              }
            }
            this.productList = resultArray.slice();
          }
      }
    )
  }

  openCustomerData(id)
  {
    this._router.navigate(['/pages/customer/customer-overview/' + id]); 
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/project/add-edit-project' , this._avRoute.snapshot.params["id"]]);
  }

  addTask()
  {
    sessionStorage.setItem("taskRelation","project_"+ this._avRoute.snapshot.params["id"] );
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

 /* openDialogs(dialog: TemplateRef<any>, event)
  {
    let _today = new Date();

    this.taskData.Title = "";
    this.taskData.TaskDesc = "";
    this.taskData.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
    this.taskData.EndDate = "";
    this.taskData.ResponsibleId = 0;
    this.taskData.TeamId = 0;
    this.taskData.StatusId = 0;
    this.selectedResponsible = "";
    this.selectedStatus = 0;
    this.selectedType = 1;
    this.selectedTeam = 0;
    this.assigneeUsers = [];
    this.taskData.TaskTime = "";

    if( event == "no_event")
    {
      this.isDialogSaveOrDetail = "save";
      this.dialogService.open(dialog);
    }
    else
    {
      this.taskID =  event.data.TaskId;
      this.assigneeUsers = [];
      this.isDialogSaveOrDetail = "detail";

      let index = this.TaskList.findIndex(x => x.TaskId == event.data.TaskId);

      this.selectedType = this.TaskList[index].TypeId;
      this.taskData.TaskId = this.TaskList[index].TaskId;
      this.taskData.Title = this.TaskList[index].Title;
      this.taskData.TaskDesc = this.TaskList[index].TaskDesc;
      this.taskData.BeginDate = this.formatIsoDateToCrypted(this.TaskList[index].BeginDate.split(" ")[0]);
      this.taskData.EndDate = this.formatIsoDateToCrypted(this.TaskList[index].EndDate.split(" ")[0]);
      this.taskData.ResponsibleId = this.TaskList[index].ResponsibleId;
      this.taskData.TeamId = this.TaskList[index].TeamId;
      this.taskData.StatusId = this.TaskList[index].StatusId;
      this.taskData.StatusDesc = this.TaskList[index].StatusDesc;
      this.taskData.Fullname = this.TaskList[index].Fullname;
      this.selectedResponsible = this.TaskList[index].ResponsibleId;
      this.selectedStatus = this.TaskList[index].StatusId;
      this.selectedTeam = this.TaskList[index].TeamId;
      this.taskData.TaskTime = this.TaskList[index].TaskTime;

      this._apicallService.Get("http://localhost:61232/api/Task/GetTaskMembers", this.taskID).subscribe(
        (result) => {
          let _resultArray = JSON.parse(JSON.stringify(result));
          if (_resultArray.length != 0 && _resultArray[0].Result) {
            if (_resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(_resultArray[0].Result == "Empty")
            {
              this.assigneeUsers = [];
              this._copyassigneeUsers = [];
            }
          }
          else
          { 
            this.assigneeUsers = _resultArray.map(x=> x.UserId);
            this._copyassigneeUsers = this.assigneeUsers.slice();
          }
        }
      )

      this.dialogService.open(dialog);
    }
  }

  addTask(ref)
  {
    this.loading = true;
    var now = new Date().toISOString();
    var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
    var endDate = <HTMLInputElement> document.getElementById("EndDate");
    let task={
      ParentId:0,
      Title: $("#taskTitle").val(),
      TaskDesc: $("#taskDesc").val(),
      TypeId: this.selectedType,
      CreatedDate: now.split("T")[0],
      ResponsibleId: this.assigneeUsers[0] ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId: this._avRoute.snapshot.params["id"],
      ContractId:1,
      BeginDate:beginDate.value,
      EndDate:endDate.value,
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: this.selectedTeam,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
      TaskTime : $("#time").val(),
    }

     if(task.Title == "" || task.EndDate == "" || task.BeginDate == "" )
     {
        this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
        this.loading = false;
     }
     else
     {
      this._apicallService.Add("http://localhost:61232/api/Task/Create",task) 
      .subscribe((result) => {
         let resultArray = JSON.parse(JSON.stringify(result));    
         if (resultArray[0].Result == "Session_Expired") {
           this.loading = false;
           this._router.navigate(['/auth']);
           return;
         }  
         else
         {
          for(let i in this.assigneeUsers)
          {
            let taskmember = { TaskId : result[0].Result , UserId : this.assigneeUsers[i] } ;
            this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember",taskmember)
              .subscribe((resultt) => {          
            }                  
            )
          }

          this.loading = false;
          ref.close();
          this._router.navigate(['/pages/task/tasks-panel',task.BoardId]);
         }
      },) 
     }
  }

  editTask(ref)
  {
    this.loading = true;
    var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
    var endDate = <HTMLInputElement> document.getElementById("EndDate");
    let task={
      TaskId: this.taskData.TaskId,
      ParentId:0,
      Title: $("#taskTitle").val(),
      TaskDesc: $("#taskDesc").val(),
      TypeId: this.selectedType,
      CreatedDate: "fromTasksComponent",
      ResponsibleId: this.assigneeUsers[0],
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this._avRoute.snapshot.params["id"],
      ContractId:1,
      BeginDate:beginDate.value,
      EndDate:endDate.value,
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: this.selectedTeam,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
      TaskTime : $("#time").val(),
    }

    if(task.Title == "" || task.EndDate == "" || task.BeginDate == ""  )
     {
      this.makeToast(this.status,this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.MissingFieldsText);
      this.loading = false;
     }
     else
     {
      this._apicallService.Add("http://localhost:61232/api/Task/Update", task) 
      .subscribe((result) => {
        for(let i in this._copyassigneeUsers)
        {
          if(this.assigneeUsers.findIndex(x => x == this._copyassigneeUsers[i]) == -1)
          {
            let _taskmember = { TaskId: this.taskID, UserId: this._copyassigneeUsers[i] }; 
            this._apicallService.Add("http://localhost:61232/api/Task/DeleteTaskMembers", _taskmember).subscribe(
              (result) =>{
              }
            )
          }
        }

        //----------- yeni eklenen varsada onları ekle
        for(let j in this.assigneeUsers)
        {
          if(this._copyassigneeUsers.findIndex(x => x == this.assigneeUsers[j]) == -1)
          {
            let _taskmember = { TaskId: this.taskID, UserId: this.assigneeUsers[j] };   
            this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
              (result) =>{
              }
            )
          }
        } 

      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        ref.close();
        if(task.BoardId)
        {
          this.loading = false;
          this._router.navigate(['/pages/task/tasks-panel',task.BoardId]);
        }
        else
        {

        }
      }          
      }
    )
     } 
  }

  addSubTaskDialog(dialog: TemplateRef<any>)
  {
    this.dialogService.open(dialog);
    this.getSubTask();
  }

  getSubTask()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/GetSubTask/",this.taskID).subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.SubTaskList = [];
            this.subTaskEmpty = "true";
          }
        }
        else
        {
          this.SubTaskList=result;
          this.subTaskEmpty = "false";
        }
    },
    )
  }

  addSubTask()
  {
    let task={
      TaskId: this.taskID,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: 0,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    if(task.TaskDesc == "")
    {
      return;
    }
    else
    {
      this._apicallService.Add("http://localhost:61232/api/Task/CreateSubTask", task) 
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this.getSubTask();
          $("#subs").val("");
        }          
        },
      )
    }
    
  }

  editSubTask(taskId)
  {
    var newSubTaskDesc = $("#subTaskk_"+taskId).val();
    let editTask={
      TaskId: taskId,
      ParentId:0,
      Title: newSubTaskDesc,
      TaskDesc: "newSubTaskDesc",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId: 1,
      EndCheckListId: 1,
      BoardId: 0,
      ContractId: 1,
      BeginDate: "",
      EndDate: "",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId: 0,
      LeadId: 0,
      InvoiceId: 0,
    };
    this._apicallService.Add("http://localhost:61232/api/Task/UpdateSubTask", editTask) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)
  }


  completeSubTask(taskId)
   {
    this._apicallService.Deactive("http://localhost:61232/api/Task/CompleteSubTask", taskId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getSubTask();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
     
     }, )
   }

   selectSubAssignee(id)
   {
     this.assignTaskId = id;
   }

   subAssignTo(event)
   {
     var userId = event.value;
     let task={
      TaskId: this.assignTaskId,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: userId ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:0,
      ContractId:1,
      BeginDate:"",
      EndDate:"",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    this._apicallService.Add("http://localhost:61232/api/Task/AssigneeSubTask", task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)

      this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteSubTaskMember", this.assignTaskId).subscribe(
        (result) =>{
        }
      )

      let _taskmember = { TaskId: this.assignTaskId, UserId: userId };   
      this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
        (result) =>{
        }
      )

  }*/

  editProform(event)
  {
    var id = `${event.data.ProformId}`;
    this._router.navigate(['/pages/proforms/add-edit-proform',id]);
  }

  /*getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/BoardComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : this._avRoute.snapshot.params["id"],
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }*/

  formatIsoDateToCrypted(date)
  {
    var splittedDate = date.split("-");
    let val = "";
    if (splittedDate[1] == "01" || splittedDate[1] == "1")
    {
        val = "Jan";
    }
    else if (splittedDate[1] == "02" || splittedDate[1] == "2")
    {
        val = "Feb";
    }
    else if (splittedDate[1] == "03" || splittedDate[1] == "3" )
    {
        val = "Mar";
    }
    else if (splittedDate[1] == "04" || splittedDate[1] == "4")
    {
        val = "Apr";
    }
    else if (splittedDate[1] == "05" || splittedDate[1] == "5" )
    {
        val = "May";
    }
    else if (splittedDate[1] == "06" || splittedDate[1] == "6" )
    {
        val = "Jun";
    }
    else if (splittedDate[1] == "07" || splittedDate[1] == "7" )
    {
        val = "Jul";
    }
    else if (splittedDate[1] == "08" || splittedDate[1] == "8" )
    {
        val = "Aug";
    }
    else if (splittedDate[1] == "09" || splittedDate[1] == "9" )
    {
        val = "Sep";
    }
    else if (splittedDate[1] == "10")
    {
        val = "Oct";
    }
    else if (splittedDate[1] == "11")
    {
        val = "Nov";
    }
    else if (splittedDate[1] == "12")
    {
        val = "Dec";
    }

    return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}
