import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardsNoteComponent } from './boards-note.component';

describe('BoardsNoteComponent', () => {
  let component: BoardsNoteComponent;
  let fixture: ComponentFixture<BoardsNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardsNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardsNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
