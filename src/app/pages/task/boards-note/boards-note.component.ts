import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { NbDialogService, NbToastrService, NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'boards-note',
  templateUrl: './boards-note.component.html',
  styleUrls: ['./boards-note.component.scss']
})
export class BoardsNoteComponent implements OnInit {

  sysLanguages: any;
  emptyComment = false;
  CommentList = [];

  //pagination 
  p: number = 1;
  collection = [];

  constructor(private datePipe:DatePipe,private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private titleService: Title , private dialogService: NbDialogService, private toastrService: NbToastrService) 
  {
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));

    this.tabs[0].title = this.sysLanguages.BoTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.BoTabsProjectTasksText;
    this.tabs[2].title = this.sysLanguages.BoardsNoteText;

    this.titleService.setTitle(this.sysLanguages.BoardsNoteText + " - Crmplus");

    this.getComments();

  }

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/task/board-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/task/tasks-panel', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'ion-ios-copy-outline',
      responsive: true,
      route: ['/pages/task/boards-note', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
  }

  getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/BoardComments",this._avRoute.snapshot.params["id"])
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }

  saveComment() {
    let jarray={
      CommentId:0,
      TaskId : 0,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : this._avRoute.snapshot.params["id"],
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateBoardComment",jarray)
    .subscribe((result) => {
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }

  openEditUrl()
  {
    this._router.navigate(['/pages/project/add-edit-project' , this._avRoute.snapshot.params["id"]]);
  }

}
