import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbDatepickerModule } from '@nebular/theme';
import { TasksComponent } from './tasks/tasks.component';
import { TasksPanelComponent } from './tasks-panel/tasks-panel.component';
import { BoardOverviewComponent } from './board-overview/board-overview.component';
import { BoardsNoteComponent } from './boards-note/boards-note.component';
import { AddEditTaskComponent } from "./add-edit-task/add-edit-task.component";

const routes: Routes = [
  {
    path: 'tasks',
    component: TasksComponent,
  },
  {
    path: 'tasks/:id',
    component: TasksComponent,
  },
  {
    path: 'tasks-panel/:id',
    component: TasksPanelComponent,
  },
  {
    path: 'board-overview/:id',
    component: BoardOverviewComponent,
  },
  {
    path: 'boards-note/:id',
    component: BoardsNoteComponent,
  },
  {
    path: 'boards-note/:id',
    component: BoardsNoteComponent,
  },
  {
    path: 'add-edit-task',
    component: AddEditTaskComponent,
  },
  {
    path: 'add-edit-task/:id',
    component: AddEditTaskComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), NbDatepickerModule.forRoot()],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
