import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TaskRoutingModule } from './task-routing.module';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbDialogModule, NbContextMenuModule, NbTabsetModule, NbRouteTabsetModule, NbProgressBarModule, NbListModule, NbPopoverModule, NbCheckboxModule, } from '@nebular/theme';
import { TasksComponent } from './tasks/tasks.component';
import { TasksPanelComponent } from './tasks-panel/tasks-panel.component';
import { BoardOverviewComponent } from './board-overview/board-overview.component';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { BoardsNoteComponent } from './boards-note/boards-note.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddEditTaskComponent } from './add-edit-task/add-edit-task.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { ChartModule } from 'angular2-chartjs';
import { TableModule } from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {ContextMenuModule} from 'primeng/contextmenu';

@NgModule({
  declarations: [TasksComponent, TasksPanelComponent, BoardOverviewComponent, BoardsNoteComponent, AddEditTaskComponent],
  imports: [
    CommonModule,
    TaskRoutingModule,
    Ng2SmartTableModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbDatepickerModule,
    NbTabsetModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbContextMenuModule,
    NbRouteTabsetModule,
    DropdownModule,
    MultiSelectModule,
    NbListModule,
    NbPopoverModule,
    NgxPaginationModule,
    NgxEchartsModule, 
    ChartModule,
    TableModule,
    CalendarModule,
    ContextMenuModule,
    NbCheckboxModule,
  ]
})
export class TaskModule { }
