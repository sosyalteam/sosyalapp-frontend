import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { NbDialogService, NbMenuService, NbDialogRef } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';
import $ from 'jquery';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'tasks-panel',
  templateUrl: './tasks-panel.component.html',
  styleUrls: ['./tasks-panel.component.scss'],
  providers:[ApicallService,HttpModule],
})
export class TasksPanelComponent implements OnInit {
  copyTaskList = []; 
  allTaskList = [];
  statusListForFilters = [];
  userListForFilters = [];
  teamListForFilters = [];
  colors: any;
  operations: any;
  isFilterAreaOpen = false;
  boardID: number;
  sysLanguages : any;
  title: string = "";
  date = new Date().toISOString();
  TeamList = [];
  UserList = [];
  StatusList = [];
  CommentList = [];
  TaskID: any;
  TaskArray = {Title : ' ', TaskDesc : ' ', BeginDate: ' ', EndDate : ' ', TaskTime : ''};
  selectedUser: any;
  selectedTeam: any;
  selectedStatus: any;
  selectedType: any;
  selectedComment: any;
  assigneeUsers = [];
  _copyassigneeUsers = [];
  selectedStatusForFilter = "all";
  selectedTeamForFilter = "all";
  selectedUserForFilter = "all";
  taskTypeList = [];
  isCheckedComplete= 0;
  subTasksID : any;
  SubTaskList :any;
  subTaskEmpty= false;
  showList = false;
  TaskList: any;
  loading = false;
  emptyComment = false;

  //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';
  
  //subTasks
  content2 = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast() {
    this.showToast(this.status, this.title1, this.content);
  }
  makeToastSubTasks()
  {
    this.showToast(this.status,this.title1,this.content2);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  //

  constructor(private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private _router: Router, private dialogService: NbDialogService, private nbMenuService: NbMenuService,private toastrService: NbToastrService, private titleService: Title) 
  {    
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.TpTaskPanelText + " - Crmplus");

    this.tabs[0].title = this.sysLanguages.BoTabsProjectOverviewText;
    this.tabs[1].title = this.sysLanguages.BoTabsProjectTasksText;

    this.title1 = this.sysLanguages.ErrorMessage;
    this.content = this.sysLanguages.ErrorMessage1;
    this.content2 = this.sysLanguages.SubTasksErrorText;

    this.settings.columns.Title.title=this.sysLanguages.TasksTableTitleText;
    this.settings.columns.TaskDesc.title=this.sysLanguages.TasksTableTascDescText;
    this.settings.columns.StatusDesc.title=this.sysLanguages.TasksTableStatusText;
    this.settings.columns.BeginDate.title=this.sysLanguages.TasksTableStartDateText;
    this.settings.columns.EndDate.title=this.sysLanguages.TasksTableEndDateText;
    this.settings.columns.Fullname.title=this.sysLanguages.TasksTableAssignText;


    
    if (this._avRoute.snapshot.params["id"]) {
      this.boardID = this._avRoute.snapshot.params["id"];
    }

    /*this.colors = [
    { title: this.sysLanguages.Red },
    { title: this.sysLanguages.Yellow },
    { title: this.sysLanguages.Green },
    { title: this.sysLanguages.Purple },
    { title: this.sysLanguages.Blue },
    ];*/

    this.operations =  [
    { title: this.sysLanguages.TpContextMenuSwitchText},
    { title: this.sysLanguages.TpContextMenuOpenFılterText},
    { title: this.sysLanguages.TpContextMenuCloseFılterText},
    ];

    this.getTeams();
    this.getUsers();
    this.getStatus();
    this.getBoardTasks();
    this.getTaskTypes();
  }
  settings = {
    columns: {
      TaskId: {
        title: 'ID'
      },
      Title: {
        title: ''
      },
      TaskDesc: {
        title: ''
      },
      StatusDesc: {
        title: '',
      },
      BeginDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },       
      },
      EndDate: {
        title: '',
        valuePrepareFunction: (value) => {
          return value.toString().split(" ")[0];
        },
      },
      Fullname: {
        title: '',
      },
    },
    actions:{
      position:'right',
      edit:false,
      delete:false,
      add:false,
    },

    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },

    mode: 'external',
  };

  tabs: any[] = [
    {
      title: '',
      icon: 'nb-grid-b-outline',
      route: ['/pages/task/board-overview', this._avRoute.snapshot.params["id"]],
    },
    {
      title: '',
      icon: 'nb-checkmark-circle',
      responsive: true,
      route: ['/pages/task/tasks-panel', this._avRoute.snapshot.params["id"]],
    },
  ];

  ngOnInit() {
    // -------- task renkleri context menuden alma işlemi
    /*this.nbMenuService.onItemClick()
    .pipe(
      filter(({ tag }) => tag === 'color-menu'),
      map(({ item: { title } }) => title),
    )
    .subscribe(
      title => this.changeTaskColor(title)
      );*/

    //--- Opertions menu
    this.nbMenuService.onItemClick()
    .pipe(
      filter(({ tag }) => tag === 'operation-menu'),
      map(({ item: { title } }) => title),
    )
    .subscribe(
      title => this.OperationMenuFunc(title)
    );

  }

  goToList()
  {
    this.showList = true;
    //this._router.navigate(['/pages/task/tasks',this.boardID]);
  }
  goToKanban()
  {
    this.showList = false;
  }

  getBoardTasks()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/GetBoardTask",this.boardID)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if (resulttArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              this.TaskList = [];
            }
          }
          else 
          {
            this.TaskList = [];
            this.copyTaskList = [];
            
            this.TaskList = resulttArray.filter(x => x.ParentId == x.TaskId);
            this.copyTaskList = resulttArray.filter(x => x.ParentId == x.TaskId);
            this.allTaskList = resulttArray.slice();

            var flags = [];
            this.userListForFilters=[]; // bunu her seferinde boşaltıyorum çünkü her kolonun taskına göre tkrar ettiği için yukarıda, içini boşalttım her seferinde ta ki ensonki tasklistin hali oluncaya kadar sonra hepsini filtreliyorum. 
            this.userListForFilters.push({ label: this.sysLanguages.AddTaskUsersText, value: "all" });
            for( let k = 0; k < this.TaskList.length; k++ ) {
              if( flags[this.TaskList[k].Fullname]) continue;
              flags[this.TaskList[k].Fullname] = true;
              if(this.TaskList[k].Fullname != "NoUser" )
              {
                this.userListForFilters.push({ label: this.TaskList[k].Fullname, value: this.TaskList[k].ResponsibleId });
              }
            }

            flags = [];
            this.teamListForFilters = [];
            this.teamListForFilters.push({ label: this.sysLanguages.TpAddTaskDialogTeamText, value: "all" });
            for( let k = 0; k < this.TaskList.length; k++ ) {
              if( flags[this.TaskList[k].TeamName]) continue;
              flags[this.TaskList[k].TeamName] = true;
              this.teamListForFilters.push({ label: this.TaskList[k].TeamName, value: this.TaskList[k].TeamId });
            }
          }         
      },

      )
  }

  OperationMenuFunc(operation)
  {
    if(operation == this.sysLanguages.TpContextMenuOpenFılterText)
    {
      this.isFilterAreaOpen = true;
    }
    else if(operation == this.sysLanguages.TpContextMenuCloseFılterText)
    {
      this.isFilterAreaOpen = false;
    }
    else if (operation == this.sysLanguages.TpContextMenuSwitchText)
    {
      this._router.navigate(['/pages/task/tasks',this.boardID]);
    }
  }

  changeTaskColor(color, _taskId)
  {
    if(color == "red")
    {
      color = "linear-gradient(to right, #ff4ca6, #ff4c6a)";
    }
    else if(color == "yellow")
    {
      color = "linear-gradient(to right, #ffcc00, #ffa100)";
    }
    else if(color == "blue")
    {
      color = "linear-gradient(to right, #4cc4ff, #4ca6ff)";
    }
    else if(color == "purple")
    {
      color = "linear-gradient(to right, #b57fff, #8a7fff)";
    }
    else if(color == "green")
    {
      color = "linear-gradient(to right, #40dcb2, #40dc7e)";
    }
    else if(color == "white")
    {
      color = "linear-gradient(to right, #FFFFFF, #FFFFFF)";
    }

    let task={ TaskId : _taskId, ColorCode : color }
    this._apicallService.Update("http://localhost:61232/api/Task/ChangeTaskColor", parseInt(_taskId.toString()), task).subscribe((result) => {
      var element = document.getElementById("task_desc_" + _taskId);
      element.style.backgroundImage = color;
    })
  }

  /*getTaskIdForColor(event,taskid)
  {
    this.TaskID=taskid;
    $("#taskIdForColor").val(event.target.id.split('_')[2]);
  }*/

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropTask(event)
  {
    var data = event.dataTransfer.getData("text2");

    // --- eğer kolon içindeki diğer saçma yerlere sürüklenirse uyarı çıkarma komutları
    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("task_desc"))
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addtaskbtnn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "cardDetailButton btn-info")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc input-md input-rectangle")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "ion-android-delete")
    {
      //window.alert("You can't drop here");
    }
    else
    {
      var taskID = data.split('_')[1];

      let _task = {TaskId: data.split('_')[1], statusID: event.target.id.split('_')[1]};

      this._apicallService.Update("http://localhost:61232/api/Task/ChangeTaskStatus", taskID, _task).subscribe((result) => {
        this.getBoardTasks();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  dragTask(event)
  {
     event.dataTransfer.setData("text2", event.target.id);
  }

  getTaskTypes()
  {
    this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskType").subscribe(
      (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if (resultArray[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resultArray[0].Result == "Empty")
            {
              this.taskTypeList = [];
            }
          }
          else
          { 
            this.taskTypeList = [];
            for(let i in resultArray)
            {
              let _types = { label: resultArray[i].TypeDesc, value: resultArray[i].TypeId };
              this.taskTypeList.push(_types);
            }
          }
      }
    )
  }

  /*openDialogComment(dialog: TemplateRef<any>)
  {
    this.dialogService.open(dialog);
  }*/

  filterTeam(team_id)
  {
    this.selectedTeamForFilter = team_id;
    if(team_id == "all")
    {
      if(this.selectedUserForFilter != "all")
        this.filterUser(this.selectedUserForFilter);

      if( this.selectedStatusForFilter != "all")
        this.filterStatus( this.selectedStatusForFilter);
        
      if(this.selectedStatusForFilter == "all" && this.selectedUserForFilter == "all")
        this.TaskList = this.copyTaskList.slice();
    }
    else
    {
      this.TaskList = this.copyTaskList.slice();

      if(this.selectedUserForFilter != "all" && this.selectedStatusForFilter == "all")
        this.filterGeneral("all", "all", this.selectedUserForFilter);

      else if(this.selectedUserForFilter == "all" && this.selectedStatusForFilter != "all")
        this.filterGeneral(this.selectedStatusForFilter, "all", "all");

      else if(this.selectedUserForFilter != "all" && this.selectedStatusForFilter != "all")
      {
        this.filterGeneral(this.selectedStatusForFilter, "all", this.selectedUserForFilter);
      }
    
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].TeamId != team_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].TeamId == this.copyTaskList[i].TeamId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
  }

  filterUser(user_id)
  {
    this.selectedUserForFilter = user_id;
    if(user_id == "all")
    {
      this.TaskList = this.copyTaskList.slice();

      if(this.selectedTeamForFilter != "all" && this.selectedStatusForFilter == "all")
        this.filterTeam(this.selectedTeamForFilter);

      else if(this.selectedTeamForFilter == "all" && this.selectedStatusForFilter != "all")
        this.filterStatus(this.selectedStatusForFilter);

      else if( this.selectedTeamForFilter != "all" && this.selectedStatusForFilter != "all")
      {
        this.filterTeam(this.selectedTeamForFilter);
        this.filterStatus(this.selectedStatusForFilter);
      }
    }
    else
    {
      this.TaskList = this.copyTaskList.slice();

      if(this.selectedTeamForFilter != "all" && this.selectedStatusForFilter == "all")
        this.filterGeneral("", this.selectedTeamForFilter, "all");

      else if(this.selectedTeamForFilter == "all" && this.selectedStatusForFilter != "all")
        this.filterGeneral(this.selectedStatusForFilter, "all", "all");

      else if(this.selectedTeamForFilter != "all" && this.selectedStatusForFilter != "all")
      {
        this.filterGeneral(this.selectedStatusForFilter, this.selectedTeamForFilter, "all");
      }
    
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].ResponsibleId != user_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].ResponsibleId == this.copyTaskList[i].ResponsibleId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
  }

  filterStatus(status_id)
  {
    this.selectedStatusForFilter = status_id;
    if(status_id == "all")
    {
      if(this.selectedTeamForFilter != "all")
        this.filterTeam(this.selectedTeamForFilter);

      if(this.selectedUserForFilter != "all")
        this.filterUser(this.selectedUserForFilter);

      if(this.selectedUserForFilter == "all" && this.selectedTeamForFilter == "all")
        this.TaskList = this.copyTaskList.slice();
    }
    else
    {
      this.TaskList = this.copyTaskList.slice();

      if(this.selectedTeamForFilter != "all" && this.selectedUserForFilter == "all")
        this.filterGeneral("all", this.selectedTeamForFilter, "all");

      else if(this.selectedTeamForFilter == "all" && this.selectedUserForFilter != "all")
        this.filterGeneral("all", "all", this.selectedUserForFilter);

      else if(this.selectedTeamForFilter != "all" && this.selectedUserForFilter != "all")
      {
        this.filterGeneral("all", this.selectedTeamForFilter, this.selectedUserForFilter);
      }
    
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].StatusId != status_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].StatusId == this.copyTaskList[i].StatusId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
  }

  filterGeneral(status_id, team_id, user_id)
  {
    this.TaskList = this.copyTaskList.slice();
    if(status_id != "all")
    {
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].StatusId != status_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].StatusId == this.copyTaskList[i].StatusId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
    if(team_id != "all")
    {
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].TeamId != team_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].TeamId == this.copyTaskList[i].TeamId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
    if(user_id != "all")
    {
      for(let i = 0; i < this.copyTaskList.length; i++)
      {
        if(this.copyTaskList[i].ResponsibleId != user_id)
        {
          for(let j = 0; j < this.TaskList.length; j++)
          {
            if(this.TaskList[j].ResponsibleId == this.copyTaskList[i].ResponsibleId)
            {
              this.TaskList.splice(j,1);
            }
          }
        }
      }
    }
  }

  addTask()
  {
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

  /*openDialogAddTask(dialog: TemplateRef<any>,statusId)
  {
    let _today = new Date();

    this.TaskArray.Title ="";
    this.TaskArray.TaskDesc = "";
    this.TaskArray.BeginDate = this.formatIsoDateToCrypted(_today.toISOString().split("T")[0]);
    this.TaskArray.EndDate="";
    this.selectedComment = "";
    this.selectedStatus = "";
    this.selectedTeam = "";
    this.selectedUser = "";
    this.selectedType = "";
    this.TaskArray.TaskTime = "";

    if(statusId == 0)
    {
      this.selectedStatus = 0;
    }
    else if(statusId == 1)
    {
      this.selectedStatus = 1;
    }
    else if(statusId == 2)
    {
      this.selectedStatus = 2;
    }
    else if(statusId == 3)
    {
      this.selectedStatus = 3;
    }
    else if(statusId == 4)
    {
      this.selectedStatus = 4;
    }
    else if(statusId == 5)
    {
      this.selectedStatus = 5;
    }
    else if(statusId == 6)
    {
      this.selectedStatus = 6;
    }

    this.assigneeUsers = [];
    this.title=this.sysLanguages.AddTaskHeaderText;

    this.dialogService.open(dialog);
  }

  openDialogEditTask(dialog: TemplateRef<any>, taskId)
  {
    this.assigneeUsers = [];

    let index = this.TaskList.findIndex(x => x.TaskId == taskId);

    this.TaskArray.Title = this.TaskList[index].Title;
    this.TaskArray.TaskDesc = this.TaskList[index].TaskDesc;
    this.TaskArray.BeginDate = this.formatIsoDateToCrypted(this.TaskList[index].BeginDate.split(" ")[0]);
    this.TaskArray.EndDate = this.formatIsoDateToCrypted(this.TaskList[index].EndDate.split(" ")[0]);
    this.selectedTeam = this.TaskList[index].TeamId;
    this.selectedStatus = this.TaskList[index].StatusId;
    this.selectedType = this.TaskList[index].TypeId;
    this.selectedUser = this.TaskList[index].ResponsibleId;
    this.TaskArray.TaskTime = this.TaskList[index].TaskTime;

    this._apicallService.Get("http://localhost:61232/api/Task/GetTaskMembers", taskId).subscribe(
      (result) => {
        let _resultArray = JSON.parse(JSON.stringify(result));
        if (_resultArray.length != 0 && _resultArray[0].Result) {
          if (_resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(_resultArray[0].Result == "Empty")
          {
            this.assigneeUsers = [];
            this._copyassigneeUsers = [];
          }
        }
        else
        { 
          this.assigneeUsers = _resultArray.map(x=> x.UserId);
          this._copyassigneeUsers = this.assigneeUsers.slice();
        }
      }
    )
    this.TaskID=taskId;
    this.title=this.sysLanguages.AddTaskEditHeaderText;
    this.dialogService.open(dialog);
    var x = document.getElementById("commentButton");
    x.style.display="inline-block";
    var y=document.getElementById("commentDiv");
    y.style.display="inline-block";
    var z=document.getElementById("deleteButton");
    z.style.display="inline-block";
    this.getComments();
    

  }

  saveComment() {
    this.loading = true;
    let jarray={
      CommentId:0,
      TaskId: this.TaskID,
      OwnerId:0,
      CommentText: $("#comment").val(),
      FileURI: $("#FileUri").val(),
      BoardId : this.boardID,
      CustomerId : 0,
      LeadId : 0,
      ProposalId : 0,
      ProformId : 0,
      InvoiceId : 0 
    }
    this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskComment",jarray)
    .subscribe((result) => {
      this.loading = false;
      this.getComments();
      $("#comment").val("");
      $("#FileUri").val("");
      }          
    )
  }

  deleteComment(commentId)
  {
    this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTaskComment/", commentId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getComments();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }   
     })
  }*/

 /* save(ref) {
    this.loading = true;
      if (this.title == this.sysLanguages.AddTaskHeaderText) {
        let task ;
        var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
        var endDate = <HTMLInputElement> document.getElementById("EndDate");
        task={
          TaskId: 0,
          ParentId:0,
          Title: $("#taskTitle").val(),
          TaskDesc: $("#taskDesc").val(),
          TypeId:this.selectedType,
          CreatedDate: this.date.split("T")[0],
          ResponsibleId: this.assigneeUsers[0] ,
          BeginCheckListId:1,
          EndCheckListId:1,
          BoardId:this.boardID,
          ContractId:1,
          BeginDate:beginDate.value,
          EndDate:endDate.value,
          StatusId: this.selectedStatus,
          CreatorId: 0,
          TeamId: this.selectedTeam,
          CustomerId:0,
          LeadId:0,
          InvoiceId:0,
          TaskTime : $("#time").val(),
        }
         if(task.ResponsibleId=="")
         {
           task.ResponsibleId=0;           
         }
         if(task.Title == "" || task.EndDate == "" || task.BeginDate == "" || task.TeamId == "" )
         {
           this.makeToast();
           this.loading = false;
         }
         else
         {
          this._apicallService.Add("http://localhost:61232/api/Task/Create",task) 
          .subscribe((result) => {
            for(let i in this.assigneeUsers)
            {
              let taskmember = { TaskId : result[0].Result , UserId : this.assigneeUsers[i] };
              this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember",taskmember)
               .subscribe((resultt) => {          
             }                  
             )
            }
            let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }  
              else
              {
               this.loading = false;
               ref.close();
               this.getBoardTasks();
               this._router.navigate(['/pages/task/tasks-panel',task.BoardId]);
              }                    
          },) 
         }       
       }
       else if (this.title == this.sysLanguages.AddTaskEditHeaderText) {
        let task;
        var beginDate = <HTMLInputElement> document.getElementById("BeginDate");
        var endDate = <HTMLInputElement> document.getElementById("EndDate");
        task={
          TaskId: this.TaskID,
          ParentId:0,
          Title: $("#taskTitle").val(),
          TaskDesc: $("#taskDesc").val(),
          TypeId:this.selectedType,
          CreatedDate: "fromTasksComponent",
          ResponsibleId: this.assigneeUsers[0] ,
          BeginCheckListId:1,
          EndCheckListId:1,
          BoardId:this.boardID,
          ContractId:1,
          BeginDate:beginDate.value,
          EndDate:endDate.value,
          StatusId: this.selectedStatus,
          CreatorId: 0,
          TeamId: this.selectedTeam,
          CustomerId:0,
          LeadId:0,
          InvoiceId:0,
          TaskTime : $("#time").val(),
        }
         if(task.ResponsibleId=="")
         {
           task.ResponsibleId=0;           
         }
         if(task.Title == "" || task.EndDate == "" || task.BeginDate == "" || task.TeamId == ""   )
         {
           this.makeToast();
           this.loading = false;
         }
         else
         {
          this._apicallService.Add("http://localhost:61232/api/Task/Update", task) 
          .subscribe((result) => {
            let resultArray = JSON.parse(JSON.stringify(result));

            for(let i in this._copyassigneeUsers)
            {
              if(this.assigneeUsers.findIndex(x => x == this._copyassigneeUsers[i]) == -1)
              {
                let _taskmember = { TaskId: this.TaskID, UserId: this._copyassigneeUsers[i] }; 
                this._apicallService.Add("http://localhost:61232/api/Task/DeleteTaskMembers", _taskmember).subscribe(
                  (result) =>{
                  }
                )
              }
            }

            //----------- yeni eklenen varsada onları ekle
            for(let j in this.assigneeUsers)
            {
              if(this._copyassigneeUsers.findIndex(x => x == this.assigneeUsers[j]) == -1)
              {
                let _taskmember = { TaskId: this.TaskID, UserId: this.assigneeUsers[j] };   
                this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
                  (result) =>{
                  }
                )
              }
            }  
            
            this.loading = false;
            ref.close();
            this.getBoardTasks();
            this._router.navigate(['/pages/task/tasks-panel', task.BoardId]);

            }
            )
         } 
       }
  }

  deleteTask(ref)
  {
    var ans = confirm(this.sysLanguages.TpTaskDeleteText +this.TaskID );
    if (ans) {
      this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteTask", this.TaskID ).subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if(resultArray[0].Result == "True")
        {
          this.getBoardTasks();
          ref.close();
        }
        else if (resultArray[0].Result == "False")
        {
          window.alert(this.sysLanguages.TpTaskDeleteWarningText)
          ref.close();
        }
        else if(resultArray[0].Result == "Session_Expired")
        {
          this._router.navigate(['/auth']);
          return;
        }
      
      },)
    }

  }

  cancelComment(ref) {
    ref.close();
  }*/

  getUsers()
  {
  this._apicallService.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.UserList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _user = { label: resultArray[i].Fullname, value: resultArray[i].UserId };
            this.UserList.push(_user);
          }
        }
    },
    )
  }

  getTeams()
  {
  this._apicallService.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.TeamList = [];
          }
        }
        else
        {
          for(let i in resultArray)
          {
            let _team = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
            this.TeamList.push(_team);
          }
        }
    },
    )
  }

  getStatus()
  {
  this._apicallService.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {        
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.StatusList = [];
          }
        }
        else
        {
          this.statusListForFilters=[];
          this.statusListForFilters.push({ label: this.sysLanguages.TpAddTaskDialogStatusText, value: "all" });
          for(let i in resultArray)
          {
            if(resultArray[i].StatusId == 0)
            {
              let _status = { label: this.sysLanguages.TasksStatusNotStarted, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 1)
            {
              let _status = { label: this.sysLanguages.TasksStatusWaiting, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 2)
            {
              let _status = { label: this.sysLanguages.TasksStatusContinues, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 3)
            {
              let _status = { label: this.sysLanguages.TasksStatusTest, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 4)
            {
              let _status = { label: this.sysLanguages.TasksStatusReturnWait, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 5)
            {
              let _status = { label: this.sysLanguages.TasksStatusCompleted, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
            else if(resultArray[i].StatusId == 6)
            {
              let _status = { label: this.sysLanguages.TasksStatusCanceled, value: resultArray[i].StatusId };
              this.StatusList.push(_status);
              this.statusListForFilters.push(_status);
            }
          }
        }
    },
    )
  }
  
  /*getComments()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/TaskComments",this.TaskID)
    .subscribe((result)=>
    {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          this.CommentList = [];
          this.emptyComment = true;
        }
      }
      else
      {
        this.CommentList = resultArray;
        this.emptyComment = false;
      }
    })
  }*/

  mouseenter(taskId)
  {
    var x =document.getElementById("markComplete_"+taskId);
    x.style.display="block";
  }

  mouseleave(taskId)
  {
    var x =document.getElementById("markComplete_"+taskId);
    x.style.display="none";
  }

  markComplete(taskId)
  {
    let task={ TaskId : taskId, TaskComplete : "0.5", StatusId : 5 }
    this._apicallService.Update("http://localhost:61232/api/Task/TaskComplete", taskId, task).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "False") {
        this.makeToastSubTasks();
      }
      else
      {
        var x =document.getElementById("task_"+taskId);
        x.style.opacity="0.5";
        this.getBoardTasks();
      }  
    })
  }

 /* addSubTask()
  {
    let task={
      TaskId: this.subTasksID,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this.boardID,
      ContractId:1,
      BeginDate: "",
      EndDate: "",
      StatusId: 0,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    if(task.TaskDesc == "")
    {
      return;
    }
    else
    {
      this._apicallService.Add("http://localhost:61232/api/Task/CreateSubTask", task) 
      .subscribe((result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray[0].Result == "True") {
          this.getSubTask();
          $("#subs").val("");
        }          
        },
      )
    }
    
  }

  editSubTask(taskId)
  {
    var newSubTaskDesc = $("#subTaskk_"+taskId).val();
    let editTask={
      TaskId: taskId,
      ParentId:0,
      Title: newSubTaskDesc,
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate:  "",
      ResponsibleId: 0 ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this.boardID,
      ContractId:1,
      BeginDate: "",
      EndDate: "",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    this._apicallService.Add("http://localhost:61232/api/Task/UpdateSubTask", editTask) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)
  }

  getSubTask()
  {
    this._apicallService.Get("http://localhost:61232/api/Task/GetSubTask/",this.subTasksID).subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.SubTaskList = [];
            this.subTaskEmpty = true;
          }
        }
        else
        {
          this.SubTaskList=result;
          this.subTaskEmpty = false;
        }
    },
    )
  }

  openDialogSubTask(dialog: TemplateRef<any>, taskId)
  {
    this.subTasksID=taskId;
    this.dialogService.open(dialog);
    this.getSubTask();
  }

  completeSubTask(taskId)
   {
    this._apicallService.Deactive("http://localhost:61232/api/Task/CompleteSubTask", taskId).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getSubTask();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
     
     }, )
   }

   selectSubAssignee(id)
   {
     this.TaskID = id;
   }

   subAssignTo(event)
   {
     var userId = event.value;
     let task={
      TaskId: this.TaskID,
      ParentId:0,
      Title: $("#subs").val(),
      TaskDesc: "SubTask",
      TypeId:1,
      CreatedDate: "",
      ResponsibleId: userId ,
      BeginCheckListId:1,
      EndCheckListId:1,
      BoardId:this.boardID,
      ContractId:1,
      BeginDate: "",
      EndDate: "",
      StatusId: this.selectedStatus,
      CreatorId: 0,
      TeamId: 0,
      CustomerId:0,
      LeadId:0,
      InvoiceId:0,
    };
    this._apicallService.Add("http://localhost:61232/api/Task/AssigneeSubTask", task) 
    .subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") {
        this.getSubTask();
      }          
      },)

      this._apicallService.Deactive("http://localhost:61232/api/Task/DeleteSubTaskMember", this.TaskID).subscribe(
        (result) =>{
        }
      )

      let _taskmember = { TaskId: this.TaskID, UserId: userId };   
      this._apicallService.Add("http://localhost:61232/api/Task/CreateTaskMember", _taskmember).subscribe(
        (result) =>{
        }
      )

   }*/

   formatIsoDateToCrypted(date)
   {
     var splittedDate = date.split("-");
     let val = "";
     if (splittedDate[1] == "01")
     {
         val = "Jan";
     }
     else if (splittedDate[1] == "02")
     {
         val = "Feb";
     }
     else if (splittedDate[1] == "03")
     {
         val = "Mar";
     }
     else if (splittedDate[1] == "04")
     {
         val = "Apr";
     }
     else if (splittedDate[1] == "05")
     {
         val = "May";
     }
     else if (splittedDate[1] == "06")
     {
         val = "Jun";
     }
     else if (splittedDate[1] == "07")
     {
         val = "Jul";
     }
     else if (splittedDate[1] == "08")
     {
         val = "Aug";
     }
     else if (splittedDate[1] == "09")
     {
         val = "Sep";
     }
     else if (splittedDate[1] == "10")
     {
         val = "Oct";
     }
     else if (splittedDate[1] == "11")
     {
         val = "Nov";
     }
     else if (splittedDate[1] == "12")
     {
         val = "Dec";
     }
 
     return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
   }
 
}

