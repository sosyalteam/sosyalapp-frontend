import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../apicall.service';
import { HttpModule, Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NbDialogService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService , NbThemeService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ThemeSettingsComponent } from '../../../@theme/components';
import {Title} from "@angular/platform-browser";
import {ExcelService} from "../../excel-service/excel.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';


@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  providers: [ApicallService,HttpModule],
})
export class TasksComponent implements OnInit {


   taskID : any;
   _copyTaskList:any;
   filtered=false;
   TaskList : any;
   StatusList = [];
   statusList = [];
   allUsers = [];
   team = [];
   _TeamList = [];
   _StatusList = [];
   _allUsers = [];
   pastDate=0;
   active=0;
   taskCount=0;
   upComingCount=0;
   myCompletedTaskCount = 0;
   myNotCompletedTaskCount = 0;
   allMyTasksCount = 0;
   boardID:number;
   date:string;
   sysLanguages : any;
   isFilteredMyTasks = false;
   filteredKanbanTaskCheck = false;
   filteredtaskListForKanban = [];

   selectedTask : any;
   cmItems = [];

   taskTypeList = [];
   tasksWithMembersList = [];
   myTaskList = [];
   copymyTaskList = [];
   statusListForKanban : any;
   taskListForKanban = [{ taskID: "", title: "", teamname: "", fullname: "", StatusId: "", endDate: ""}];
   openKanbanText ="";
   ListToKanban = false;

   loading = false;
   AdminUser = "";

   //toastr
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  //grafik
  options: any = {};
  themeSubscription: any;

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  //
   


  constructor(private datePipe:DatePipe,private _avRoute: ActivatedRoute,public http: Http, private _router: Router, private _apiCallservice: ApicallService,private dialogService: NbDialogService,private toastrService: NbToastrService, private titleService:Title, private excelService:ExcelService, private theme: NbThemeService ) { 
    this.getTasks();
    this.getTasksByUser();
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.getStatus();
    this.getUsers();
    this.getStatusList();
    this.getTeams();


    this.settingsForFilterMe.columns.Title.title = this.sysLanguages.TasksTableTitleText;
    this.settingsForFilterMe.columns.TaskDesc.title = this.sysLanguages.TasksTableTascDescText;
    this.settingsForFilterMe.columns.StatusDesc.title = this.sysLanguages.TasksTableStatusText;
    this.settingsForFilterMe.columns.StatusDesc.filter.config.selectText = this.sysLanguages.TasksTableFilterSelectText;
    this.settingsForFilterMe.columns.BeginDate.title = this.sysLanguages.TasksTableStartDateText;
    this.settingsForFilterMe.columns.EndDate.title = this.sysLanguages.TasksTableEndDateText;

    this.title1=this.sysLanguages.ErrorMessage;
    this.content=this.sysLanguages.ErrorMessage1;
    
    this.titleService.setTitle(this.sysLanguages.TasksHeader + " - Crmplus");

    this.openKanbanText = this.sysLanguages.TasksBackToKanbanButtonText;

    this.AdminUser = localStorage.getItem("UserId");
    

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;
  
      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight, colors.success, colors.info],
        tooltip: {
          trigger: 'item',
          formatter: '{b} : {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['', '', '' , '', '','',''],
          textStyle: {
            color: echarts.textColor,
          },
        },
        series: 
          {
            name: '',
            type: 'pie',
            radius: '60%',
            center: ['50%', '50%'],
            data:  [
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' },            
            { value: 0, name: '' },
            { value: 0, name: '' },
            { value: 0, name: '' }
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
      };
    });

  }

  settingsForFilterMe = {
    columns: {
      TaskId: {
        title: 'ID'
      },
      Title: {
        title: ''
      },
      TaskDesc: {
        title: ''
      },
      StatusDesc: {
        title: '',
        filter: {
          type: 'list',
            config: {
              selectText: 'Select',
                list: this.statusList
              }
            },
      },
      BeginDate: {
        title: '',
        valuePrepareFunction: (value) => {
          if(value)
          {
            return value.toString().split(" ")[0];
          }
        },       
      },
      EndDate: {
        title: '',
        valuePrepareFunction: (value) => {
          if(value)
          {
            return value.toString().split(" ")[0];
          }
        },
      },
    },
    actions:{
      position:'right',
      edit:false,
      delete:false,
      add:false,
    },

    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },

    mode: 'external',
  };

  ngOnInit() {

    this.options.series.data[0].name=this.sysLanguages.TasksStatusNotStarted;
    this.options.series.data[1].name=this.sysLanguages.TasksStatusWaiting;
    this.options.series.data[2].name=this.sysLanguages.TasksStatusContinues;
    this.options.series.data[3].name=this.sysLanguages.TasksStatusTest;
    this.options.series.data[4].name=this.sysLanguages.TasksStatusReturnWait;
    this.options.series.data[5].name=this.sysLanguages.TasksStatusCompleted;
    this.options.series.data[6].name=this.sysLanguages.TasksStatusCanceled;

    this.cmItems = [
      { label: this.sysLanguages.TpMarkCompleteText, icon: 'pi pi-check', command: (event) => this.selectedTask.StatusId != 5 ? this.markComplete(this.selectedTask.TaskId) : this.makeToast(NbToastStatus.WARNING,this.sysLanguages.AlreadyCompletedTask,this.sysLanguages.PcToastrErrorTitle) },
    ];
    
  }

  filterTable(value)
  {
    var past = 0;
    var active = 0;
    var upcoming = 0;
    var myDate = this.datePipe.transform(new Date,"yyyy-MM-dd");
    this.taskCount = value.totalRecords;
    if(value.filteredValue != null)
    {
      this.filtered = true;
      for(var i = 0; i < this.taskCount; i++ )
      {
        var endDate = value.filteredValue[i].EndDate.split(" ")[0];
        var remainingDays = new Date(endDate).getTime() - new Date(myDate).getTime();
        var diff = Math.ceil(remainingDays/(1000*3600*24));
        if(endDate < myDate && value.filteredValue[i].StatusId != 5 && value.filteredValue[i].StatusId != 6)
        {
          past++;        
        }
        if(value.filteredValue[i].StatusId == 5)
        {
          active++;
        }
        if(diff>=0 && diff<=7 && value.filteredValue[i].StatusId != 5 && value.filteredValue[i].StatusId != 6)
        {
          upcoming++;
        }
      }
      this.pastDate = past;
      this.active = active;
      this.upComingCount = upcoming;
      this._copyTaskList = value.filteredValue;
    }
    else
    {
      this.filtered = false;
      for(var i = 0; i < this.TaskList.length; i++ )
      {
        var endDate = this.TaskList[i].EndDate.split(" ")[0];
        var remainingDays = new Date(endDate).getTime() - new Date(myDate).getTime();
        var diff = Math.ceil(remainingDays/(1000*3600*24));
        if(endDate < myDate && this.TaskList[i].StatusId != 5 && this.TaskList[i].StatusId != 6)
        {
          past++;        
        }
        if(this.TaskList[i].StatusId == 5)
        {
          active++;
        }
        if(diff>=0 && diff<=7 && this.TaskList[i].StatusId != 5 && this.TaskList[i].StatusId != 6)
        {
          upcoming++;
        }
      }
      this.pastDate = past;
      this.active = active;
      this.upComingCount = upcoming;
    }
  }

  getTasks(){
      this.taskCount=0;
      this.pastDate=0;
      this.active=0;
      this.upComingCount=0;

      this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTasksWithParameters").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.TaskList = [];
          }
        }
        else {
          this.TaskList = resulttArray;

          var myDate = this.datePipe.transform(new Date,"yyyy-MM-dd");
          this.taskCount = resulttArray.length;
          for(var i = 0; i < resulttArray.length; i++ )
          {
            var endDate = resulttArray[i].EndDate.split(" ")[0];
            var remainingDays = new Date(endDate).getTime() - new Date(myDate).getTime();
            var diff = Math.ceil(remainingDays/(1000*3600*24));
            if(endDate < myDate && resulttArray[i].StatusId != 5 && resulttArray[i].StatusId != 6)
            {
              this.pastDate++;           
            }
            if(resulttArray[i].StatusId == 5)
            {
              this.active++;
            }
            if(diff>=0 && diff<=7 && resulttArray[i].StatusId != 5 && resulttArray[i].StatusId != 6){
              this.upComingCount++;
            }
          }
        }
      }
    ) 
  }

  getTasksByUser()
  {
    this.myCompletedTaskCount=0;
    this.myNotCompletedTaskCount=0;
    this.allMyTasksCount=0;

    this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskByUser").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.myCompletedTaskCount=0;
            this.myNotCompletedTaskCount=0;
            this.allMyTasksCount=0;
            return;
          }
        }
        else
        {
          debugger;
          this.myTaskList = resulttArray.slice();
          this.copymyTaskList = resulttArray.slice();

          for(var i = 0; i < resulttArray.length; i++ )
          {
            if(resulttArray[i].StatusId == 5)
            {
              this.myCompletedTaskCount++;
            }
            else if(resulttArray[i].StatusId != 5 && resulttArray[i].StatusId != 6)
            {
              this.myNotCompletedTaskCount++;
            }

            this.allMyTasksCount++;
          }
        }
      }
    )
  }

  getStatus()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length !=0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resulttArray[0].Result == "Empty")
        {
          this.statusList = [];
        }
      }
      else
      {
        for(var i = 0; i < resulttArray.length; i++ ){
          this.statusList.push({value:resulttArray[i].StatusDesc,title:resulttArray[i].StatusDesc});
        }
      }
    },
    )
  }

  getUsers()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/User/Index").subscribe(
    (result) => {     
      let resulttArray = JSON.parse(JSON.stringify(result));
      if (resulttArray.length != 0 && resulttArray[0].Result) {
        if (resulttArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if (resulttArray[0].Result == "Empty") {
          this.allUsers = [];
        }
      }
      else
      {
        this._allUsers.push({label:this.sysLanguages.PcAll,value:null});
        for(var i = 0; i < resulttArray.length; i++ ){
          this._allUsers.push({label:resulttArray[i].Fullname,value:resulttArray[i].Fullname});
          let _users = { label: resulttArray[i].Fullname, value: resulttArray[i].UserId };
          this.allUsers.push(_users);
        }
      }
    },
    )
  }

  getStatusList()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/Task/GetTaskStatus").subscribe(
    (result) => {         
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length !=0 && resultArray[0].Result) {
          if (resultArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resultArray[0].Result == "Empty")
          {
            this.StatusList = [];
            this._StatusList = [];
          }
        }
        else
        {
          this.statusListForKanban = result;
          for(var i = 0; i < resultArray.length; i++ ){
            this._StatusList.push({label:resultArray[i].StatusDesc,value:resultArray[i].StatusDesc});
            let _status = { label: resultArray[i].StatusDesc, value: resultArray[i].StatusId };
            this.StatusList.push(_status);
          }
        }
    },
    )
  }

  getTeams()
  {
  this._apiCallservice.GetAll("http://localhost:61232/api/Group/GetTeam").subscribe(
    (result) => {         
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray.length !=0 && resultArray[0].Result) {
        if (resultArray[0].Result == "Session_Expired") {
          this._router.navigate(['/auth']);
          return;
        }
        else if(resultArray[0].Result == "Empty")
        {
          //do
        }
      }
      else
      {
        this._TeamList.push({label:this.sysLanguages.PcAll,value:null});
        for(var i = 0; i < resultArray.length; i++ ){
          this._TeamList.push({ label: resultArray[i].TeamName, value: resultArray[i].TeamName });
          let _teams = { label: resultArray[i].TeamName, value: resultArray[i].TeamId };
          this.team.push(_teams);
        }
      }
    },
    )
  }

  addTask()
  {
    this._router.navigate(['/pages/task/add-edit-task']);
  }

  editTask(event)
  {
    this._router.navigate(['/pages/task/add-edit-task',event]);
  }

  filteredTask(value)
  {
    if(value=="myUncompleted")
    {
      this.isFilteredMyTasks = true;
      this.myTaskList = this.copymyTaskList.filter(x=>x.StatusId != 5 && x.StatusId != 6);
    }
    else if(value=="myCompleted")
    {
      this.isFilteredMyTasks = true;
      this.myTaskList = this.copymyTaskList.filter(x=>x.StatusId == 5);
    }
    else if(value == "all")
    {
      this.isFilteredMyTasks = false;
    }
  }

  openKanban()
  {
    if(this.openKanbanText==this.sysLanguages.TasksBackToKanbanButtonText)
    {
      this.ListToKanban= true;
      this.openKanbanText= this.sysLanguages.TasksListButtonText;
      this.taskListForKanban = [];

      for(let i in this.statusListForKanban) 
      {
        for(let j in this.TaskList)
        {
          if(this.TaskList[j].StatusId == this.statusListForKanban[i].StatusId)
            {
                let tasklist = {taskID: this.TaskList[j].TaskId, title: this.TaskList[j].Title, teamname : this.TaskList[j].TeamName , StatusId: this.TaskList[j].StatusId, fullname: this.TaskList[j].Fullname,endDate : this.TaskList[j].EndDate.split(" ")[0], markComplete : this.TaskList[j].TaskComplete};
                this.taskListForKanban.push(tasklist);
            }
        }
      }
    }
    else if (this.openKanbanText==this.sysLanguages.TasksListButtonText)
    {
      this.ListToKanban = false;
      this.openKanbanText=this.sysLanguages.TasksBackToKanbanButtonText;
    }   
  }

  mouseenter(taskId)
  {
    var x =document.getElementById("markComplete_"+taskId);
    x.style.display="block";
  }

  mouseleave(taskId)
  {
    var x =document.getElementById("markComplete_"+taskId);
    x.style.display="none";
  }

  markComplete(taskId)
  {
    let task={ TaskId : taskId, TaskComplete : "0.5", StatusId : 5 }
    this._apiCallservice.Update("http://localhost:61232/api/Task/TaskComplete", taskId, task).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if (resultArray[0].Result == "True") 
      {
        if(this.ListToKanban == true)
        {
          var x =document.getElementById("task_"+taskId);
          x.style.opacity="0.5";
          this.getTasks();
        }
        else
        {
          this.getTasks();
        }
      }
      else if (resultArray[0].Result == "False") {
        this.makeToast(NbToastStatus.DANGER, this.sysLanguages.PcToastrErrorTitle, this.sysLanguages.TasksToastrCantCompleted);
      }
    })
  }

  dragTask(event)
  {
     event.dataTransfer.setData("text2", event.target.id);
  }

  allowDrop(event) {
    if(event)
      event.preventDefault();
  }

  dropTask(event)
  {
    var data = event.dataTransfer.getData("text2");

    // --- eğer kolon içindeki diğer saçma yerlere sürüklenirse uyarı çıkarma komutları
    if(event.target.id.split('_')[0] == "colname")
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id.split('-')[0] == "column")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.id.includes("task_desc"))
    {
      // window.alert("You can't drop here");
    }
    else if(event.target.id == "addtaskbtnn")
    { 
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "cardDetailButton btn-info")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "columnDesc input-md input-rectangle")
    {
      //window.alert("You can't drop here");
    }
    else if(event.target.className == "ion-android-delete")
    {
      //window.alert("You can't drop here");
    }
    else
    {
      var taskID = data.split('_')[1];

      let _task = {TaskId: data.split('_')[1], statusID: event.target.id.split('_')[1]};

      this._apiCallservice.Update("http://localhost:61232/api/Task/ChangeTaskStatus", taskID, _task).subscribe((result) => {
        this.getTasks();
      })

      event.preventDefault();
      var data = event.dataTransfer.getData("text2");
      event.target.appendChild(document.getElementById(data));
    }
  }

  filterKanban(event)
  {
    if(event.target.checked == true)
    {
      this.filteredKanbanTaskCheck = true;
      var name = this.allUsers.filter(x=>x.value == this.AdminUser);
      this.filteredtaskListForKanban = this.taskListForKanban.filter(x=>x.fullname == name[0].label );
    }
    else
    {
      this.filteredKanbanTaskCheck = false;
    }
  }

  openExportArea(dialog: TemplateRef<any>)
  {
    if(this.filtered == true)
    {
      this.options.series.data[0].value = 0;
      this.options.series.data[1].value = 0;
      this.options.series.data[2].value = 0;
      this.options.series.data[3].value = 0;
      this.options.series.data[4].value = 0;
      this.options.series.data[5].value = 0;
      this.options.series.data[6].value = 0;
      for(var i in this._copyTaskList)
      {
        if(this._copyTaskList[i].StatusId == 0)
        {
          this.options.series.data[0].value++;
        }
        else if(this._copyTaskList[i].StatusId == 1)
        {
          this.options.series.data[1].value++;
        }
        else if(this._copyTaskList[i].StatusId == 2)
        {
          this.options.series.data[2].value++;
        }
        else if(this._copyTaskList[i].StatusId == 3)
        {
          this.options.series.data[3].value++;
        }
        else if(this._copyTaskList[i].StatusId == 4)
        {
          this.options.series.data[4].value++;
        }
        else if(this._copyTaskList[i].StatusId == 5)
        {
          this.options.series.data[5].value++;
        }
        else if(this._copyTaskList[i].StatusId == 6)
        {
          this.options.series.data[6].value++;
        }
      }
    }
    else
    {
      this.options.series.data[0].value = 0;
      this.options.series.data[1].value = 0;
      this.options.series.data[2].value = 0;
      this.options.series.data[3].value = 0;
      this.options.series.data[4].value = 0;
      this.options.series.data[5].value = 0;
      this.options.series.data[6].value = 0;
      for(var i in this.TaskList)
      {
        if(this.TaskList[i].StatusId == 0)
        {
          this.options.series.data[0].value++;
        }
        else if(this.TaskList[i].StatusId == 1)
        {
          this.options.series.data[1].value++;
        }
        else if(this.TaskList[i].StatusId == 2)
        {
          this.options.series.data[2].value++;
        }
        else if(this.TaskList[i].StatusId == 3)
        {
          this.options.series.data[3].value++;
        }
        else if(this.TaskList[i].StatusId == 4)
        {
          this.options.series.data[4].value++;
        }
        else if(this.TaskList[i].StatusId == 5)
        {
          this.options.series.data[5].value++;
        }
        else if(this.TaskList[i].StatusId == 6)
        {
          this.options.series.data[6].value++;
        }
      }
    }
    this.dialogService.open(dialog);
  }
  
  exportExcel(ref)
  {
    if(this.filtered == true)
    {
      this.excelService.exportAsExcelFile(this._copyTaskList, this.sysLanguages.TasksHeader);
      ref.close();
      this.filtered = false;
    }
    else
    {
      this.excelService.exportAsExcelFile(this.TaskList, this.sysLanguages.TasksHeader);
      ref.close();
      this.filtered = false;
    }
  }

  exportPdf(ref)
  {
    var doc = new jsPDF("l","mm","a4");
    var col = ["Title", "Desc","Team","Responsible","Status","BeginDate","EndDate","Time"];
    var rows = [];
    if(this.filtered == true)
    {
      var itemNew = this._copyTaskList;
    }
    else
    {
      var itemNew = this.TaskList;
    }
    
   itemNew.forEach(element => {      
        var temp = [element.Title.toUpperCase(), element.TaskDesc, element.TeamName.toUpperCase(), element.Fullname.toUpperCase(), element.StatusDesc.toUpperCase(), element.BeginDate.split(" ")[0], element.EndDate.split(" ")[0], element.TaskTime];
        rows.push(temp);
    });        

    doc.autoTable(col, rows,{
        columnStyles: {
        0: {columnWidth: 60},
        1: {columnWidth: 60},
        2: {columnWidth: 30},
        3: {columnWidth: 30},
        4: {columnWidth: 34},
        5: {columnWidth: 23},
        6: {columnWidth: 23},
        7: {columnWidth: 15}
        },
        theme : "grid"
      }
    );
    doc.save(this.sysLanguages.TasksHeader + ".pdf");
    ref.close();
    this.filtered = false;
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

   formatIsoDateToCrypted(date)
   {
     var splittedDate = date.split("-");
     let val = "";
     if (splittedDate[1] == "01")
     {
         val = "Jan";
     }
     else if (splittedDate[1] == "02")
     {
         val = "Feb";
     }
     else if (splittedDate[1] == "03")
     {
         val = "Mar";
     }
     else if (splittedDate[1] == "04")
     {
         val = "Apr";
     }
     else if (splittedDate[1] == "05")
     {
         val = "May";
     }
     else if (splittedDate[1] == "06")
     {
         val = "Jun";
     }
     else if (splittedDate[1] == "07")
     {
         val = "Jul";
     }
     else if (splittedDate[1] == "08")
     {
         val = "Aug";
     }
     else if (splittedDate[1] == "09")
     {
         val = "Sep";
     }
     else if (splittedDate[1] == "10")
     {
         val = "Oct";
     }
     else if (splittedDate[1] == "11")
     {
         val = "Nov";
     }
     else if (splittedDate[1] == "12")
     {
         val = "Dec";
     }
 
     return val + " " + splittedDate[2] + "," + " " + splittedDate[0];
   }


}
