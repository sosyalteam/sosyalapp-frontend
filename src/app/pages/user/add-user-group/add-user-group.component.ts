import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { HttpModule } from '@angular/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'add-user-group',
  templateUrl: './add-user-group.component.html',
  styleUrls: ['./add-user-group.component.scss'],
  providers: [ApicallService,HttpModule]
})
export class AddUserGroupComponent implements OnInit {
  public GroupList: any;
  public usergroupdetail: any;
  public sysLanguages: any;
  id: number;
  GroupId: number;
  groupname: string;

  constructor(public http: HttpModule, private _router: Router, private _avRoute: ActivatedRoute, private _apicallService: ApicallService, private titleService: Title) 
  { 
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.UserText + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.id = this._avRoute.snapshot.params["id"];
    }

    this.id = parseInt(sessionStorage.getItem('editUserGroupId'));
    sessionStorage.removeItem('editUserGroupId');
    this.getUsersGroup();
  }

  ngOnInit() {
  }


  getUsersGroup() {
    this._apicallService.GetAll('http://localhost:61232/api/User/GetGroups').subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if(resulttArray.length != 0 && resulttArray[0].Result)
        {
            if(resulttArray[0].Result == "Session_Expired")
            {
              this._router.navigate(['/auth']);
              return;
            }
            else if(resulttArray[0].Result == "Empty")
            {
              this.GroupList = [];
            }
        }
        else
        {
          this.GroupList = resulttArray;
          
          this._apicallService.Get("http://localhost:61232/api/User/GetuserGroup/", this.id).subscribe(
            (resultt) => {
              let resultArray = JSON.parse(JSON.stringify(resultt));
              if(resultArray.length != 0 && resultArray[0].Result)
              {
                  if(resultArray[0].Result == "Empty")
                  {
                    this.usergroupdetail = [];
                  }
              }
              else
              {
                this.usergroupdetail = resultArray;
              }
          })
        }
      }
    )
  }         

  addUsersGroup()
  {
    let checkboxes = <HTMLCollection>document.getElementsByClassName("checkboxGroup");
    for (var i = 0; i < checkboxes.length; i++)
    {
      var element = <HTMLInputElement>document.getElementById(checkboxes[i].id)
      if (element.checked == true)
      {

        var flag = false;
        for (let i = 0; i < this.usergroupdetail.length; i++) {
          if (this.usergroupdetail[i].GroupId == parseInt(element.defaultValue)) {
            flag = true;
            break;
          }
        }
        if (flag == true) {
          window.alert(this.sysLanguages.UserAddGroupAlertText);
        }
        else {
          let usergroup = { Id: 0, GroupId: parseInt(element.defaultValue), UserId: this.id, GrantDate: "NoMatter" };
          this._apicallService.Add("http://localhost:61232/api/User/AddUserGroup", usergroup)
            .subscribe((result) => {
              if (result[0].Result == "True") {
                sessionStorage.setItem('editUserGroupId', this.id.toString());
                this.getUsersGroup();
                this._router.navigate(['/pages/user/add-user-group']);
              }
              else if (result[0].Result == "Session_Expired") {
                this._router.navigate(['/auth']);
                return;
              }
            },
            )
        }

      }
    }
  }

  deleteUserGroup() {
    let checkboxes = <HTMLCollection>document.getElementsByClassName("checkboxUser");
    for (var i = 0; i < checkboxes.length; i++) {
      var element = <HTMLInputElement>document.getElementById(checkboxes[i].id)
      if (element.checked == true) {
        let usergroup = { Id: 0, GroupId: parseInt(element.defaultValue), UserId: this.id, GrantDate: "NoMatter" };
        this._apicallService.Add("http://localhost:61232/api/User/DeleteUserGroup", usergroup)
          .subscribe((result) => {
            if (result[0].Result == "True") {
              sessionStorage.setItem('editUserGroupId', this.id.toString());
              this.getUsersGroup();
              this._router.navigate(['/pages/user/add-user-group']);
            }
            else if (result[0].Result == "Session_Expired") {
              this._router.navigate(['/auth']);
              return;
            }
          },
          )
      }
    }
  }

}