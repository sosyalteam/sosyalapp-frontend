import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ApicallService } from '../../apicall.service';
import { NbDialogService } from '@nebular/theme';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'get-user',
  templateUrl: './get-user.component.html',
  providers: [ApicallService,HttpModule],
  styleUrls: ['./get-user.component.scss']
})
export class GetUserComponent implements OnInit {
  public userList: any;
  public sysLanguages: any;
  public UserId : any;
  public userArray = [{ Username :' ',Fullname :' ', OldPassword : '',NewPassword : ' ', ControlPassword : ' ', Email : '' }];
  editList: {username: any, fullname: any, password: any, email: any};
  title : string = "";
  //yorum
  
  constructor(public http: HttpModule, private _router: Router, private _apicallService: ApicallService,private dialogService: NbDialogService, private titleService: Title) {
    
    this.sysLanguages = JSON.parse(sessionStorage.getItem("langFile"));
    this.titleService.setTitle(this.sysLanguages.UserText + " - Crmplus");

    this.settings.columns.Fullname.title = this.sysLanguages.UserTableFullnameText;
    this.settings.columns.Username.title = this.sysLanguages.UserTableUsernameText;
    this.settings.actions.columnTitle = this.sysLanguages.UserTableActionText;
    this.getUsers();
   }

   settings = {
    columns: {
      UserId: {
        title: 'ID',
        width: '3%'
      },
      Username: {
        title: ''
      },
      Fullname: {
        title: ''
      },
      Email: {
        title: 'Email'
      }
    },
    actions:{
      position:'right',
      columnTitle:"",
      add:false
    },
    add:{
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    mode:"external",
  };

  ngOnInit() {
  }

  getUsers() {
    this._apicallService.GetAll("http://localhost:61232/api/user/Index").subscribe(
      (result) => {
        let resulttArray = JSON.parse(JSON.stringify(result));
        if (resulttArray.length != 0 && resulttArray[0].Result) {
          if (resulttArray[0].Result == "Session_Expired") {
            this._router.navigate(['/auth']);
            return;
          }
          else if(resulttArray[0].Result == "Empty")
          {
            this.userList = [];
          }
        }
        else {
          this.userList = result;
        }
      }
    )
  }

  changePassword(event){
    var id = `${event.data.UserId}`;
    sessionStorage.setItem('editUserId', id);
    this._router.navigate(['/pages/user/add-user']);
  }

  onCustomAction(event){
    var id = `${event.data.UserId}`;
    sessionStorage.setItem('editUserGroupId', id);
    this._router.navigate(['/pages/user/add-user-group']);

  }

  onSaveConfirm(event){
    var id = `${event.data.UserId}`;
    var name=`${event.newData.Fullname}`;
    var emaill=`${event.newData.Email}`;
    let userArray = {  UserId: parseInt(id), Fullname:name , Email:emaill,StatusId : 1};
           this._apicallService.Update("http://localhost:61232/api/User/Edit", parseInt(id), userArray)
             .subscribe((result) => {
               if(result[0].Result == "True")
               {  
                  this.getUsers();
                  this._router.navigate(['/pages/user/get-user']);
               }
               else if(result[0].Result == "Session_Expired")
               {
                 this._router.navigate(['/auth']);
                 return;
               }
             },)

  }

  onDeleteConfirm(event) {
    var id = `${event.data.UserId}`;
    var ans = confirm(this.sysLanguages.UserDeleteText + parseInt(id));
   if (ans) {
     this._apicallService.Deactive("http://localhost:61232/api/User/Delete/", parseInt(id)).subscribe((result) => {
      let resultArray = JSON.parse(JSON.stringify(result));
      if(resultArray[0].Result == "True")
      {
        this.getUsers();
      }
      else if(resultArray[0].Result == "Session_Expired")
      {
        this._router.navigate(['/auth']);
        return;
      }
     
     }, error => console.error(error))
   }
    
  }

  openDialog(dialog: TemplateRef<any>,event)
  {
    this.UserId=parseInt(`${event.data.UserId}`);
    this._apicallService.Get("http://localhost:61232/api/User/Details/", this.UserId)
        .subscribe(
         (result) => {
           let resultArray = JSON.parse(JSON.stringify(result));
           if(resultArray.length != 0 && resultArray[0].Result)
           {
               if(resultArray[0].Result == "Session_Expired")
               {
                 this._router.navigate(['/auth']);
                 return;
               }
               else if(resultArray[0].Result == "Empty")
               {
                 // do smth
               }
           }
           else
           {
            this._apicallService.Get("http://localhost:61232/api/User/UserAuth/", this.UserId)
            .subscribe(
            (resultt) => {
              let resulttArray = JSON.parse(JSON.stringify(resultt));
              if(resulttArray.length != 0 && resulttArray[0].Result)
              {
                  if(resulttArray[0].Result == "Empty")
                  {
                    //do smth.
                  }
              }
              else
              {
                this.editList = {username: resulttArray[0].Username, fullname: resultArray[0].Fullname, password: resulttArray[0].EncryptedKey, email: resultArray[0].Email};
                this.userArray[0].Username = resulttArray[0].Username;     
                this.userArray[0].Fullname = resultArray[0].Fullname;
                this.userArray[0].Email = resultArray[0].Email;
              }
              }, 
            )
           }          
         }, 
     )
      this.title=this.sysLanguages.UserEditHeader;
      this.dialogService.open(dialog);
    }
      

  openDialog1(dialog: TemplateRef<any>)
  {
    this.userArray[0].Username = '';     
    this.userArray[0].Fullname = '';
    this.userArray[0].Email = '';
    this.title=this.sysLanguages.UserAddNewText;
    this.dialogService.open(dialog);
  }

  cancel(ref) {
    ref.close();
  }

  passwordCheck()
  {
    if( $("#password").val() ==  $("#password2").val())
    {
      return true;
    }
    else
      return false;
  }

  save(ref)
  {
    var isSameForm = false;
    var isUsernameChanged = false;
    var isPasswordChanged = false;
    if(this.passwordCheck() == false)
    {
       window.alert(this.sysLanguages.UserEditNotSamePasswordAlert);
       return;
    }
    else
    {
      if (this.title==this.sysLanguages.UserAddNewText) {
        // Burda önce kullanıcıyı ekletiyorum ardından kullanıcı tablosundan son eklenen datanın id değerini alıp o değerle authentication tablosuna kayıt yapıyorum.
        let user = {
          userId: 0,
          username: $("#username").val(),
          fullname: $("#fullname").val(),
          password: $("#password").val(),
          oldpassword: $("#oldpassword").val(),
          password2: $("#password2").val(),
          email: $("#email").val(),
          StatusId : 1
        };
        this._apicallService.Add("http://localhost:61232/api/User/Create", user)
          .subscribe((data) => {
            let userAuth = { UserId: data[0].Result, Username: user.username, AuthenticationTypeId: 1, EncryptedKey: user.password  };
            this._apicallService.Add("http://localhost:61232/api/User/PostUserAuth", userAuth)
              .subscribe((data) => {
                ref.close();
                this.getUsers();
            }, )
          }, )
      }
      else if (this.title==this.sysLanguages.UserEditHeader) {
        if($("#password").val() != "" && $("#password2").val() != "")
        {
          this.editList.password = $("#password").val();
          isPasswordChanged = true;
        }
        if($("#username").val() != "")
        {
          this.editList.username = $("#username").val();
          isUsernameChanged = true;
        }
        if($("#fullname").val() != "")
        {
          this.editList.fullname = $("#fullname").val();
          isSameForm = true;
        }
        if($("#email").val() != "")
        {
          this.editList.email = $("#email").val();
          isSameForm = true;
        }
        if(isSameForm == false && isUsernameChanged == false && isPasswordChanged == false)
        {
          window.alert(this.sysLanguages.UserEditNoChangeAlert);
        }
        else
        {
          if(isPasswordChanged == true)
          {
            this._apicallService.Update("http://localhost:61232/api/User/UpdateEncyptedKey", this.UserId, {EncryptedKey: this.editList.password, UserId: $("#oldpassword").val()})
            .subscribe((result) => {
              if(result[0].Result == "True")
              {
                ref.close();
                this.getUsers();
                this._router.navigate(['/pages/user/get-user']);
              }
              else if(result[0].Result == "FalseOldPassword")
              {
                alert("Your entered old password is false.");
              }
              else if(result[0].Result == "NotFound")
              {
                alert("Record was not found.");
              }
              else if(result[0].Result == "Session_Expired")
              {
                this._router.navigate(['/auth']);
                return;
              }
            },)
          }
          if(isUsernameChanged == true)
          {                        
            this._apicallService.Update("http://localhost:61232/api/User/UpdateUsername", this.UserId, {Username: this.editList.username, UserId: this.UserId})
            .subscribe((result) => {
              if(result[0].Result == "True")
              {
                ref.close();
                this.getUsers();
                this._router.navigate(['/pages/user/get-user']);
              }
              else if(result[0].Result == "Session_Expired")
              {
                this._router.navigate(['/auth']);
                return;
              }
            },)
          }
          if(isSameForm == true)
          {
          let userArray = {  userId: this.UserId, fullname: this.editList.fullname, email: this.editList.email,StatusId : 1};
          this._apicallService.Update("http://localhost:61232/api/User/Edit", this.UserId, userArray)
            .subscribe((result) => {
              if(result[0].Result == "True")
              {
                ref.close();
                this.getUsers();
                this._router.navigate(['/pages/user/get-user']);
              }
              else if(result[0].Result == "Session_Expired")
              {
                this._router.navigate(['/auth']);
                return;
              }
            },)
          }
        }
      }

    }

  }


}
