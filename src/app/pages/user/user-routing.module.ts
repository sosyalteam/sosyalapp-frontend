import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetUserComponent } from './get-user/get-user.component';
import { AddUserGroupComponent } from './add-user-group/add-user-group.component';

const routes: Routes = [
  {
    path: 'get-user',
    component: GetUserComponent,
  },
  {
    path: 'add-user-group',
    component: AddUserGroupComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
