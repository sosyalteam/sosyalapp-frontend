import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { UserRoutingModule } from './user-routing.module';
import { GetUserComponent } from './get-user/get-user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddUserGroupComponent } from './add-user-group/add-user-group.component';
import { NbCardModule, NbListModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [GetUserComponent, AddUserGroupComponent],
  imports: [
    CommonModule,
    HttpModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NbCardModule,
    NbListModule,
    Ng2SmartTableModule,
  ]
})
export class UserModule { 
  /**
   *
   */
  constructor() {
    
  }
}
