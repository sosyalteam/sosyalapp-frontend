import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProposalComponent } from './proposal/proposal.component';
import { BrowserlanguageService } from '../pages/syslanguages/browserlanguage.service';
import { ProposalpdfComponent } from './proposalpdf/proposalpdf.component';

const routes: Routes = [
  {
    path: ':id',
    component: ProposalComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
  {
    path: 'pdf/:id',
    component: ProposalpdfComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [BrowserlanguageService],
})
export class ProposalRoutingModule { }
