import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProposalRoutingModule } from './proposal-routing.module';
import { ProposalComponent } from './proposal/proposal.component';
import { NbLayoutModule, NbCardModule, NbButtonComponent, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { ProposalpdfComponent } from './proposalpdf/proposalpdf.component';

@NgModule({
  declarations: [ProposalComponent, ProposalpdfComponent],
  imports: [
    CommonModule,
    ProposalRoutingModule,
    NbLayoutModule,
    NbCardModule,
    NbButtonModule,
    NbDialogModule.forRoot(),
  ]
})
export class ProposalModule { }
