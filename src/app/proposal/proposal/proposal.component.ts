import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApicallService } from '../../pages/apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService, NbDialogService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.scss']
})
export class ProposalComponent implements OnInit {

  sysLanguages: any;
  proposalData = { Address: "", Amount: "", City: "", CreatedDate: "", ExpiredDate: "", Phone: "", PostCode: "", Subject: "", Town: "", Status: "", CustomerName: "" , ContactName : "" , CustomerId : 0 , Discount : "", Note: ""};
  lineData = [];
  proposalId: any;
  isTimeExpired = false;

  currency :any;
  
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _apicallService: ApicallService, private _router: Router, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private dialogService: NbDialogService, private titleService: Title) 
  {
    this.sysLanguages = this._avRoute.snapshot.data['lang'];
    this.titleService.setTitle(this.sysLanguages.BpProposal + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.proposalId = this._avRoute.snapshot.params["id"];
    }

    this.getProposalDetail();
  }

  ngOnInit() {
  }

  getProposalDetail()
  {
    this.lineData = [];

    if(this.proposalId>0)
    {
      this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposalWoSession", this.proposalId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            if(resulttArray[0].StatusDesc == "Taslak")
              this.proposalData.Status = this.sysLanguages.PcDraftStatus;
            else if(resulttArray[0].StatusDesc == "Gönderildi")
              this.proposalData.Status = this.sysLanguages.PcSentStatus;
            else if(resulttArray[0].StatusDesc == "Açık")
              this.proposalData.Status = this.sysLanguages.PcOpenStatus;
            else if(resulttArray[0].StatusDesc == "Revize")
              this.proposalData.Status = this.sysLanguages.PcRevizeStatus;
            else if(resulttArray[0].StatusDesc == "Reddedildi")
              this.proposalData.Status = this.sysLanguages.PcRefusedStatus;
            else if(resulttArray[0].StatusDesc == "Kabul Edildi")
              this.proposalData.Status = this.sysLanguages.PcAcceptedStatus;
            else if(resulttArray[0].StatusDesc == "Dönüştürüldü")
              this.proposalData.Status = this.sysLanguages.PcConvertedStatus;

            this.proposalData.Phone = resulttArray[0].Phone;
            this.proposalData.PostCode = resulttArray[0].PostCode;
            this.proposalData.Subject = resulttArray[0].Subject;
            this.proposalData.Town = resulttArray[0].Town;
            this.proposalData.Address = resulttArray[0].Address;
            this.proposalData.Amount = resulttArray[0].Amount;
            this.proposalData.City = resulttArray[0].City;     
            this.proposalData.CustomerName = resulttArray[0].CustomerName;
            this.proposalData.CustomerId = resulttArray[0].CustomerId;
            this.proposalData.Discount = resulttArray[0].Discount;
            this.proposalData.ExpiredDate = resulttArray[0].ExpiredDate.split(" ")[0];
            this.proposalData.CreatedDate = resulttArray[0].CreatedDate.split(" ")[0];
            this.proposalData.Note = resulttArray[0].AdminNote;
            
            if(resulttArray[0].CurrencyTypeId == 1)
            {
              this.currency = "TL"
            }
            else if(resulttArray[0].CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(resulttArray[0].CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(resulttArray[0].CurrencyTypeId == 4)
            {
              this.currency = "£";
            }

            if(this.proposalData.CustomerId == 0)
            {
              this.proposalData.CustomerName = resulttArray[0].customname.CustomerName;
              this.proposalData.CustomerId =  resulttArray[0].customname.CustomerId;
            }
            
            let expDate = new Date(this.proposalData.ExpiredDate).getTime();
            let today = new Date().getTime();

            if(today > expDate)
            {
              this.isTimeExpired = true;
            }
            
            this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.proposalData.CustomerId).subscribe(
            (result) => {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray.length != 0 && resultArray[0].Result) {
                if (resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
                else if(resultArray[0].Result == "Empty")
                {

                }
              }
              else
              {
                this.proposalData.ContactName = resultArray[0].Fullname;
              }
            }
          )
        }              
      }
      )

      this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposalLinesWoSession", this.proposalId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }
          }
        }
      )
    }
  }

  refuseProposal()
  {
    this._apicallService.Update("http://localhost:61232/api/Proposal/RefuseProposal", this.proposalId, "notImportantWhatYouSent").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "True")
          {
            this.getProposalDetail();
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrRefuseProposal);
          }
        }
      }
    )
  }

  acceptProposal()
  {
    this._apicallService.Update("http://localhost:61232/api/Proposal/AcceptProposal", this.proposalId, "notImportantWhatYouSent").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "True")
          {
            this.getProposalDetail();
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrAcceptProposal);
          }
        }
      }
    )
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  goToPdfPage()
  {
    this._router.navigate([]).then(result => {  window.open('/proposal/pdf/' + this.proposalId, '_blank'); });
  }

}
