import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalpdfComponent } from './proposalpdf.component';

describe('ProposalpdfComponent', () => {
  let component: ProposalpdfComponent;
  let fixture: ComponentFixture<ProposalpdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposalpdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalpdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
