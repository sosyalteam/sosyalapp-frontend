import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../pages/apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

@Component({
  selector: 'proposalpdf',
  templateUrl: './proposalpdf.component.html',
  styleUrls: ['./proposalpdf.component.scss']
})
export class ProposalpdfComponent implements OnInit {

  sysLanguages: any;
  proposalData = { Address: "", Amount: "", City: "", CreatedDate: "", ExpiredDate: "", Phone: "", PostCode: "", Subject: "", Town: "", Status: "", CustomerName: "" , ContactName : "" , Note: "", CustomerId : 0 , Discount : ""};
  lineData = [];
  proposalId: any;
  currency :any;

  constructor(private _apicallService: ApicallService, private _router: Router, private _avRoute: ActivatedRoute) 
  {
    this.sysLanguages = this._avRoute.snapshot.data['lang'];

    if (this._avRoute.snapshot.params["id"]) {
      this.proposalId = this._avRoute.snapshot.params["id"];
    }

    this.getProposalDetail();
  
  }

  ngOnInit() 
  {

    setTimeout(()=>{
      this.converttoPdf();
    }, 1000);

  }

  getProposalDetail()
  {
    this.lineData = [];

    if(this.proposalId>0)
    {
      this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposalWoSession", this.proposalId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            if(resulttArray[0].StatusDesc == "Taslak")
              this.proposalData.Status = this.sysLanguages.PcDraftStatus;
            else if(resulttArray[0].StatusDesc == "Gönderildi")
              this.proposalData.Status = this.sysLanguages.PcSentStatus;
            else if(resulttArray[0].StatusDesc == "Açık")
              this.proposalData.Status = this.sysLanguages.PcOpenStatus;
            else if(resulttArray[0].StatusDesc == "Revize")
              this.proposalData.Status = this.sysLanguages.PcRevizeStatus;
            else if(resulttArray[0].StatusDesc == "Reddedildi")
              this.proposalData.Status = this.sysLanguages.PcRefusedStatus;
            else if(resulttArray[0].StatusDesc == "Kabul Edildi")
              this.proposalData.Status = this.sysLanguages.PcAcceptedStatus;
            else if(resulttArray[0].StatusDesc == "Dönüştürüldü")
              this.proposalData.Status = this.sysLanguages.PcConvertedStatus;
              
            this.proposalData.Phone = resulttArray[0].Phone;
            this.proposalData.PostCode = resulttArray[0].PostCode;
            this.proposalData.Subject = resulttArray[0].Subject;
            this.proposalData.Town = resulttArray[0].Town;
            this.proposalData.Address = resulttArray[0].Address;
            this.proposalData.Amount = resulttArray[0].Amount;
            this.proposalData.City = resulttArray[0].City; 
            this.proposalData.CustomerName = resulttArray[0].CustomerName;
            this.proposalData.CustomerId = resulttArray[0].CustomerId;
            this.proposalData.Discount = resulttArray[0].Discount;
            this.proposalData.ExpiredDate = resulttArray[0].ExpiredDate.toString().split(" ")[0];
            this.proposalData.CreatedDate = resulttArray[0].CreatedDate.toString().split(" ")[0];
            this.proposalData.Note = resulttArray[0].AdminNote;

            if(resulttArray[0].CurrencyTypeId == 1)
            {
              this.currency = "TL"
            }
            else if(resulttArray[0].CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(resulttArray[0].CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(resulttArray[0].CurrencyTypeId == 4)
            {
              this.currency = "£";
            }

            if(this.proposalData.CustomerId == 0)
            {
              this.proposalData.CustomerName = resulttArray[0].customname.CustomerName;
              this.proposalData.CustomerId =  resulttArray[0].customname.CustomerId;
            }

            this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.proposalData.CustomerId).subscribe(
            (result) => {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray.length != 0 && resultArray[0].Result) {
                if (resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
                else if(resultArray[0].Result == "Empty")
                {

                }
              }
              else
              {
                this.proposalData.ContactName = resultArray[0].Fullname;
              }
            }
          )
          }              
      }
      )

      this._apicallService.Get("http://localhost:61232/api/Proposal/GetProposalLinesWoSession", this.proposalId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName, LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }
          }
        }
      )
    }
  }

  converttoPdf()
  {

    var data = document.getElementById('pdfScreen');  
    html2canvas(data).then(canvas => {  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      const pdf = new jspdf({
        orientation: 'landscape',
      }); 
      const imgProps= pdf.getImageProperties(contentDataURL);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, pdfWidth, pdfHeight)  
      pdf.save(this.proposalId + '_proposal.pdf'); // Generated PDF  
      window.top.close();
    }); 
  }

}
