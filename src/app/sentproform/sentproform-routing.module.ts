import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SentproformComponent } from './sentproform/sentproform.component';
import { BrowserlanguageService } from '../pages/syslanguages/browserlanguage.service';
import { SentproformpdfComponent } from './sentproformpdf/sentproformpdf.component';

const routes: Routes = [
  {
    path: ':id',
    component: SentproformComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
  {
    path: 'pdf/:id',
    component: SentproformpdfComponent,
    resolve: {
      lang: BrowserlanguageService
     }, 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SentproformRoutingModule { }
