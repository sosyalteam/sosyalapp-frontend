import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SentproformRoutingModule } from './sentproform-routing.module';
import { SentproformComponent } from './sentproform/sentproform.component';
import { SentproformpdfComponent } from './sentproformpdf/sentproformpdf.component';
import { NbLayoutModule, NbDialogModule, NbCardModule, NbButtonModule } from '@nebular/theme';

@NgModule({
  declarations: [SentproformComponent, SentproformpdfComponent],
  imports: [
    CommonModule,
    SentproformRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbDialogModule.forRoot(),
    NbLayoutModule,
  ]
})
export class SentproformModule { }
