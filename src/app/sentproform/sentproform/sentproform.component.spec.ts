import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentproformComponent } from './sentproform.component';

describe('SentproformComponent', () => {
  let component: SentproformComponent;
  let fixture: ComponentFixture<SentproformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentproformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentproformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
