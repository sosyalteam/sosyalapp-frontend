import { Component, OnInit } from '@angular/core';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbGlobalLogicalPosition, NbToastrService, NbDialogService } from '@nebular/theme';
import { ApicallService } from '../../pages/apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'sentproform',
  templateUrl: './sentproform.component.html',
  styleUrls: ['./sentproform.component.scss'],
  providers: [ApicallService]
})
export class SentproformComponent implements OnInit {

  sysLanguages: any;
  proformData = { CustomerName: "", Address: "", Amount: "", City: "", CreatedDate: "", ExpirementDate: "", Phone: "", PostCode: "", Status: "" , ContactName : "" , CustomerId : 0 , Discount : "" };
  lineData = [];
  proformId: any;
  isTimeExpired = false;
  currency : any;
  
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.DANGER;

  title1 = '';
  content = '';

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  constructor(private _apicallService: ApicallService, private _router: Router, private _avRoute: ActivatedRoute, private toastrService: NbToastrService, private titleService: Title) 
  {
    this.sysLanguages = this._avRoute.snapshot.data['lang'];
    this.titleService.setTitle(this.sysLanguages.BpProform + " - Crmplus");

    if (this._avRoute.snapshot.params["id"]) {
      this.proformId = this._avRoute.snapshot.params["id"];
    }

    this.getProformDetail();
  }

  ngOnInit() {
  }

  getProformDetail()
  {
    this.lineData = [];

    if(this.proformId>0)
    {
      this._apicallService.Get("http://localhost:61232/api/Proform/GetProformWoSession", this.proformId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            if(resulttArray[0].StatusDesc == "Taslak")
              this.proformData.Status = this.sysLanguages.PcDraftStatus;
            else if(resulttArray[0].StatusDesc == "Gönderildi")
              this.proformData.Status = this.sysLanguages.PcSentStatus;
            else if(resulttArray[0].StatusDesc == "Süresi Geçti")
              this.proformData.Status = this.sysLanguages.PcExpiredStatus;
            else if(resulttArray[0].StatusDesc == "Reddedildi")
              this.proformData.Status = this.sysLanguages.PcRefusedStatus;
            else if(resulttArray[0].StatusDesc == "Kabul Edildi")
              this.proformData.Status = this.sysLanguages.PcAcceptedStatus;

            this.proformData.Phone = resulttArray[0].Phone;
            this.proformData.PostCode = resulttArray[0].PostCode;
            this.proformData.CustomerName = resulttArray[0].CustomerName;
            this.proformData.Address = resulttArray[0].Address;
            this.proformData.Amount = resulttArray[0].Amount;
            this.proformData.City = resulttArray[0].City;
            this.proformData.CustomerId = resulttArray[0].CustomerId;
            this.proformData.Discount = resulttArray[0].Discount;
            this.proformData.ExpirementDate = resulttArray[0].ExpirementDate.split(" ")[0];
            this.proformData.CreatedDate = resulttArray[0].CreatedDate.split(" ")[0];
            if(resulttArray[0].CurrencyTypeId == 1)
            {
              this.currency = "TL"
            }
            else if(resulttArray[0].CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(resulttArray[0].CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(resulttArray[0].CurrencyTypeId == 4)
            {
              this.currency = "£";
            }
            
            let expDate = new Date(this.proformData.ExpirementDate).getTime();
            let today = new Date().getTime();

            if(today > expDate)
            {
              this.isTimeExpired = true;
            }

            this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.proformData.CustomerId).subscribe(
            (result) => {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray.length != 0 && resultArray[0].Result) {
                if (resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
                else if(resultArray[0].Result == "Empty")
                {

                }
              }
              else
              {
                this.proformData.ContactName = resultArray[0].Fullname;
              }
            }
          )
          }              
      }
      )

      this._apicallService.Get("http://localhost:61232/api/Proform/GetProformLinesWoSession", this.proformId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName , LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }
          }
        }
      )
    }
  }

  refuseProform()
  {
    this._apicallService.Update("http://localhost:61232/api/Proform/RefuseProform", this.proformId, "notImportantWhatYouSent").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "True")
          {
            this.getProformDetail();
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrRefuseProform);
          }
        }
      }
    )
  }

  acceptProform()
  {
    this._apicallService.Update("http://localhost:61232/api/Proform/AcceptProform", this.proformId, "notImportantWhatYouSent").subscribe(
      (result) => {
        let resultArray = JSON.parse(JSON.stringify(result));
        if (resultArray.length != 0 && resultArray[0].Result) {
          if (resultArray[0].Result == "True")
          {
            this.getProformDetail();
            this.makeToast(NbToastStatus.SUCCESS, this.sysLanguages.PcToastrSuccessTitle, this.sysLanguages.PcToastrAcceptProform);
          }
        }
      }
    )
  }

  makeToast(status, title, content) {
    this.showToast(status, title, content);
  }

  private showToast(type: NbToastStatus, title1: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title1 ? `${title1}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  goToPdfPage()
  {
    this._router.navigate([]).then(result => {  window.open('/proform/pdf/' + this.proformId, '_blank'); });
  }

}
