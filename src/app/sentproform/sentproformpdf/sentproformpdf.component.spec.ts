import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentproformpdfComponent } from './sentproformpdf.component';

describe('SentproformpdfComponent', () => {
  let component: SentproformpdfComponent;
  let fixture: ComponentFixture<SentproformpdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentproformpdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentproformpdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
