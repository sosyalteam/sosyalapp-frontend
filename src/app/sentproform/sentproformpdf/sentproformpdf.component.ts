import { Component, OnInit } from '@angular/core';
import { ApicallService } from '../../pages/apicall.service';
import { Router, ActivatedRoute } from '@angular/router';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';  

@Component({
  selector: 'sentproformpdf',
  templateUrl: './sentproformpdf.component.html',
  styleUrls: ['./sentproformpdf.component.scss'],
  providers: [ApicallService]
})
export class SentproformpdfComponent implements OnInit {

  sysLanguages: any;
  proformData = { CustomerName: "", Address: "", Amount: "", City: "", CreatedDate: "", ExpirementDate: "", Phone: "", PostCode: "", Status: "" , ContactName : "" , CustomerId : 0, Discount : ""};
  lineData = [];
  proformId: any;
  currency : any;
  

  constructor(private _apicallService: ApicallService, private _router: Router, private _avRoute: ActivatedRoute) 
  {
    this.sysLanguages = this._avRoute.snapshot.data['lang'];

    if (this._avRoute.snapshot.params["id"]) {
      this.proformId = this._avRoute.snapshot.params["id"];
    }

    this.getProformDetail();
  
  }

  ngOnInit() 
  {

    setTimeout(()=>{
      this.converttoPdf();
    }, 1000);

  }

  getProformDetail()
  {
    this.lineData = [];

    if(this.proformId>0)
    {
      this._apicallService.Get("http://localhost:61232/api/Proform/GetProformWoSession", this.proformId)
      .subscribe(
        (result) => {
          let resulttArray = JSON.parse(JSON.stringify(result));
          if (resulttArray.length != 0 && resulttArray[0].Result) {
            if(resulttArray[0].Result == "Empty")
            {
              // do smth.
            }
          }
          else
          {
            if(resulttArray[0].StatusDesc == "Taslak")
              this.proformData.Status = this.sysLanguages.PcDraftStatus;
            else if(resulttArray[0].StatusDesc == "Gönderildi")
              this.proformData.Status = this.sysLanguages.PcSentStatus;
            else if(resulttArray[0].StatusDesc == "Süresi Geçti")
              this.proformData.Status = this.sysLanguages.PcExpiredStatus;
            else if(resulttArray[0].StatusDesc == "Reddedildi")
              this.proformData.Status = this.sysLanguages.PcRefusedStatus;
            else if(resulttArray[0].StatusDesc == "Kabul Edildi")
              this.proformData.Status = this.sysLanguages.PcAcceptedStatus;
              
            this.proformData.Phone = resulttArray[0].Phone;
            this.proformData.PostCode = resulttArray[0].PostCode;
            this.proformData.CustomerName = resulttArray[0].CustomerName;
            this.proformData.Address = resulttArray[0].Address;
            this.proformData.Amount = resulttArray[0].Amount;
            this.proformData.City = resulttArray[0].City;
            this.proformData.CustomerId = resulttArray[0].CustomerId;
            this.proformData.Discount= resulttArray[0].Discount;
            this.proformData.ExpirementDate = resulttArray[0].ExpirementDate.toString().split(" ")[0];
            this.proformData.CreatedDate = resulttArray[0].CreatedDate.toString().split(" ")[0];
            if(resulttArray[0].CurrencyTypeId == 1)
            {
              this.currency = "TL"
            }
            else if(resulttArray[0].CurrencyTypeId == 2)
            {
              this.currency = "$";
            }
            else if(resulttArray[0].CurrencyTypeId == 3)
            {
              this.currency = "€";
            }
            else if(resulttArray[0].CurrencyTypeId == 4)
            {
              this.currency = "£";
            }

            this._apicallService.Get("http://localhost:61232/api/Customer/GetContactById",this.proformData.CustomerId).subscribe(
            (result) => {
              let resultArray = JSON.parse(JSON.stringify(result));
              if (resultArray.length != 0 && resultArray[0].Result) {
                if (resultArray[0].Result == "Session_Expired") {
                  this._router.navigate(['/auth']);
                  return;
                }
                else if(resultArray[0].Result == "Empty")
                {

                }
              }
              else
              {
                this.proformData.ContactName = resultArray[0].Fullname;
              }
            }
          )

          }              
      }
      )

      this._apicallService.Get("http://localhost:61232/api/Proform/GetProformLinesWoSession", this.proformId).subscribe(
        (result) => {
          let resultArray = JSON.parse(JSON.stringify(result));
          if (resultArray.length != 0 && resultArray[0].Result) {
            if(resultArray[0].Result == "Empty")
            {
              this.lineData = [];
            }
          }
          else
          {
            for(var i in resultArray)
            {
              if(resultArray[i].Description.toString().includes("<br/>"))
              {
                var splittedDesc = resultArray[i].Description.toString().split("<br/>");
                let _desc = [];

                for(let j in splittedDesc)
                {
                  _desc.push({desc: splittedDesc[j]});
                }

                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: _desc, ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName ,  LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
              else
              {
                let _lineData = { Id: resultArray[i].Id, ProductName: resultArray[i].ProductName, LineDesc: [{ desc: resultArray[i].Description }], ProductCount: resultArray[i].ProductCount, ProductPrice: resultArray[i].Price, LineAmount: resultArray[i].LineAmount, updatedefaultTax: resultArray[i].LineTax, ProductId: resultArray[i].ProductId, LineName: resultArray[i].LineName ,  LineDiscount : resultArray[i].LineDiscount };
                this.lineData.push(_lineData);
              }
            }
          }
        }
      )
    }
  }

  converttoPdf()
  {
    var data = document.getElementById('pdfScreen');  
    html2canvas(data).then(canvas => { 
  
      const contentDataURL = canvas.toDataURL('image/png')  
      const pdf = new jspdf({
        orientation: 'landscape',
      }); 
      const imgProps= pdf.getImageProperties(contentDataURL);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, pdfWidth, pdfHeight)
      pdf.save('proform.pdf'); // Generated PDF   
      window.top.close();
    }); 
  }

}
